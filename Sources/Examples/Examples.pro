include("../Moonfit/moonfit.pri")

# QT += widgets or core gui are necessary for the production of the ui_....h automatically.
QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport


#this options are for the use of QWT library
#QT += printsupport
#QT += opengl
#QT += svg

# ======================== BOOST PART ==================================
# boost is not required anymore for the code.
#win32: INCLUDEPATH += C:\Qt\Boost\boost_1_62_0\boost_1_62_0
#in windows, boosts raises the following error : "boost unable to find numeric literal operator operator Q" => need to add the following option
#win32: QMAKE_CXXFLAGS += -fext-numeric-literals
# to avoid warnings from boost library files (wtf !)
QMAKE_CXXFLAGS += -Wno-unused-local-typedefs -Wreorder -Wno-unused-parameter
# Necessary only for boost - would also be necessary for the syntax vector<int> bla = {1, 2, 3};
QMAKE_CXXFLAGS += -std=c++11

#name of the executable file generated
TARGET = MoonfitExamples

#put += console to run in a separate terminal
#CONFIG += console

#bundles might be required for MAC OS ??
#CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    expsGradCourse.h \
    namesGradCourse.h \
    Models/1-ModeleLogistic.h \
    Models/2-ModeleLotkaVolterra.h \
    Models/3-ModeleDevelopment.h \
    Models/5-ModeleInfluenza.h \
    Models/4-ModeleDevelopmentNoFeedback.h


SOURCES += \
    expsGradCourse.cpp \
    namesGradCourse.cpp \
    scriptsGradCourse.cpp \
    Models/1-ModeleLogistic.cpp \
    Models/2-ModeleLotkaVolterra.cpp \
    Models/3-ModeleDevelopment.cpp \
    Models/5-ModeleInfluenza.cpp \
    Models/4-ModeleDevelopmentNoFeedback.cpp


DISTFILES += \
    DATA/Data_new.txt \
    DATA/DataStd_new.txt \
    DATA/DevelopmentDatasetMutant.txt \
    DATA/DevelopmentDatasetWT.txt \
    DATA/InfluenzaDataset.txt \
    DATA/LogisticDataset1.txt \
    DATA/LogisticDataset2.txt \
    DATA/LotkaVolterraDataset.txt \
    DATA/DevelopmentRealMutant.txt \
    DATA/DevelopmentRealWT.txt

