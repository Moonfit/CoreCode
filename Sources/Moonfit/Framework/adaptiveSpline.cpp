#include "adaptiveSpline.h"
#include <cmath>


#include <iostream>
#include <sstream>

using namespace std;

adaptiveSpline::adaptiveSpline(int _size_groups, bool usingSpline) : size_groups(_size_groups), spline3(usingSpline) {
    if(size_groups < 2) cerr << "Gnaaa, sizegroups should be more than 1" << endl;
    in_current_group = 0;
    currentGroup = 0;
    vector<double>* firstGroupX = new vector<double>(size_groups, 0);
    listX.push_back(firstGroupX);
    vector<double>* firstGroupY = new vector<double>(size_groups, 0);
    listY.push_back(firstGroupY);
    listSplines.push_back(nullptr);
    minXPerGroup.push_back(1e18);
    maxXPerGroup.push_back(-1e18);
    lastXinSpline = -1e18;
    previousX = -1e18;
}

adaptiveSpline::~adaptiveSpline(){
    for(size_t i = 0; i < listX.size(); ++i){
        if(listX[i]) delete listX[i];
    }
    for(size_t i = 0; i < listY.size(); ++i){
        if(listY[i]) delete listY[i];
    }
    for(size_t i = 0; i < listSplines.size(); ++i){
        if(listSplines[i]) delete listSplines[i];
    }
}

void adaptiveSpline::add(double X, double Y){
    if(X <= previousX) {
        cerr << "ERR: adaptive spline can only be used with increasing X values" << endl;
        return;
    }

    previousX = X;

    if(in_current_group >= size_groups){
        // always add one point extra to the previous for the spline and to the new one as well
        vector<double>* newGroupX = new vector<double>(size_groups, 0);
        listX.push_back(newGroupX);
        vector<double>* newGroupY = new vector<double>(size_groups, 0);
        listY.push_back(newGroupY);
        double lastX = listX[currentGroup]->back();
        double lastY = listY[currentGroup]->back();
        listX[currentGroup]->push_back(X);
        listY[currentGroup]->push_back(Y);
        minXPerGroup[currentGroup] = min(minXPerGroup[currentGroup], X);
        maxXPerGroup[currentGroup] = max(maxXPerGroup[currentGroup], X);

        currentGroup++;
        lastXinSpline = -1e18;
        (*listX[currentGroup])[0] = lastX;
        (*listY[currentGroup])[0] = lastY;
        minXPerGroup.push_back(lastX);
        maxXPerGroup.push_back(lastX);
        listSplines.push_back(NULL);
        in_current_group = 1;
    }
    (*listX[currentGroup])[in_current_group] = X;
    (*listY[currentGroup])[in_current_group] = Y;
    minXPerGroup[currentGroup] = min(minXPerGroup[currentGroup], X);
    maxXPerGroup[currentGroup] = max(maxXPerGroup[currentGroup], X);
    in_current_group++;
}

string adaptiveSpline::print(){
    stringstream res;
    res << "adaptiveSpline, groups of size " << size_groups << endl;
    res << "   -> currently contains " << currentGroup +1 << " groups, last group has " << in_current_group << " elements " <<  endl;
    res << "   -> the last spline has been created till " << lastXinSpline << endl;

    for(int i = 0; i <= currentGroup; ++i){
        res << "Group " << i << " size " << listX[i]->size() << " X in [" << minXPerGroup[i] << "," << maxXPerGroup[i] << "]" << ((listSplines[i] == NULL) ? "NoSpline" : "SplineRecorded") << endl;
        for(unsigned int j = 0; j < listX[i]->size(); ++j){
            if(!((i == currentGroup) && ((int) j >= in_current_group))){
                res << "\t" << (*listX[i])[j] << "->" << (*listY[i])[j];
                if((i != currentGroup) || ((*listX[i])[j] <= lastXinSpline)) if(listSplines[i]) res << "|" << (*listSplines[i])((*listX[i])[j]);
            }
        }
        res << endl;
    }
    return res.str();
}
double adaptiveSpline::get(double X){
    int i = getGroup(X);
    if(i < 0) return NAN;
    // first case: finished group (full spline) or X is contained in the last group spline,
    if((i < currentGroup) || (X <= lastXinSpline)){
        if(!listSplines[i]) { // should not happen if X < lastXinSpline, so the vectors ListX are full. Spline makes error with the zeroes in the last group
            listSplines[i] = new tk::spline();
            listSplines[i]->set_points(*listX[i], *listY[i], spline3);
        }
    // second case: in last group and need to extend the last spline
    } else {
        if(listSplines[i]) delete listSplines[i];
        listSplines[i] = new tk::spline();
        vector<double> Xs = vector<double> (listX[i]->begin(), listX[i]->begin() + in_current_group);
        vector<double> Ys = vector<double> (listY[i]->begin(), listY[i]->begin() + in_current_group);
        //cout << "EXTENDS spline " << in_current_group << endl;
        for(unsigned int k = 0; k < Xs.size() ; ++k){
            //cout << "\t" << Xs[k] << "-" << Ys[k];
        }
        //cout << endl;
        listSplines[i]->set_points(Xs, Ys, spline3);
        lastXinSpline = Xs.back();
        // now we are in the last group, so even if the spline exists, it might not go far enough
    }
    return (*listSplines[i])(X);
}

int adaptiveSpline::getGroup(double X){
    for(int i = currentGroup; i >= 0; --i){
        if((X >= minXPerGroup[i]) && (X <= maxXPerGroup[i])){
            // when is shared between end of previous group and begin of next, prefers the previous
            if((i > 0) && (X >= minXPerGroup[i-1]) && (X <= maxXPerGroup[i-1])){
                return i-1;
            } else {
                return i;
            }
        }
    }
    cerr << "ERR: adaptiveSpline::getGroup(" << X << "), no values have been recorded in that region yet" << endl;
    return -1;
}


void testAdaptiveSpline(){
    adaptiveSpline a = adaptiveSpline(5, false);
    cout << a.print() << endl;
    for(int i = -3; i < 17; ++i){
        a.add(i, 10*i);
    }
    cout << a.print() << endl;
    cout << "Get group of values: 5 " << a.getGroup(5) << ", 20 " << a.getGroup(20) << ", -2" << a.getGroup(-50) << ", 250 " << a.getGroup(250) << endl;
    cout << "Get group of values: 5 " << a.get(5) << ", 20 " << a.get(20) << ", -2" << a.get(-50) << ", 250 " << a.get(250) << endl;

    cout << "Now getting values from it " << endl;
    for(double d = -4; d < 2; d += 0.5){
        cout << d << " -> " << a.get(d) << endl;
    }

    cout << 16 << " -> " << a.get(16) << endl;
    cout << a.print() << endl;
    a.add(17,165);
    cout << 17 << " -> " << a.get(17) << endl;
    cout << 17.5 << "(bad) -> " << a.get(17.5) << endl;
    a.add(18,170);
    cout << 17.5 << "(good) -> " << a.get(17.5) << endl;
    a.add(19,175);
    cout << a.print() << endl;
    cout << 17.5 << " -> " << a.get(17.5) << endl;
    cout << a.print() << endl;
    cout << 18 << " -> " << a.get(18) << endl;
    cout << 19 << " -> " << a.get(19) << endl;
    a.add(20, 200);
    cout << a.print() << endl;
    return;
}







