#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;



int listCases(void){

    string folder = "C:/Users/pprobert/Desktop/2020-12-19-AllHeikeFixG0/";
    stringstream res;
    for(int i = 9; i < 311; ++i){
        for(int j = 0; j < 10; ++j){
            for(int k = 0; k < 25; ++k){
                stringstream fname; fname << folder << "Case" << i << "_Comb" << j << "_Rep" << k << "/TestHistory.txt";
                string fn = fname.str();
                //cout << fn << endl;
                ifstream test(fn.c_str());
                if(test){
                    char buf[10000];
                    test.getline(buf, 9999);
                    test.getline(buf, 9999);
                    res << fname.str() << "\tCase" << i << "\tComb" << j << "\t" << buf << "\n";
                    test.close();
                }
            }
            //res << "\n";
        }
    }
    cout << res.str();
    return 0;
}

int listIdentifs(){

    string folder = "C:/Users/pprobert/Desktop/2020-12-03-ResultsIRR/";
    stringstream res;
    for(int i = 9; i < 80; ++i){
        for(int j = 0; j < 15; ++j){
            vector<string> toDos = {"", "NoDNA_", "WithFrac_"};
            for(size_t k = 0; k < toDos.size(); ++k){
                string thisCase = toDos[k];
                stringstream fname; fname << folder << thisCase << "Case" << i << "_Identif" << j << "/LastIdentifiability.txt";
                string fn = fname.str();
                ifstream test(fn.c_str());
                if(test){
                    char buf[10000];
                    test.getline(buf, 9999); //escapes the default parameter value
                    for(int z = 0; z < 100; ++z){
                        test.getline(buf, 9999);
                        if(string(buf).size() > 1){
                            res << fname.str() << "\tData" << thisCase << "\tCase" << i << "\tIdentif" << j << "\t" << buf << "\n";
                        }
                    }
                    test.close();
                }
            }
        }
    }
    cout << res.str();
    return 0;
}

int main(void){
    return listCases();
}
