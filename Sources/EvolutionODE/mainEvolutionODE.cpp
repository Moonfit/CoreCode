// The file common.h checks the system and defines WINDOWS/UNIX and QT4/QT5.
#include "../Moonfit/common.h"
#include "../Moonfit/moonfit.h"

#include "expsEvo.h"
#include "model1.h"

#include <vector>
#include <ctime>
using namespace std;


int main(int argc, char *argv[]){

    // From common.h, you can define whether you want to use graphical interface or not (#define WITHOUT_QT)
    #ifndef WITHOUT_QT
    QApplication b(argc, argv);     // Starts the Qt application

    // note: when launched from QtCreator, the running folder is the buildxxx generated folder. Might need to use ../ or absolute folder here.
    //TableCourse* Data1 = new TableCourse(string("DATA/Data1.txt"));
    //TableCourse* Data2 = new TableCourse(string("DATA/Data1.txt"));

    string configFile = string("BasicConfigEvo.txt");

    Model* currentModel = new modeleNoEvo();

    Experiment* currentExperiment = new expOne(currentModel);
    /*currentExperiment->giveData(Data1, EXP_KIN1);
    currentExperiment->giveData(Data2, EXP_KIN2);
    //currentExperiment->giveStdDevs(DataXX, EXP_XXX);
    currentExperiment->giveHowToReadNamesInKinetics(getGlobalNames());
    currentExperiment->loadEvaluators();*/

    simuWin* p = new simuWin(currentExperiment);
    cout << "Launching Graphical Interface ..." << endl;
    p->loadConfig(configFile);
    p->show();


    return b.exec();                // to leave the control to Qt instead of finishing the program
    #endif


    // if no Qt defined
    return 0;
}



