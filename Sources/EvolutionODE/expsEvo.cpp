#include "../Moonfit/moonfit.h"
#include "expsEvo.h"
#include "model1.h"

#include <iostream>
#include <fstream>
#include <sstream>

// Here we can also put the predicted steady state as third experiment...
expOne::expOne(Model* _m) : Experiment(_m, NB_EXP_KIN){
    Identification = string("Kinetics");
    names_exp[EXP_KIN1] = string("EXP_1");
    names_exp[EXP_KIN2] = string("EXP_2");
    names_exp[EXP_STEADY] = string("Steady");
    m->setBaseParameters();
}


void expOne::simulate(int IdExp, Evaluator* E, bool force){
    if(motherPrepareSimulate(IdExp, E, force)){
        switch(IdExp){
            /*case EXP_XXX:  {
                m->initialise(Back::XXX); break;}*/
            case EXP_STEADY: {m->initialise(Back::STEADY); break;}
            default: {m->initialise(); break;}
        }
        switch(IdExp){
        case EXP_KIN1:{
            m->simulate(10, E);
            m->action("infect", 10.);
            m->simulate(m->getParam(modeleNoEvo::latency), E);
            m->action("infectPast", 10.);
            //m->setValue(N::GlobVAR3, 130.0);
            m->simulate(240, E); break; // 130 days -> might change later
        }
        case EXP_KIN2:{
            m->simulate(10, E);
            m->action("infect", 20.);
            m->simulate(m->getParam(modeleNoEvo::latency), E);
            m->action("infectPast", 20.);
            //m->setValue(N::GlobVAR3, 130.0);
            m->simulate(240, E); break; // 130 days -> might change later
        }
        case EXP_STEADY:{
            m->simulate(10, E);
            //m->action("infect", (double) 20);
            m->simulate(m->getParam(modeleNoEvo::latency), E);
            //m->action("infectPast", (double) 20);
            //m->setValue(N::GlobVAR3, 130.0);
            m->simulate(240, E); break; // 130 days -> might change later
        }

        }
        m->setOverrider(nullptr);
    }
}
