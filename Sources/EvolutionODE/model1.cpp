#include "expsEvo.h"
#include "model1.h"



void modeleNoEvo::giveInfectionParameters(double _Rinf, double _Rcure, double _RDinf, int _nInitInf){
   /* params[Rinf] = _Rinf;
    params[Rcure] = _Rcure;
    params[RDinf] = _RDinf;
    val[E] += (double) _nInitInf;
    val[S] -= (double) _nInitInf;*/
}

//double Rinf = p->virulence * AvgNneigh / (x[T] + 1e-12);
//double Rcure = maxRecoveryRate * functAverageRecognition;
//double RDinf = p->morbidity * functAverageRecognition;

modeleNoEvo::modeleNoEvo() : Model(NbVariables, NbParameters) {
    name = string("evolutionWithoutEvolution");
    dt = 0.01;             // initial time step -> then it is adaptive
    print_every_dt = 0.5;
    names[S] = string("S - Uninfected");
    names[E] = string("E - Early infected");
    names[L] = string("L - Late infected");
    names[Spast] = string("Spast");
    names[Epast] = string("Epast");
    names[Lpast] = string("Lpast");
    names[IntT] = string("IntT");
    names[f] = string("f");
    names[fpast] = string("fpast");
    extNames[S] = S;
    extNames[E] = E;
    extNames[L] = L;
    extNames[IntT] = IntT;
    extNames[Spast] = Spast;
    extNames[Epast] = Epast;
    extNames[Lpast] = Lpast;
    extNames[f]= f;
    extNames[fpast] = fpast;
    paramNames[Rcure] = string("RCure");
    paramNames[Rinf] = string("Rinf");
    paramNames[RDinf] = string("RDinf Death");
    paramNames[RD] = string("RD");
    paramNames[RBirth] = string("RBirth");
    paramNames[latency] = string("latency");
    paramNames[maxSize] = string("maxSize");
    paramNames[timeInfection] = string("timeInfection");
    paramNames[nbIndividualsToInfect] = string("nbIndividualsToInfect");
    paramNames[initialSize] = string("initialSize");
    paramNames[tFinal] = string("tFinal");

    p=nullptr;
    previousf = new adaptiveSpline(10);
}

void modeleNoEvo::setBaseParameters(){
    p = new masterSimulation();
    params[Rcure] = p->maxRecoveryRate;
    params[Rinf] = p->virulence;
    params[RDinf] = p->morbidity;

    params[RD] = p->rateDeath;
    params[RBirth] = p->rateBirth;
    params[latency] = p->latency;
    params[maxSize] = p->maxSize;
    params[timeInfection] = p->timeInfection;
    params[nbIndividualsToInfect] = p->nbIndividualsToInfect;
    params[tFinal] = p->tFinal;
    params[initialSize] = p->initialSize;
    setBaseParametersDone();
}

void modeleNoEvo::action(string nameAction, double parameter){
    if(!nameAction.compare("infect")){
        cout << "ODE model, infect " << parameter << " individuals " << endl;
        if((parameter < 0) || (parameter > 1e9)) cerr << "ERR: action infect, wong number of individuals " << parameter << ". Note: can be double" << endl;
        val[L] += parameter;
        val[S] -= parameter;
    }
    if(!nameAction.compare("infectPast")){
        cout << "ODE model, infect Past " << parameter << " individuals " << endl;
        if((parameter < 0) || (parameter > 1e9)) cerr << "ERR: action infect, wong number of individuals " << parameter << ". Note: can be double" << endl;
        val[Lpast] += parameter;
        val[Spast] -= parameter;
    }
}



void modeleNoEvo::initialise(long long background){ // don't touch to parameters !
    if(!p) return;
    val.clear();
    val.resize(NbVariables, 0.0);
    init.clear();
    init.resize(NbVariables, 0.0);

    init[S] = params[initialSize];
    init[E] = 0;
    init[L] = 0;
    init[Spast] = init[S];
    init[E] = 0;
    init[L] = 0;
    init[f] = 0;
    init[IntT] = 0;
    init[fpast] = 0;

    // Parameters: Rcure, Rinf, RDinf, RD, RBirth, latency, maxSize, timeInfection, nbIndividualsToInfect, tFinal, initialSize, NbParameters
    if(background == Back::STEADY){
        double Ce = (params[Rcure] + params[RD] + params[RDinf]) / (params[Rinf] * exp(-params[RD] * params[latency]) + 1e-12);
        double T = params[maxSize] - params[maxSize] * params[RD] * (1 + (params[RDinf] * (1 - Ce)) / (Ce * params[Rinf] - params[Rcure] - params[RDinf] + 1e-12)) / (params[RBirth] + 1e-12);
        double inS = Ce * T;
        double inL = T * (- params[RD] + params[RBirth] * (1 - T / (params[maxSize] + 1e-12))) / (params[RDinf] + 1e-12);
        double inE = T - inS - inL;
        init[S] = inS;
        init[E] = inE;
        init[L] = inL;
        init[Spast] = inS;
        init[Epast] = inE;
        init[Lpast] = inL;
    }

    delete previousf;
    previousf = new adaptiveSpline(10);

    for(size_t i = 0; i < NbVariables; ++i){
        val[i] = init[i];}
    t = 0;
    initialiseDone();
}


void modeleNoEvo::updateDerivedVariables(double _t){
    if(!p) return;
    val[IntT] = val[S] + val[E] + val[L];

    val[f] =  val[Lpast] * params[Rinf] * val[Spast] / (val[Spast] + val[Epast] + val[Lpast] + 1e-12) * exp(-params[RD] * params[latency]); //* exp((- p->rateDeath + p->rateBirth) * p->virulence - val[IntT] * p->rateBirth / p->maxSize);

    //if(t > previousf->previousX) previousf->add(t, val[f]);
    val[fpast] = 0;
    if(_t > params[latency]) val[fpast] = previousf->get(_t-params[latency]);
}

void modeleNoEvo::derivatives(const vector<double> &x, vector<double> &dxdt, const double _t){

    if(!p) return;

    dxdt[f] = 0;
    dxdt[fpast] = 0;

    double currentf =  x[Lpast] * params[Rinf] * x[Spast] / (x[Spast] + x[Epast] + x[Lpast] + 1e-12) * exp(-params[RD] * params[latency]);             //    (- p->rateDeath + p->rateBirth) * p->virulence - x[IntT] * p->rateBirth / p->maxSize);

    dxdt[S] = - x[L] * params[Rinf] * x[S] / (x[S] + x[E] + x[L] + 1e-12) + params[Rcure] * x[L] - params[RD] * x[S] + params[RBirth] * (x[S] + x[E] + x[L]) * (1 - (x[S] + x[E] + x[L]) / (params[maxSize] + 1e-12));
    dxdt[E] = x[L] * params[Rinf] * x[S] / (x[S] + x[E] + x[L] + 1e-12) - currentf - params[RD] * x[E];
    dxdt[L] = currentf - params[Rcure] * x[L] - (params[RD] + params[RDinf]) * x[L];

    if(_t < params[latency]) {
        dxdt[Spast] = 0;
        dxdt[Epast] = 0;
        dxdt[Lpast] = 0;
    } else {
        double pastflow = previousf->get(_t - params[latency]);
        dxdt[Spast] = - x[Lpast] * params[Rinf] * x[Spast] / (x[Spast] + x[Epast] + x[Lpast] + 1e-12) + params[Rcure] * x[Lpast] - params[RD] * x[Spast] + params[RBirth] * (x[Spast] + x[Epast] + x[Lpast]) * (1 - (x[Spast] + x[Epast] + x[Lpast]) / (params[maxSize] + 1e-12));
        dxdt[Epast] = x[Lpast] * params[Rinf] * x[Spast] / (x[Spast] + x[Epast] + x[Lpast] + 1e-12) - pastflow - params[RD] * x[Epast];
        dxdt[Lpast] = pastflow - params[Rcure] * x[Lpast] - (params[RD] + params[RDinf]) * x[Lpast];
    }
    //dxdt[IntT] = x[S] + x[E] + x[L];
    //if(t >= p->latency) dxdt[IntT] += - x[Spast] -x[Epast] -x[Lpast];

    if(_t > previousf->previousX) previousf->add(_t, currentf); // weird, because the solver will call the same thing again with different steps, lets see
}

masterSimulation::masterSimulation() {
    tFinal      = 250;
    dt          = 0.1;
    rateDeath   = 0.5;
    rateBirth   = 0.8;
    initialSize = 100;
    maxSize     = 500;
    rateMutTCR  = 0.5;
    rateMutMHC  = 1e-10;
    affinityDeath           = 0.5;
    affinityPresentation    = 0.15;
    virusDT                 = 1.0/3600.0;
    activationThreshold     = 0.12;
    maxRecoveryRate         = 0.5;
    timeInfection           = 10;
    nbIndividualsToInfect   = 10;
    morbidity               = 0.35;
    virulence               = 0.2;
    latency                 = 20.0;
}




/* Old formula
void modeleNoEvo::updateDerivedVariables(double t){
    if(!p) return;
    val[f] =  val[Lpast] * params[Rinf] * val[Spast] / (val[Spast] + val[Epast] + val[Lpast] + 1e-12) * exp((- p->rateDeath + p->rateBirth) * p->virulence - val[IntT] * p->rateBirth / p->maxSize);
    if(t > previousf->previousX) previousf->add(t, val[f]);
    if(t > p->latency) val[fpast] = previousf->get(t-p->latency);
}

void modeleNoEvo::derivatives(const vector<double> &x, vector<double> &dxdt, const double t){

    if(!p) return;
    dxdt[f] = 0;
    dxdt[fpast] = 0;

    double currentf =  x[Lpast] * params[Rinf] * x[Spast] / (x[Spast] + x[Epast] + x[Lpast] + 1e-12) * exp((- p->rateDeath + p->rateBirth) * p->virulence - x[IntT] * p->rateBirth / p->maxSize);

    // dynamic neighboring ?
    dxdt[S] = - x[L] * params[Rinf] * x[S] / (x[S] + x[E] + x[L] + 1e-12) + params[Rcure] * x[L] - p->rateDeath * x[S] + p->rateBirth * x[S] * (1 - (x[S] + x[E] + x[L]) / (p->maxSize + 1e-12));
    dxdt[E] = x[L] * params[Rinf] * x[S] / (x[S] + x[E] + x[L] + 1e-12) - currentf - p->rateDeath * x[E] + p->rateBirth * x[E] * (1 - (x[S] + x[E] + x[L]) / (p->maxSize + 1e-12));
    dxdt[L] = currentf - params[Rcure] * x[L] - (p->rateDeath + params[RDinf]) * x[L] + p->rateBirth * x[L] * (1 - (x[S] + x[E] + x[L]) / (p->maxSize + 1e-12));

    double pastflow = 0;
    if(t > p->latency) pastflow = previousf->get(t - p->latency);
    if(t < p->latency) dxdt[Spast] = 0;
    else dxdt[Spast] = - x[Lpast] * params[Rinf] * x[Spast] / (x[Spast] + x[Epast] + x[Lpast] + 1e-12) + params[Rcure] * x[Lpast] - p->rateDeath * x[Spast] + p->rateBirth * x[Spast] * (1 - (x[Spast] + x[Epast] + x[Lpast]) / (p->maxSize + 1e-12));
    if(t < p->latency) dxdt[Epast] = 0;
    else dxdt[Epast] = x[Lpast] * params[Rinf] * x[Spast] / (x[Spast] + x[Epast] + x[Lpast] + 1e-12) - pastflow - p->rateDeath * x[Epast] + p->rateBirth * x[Epast] * (1 - (x[Spast] + x[Epast] + x[Lpast]) / (p->maxSize + 1e-12));
    if(t < p->latency) dxdt[Lpast] = 0;
    else dxdt[Lpast] = pastflow - params[Rcure] * x[Lpast] - (p->rateDeath + params[RDinf]) * x[Lpast] + p->rateBirth * x[Lpast] * (1 - (x[Spast] + x[Epast] + x[Lpast]) / (p->maxSize + 1e-12));

    dxdt[IntT] = x[S] + x[E] + x[L];
    if(t >= p->latency) dxdt[IntT] += - x[Spast] -x[Epast] -x[Lpast];

    if(t > previousf->previousX) previousf->add(t, currentf); // weird, because the solver will call the same thing again with different steps, lets see
}
*/
