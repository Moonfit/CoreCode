#ifndef  modele1_H
#define  modele1_H
#include "../Moonfit/moonfit.h"
#include "../Moonfit/common.h"

struct masterSimulation {
    masterSimulation();

    double  tFinal;                 // time of a simulation
    double  dt;                     // dt for updating the population (birth / natural death)
    double  rateDeath;              // natural death rate per year per person
    double  rateBirth;              // birth rate per year per person
    int     initialSize;            // initial founder (identical) individuals
    int     maxSize;                // maximum size of the population (logistic constraint)
    double  rateMutTCR;
    double  rateMutMHC;
    double  affinityDeath;          // Autoimmunity threshold
    double  affinityPresentation;

    double  virusDT;                // SHOULD BE A DIVIDER OF dt
    double  activationThreshold;
    double  maxRecoveryRate;

    double  timeInfection;
    int     nbIndividualsToInfect;

    double  morbidity;
    double  virulence;
    double  latency;
};

struct adaptiveSpline;

/** @brief Class that simulates the ODE for the same model but without evolution (as side comparison) */
struct modeleNoEvo : public Model {
    modeleNoEvo();
    enum{S, E, L, Spast, Epast, Lpast, IntT, f, fpast, NbVariables};
    //   recovery, virulence, morbidity, maxSize, timeInfection,
    enum{Rcure, Rinf, RDinf, RD, RBirth, latency, maxSize, timeInfection, nbIndividualsToInfect, tFinal, initialSize, NbParameters}; // parameters on top of the simulation ones
    masterSimulation* p;
    adaptiveSpline* previousf;
    virtual void derivatives(const vector<double> &x, vector<double> &dxdt, const double t);
    void initialise(long long background);
    void setBaseParameters();
    void updateDerivedVariables(double t);
    void action(string name, double parameter);
    void giveInfectionParameters(double _Rinf, double _Rcure, double _RDinf, int _nInitInf); // can only be estimated when the virus has been created
};




#endif

