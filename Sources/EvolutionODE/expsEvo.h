#ifndef EXPERIMENTSNEWPROJ_H
#define EXPERIMENTSNEWPROJ_H


#include "../Moonfit/moonfit.h"

#include <vector>
#include <string>
#include <iostream>


// you can define an enum for each experiment class.
enum EXP_KINETICS {EXP_KIN1, EXP_KIN2, EXP_STEADY, NB_EXP_KIN};

// an enum for the backgrounds / options of simulations, in case wanted
namespace Back {
    enum : long long {
        CURVES= 1,
        STEADY=2,
    };
}

// Important: here, you don't need to include your modele subclass. An experiment is designed to take a model, whatever it is !

using namespace std;

struct expOne : public Experiment {
    expOne(Model* _m);

    //void init();
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false); // if no E is given, VTG[i] is used
};

/* etc ...
struct expTwo : public Experiment {
    expTwo(Modele* _m);

    //void init();
    void simulate(int IdExp, Evaluator* E = NULL, bool force = false); // if no E is given, VTG[i] is used
};*/



#endif
