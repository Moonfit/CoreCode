include("../Moonfit/moonfit.pri")

#name of the executable file generated
TARGET = EvoODE1.0


#put += console to run in a separate terminal
#CONFIG += console

#bundles might be required for MAC OS ??
#CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    model1.h \
    expsEvo.h

SOURCES += \
    model1.cpp \
    mainEvolutionODE.cpp \
    expsEvo.cpp

