include("../Moonfit/moonfit.pri")

#name of the executable file generated
TARGET = LeishmaniaExe

#put += console to run in a separate terminal
#CONFIG += console

#bundles might be required for MAC OS ??
#CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    Models/modeleLeishmaniaMajor.h \
    expLeishmania.h

SOURCES += \
    Script13Jan2017.cpp \
    Models/modeleLeishmaniaMajor.cpp

