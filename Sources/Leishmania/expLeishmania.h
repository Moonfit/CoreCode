#ifndef EXPERIMENTSTHALL_H
#define EXPERIMENTSTHALL_H

#include "../Moonfit/moonfit.h"
#include "Models/modeleLeishmaniaMajor.h"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;




struct expLMajor : public Experiment {
    enum EXP{Small_Dose, /*Big_Dose,*/ NB_EXP}; // experiments    - use expLMajor::Small_Dose
    expLMajor(Model* _m) : Experiment(_m, NB_EXP) {

        Identification = string("Leishmaniasis");
        names_exp[Small_Dose] = string("Small_Dose"); // activation (TCR Only)");
        //m->setBaseParameters();
    }

    //void init();
    void simulate(int IdExp, Evaluator* E = NULL, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){

            default: {m->initialise(Back::WT); break;}
            }
            switch(IdExp){
            case Small_Dose:{
                //            m->setValue("P", 3391.69); // it was 5000
                m->simulate(133, E); break;} // 124 days -> might change later
            }
            m->setOverrider(NULL);
        }
    }
};

struct expLMajor_Blocking : public Experiment {
    enum EXP{Control, BlockadeActivation, BlockadeRecruitment, BlockadeActivationRecruitment, NB_EXP}; // experiments    - use expLMajor::Small_Dose
    expLMajor_Blocking(modeleLeishmania* _m) : Experiment(_m, NB_EXP) {

        Identification = string("Leishmaniasis");
        names_exp[Control] = string("Control");
        names_exp[BlockadeActivation] = string("BlockadeActivation");
        names_exp[BlockadeRecruitment] = string("BlockadeRecruitment");
        names_exp[BlockadeActivationRecruitment] = string("BlockadeActivationRecruitment");
        //m->setBaseParameters();
    }

    //void init();
    void simulate(int IdExp, Evaluator* E = NULL, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){

            default: {m->initialise(Back::WT); break;}
            }
            switch(IdExp){
            // Comment out case control in order to be able to REPORT the results..otherwise it crashes...
            case Control:{
                //            m->setValue("P", 3391.69); // it was 5000
                m->simulate(133, E);
                break;
            } // 124 days -> might change later
//            case BlockadeActivation:{
//                m->simulate(18, E); // change the day (18) of blockade
//                m->setParam(modeleLeishmania::k_a, 0);
//                m->setParam(modeleLeishmania::k_ai, 0);
//                // If I want to re-activate after some days...
//                //---------------------------------------------------
//                //m->simulate(3, E);
//                //m->setParam(modeleLeishmania::k_a, 3752.51);
//                //---------------------------------------------------
//                m->simulate(115,E); // simulate the rest days after blockade...
//                break;
//            }
            case BlockadeRecruitment:{
                m->simulate(18, E); // change the day (21) of blockade
                double p1 = m->getParam(modeleLeishmania::k_Mrec0);
                double p2 = m->getParam(modeleLeishmania::alpha);
                m->setParam(modeleLeishmania::k_Mrec0, 0);
                m->setParam(modeleLeishmania::alpha, 0); // I cannot understand why it is not reporting it...it does when only kmrec is used...
                m->simulate(115,E); // simulate the rest days after blockade...
                m->setParam(modeleLeishmania::k_Mrec0, p1);
                m->setParam(modeleLeishmania::alpha, p2); // I cannot understand why it is not reporting it...it does when only kmrec is used...
                break;
            }
//            case BlockadeActivationRecruitment:{
//                m->simulate(18, E); // change the day (21) of blockade
//                m->setParam(modeleLeishmania::k_a, 0);
////                m->setParam(modeleLeishmania::k_ai, 0);
//                m->setParam(modeleLeishmania::k_Mrec0, 0);
//                m->setParam(modeleLeishmania::alpha, 0);
//                m->simulate(115,E); // simulate the rest days after blockade...
//                break;
//            }
            default: {

            }
            }
            m->setOverrider(NULL);
        }
    }
};


struct expDoubleLMajor : public Experiment {
    enum EXP{Small_Dose, Big_Dose, NB_EXP}; // experiments    - use expLMajor::Small_Dose

    // please call this experiment with an extended model
    expDoubleLMajor(modelExtended* _m) : Experiment((Model*) _m, NB_EXP) {
        Identification = string("DoubleLeishmaniasis");
        names_exp[Small_Dose] = string("Small_Dose");
        names_exp[Big_Dose] = string("Big_Dose");
        //m->setBaseParameters();
    }

    //void init();
    void simulate(int IdExp, Evaluator* E = NULL, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                case Small_Dose:{
                    //            m->setValue("P", 3391.69); // it was 5000

                    m->initialise(modelExtended::ApplyCoefficients);
                    m->simulate(133, E);
                    break;
                } // 124 days -> might change later
                case Big_Dose:{
                    //            m->setValue("P", 3391.69); // it was 5000
                    m->initialise(modelExtended::WT);
                    m->simulate(133, E);
                    break;
                } // 124 days -> might change later
            }
            m->setOverrider(NULL);
        }
    }
};

struct InitialPopulation : public Experiment {
    enum{DoseDefault,/*Dose1k,*/Dose3k,Dose5k,/*Dose7k,Dose9k,*/Dose10k,Dose15k,Dose30k,Dose50k,Dose75k,Dose100k,NbInitDoses};    // experiments    - use InitialPopulation::Small_Dose
    InitialPopulation(Model* _m) : Experiment(_m, NbInitDoses) {

        Identification = string("Initial Dose leishmaniasis");
        names_exp[DoseDefault] = string("dose default");
//        names_exp[Dose1k] = string("dose 1k");
        names_exp[Dose3k] = string("dose 3k");
        names_exp[Dose5k] = string("dose 5k");
        names_exp[Dose10k] = string("dose 10k");
        names_exp[Dose15k] = string("dose 15k");
        names_exp[Dose30k] = string("dose 30k");
        names_exp[Dose50k] = string("dose 50k");
        names_exp[Dose75k] = string("dose 75k");
        names_exp[Dose100k] = string("dose 100k");
//        names_exp[Dose7k] = string("dose 7k");
//        names_exp[Dose9k] = string("dose 9k");

        //m->setBaseParameters();
    }

    //void init();
    void simulate(int IdExp, Evaluator* E = NULL, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){

            default: {m->initialise(Back::WT); break;}
            }
            switch(IdExp){
            case DoseDefault:{
                m->setValue("P", 29200); // here instead of the fitted default, it uses this one..
                m->simulate(150, E); break;}
//            case Dose1k:{
//                m->setValue("P", 1000);
//                m->simulate(130, E); break;}
            case Dose3k:{
                m->setValue("P", 3000);
                m->simulate(150, E); break;}
            case Dose5k:{
                m->setValue("P", 5000); //5000
                m->simulate(150, E); break;}
//            case Dose7k:{
//                m->setValue("P", 7000); //7000
//                m->simulate(130, E); break;}
            case Dose10k:{
                m->setValue("P", 10000);
                m->simulate(150, E); break;}
//            case Dose9k:{
//                m->setValue("P", 9000); //9000
//                m->simulate(130, E); break;}
            case Dose15k:{
                m->setValue("P", 15000);
                m->simulate(150, E); break;}
            case Dose30k:{
                m->setValue("P", 30000);
                m->simulate(150, E); break;}
            case Dose50k:{
                m->setValue("P", 50000);
                m->simulate(150, E); break;}
            case Dose75k:{
                m->setValue("P", 75000);
                m->simulate(150, E); break;}
            case Dose100k:{
                m->setValue("P", 100000);
                m->simulate(150, E); break;}
        }
            m->setOverrider(NULL);
        }
    }
};



struct PerturbParam : public Experiment {
    enum{Param005, Param01, Param02,Param05,Param08,Param12,Param15,Param20, Param50, Param100, ParamDefault, NbParamChange};
    int parameter;
    double valueAround;
    PerturbParam(Model* _m, int _parameter, double _valueAround) : Experiment(_m, NbParamChange), parameter(_parameter), valueAround(_valueAround) {

        Identification = string("Perturbate") + _m->getParamName(parameter);
        stringstream valInString; valInString << valueAround; string val = valInString.str();
        names_exp[ParamDefault] = string("default=") + val;
        names_exp[Param005] = string("5%");
        names_exp[Param01] = string("10%");
        names_exp[Param02] = string("20%");
        names_exp[Param05] = string("50%");
        names_exp[Param08] = string("80%");
        names_exp[Param12] = string("120%");
        names_exp[Param15] = string("150%");
        names_exp[Param20] = string("x2");
        names_exp[Param50] = string("x5");
        names_exp[Param100] = string("x10");

        //m->setBaseParameters();
    }

    /// 2018-11-21 Should not use the parameter value from creation, but the current from the table
    void init(){
        valueAround = m->getParam(parameter);
        motherInit();
    }

    /// Careful, this function should restore the exact same paameter values as before being called
    //void init();
    void simulate(int IdExp, Evaluator* E = NULL, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){

            default: {m->initialise(Back::WT); break;}
            }
            switch(IdExp){
            case Param005:{
                m->setParam(parameter, valueAround * 0.05);
                m->simulate(130, E); break;}
            case Param01:{
                m->setParam(parameter, valueAround * 0.1);
                m->simulate(130, E); break;}
            case Param02:{
                m->setParam(parameter, valueAround * 0.2);
                m->simulate(130, E); break;}
            case Param05:{
                m->setParam(parameter, valueAround * 0.5);
                m->simulate(130, E); break;}
            case Param08:{
                m->setParam(parameter, valueAround * 0.8);
                m->simulate(130, E); break;}
            case Param12:{
                m->setParam(parameter, valueAround * 1.2);
                m->simulate(130, E); break;}
            case Param15:{
                m->setParam(parameter, valueAround * 1.5);
                m->simulate(130, E); break;}
            case Param20:{
                m->setParam(parameter, valueAround * 2.0);
                m->simulate(130, E); break;}
            case Param50:{
                m->setParam(parameter, valueAround * 5.0);
                m->simulate(130, E); break;}
            case Param100:{
                m->setParam(parameter, valueAround * 10.0);
                m->simulate(130, E); break;}
            // cheating: finishing by the original parameter value to restore the good one
            case ParamDefault:{
                m->setParam(parameter, valueAround);
                cerr << " value for param " << m->getParamName(parameter) << " is " << valueAround << endl;
                m->simulate(130, E); break;}
            }
            m->setOverrider(NULL);
        }
    }
};



#endif // EXPERIMENTSTHALL_H
