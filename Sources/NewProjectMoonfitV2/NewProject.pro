include("../Moonfit/moonfit.pri")

#name of the executable file generated
TARGET = MyNewProject

#put += console to run in a separate terminal
#CONFIG += console

#bundles might be required for MAC OS 
#CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    myModel.h \
    myExperiments.h

SOURCES += \
    main.cpp \
    myModel.cpp \
    myExperiments.cpp
