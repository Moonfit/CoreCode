// -------------- ODE model for perturbed thymyus dynamcis -------------
//
// Author:      Philippe A. Robert (philippe [dot] robert [at] ens-lyon.org, 8th Feb. 2020
// Source:      gitlab.com/Moonfit/Balthyse
// References   [1] Robert, P., Jönsson, E and Meyer-Hermann, M MoonFit, a minimal interface for fitting ODE
//                  dynamical models, bridging simulation by experimentalists and customization by C++ programmers,
//                  BioRxiV 2018, https://doi.org/10.1101/281188
//              [2] Elfaki, Y. et al. Influenza A virus-induced thymic atrophy differentially affects conventional
//                  and regulatory T cell developmental dynamics. EJI 2020

#ifndef modele6Combined_H
#define modele6Combined_H
#include "../Moonfit/moonfit.h"


// ==================================== USER OPTIONS =====================================

//1: choose model structure, A, B or C
// careful of capital letters !!!
#define ModelStructureB

//2: choose hypotheses of Factor F impacting the different populations:
// Hypothesis1 : only DP death
// Hypothesis2 : increased Treg differentiation
// Hypothesis3 : increase export of all SP population equally (as additive rate)
// Hypothesis4 : increase export of Tregs versus Tconv with different strength.
#define Hypothesis3

//3: possible to calculate the age of cells on lateDPs and downstream population.
// computation heavy, so do not use when fitting.
//#define calculate_ages

// ==================================== ************ =====================================



// Now these blocks transform the user option (structure and hypothesis) into
// smaller defines that will define the equations (do not touch!)

#ifdef ModelStructureA
#define DPtoFp3Prec
#define DPto25Prec
#endif

#ifdef ModelStructureB
#endif

#ifdef ModelStructureC
#define DPto25Prec
#endif



// The following 'define' will tell if some parameters should be linked (depending on the hypothesis)
// for instance, increase export of all SP equally mean the export parameter of each population should be linked
// and always have the same value.
#ifdef Hypothesis1
    // nothing to do
#endif

#ifdef Hypothesis2
    // nothing to do
#endif

#ifdef Hypothesis3
    //- for increased export of all SP cells equally, including SP8 [the parameter for Tconv applies to all then]
    //  this means that the parameter: "increased export of Tconv" is taken for all the other increased export of SP population
    #define equalExportAll
#endif

#ifdef Hypothesis4
    //- For equal increased export of Tregs and progenitors, [the parameter for mature DPTregs applies to all then]
    //  this means that the parameter: "increased export of mature Treg" is taken for all of the Treg + Treg progenitor populations.
    #define equalExportTregs

    // This time, we only link the SP4 and SP8 parameter, but do not link it to the Treg populations, that have a different shared value
    #define equalExportSP4SP8
#endif


// These are 'Backgrounds' (imagine different mice backgrounds), allowing to simulate the same model only under one hypothesis.
// For the present simulations, we use the background "B_AllCombined", which means any possible perturbation is applied, provided the
// parameters are not 0 (additive) or 1 (multiplicative).
// see inside expThymus.h/cpp that defines how the model is simulated.
namespace Back {
    enum : long long {WT= 1, B_ReducedInflow = 2, B_MoreDeath = 4, B_MoreOutputThymus = 8, B_FasterDifferentiation = 16, B_SpaceDependentOutput = 32, B_DivisionReduced = 64, B_AllCombined = 127, B_LogisticTotalProlif = 256, B_InitThiault = 512, B_StartSteadyState = 1024, B_BRDU = 2048, B_Reconstitution = 4096, B_ProlifBlocked = 8192, B_UseC0 = 16384, B_UseHighC = 32768, B_UseStep = 65536, B_UseSquare = 131072};
}


// This class defined the equations for thymic atrophy, designed as a mother class of Moonfit Model class
struct modele6GenericTVaslin : public Model {
    modele6GenericTVaslin();


    // Option: to calculate the age of the populations of late DPs and downstream.
    // This is computationally expensive, so do not use when fitting.
#ifdef calculate_ages
    // The curve of some variables will be memorized at previous (already calculated) time-points
    // to calculate the age of the populations.
    vector<adaptiveSpline*> memorizedVariables;
#endif

    // This function calculates flu-dependent derived parameters (like the export rate of a population)
    void calculateParameters(double xFlu = 0.0);

    // the updated parameters are: (flu-dependent, depending on the hypotheses)
    double          InflowCoeffETP;
    double          DeathCoeffDN;
    double          DeathCoeffDP;
    double          DeathCoeffTconv;
    double          DeathCoeffProFp3;
    double          DeathCoeffPro25;
    double          DeathCoeffDPTregs;
    double          DeathCoeffSP8;
    double          DeathCoeffEarlyDP = 0; // attention !
    double          OutputCoeffOutDP;
    double          OutputCoeffOutTconv;
    double          OutputCoeffOutDPTregs;
    double          OutputCoeffOutProFP3;
    double          OutputCoeffOutPro25;
    double          OutputCoeffOutSP8;
    double          LessDivisionsDN;
    double          LessDivisionsDP;
    double          FasterCoeffProFP3;
    double          FasterCoeffPro25;
    double          FasterCoeffTconv;
    double          FasterCoeffSP8;
    double          OutputCoeffDNtoDP;
    double          SpaceOutputCoeffAll;        // this one is not implemented (space-dependent export)
    double          LessProlifSpeedDN;
    double          LessProlifSpeedDP;
    double          LessProlifSpeedTregPro25;
    double          LessProlifSpeedTregProFP3;
    double          LessProlifSpeedDPTreg;
    double          LessProlifSpeedTconv;
    double          LessProlifSpeedCD8;
    double          OutputCoeffOutDN;   // not used yet

    // Maximum number of generations of DN cells (number of equations)
    size_t NmaxDN;
    // Maximum number of generations of DP cells (number of equations)
    size_t NmaxDP;

    // Variables for changing the number of divisions during simulation (for instance via logistic control/growth)
    double divFloatDN;              // dynamic current number of divisions
    vector<double> DoProlifDN;      // fraction of cells staying in the compartment after each division
    double divFloatDP;              // dynamic current number of divisions
    vector<double> DoProlifDP;      // fraction of cells staying in the compartment after each division


    // This defines the list of simulated variables. ***Do not change the order***, it matches the provided configuration files.

    // the names follow:
    //     tXXXN = thymic population, absolute numbers, rag Negative
    //     tXXXP = thymic population, absolute numbers, rag Positive
    //     pctXXXN = percent of rag Negative cells among summed Rag positive and negative thymocytes or among CD4SP (for Treg populations)
    //     pctXXXP = percent of rag Positive cells among summed Rag positive and negative thymocytes or among CD4SP (for Treg populations)
    //     t/pctXXXtot = absolute or percentage numbers of populations, summed Rag+ and Rag-
    //     Treg Precursor P1 = CD4+CD25+Foxp3- (also called P25 sometimes)
    //     Treg Precursor P2 = CD4+CD25-Foxp3+ (also called PFp3 sometimes) - sorry for the inconsistency
    //     'Treg' alone or TregDP (double positive for CD25 and Foxp3) refer for mature thymic Tregs (sorry for double naming as well)
    //     AgeXXX = average age of cells from entry into late DP (to mimic Rag timer)
    //     AgeXXXinXXX = average age of cells inside this population (since they entered it)
    //
    enum{
        flu,            // factor F, downstream the flu
        DNtot,
        eDPtot,         // early DPs, summing all generations, all Rag positive
        lDP,            // late DPs, one population only, no generation, all Rag positive
        DPtot,          // eDP + lDP
        tSP8P,
                    AgeTconvInTconv,
        tSP4P,
                    AgeTregP1inP1,
        tTconvP,
        tTRegP25P,
        tTRegPFp3P,
        tTregP,
        tSP8RagN,
        tSP4RagN,
        tTconvRagN,
        tTregP1RagN,
        tTregP2RagN,
        tTregRagN,
        tSP8tot,
        tSP4tot,
        tTconvtot,
        tTregP1tot,
        tTregP2tot,
        tTregtot,
        ttotal,

        // percents of total T cells. SP4 include Tregs and co
        pctDN,
        pctDP,
        pctSP8,
        pctSP4,

        // percents inside CD4SP
        pctTconvtot,
        pctTRegP1tot,
        pctTRegP2tot,
        pctTregtot,

        pctTconvP,
        pctTRegP1P,
        pctTRegP2P,
        pctTregP,

        pctSP8RagN,
        pctSP4RagN,
        pctTconvRagN,
        pctTRegP1RagN,
        pctTRegP2RagN,
        pctTregRagN,


        // Thomas-Vaslin for SP4 and SP8
                    AgeTregP2inP2,
                    AgeAllDPs,
                    AgelDP, AgeTregFromP1, AgeTregFromP1inMature,
                    TregsFromP2, TregsFromP1,
                    AgeTregFromP2,

                    AgeTconv,
                    AgeTregP1,
                    AgeTregP2,
                    AgeMatureTregs,
                    AgeTregFromP2inMature,

                    // This is a variable storing the pre
                    AgeMatureTregsinMature,

        floweDNtoeDP,
        floweDPtolDP,
        flowDPtoSP4,
        flowDPtoSP8,
        flowDPtoTconv,
        flowDPtoTregP1, // this is flow DP OR Tconv to P1 depending on the model structure
        flowDPtoTregP2, // this is flow DP OR Tconv to P2 depending on the model structure
        flowTregP1toTreg,
        flowTregP2toTreg,
        flowSP4toOut,
        flowSP8toOut,
        flowTregtoOut,
        floweDPtoDead,
        flowlDPtoDead,
        flowSP4toDead,
        flowSP8toDead,
        flowTconvtoDead,
        flowTregP1toDead,
        flowTregP2toDead,
        flowTregtoDead,

        divDNg0, divDNg1, divDNg2, divDNg3, divDNg4, divDNg5, divDNg6, divDNg7, divDNg8, divDNg9, divDNg10, divDNg11, divDNg12, divDNg13, divDNg14, divDNg15, divDNg16, divDNg17, divDNg18, divDNg19, divDNg20, divDNg21, divDNg22, divDNg23, divDNg24, divDNg25, divDNg26, divDNg27, divDNg28, divDNg29, divDNg30LastDiv,
        eDPg0, eDPg1, eDPg2, eDPg3, eDPg4, eDPg5, eDPg6,eDPg7, eDPg8,eDPg9, eDPg10,

        // additional variables that could be included to simulate the periphery (like spleen)
        // stotal, sTconv, sTreg, sCD8, sBcells,
        // sTconvRagN, sTregRagN, sCD8RagN,
        // pcsTconv, pcsTreg, pcsCD8, pcsBcells,
        // pcsTconvRagN, pcsTregRagN, pcsCD8RagN,

        NbVariables};

    // This is the list of parameters.
    // two types:   those defining the WT (normal dynamics)
    //              and those implementing a perturbation (starting by hyp- in their name).
    //                  depending if they are additive or multiplicative, 0 or 1 as value means no effect.
    // Do not change the order! to be compatible with config files.
    enum{
        flu_peak,
        flu_std,
        fETP_tDN, // inflow

        // General N-div model. pDN derived from other coefficients
        NdivDN,
        pDN,
        dDN,
        T_DN,

        // Thomas-Vaslin for DP. drestDP + diffDPtoSP4 + diffDPtoSP8 is the 'outflow' of DP and can be kept constant / identifiable
        NdivDP,
        peDP,
        deDP,
                        // Unused parameters,
                        hypDelayTregEgress,    // = 0, because we assume no proliferation of late DPs.
        drestDP,
        // the diff INCLUDES tregs
        out_tDP,
        diffDPtoSP4,
        diffDPtoSP8,

        // two-step ODE model for SP4 and SP8, just p and d + diff first ODE to second ODE,
        ptTconv,
        dtTconv,
                        TV_diffSP4P69PosToNeg, // unused, see TV-parameters below
        out_tTconv,

        ptSP8,
        dtSP8,
                        TV_diffSP8P69PosToNeg, // unused, see TV-parameters below
        out_SP8,

        // two-step Tregs models:
        ftDP_tTregP25,
        ftDP_tTregFP3,

        ptTregP25,
        ftTregP25_tDPTreg,
        dtTregP25,

        ptTregFP3, // prolif singer
        ftTregFP3_tDPTreg, // conversion singer to mature DPTregs
        dtTregFP3,

        ptDPTregs,
        dtDPTregs,
        out_tDPTregs,

                // Parameters That were use in previous code versions to simulate the SP4 and SP8 from Thomas-Vaslin 2007 JI paper in parallel, as comparison.
                // These parameters are now useless - but we don't remove to stik to the previous format of the config files
                TV_NdivSP4,
                TV_pSP4,
                TV_dSP4,
                TV_pcConvLastGenSP4,
                TV_NdivSP8,
                TV_pSP8,
                TV_dSP8,
                TV_pcConvLastGenSP8,

        // possible outflow of treg progenitors, normally we put it to 0, but possible to simulate
        out_tTregP25,
        out_tTregFP3,

                // These parameters were perpared to simulate the periphery (spleen) but we never reached it.
                // so they are unused
                Periph_fracTtoS,
                Periph_dsTconv, Periph_dsTreg, Periph_dsCD8, Periph_dsBcells,
                Periph_decayRag,

        // ==== Hypotheses perturbation parameters ====
        //ReducedInflow,
        hypInflowCoeffETP,

        //MoreDeath,
        hypDeathCoeffDN,
        hypDeathCoeffAddEarlyDP,
        hypDeathCoeffDP,
        hypDeathCoeffTconv,
        hypDeathCoeffProFp3,
        hypDeathCoeffPro25,
        hypDeathCoeffDPTregs,
        hypDeathCoeffSP8,

        //MoreOutputThymus,
        hypOutputCoeffOutDP,
        hypOutputCoeffOutTconv,
        hypOutputCoeffOutDPTregs,
        hypOutputCoeffOutProFP3,
        hypOutputCoeffOutPro25,
        hypOutputCoeffOutSP8,

        //FasterDifferentiation,
        hypFasterCoeffDNtoDP,
        hypFasterCoeffDPtolDP,
        hypFasterCoeffProFP3,
        hypFasterCoeffPro25,
        hypFasterCoeffTconv,
        hypFasterCoeffSP8,

        //Modulation of DN reaching DPs.
        hypOutputCoeffDNtoDP,

        //We didn't yet simulate a Space Dependent output of thymocytes.

        //ImpactProlifRates
        hypSpeedDivDN,
        hypSpeedDivDP,
        hypSpeedDivTregPro25,
        hypSpeedDivTregProFP3,
        hypSpeedDivDPTreg,
        hypSpeedDivTconv,
        hypSpeedDivCD8,

        logisticThymus,
        logisticStrength,

        NbParameters};

    long long background; // for different models
    virtual void derivatives(const vector<double> &x, vector<double> &dxdt, const double t);
    void initialise(long long _background = Back::WT);
	void setBaseParameters();
    void updateDerivedVariables(double _t);

    void action(string name, double parameter){
        if(!name.compare("wash")){
            if((parameter > 1.0) || (parameter < 0)) {cerr << "ERR: ModeleMinLatent::action(" << name << ", " << parameter << "), wrong parameter value\n"; return;}
            // val[xxx] =   xxx; // example of how the hell it works
            return;
        }
    }

    virtual ~modele6GenericTVaslin(){}
};

#endif


