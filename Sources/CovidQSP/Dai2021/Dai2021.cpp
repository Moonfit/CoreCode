// ------- Automatically generated model -------- //
#include "Dai2021.h"


void modelDai2021::derivatives(const vector<double> &x, vector<double> &dxdt, const double _t){




      // LUNG TO CENTRAL TRANSPORT RATES
        double tr_TNFa = params[ktr_TNFa]*x[TNFa];
        double tr_IL6 = params[ktr_IL6]*x[IL6];
        double tr_IL1b = params[ktr_IL1b]*x[IL1b];
        double tr_IFNb = params[ktr_IFNb]*x[IFNb];
        double tr_IFNg = params[ktr_IFNg]*x[IFNg];
        double tr_IL2 = params[ktr_IL2]*x[IL2];
        double tr_IL12 = params[ktr_IL12]*x[IL12];
        double tr_IL17 = params[ktr_IL17]*x[IL17];
        double tr_IL10 = params[ktr_IL10]*x[IL10];
        double tr_TGFb = params[ktr_TGFb]*x[TGFb];
        double tr_GMCSF = params[ktr_GMCSF]*x[GMCSF];
        double tr_SPD = params[ktr_SPD]*x[SPD];
        double tr_FER = params[ktr_FER]*x[FER];


        double tr_pDC = params[ktr_pDC]*(x[pDC]/params[vol_alv_ml] -x[pDC_c]);
        double tr_M1 =  params[ktr_M1]*(x[M1]/params[vol_alv_ml] -x[M1_c]);
        double tr_N = params[ktr_N]*(x[N]/params[vol_alv_ml] -x[N_c]);
        double tr_Th1 = params[ktr_Th1]*(x[Th1]/params[vol_alv_ml] -x[Th1_c]);
        double tr_Th17 = params[ktr_Th17]*(x[Th17]/params[vol_alv_ml] -x[Th17_c]);
        double tr_CTL = params[ktr_CTL]*(x[CTL]/params[vol_alv_ml] -x[CTL_c]);
        double tr_Treg = params[ktr_Treg]*(x[Treg]/params[vol_alv_ml] -x[Treg_c]);

        // viral dynamics

        //  Virus (# viral mRNA/mL)
        double prod_virus_shedding = params[A_V]*x[I];
        double virus_endocytosis = params[k_int]*x[AT2]*x[V]*(1-x[IFNb]/(params[km_int_IFNb]+x[IFNb]));
        double innate_clearance = params[k_V_innate]*x[V]*(x[M1]+x[pDC]);
        double deg_virus = params[b_V]*x[V];

        // not in use for now
        double ab_clearance = 0*params[k_Ab_V]*x[Ab]*x[V];


         if(!over(V))    dxdt[V] = prod_virus_shedding - params[f_int]*virus_endocytosis - deg_virus - innate_clearance - ab_clearance;

        // healthy alveolar type 2 cells (AT2) (# cells)
        double ICAT1 = init[AT1];
        double ICAT2 = init[AT2];
        double damage_cyt_AT = 1*params[k_damage_cyt]*(params[k_damage_TNFa]*(x[TNFa]/(params[km_damage_TNFa]+x[TNFa])) +  params[k_damage_IL6]*(x[IL6]/(params[km_damage_IL6]+x[IL6])) + params[k_damage_IL1b]*(x[IL1b]/(params[km_damage_IL1b] + x[IL1b])) + params[k_damage_IFNg]*(x[IFNg]/(params[km_damage_IFNg]+x[IFNg])));
        double damage_ROS_AT2 = params[k_ROS_AT2]*x[N]/(params[km_ROS_AT2]+x[N]);
        double growth_AT2 = params[mu_AT2]*(1 + params[k_mu_AT2]*(max((ICAT1+ICAT2)-(x[AT2]+x[AT1]),0.)/(params[km_AT1_AT2]*(ICAT1+ICAT2) + max((ICAT1+ICAT2)-(x[AT2]+x[AT1]),0.))))*x[AT2]; // induced differentiation and growth induced by AT1 decrease from baseline
        double deg_AT2 = params[b_AT2]*x[AT2];
        double diff_AT2 = params[k_AT1_AT2] * x[AT2] * (1 + params[k_diff_AT1]*max(ICAT1-x[AT1],0.)/(params[km_AT1_AT2]*ICAT1 + max(ICAT1-x[AT1],0.)));
         if(!over(AT2))    dxdt[AT2] = growth_AT2 - virus_endocytosis - damage_ROS_AT2*x[AT2] - deg_AT2 - damage_cyt_AT*x[AT2] - diff_AT2;

        // Infected AT2 (# cells)
        double kill_CTL_I = params[k_kill]*(1+params[k_IFNb_kill]*x[IFNb]/(params[km_kill]+x[IFNb]))*x[I]*x[CTL];
        double deg_I = params[b_I]*x[I];

         if(!over(I))    dxdt[I] = virus_endocytosis - deg_I ;//- kill_CTL_I - params[k_V_innate]*x[I]*(x[M1]+x[pDC]) - damage_ROS_AT2*x[I];

        // health alveolar type 1 cells (AT1) (# cells)
        double growth_AT1 = params[mu_AT1]*(ICAT1-x[AT1]); // no growth, no division, source is from AT2
        double damage_ROS_AT1 = damage_ROS_AT2*x[AT1];
        double deg_AT1 = params[b_AT1]*x[AT1];

        if(!over(AT1))    dxdt[AT1] = growth_AT1 - damage_ROS_AT1 - deg_AT1 - damage_cyt_AT*x[AT1] + diff_AT2 ;

        // damaged AT1 cells (# cells)
        double deg_dAT1 = params[b_dAT1]*x[dAT1];
         if(!over(dAT1))    dxdt[dAT1]= damage_ROS_AT1 - deg_dAT1 + damage_cyt_AT*x[AT1];

        // damaged AT2 cells (# cells)
        double deg_dAT2 = params[b_dAT1]*x[dAT2];
         if(!over(dAT2))    dxdt[dAT2] = damage_ROS_AT2*(x[I]+x[AT2])  - deg_dAT2 + damage_cyt_AT*x[AT2];



        // % % % % % Immune cells. % % % % %

        // lung pulmonary dendritic cells (# cells)
        double act_pDC_TNFa = params[k_DC_TNFa]*(x[TNFa]/(params[km_DC_TNFa]+x[TNFa]));
        double act_pDC_IFNg = params[k_DC_IFNg]*(x[IFNg]/(params[km_DC_IFNg]+x[IFNg]));
        ////////////// Problem, one variable is not read => I addeed it in the formula
        double act_pDC_IL6 = params[k_DC_IL6]*(x[IL6]/(params[km_DC_IL6]+x[IL6]));
        double act_pDC_GMCSF = params[k_M1_GMCSF]*(x[GMCSF]/(params[km_M1_GMCSF]+x[GMCSF]));
        double inh_pDC_IL10 = (params[km_DC_IL10]/(params[km_DC_IL10]+x[IL10]));

         if(!over(pDC))    dxdt[pDC] = params[a_DC]*(params[kbasal_DC]+ params[k_v]*x[V]/(params[km_v] + x[V]) + params[k_I]*x[I]/(params[km_I] + x[I]) + params[k_dAT]*(x[dAT1]+x[dAT2])/(params[km_dAT] + x[dAT1]+x[dAT2]))*(1+act_pDC_TNFa + act_pDC_IFNg + act_pDC_GMCSF + act_pDC_IL6)* inh_pDC_IL10 - params[b_DC]*x[pDC] - tr_pDC*params[vol_alv_ml] ;

        // lung M1 macrophages (# cells)
        double act_M1_TNFa = params[k_M1_TNFa]*(x[TNFa]/(params[km_M1_TNFa]+x[TNFa]));
        double act_M1_GMCSF = params[k_M1_GMCSF]*(x[GMCSF]/(params[km_M1_GMCSF]+x[GMCSF]));
        double act_M1_IFNg = params[k_M1_IFNg]*(x[IFNg]/(params[km_M1_IFNg]+x[IFNg]));
        double inh_M1_IL10 = (params[km_M1_IL10]/(params[km_M1_IL10]+x[IL10]));



         if(!over(M1))    dxdt[M1] = params[a_M1]*(params[kbasal_M1]+ params[k_v]*x[V]/(params[km_v]/1 + x[V]) + params[k_I]*x[I]/(params[km_I] + x[I]) + params[k_dAT]*(x[dAT1]+x[dAT2])/(params[km_dAT] + x[dAT1]+x[dAT2]))*(1+ act_M1_TNFa + act_M1_GMCSF + act_M1_IFNg)*inh_M1_IL10 - params[b_M1]*x[M1] - tr_M1*params[vol_alv_ml] ;

        // lung Neutrophils (# cells)
        double act_N_IFNg = params[k_N_IFNg]*x[IFNg]/(x[IFNg]+params[km_N_IFNg]);
        double act_N_TNFa= params[k_N_TNFa]*x[TNFa]/(x[TNFa]+params[km_N_TNFa]);
        double act_N_GMCSF = params[k_N_GMCSF]*x[GMCSF]/(x[GMCSF]+params[km_N_GMCSF]);
        double rec_N_IL17c = params[k_N_IL17c]*x[IL17_c]/(x[IL17_c] + params[km_N_IL17c]);

         if(!over(N))    dxdt[N] = params[a_N]*(params[kbasal_N]+  params[k_v]*x[V]/(params[km_v]/1 + x[V]) +  params[k_I]*x[I]/(params[km_I] + x[I]) + params[k_dAT]*(x[dAT1]+x[dAT2])/(params[km_dAT] + x[dAT1]+x[dAT2]))*(1+act_N_IFNg + act_N_TNFa + act_N_GMCSF) + rec_N_IL17c - params[b_N]*x[N] - tr_N*params[vol_alv_ml] ;

        // lung Th1 Cells (# cells)
        double act_Th1_IL12 = params[k_Th1_IL2]*(x[IL12]/(params[K_Th1_IL12]+x[IL12]))*(1+params[k_Th1_IL12IL2]*(x[IL2]/(params[K_Th1_IL12IL2]+x[IL2])));
        double act_Th1_IFNg = params[k_Th1_IFNg]*(x[IFNg]/(params[K_IFNg_Th1] + x[IFNg])) * (params[K_Th1_IL6]/(params[K_Th1_IL6]+x[IL6]));
        double inh_Th1_IL10_TGFb = (params[K_Th1_IL10]/(params[K_Th1_IL10]+x[IL10])) * (params[K_Th1_TGFb]/(params[K_Th1_TGFb]+x[TGFb]));
        double diff_Th1_Th17 = params[k_Th1_Th17]*x[Th17]*(x[IL12]/(params[K_Th1_Th17]+x[IL12]))*(params[K_Th1_TGFb]/(params[K_Th1_TGFb]+x[TGFb]));
        double diff_Th1_Treg = params[k_Th1_Treg]*x[Treg]*(x[IL12]/(params[K_Th1_Treg]+x[IL12]));

         if(!over(Th1))    dxdt[Th1] = params[a_Th1]*x[pDC]*(act_Th1_IL12 + act_Th1_IFNg)*inh_Th1_IL10_TGFb + diff_Th1_Th17 + 1*diff_Th1_Treg - params[b_Th1]*x[Th1] - tr_Th1*params[vol_alv_ml];

        // lung TH17 cells (# cells)
        double inh_Th17 = (params[K_Th17_IL2]/(params[K_Th17_IL2] + x[IL2])) * (params[K_Th17_IFNg]/(params[K_Th17_IFNg] + x[IFNg])) * (params[K_Th17_IL10]/(params[K_Th17_IL10] + x[IL10]));
        double act_TH17_TGFb = params[k_Th17_TGFb]*(x[TGFb]/(params[K_Th17_TGFb] + x[TGFb])) * inh_Th17;
        double act_Th17_IL6 = params[k_Th17_IL6]*(x[IL6]/(params[km_Th17_IL6] + x[IL6])) * inh_Th17;
        double act_Th17_IL1b = params[k_Th17_IL1b]*(x[IL1b]/(params[km_Th17_IL1b] + x[IL1b])) * inh_Th17;
         if(!over(Th17))    dxdt[Th17] = params[a_Th17]*pDC*(act_TH17_TGFb + act_Th17_IL6 + act_Th17_IL1b) - diff_Th1_Th17 - params[b_Th17]*Th17 - tr_Th17*params[vol_alv_ml];

        // lung CTL (# cells)
        double act_CTL_IFNb = params[kmax_MHC1]*(x[IFNb]/(params[km_MHC1_IFNb]+x[IFNb]));
        double act_CTL_IL12 = params[k_CTL_IL2]*(x[IL12]/(params[K_CTL_IL12]+x[IL12]))*(1+params[k_CTL_IL12IL2]*(x[IL12]/(params[K_CTL_IL12IL2]+x[IL12]))) * (params[K_CTL_IL10]/(params[K_CTL_IL10]+x[IL10])) * (params[K_CTL_TGFb]/(params[K_CTL_TGFb]+x[TGFb]));
        double act_CTL_IFNg = params[k_CTL_IFNg]*(x[IFNg]/(params[K_CTL_IFNg] + x[IFNg])) * (params[K_CTL_IL10]/(params[K_CTL_IL10]+x[IL10])) * (params[K_CTL_TGFb]/(params[K_CTL_TGFb]+x[TGFb])) * (params[K_CTL_IL6]/(params[K_CTL_IL6]+x[IL6]));

         if(!over(CTL))    dxdt[CTL] = params[a_CTL]*x[pDC]*(1+act_CTL_IFNb)*(1+ act_CTL_IL12 + act_CTL_IFNg)  - params[b_CTL]*CTL - tr_CTL*params[vol_alv_ml];//% + ktr_CTL*CTL_c;

        // lung Treg (# cells)
        double act_Treg_IL2 = params[k_Treg_IL2]*(x[IL2]/(params[K_Treg_IL2] + x[IL2])) * (params[K_Treg_IL17]/(params[K_Treg_IL17] + x[IL17])) * (params[K_Treg_IL6]/(params[K_Treg_IL6] + x[IL6]));
        double act_Treg_TGFb = params[k_Treg_TGFb]*(x[TGFb]/(params[K_Treg_TGFb] + x[TGFb])) * (params[K_Treg_IL17]/(params[K_Treg_IL17] + x[IL17])) * (params[K_Treg_IL6]/(params[K_Treg_IL6] + x[IL6]));

         if(!over(Treg))    dxdt[Treg] = params[a_Treg]*x[pDC]*(act_Treg_IL2 + act_Treg_TGFb) - diff_Th1_Treg - params[b_Treg]*x[Treg] - tr_Treg*params[vol_alv_ml];


        // Biomarkers

        // lung surfactant protein D (pmol)
         if(!over(SPD))    dxdt[SPD] = params[kbasal_SPD] + params[a_SPD] * (x[dAT1] + x[dAT2] + params[k_CTL_I_SPD]*kill_CTL_I) - tr_SPD ;

        // lung Ferritin (pmol)
         if(!over(FER))    dxdt[FER] = params[kbasal_FER] + params[a_FER] * (x[dAT1] + x[dAT2] + params[k_CTL_I_Fer]*kill_CTL_I) - tr_FER;

        // Liver CRP Production (pmol)
        double Liver_CRP = params[k_livercrp]*params[Liver]*((x[IL6_c]*params[Liver]));
        double prod_CRP_liver = params[kCRPSecretion]*Liver_CRP;
        double tr_CRP = params[kCRP_BloodtoLiver]*x[Blood_CRP]-params[kCRP_LivertoBlood]*Liver_CRP/params[Liver];
        double prod_CRP_blood = params[kbasal_CRP];
         if(!over(CRPExtracellular))    dxdt[CRPExtracellular] = prod_CRP_liver + tr_CRP*params[Liver] - params[kdeg_CRP]*x[CRPExtracellular];
         if(!over(Blood_CRP))    dxdt[Blood_CRP] = (-tr_CRP + prod_CRP_blood - params[kdeg_CRP]*x[Blood_CRP]);


        // Cytokine Dynamics

        // TNF-A (pmol)
        double prod_tnf_dat1 = params[a_tnf_at1]*x[dAT1];
        double prod_tnf_i = params[a_tnf_i]*x[I];
        double prod_tnf_dat2 = params[a_tnf_at2]*x[dAT2];
        double prod_tnf_m1 = params[a_tnf_m1]*x[M1];
        double prod_tnf_th1 = params[a_tnf_th1]*x[Th1];
        double prod_tnf_th17 = params[a_tnf_th17]*x[Th17];
        double deg_tnf = params[b_tnf]*x[TNFa];


         if(!over(TNFa))    dxdt[TNFa] = params[a_tnf]*(params[basal_tnfa] + prod_tnf_i + prod_tnf_dat2 + prod_tnf_dat1 + prod_tnf_m1 + prod_tnf_th1 + prod_tnf_th17) - deg_tnf - tr_TNFa;


        // IL-6 (pmol)
        double prod_il6_dat1 = params[a_il6_at1]*x[dAT1];
        double prod_il6_i = params[a_il6_i]*x[I];
        double prod_il6_dat2 = params[a_il6_at2]*x[dAT2];
        double prod_il6_m1 = params[a_il6_m1]*x[M1];
        double prod_il6_th17 = params[a_il6_th17]*x[Th17];
        double prod_il6_neu =params[a_il6_neu]*x[N];
        double deg_il6 = params[b_il6]*x[IL6];

         if(!over(IL6))    dxdt[IL6] = params[a_il6]*(params[basalil6] + prod_il6_dat1 + prod_il6_i + prod_il6_dat2 + prod_il6_m1 + prod_il6_th17 + prod_il6_neu) - deg_il6 - tr_IL6;


        // IL1-B (pmol)
        double prod_il1b_dat1 = params[a_il1b_at2]*x[dAT1];
        double prod_il1b_i = params[a_il1b_i]*x[I];
        double prod_il1b_dat2 = params[a_il1b_at2]*x[dAT2];
        double prod_il1b_m1 = params[a_il1b_m1]*x[M1];
        double a_il1b_dc = params[a_il1b_m1];
        double prod_il1b_dc = params[a_il1b_dc]*x[pDC];
        double deg_il1b = params[b_il1b]*x[IL1b];

         if(!over(IL1b))    dxdt[IL1b] = params[a_il1b]*(params[basalil1] + prod_il1b_dat1 + prod_il1b_i + prod_il1b_dat2 + prod_il1b_m1 + prod_il1b_dc) - deg_il1b - tr_IL1b;

        // IFN-G (pmol)
        double prod_ifng_dc = params[a_ifng_dc]*x[pDC];
        double prod_ifng_th1 = params[a_ifng_th1]*x[Th1];
        double prod_ifng_ctl = params[a_ifng_ctl]*x[CTL];
        double del_ifng = params[b_ifng]*x[IFNg];

         if(!over(IFNg))    dxdt[IFNg] = params[a_ifng]*(params[basalifng] + prod_ifng_dc + prod_ifng_th1 + prod_ifng_ctl) - del_ifng - tr_IFNg;

        // IFN-B (pmol)
        double prod_ifnb_i = params[a_ifnb_i]*x[I];
        double prod_ifnb_dc = params[a_ifnb_dc]*x[pDC];
        double del_ifnb = params[b_ifnb]*x[IFNb];

         if(!over(IFNb))    dxdt[IFNb] = params[a_ifnb]*(params[basalifnb] + prod_ifnb_i + prod_ifnb_dc) - del_ifnb - tr_IFNb;

        // IL-2 (pmol)
        double prod_il2_dc = params[a_il2_dc]*x[pDC];
        double prod_il2_th1 = params[a_il2_th1]*x[Th1];
        double deg_il2 = params[b_il2]*x[IL2];

         if(!over(IL2))    dxdt[IL2] = params[a_il2]*(params[basalil2] + prod_il2_dc + prod_il2_th1) - deg_il2 - tr_IL2;

        // IL-12 (pmol)
        double prod_il12_m1 = params[a_il12_m1]*x[M1];
        double prod_il12_dc = params[a_il12_dc]*x[pDC];
        double deg_il12 = params[b_il12]*x[IL12];

         if(!over(IL12))    dxdt[IL12] = params[a_il12]*(params[basalil12] + prod_il12_m1 + prod_il12_dc) - deg_il12 - tr_IL12;

        // IL-17 (pmol)
        double prod_il17_th17 = params[a_il17_th17]*x[Th17];
        double prod_il17_ctl = params[a_il17_ctl]*x[CTL];
        double deg_il17 = params[b_il17]*x[IL17];

         if(!over(IL17))    dxdt[IL17] = params[a_il17]*(prod_il17_th17 + prod_il17_ctl) - deg_il17 - tr_IL17;

        // IL-10 (pmol)
        double prod_il10_treg = params[a_il10_treg]*x[Treg];
        double deg_il10 = params[b_il10]*x[IL10];

         if(!over(IL10))    dxdt[IL10] = params[a_il10]*(params[basalil10]+ prod_il10_treg) - deg_il10 - tr_IL10;

        // TGF-B (pmol)
        double prod_tgfb_th17 = params[a_tgfb_th17]*x[Th17];
        double prod_tgfb_treg = params[a_tgfb_treg]*x[Treg];
        double deg_tgfb = params[b_tgfb]*x[TGFb];

         if(!over(TGFb))    dxdt[TGFb] = params[a_tgfb]*(params[basaltgfb] + prod_tgfb_th17 + prod_tgfb_treg) - deg_tgfb - tr_TGFb;

        // GM-CSF (pmol)
        double prod_gmcsf_m1 = params[a_gmcsf_m1]*x[M1];
        double prod_gmcsf_th1 = params[a_gmcsf_th1]*x[Th1];
        double prod_gmcsf_th17 = params[a_gmcsf_th17]*x[Th17];
        double deg_gmcsf = params[b_gmcsf]*x[GMCSF];

         if(!over(GMCSF))    dxdt[GMCSF] = params[a_gmcsf]*(params[basalgmcsf] + prod_gmcsf_m1 + prod_gmcsf_th1 + prod_gmcsf_th17) - deg_gmcsf - tr_GMCSF;

        // neutralizing antibody Don't get, this equation works at any time, no need to have the time constraint
        dxdt[Ab] = 0;
        if(_t>=params[tau_Ab]){
            if(!over(Ab))    dxdt[Ab] = params[a_Ab] - x[Ab]*params[b_Ab] - ab_clearance;
        }



        // Central compartment cytokines (pmol/mL)
         if(!over(TNFa_c))    dxdt[TNFa_c] = tr_TNFa/vol_plasma - params[b_tnf]*x[TNFa_c];
         if(!over(IL6_c))    dxdt[IL6_c] = tr_IL6/vol_plasma - params[b_il6]*x[IL6_c];
         if(!over(IL1b_c))    dxdt[IL1b_c] = tr_IL1b/vol_plasma - params[b_il1b]*x[IL1b_c];
         if(!over(IFNb_c))    dxdt[IFNb_c] = tr_IFNb/vol_plasma - params[b_ifnb]*x[IFNb_c];
         if(!over(IFNg_c))    dxdt[IFNg_c] = tr_IFNg/vol_plasma - params[b_ifng]*x[IFNg_c];
         if(!over(IL2_c))    dxdt[IL2_c] = tr_IL2/vol_plasma - params[b_il2]*x[IL2_c];
         if(!over(IL12_c))    dxdt[IL12_c] = tr_IL12/vol_plasma - params[b_il12]*x[IL12_c];
         if(!over(IL17_c))    dxdt[IL17_c] = tr_IL17/vol_plasma - params[b_il17]*x[IL17_c];
         if(!over(IL10_c))    dxdt[IL10_c] = tr_IL10/vol_plasma - params[b_il10]*x[IL10_c];
         if(!over(TGFb_c))    dxdt[TGFb_c] = tr_TGFb/vol_plasma - params[b_tgfb]*x[TGFb_c];
         if(!over(GMCSF_c))    dxdt[GMCSF_c] = tr_GMCSF/vol_plasma - params[b_gmcsf]*x[GMCSF_c];
         if(!over(SPD_c))    dxdt[SPD_c] = tr_SPD/vol_plasma - params[b_SPD]*x[SPD_c];
         if(!over(FER_c))    dxdt[FER_c] = params[a_FER_c] + tr_FER/vol_plasma - params[b_FER]*x[FER_c];

        // Central compartment cells (# cells/uL)
         if(!over(pDC_c))    dxdt[pDC_c] =  tr_pDC*params[vol_alv_ml]/vol_plasma - x[pDC_c]*params[b_DC];
         if(!over(M1_c))    dxdt[M1_c] =  tr_M1*params[vol_alv_ml]/vol_plasma - x[M1_c]*params[b_M1];
         if(!over(N_c))    dxdt[N_c] = tr_N*params[vol_alv_ml]/vol_plasma -  x[N_c]*params[b_N];
         if(!over(Th1_c))    dxdt[Th1_c] =  tr_Th1*params[vol_alv_ml]/vol_plasma - x[Th1_c]*params[b_Th1];
         if(!over(Th17_c))    dxdt[Th17_c] =  tr_Th17*params[vol_alv_ml]/vol_plasma - x[Th17_c]*params[b_Th17];
         if(!over(CTL_c))    dxdt[CTL_c] =  tr_CTL*params[vol_alv_ml]/vol_plasma - x[CTL_c]*params[b_CTL];
         if(!over(Treg_c))    dxdt[Treg_c] =  tr_Treg*params[vol_alv_ml]/vol_plasma - x[Treg_c]*params[b_Treg];

}

void modelDai2021::updateDerivedVariables(double _t){
    // molecular weights g/mol. Da
    double mw_il6 = 21000;
    double mw_il2 = 15500;
    double mw_il1b = 17500;
    double mw_il10 = 18000;
    double mw_il12 = 70000;
    double mw_il17 = 35000;
    double mw_tnfa = 25900;
    double mw_ifnb = 20000;
    double mw_ifng = 23000;
    double mw_tgfb = 4760;
    double mw_gmcsf = 25000; // 14 - 35kDa
    // molecular weights kDa
    double mw_fer = 484; //kDa
    double mw_spd = 43; //kDa
    double mw_crp = 20; //kDa

    // variables from IBD model needed for parameters
    double data_dictionary_iDC = 1e7;
    double data_dictionary_M0 = 1e7;
    double data_dictionary_Th0 = 1e7;
    double data_dictionary_iCD4 = 300;  // #/uL plasma naive T cells
    double data_dictionary_iCD8 = 200;  // #/uL plasma naive T cells
    double data_dictionary_iMono = 500; // #/uL plasma monocytes
    double data_dictionary_iN = 2000;   // immature N

    val[V_plot] = val[V] / 1;
    val[AT1_plot] = val[AT1] / 2.00E+10;
    val[AT2_plot] = val[AT2] / 3.00E+10;
    val[I_plot] = val[I] / 1.98E+09;
    val[dAT1_plot] = val[dAT1] / 2.21E+06;
    val[dAT2_plot] = val[dAT2] / 3.02E+06;
    val[pDC_plot] = val[pDC] / vol_alv_ml/1000;
    val[M1_plot] = val[M1] / vol_alv_ml/1000;
    val[N_plot] = val[N] / vol_alv_ml/1000;
    val[N_tot_plot] = val[N] / vol_alv_ml/1000+data_dictionary_iN;
    val[Th1_plot] = val[Th1] / vol_alv_ml/1000;
    val[Th17_plot] = val[Th17] / vol_alv_ml/1000;
    val[CTL_plot] = val[CTL] / vol_alv_ml/1000;
    val[CTL_tot_plot] = val[CTL] / vol_alv_ml/1000+data_dictionary_iCD8;
    val[Treg_plot] = val[Treg] / vol_alv_ml/1000;
    val[TNFa_plot] = val[TNFa] / vol_alv_ml*mw_tnfa;
    val[IL6_plot] = val[IL6] / vol_alv_ml*mw_il6;
    val[IL1b_plot] = val[IL1b] / vol_alv_ml*mw_il1b;
    val[IFNb_plot] = val[IFNb] / vol_alv_ml*mw_ifnb;
    val[IFNg_plot] = val[IFNg] / vol_alv_ml*mw_ifng;
    val[IL2_plot] = val[IL2] / vol_alv_ml*mw_il2;
    val[IL12_plot] = val[IL12] / vol_alv_ml*mw_il12;
    val[IL17_plot] = val[IL17] / vol_alv_ml*mw_il17;
    val[IL10_plot] = val[IL10] / vol_alv_ml*mw_il10;
    val[TGFb_plot] = val[TGFb] / vol_alv_ml*mw_tgfb;
    val[GMCSF_plot] = val[GMCSF] / vol_alv_ml*mw_gmcsf;
    val[SPD_plot] = val[SPD] / vol_alv_ml*mw_spd;
    val[FER_plot] = val[FER] / vol_alv_ml*mw_fer;
    val[TNFa_c_plot] = val[TNFa_c] *mw_tnfa;
    val[IL6_c_plot] = val[IL6_c] *mw_il6;
    val[IL1b_c_plot] = val[IL1b_c] *mw_il1b;
    val[IFNb_c_plot] = val[IFNb_c] *mw_ifnb;
    val[IFNg_c_plot] = val[IFNg_c] *mw_ifng;
    val[IL2_c_plot] = val[IL2_c] *mw_il2;
    val[IL12_c_plot] = val[IL12_c] *mw_il12;
    val[IL17_c_plot] = val[IL17_c] *mw_il17;
    val[IL10_c_plot] = val[IL10_c] *mw_il10;
    val[TGFb_c_plot] = val[TGFb_c] *mw_tgfb;
    val[GMCSF_c_plot] = val[GMCSF_c] *mw_gmcsf;
    val[SPD_c_plot] = val[SPD_c] *mw_spd;
    val[FER_c_plot] = val[FER_c] *mw_fer;
    val[Ab_plot] = val[Ab];
    val[pDC_c_plot] = val[pDC_c] / 1000;
    val[M1_c_plot] = val[M1_c] / 1000;
    val[N_c_plot] = val[N_c] / 1000 + data_dictionary_iN;
    val[Th1_c_plot] = val[Th1_c] / 1000;
    val[Th17_c_plot] = val[Th17_c] / 1000;
    val[CTL_c_plot] = val[CTL_c] / 1000 + data_dictionary_iCD8;
    val[Treg_c_plot] = val[Treg_c] / 1000;
    val[CRPExtracellular_plot] = val[CRPExtracellular];
    val[Blood_CRP_plot] = val[Blood_CRP] *mw_crp/1000;
    val[CD4_c_tot_plot] = val[Th1_c]/1000.+val[Th17_c]/1000+val[Treg_c]/1000. + data_dictionary_iCD4;
    val[CD3_c_tot_plot] = val[Th1_c]/1000.+val[Th17_c]/1000+val[Treg_c]/1000.+val[CTL_c]/1000.+data_dictionary_iCD4 + data_dictionary_iCD8;
    val[CD4_tot_plot] = val[Th1]/params[vol_alv_ml]/1000.+val[Th17]/params[vol_alv_ml]/1000.+val[Treg]/params[vol_alv_ml]/1000. + data_dictionary_iCD4;
    val[CD3_tot_plot] = val[Th1]/params[vol_alv_ml]/1000.+val[Th17]/params[vol_alv_ml]/1000.+val[Treg]/params[vol_alv_ml]/1000.+val[CTL]/params[vol_alv_ml]/1000+data_dictionary_iCD4+data_dictionary_iCD4;
    val[CD4_lung_plot] = val[Th1]/params[vol_alv_ml]/1000.+val[Th17]/params[vol_alv_ml]/1000.+val[Treg]/params[vol_alv_ml]/1000.;
    val[CD3_lung_plot] = val[CTL]/params[vol_alv_ml]/1000.;


}

modelDai2021::modelDai2021() : Model(NBVAR, NBPARAM), background(0) {
    name = string("Erwin 2017");    // name of this particular model
    dt = 0.001;   print_every_dt = 0.1;              // initial values for dt of simulation and for following the kinetics. Can be modified by user later.

    names[V] = "V";
    names[AT1] = "AT1";
    names[AT2] = "AT2";
    names[I] = "I";
    names[dAT1] = "dAT1";
    names[dAT2] = "dAT2";
    names[pDC] = "pDC";
    names[M1] = "M1";
    names[N] = "N";
    names[Th1] = "Th1";
    names[Th17] = "Th17";
    names[CTL] = "CTL";
    names[Treg] = "Treg";
    names[TNFa] = "TNFa";
    names[IL6] = "IL6";
    names[IL1b] = "IL1b";
    names[IFNb] = "IFNb";
    names[IFNg] = "IFNg";
    names[IL2] = "IL2";
    names[IL12] = "IL12";
    names[IL17] = "IL17";
    names[IL10] = "IL10";
    names[TGFb] = "TGFb";
    names[GMCSF] = "GMCSF";
    names[SPD] = "SPD";
    names[FER] = "FER";
    names[TNFa_c] = "TNFa_c";
    names[IL6_c] = "IL6_c";
    names[IL1b_c] = "IL1b_c";
    names[IFNb_c] = "IFNb_c";
    names[IFNg_c] = "IFNg_c";
    names[IL2_c] = "IL2_c";
    names[IL12_c] = "IL12_c";
    names[IL17_c] = "IL17_c";
    names[IL10_c] = "IL10_c";
    names[TGFb_c] = "TGFb_c";
    names[GMCSF_c] = "GMCSF_c";
    names[SPD_c] = "SPD_c";
    names[FER_c] = "FER_c";
    names[Ab] = "Ab";
    names[CRPExtracellular] = "CRPExtracellular";
    names[Blood_CRP] = "Blood_CRP";
    names[pDC_c] = "pDC_c";
    names[M1_c] = "M1_c";
    names[N_c] = "N_c";
    names[Th1_c] = "Th1_c";
    names[Th17_c] = "Th17_c";
    names[CTL_c] = "CTL_c";
    names[Treg_c] = "Treg_c";
    names[V_plot] = "Viral Load (RNA molecules/mL)";
    names[AT1_plot] = "AT1 (#, norm 2e10)";
    names[AT2_plot] = "AT2 (#, norm 3e10)";
    names[I_plot] = "Infected (#, norm max)";
    names[dAT1_plot] = "Damaged AT1 (#, norm max)";
    names[dAT2_plot] = "Damaged AT2 (#, norm max)";
    names[pDC_plot] = "(activated) pDC lung (#/μL)";
    names[M1_plot] = "M1 lung (#/μL)";
    names[N_plot] = "Activated N lung (#/μL)";
    names[N_tot_plot] = "Total N lung (#/μL)";
    names[Th1_plot] = "Th1 lung (#/μL)";
    names[Th17_plot] = "Th17 lung (#/μL)";
    names[CTL_plot] = "Activated CTL lung (#/μL)";
    names[CTL_tot_plot] = "Total CTL lung (#/μL + nonspec)";
    names[Treg_plot] = "Treg lung (#/μL)";
    names[TNFa_plot] = "TNFa lung  (pg/ml)";
    names[IL6_plot] = "IL6 lung (pg/mL)";
    names[IL1b_plot] = "IL1b lung  (pg/ml)";
    names[IFNb_plot] = "Type I IFN lung  (pg/ml)";
    names[IFNg_plot] = "IFN gamma lung  (pg/ml)";
    names[IL2_plot] = "IL2 lung  (pg/ml)";
    names[IL12_plot] = "IL12 lung (pg/ml)";
    names[IL17_plot] = "IL17 lung  (pg/ml)";
    names[IL10_plot] = "IL10 lung  (pg/ml)";
    names[TGFb_plot] = "TGFb lung  (pg/ml)";
    names[GMCSF_plot] = "GMCSF lung  (pg/ml)";
    names[SPD_plot] = "Surfactant Protein D lung (ng/mL)";
    names[FER_plot] = "Ferritin lung (ug/L)";
    names[TNFa_c_plot] = "TNFa plasma  (pg/ml)";
    names[IL6_c_plot] = "IL6 plasma (pg/mL)";
    names[IL1b_c_plot] = "IL1b plasma  (pg/ml)";
    names[IFNb_c_plot] = "IFNb plasma  (pg/ml)";
    names[IFNg_c_plot] = "IFNg plasma  (pg/ml)";
    names[IL2_c_plot] = "IL2 plasma  (pg/ml)";
    names[IL12_c_plot] = "IL12 plasma (pg/ml)";
    names[IL17_c_plot] = "IL17 plasma  (pg/ml)";
    names[IL10_c_plot] = "IL10 plasma  (pg/ml)";
    names[TGFb_c_plot] = "TGFb plasma  (pg/ml)";
    names[GMCSF_c_plot] = "GMCSF plasma  (pg/ml)";
    names[SPD_c_plot] = "Surfactant Protein D plasma (ng/mL)";
    names[FER_c_plot] = "Ferritin plasma (ug/L)";
    names[Ab_plot] = "Antibodies (#)";
    names[pDC_c_plot] = "pDC plasma (#/μL)";
    names[M1_c_plot] = "M1 plasma (#/μL)";
    names[N_c_plot] = "Total N plasma (#/μL + nonspec)";
    names[Th1_c_plot] = "Th1 plasma (#/μL)";
    names[Th17_c_plot] = "Th17 plasma (#/μL)";
    names[CTL_c_plot] = "Total CTL plasma (#/μL + nonspec)";
    names[Treg_c_plot] = "Treg plasma (#/μL)";
    names[CRPExtracellular_plot] = "CRP extracellular (unit?)";
    names[Blood_CRP_plot] = "Blood CRP mg/L'";
    names[CD4_c_tot_plot] = "Total CD4 plasma (#/μL + nonspec)";
    names[CD3_c_tot_plot] = "Total CD3 plasma (#/μL + nonspec)";
    names[CD4_tot_plot] = "CD4 lung (#/μL)";
    names[CD3_tot_plot] = "CD3 lung (#/μL)";
    names[CD4_lung_plot] = "'Activated CD4+';' T cells lung (#/μL)";
    names[CD3_lung_plot] = "Activated CTL lung (#/μL)";



    extNames[V] = "V";
    extNames[AT1] = "AT1";
    extNames[AT2] = "AT2";
    extNames[I] = "I";
    extNames[dAT1] = "dAT1";
    extNames[dAT2] = "dAT2";
    extNames[pDC] = "pDC";
    extNames[M1] = "M1";
    extNames[N] = "N";
    extNames[Th1] = "Th1";
    extNames[Th17] = "Th17";
    extNames[CTL] = "CTL";
    extNames[Treg] = "Treg";
    extNames[TNFa] = "TNFa";
    extNames[IL6] = "IL6";
    extNames[IL1b] = "IL1b";
    extNames[IFNb] = "IFNb";
    extNames[IFNg] = "IFNg";
    extNames[IL2] = "IL2";
    extNames[IL12] = "IL12";
    extNames[IL17] = "IL17";
    extNames[IL10] = "IL10";
    extNames[TGFb] = "TGFb";
    extNames[GMCSF] = "GMCSF";
    extNames[SPD] = "SPD";
    extNames[FER] = "FER";
    extNames[TNFa_c] = "TNFa_c";
    extNames[IL6_c] = "IL6_c";
    extNames[IL1b_c] = "IL1b_c";
    extNames[IFNb_c] = "IFNb_c";
    extNames[IFNg_c] = "IFNg_c";
    extNames[IL2_c] = "IL2_c";
    extNames[IL12_c] = "IL12_c";
    extNames[IL17_c] = "IL17_c";
    extNames[IL10_c] = "IL10_c";
    extNames[TGFb_c] = "TGFb_c";
    extNames[GMCSF_c] = "GMCSF_c";
    extNames[SPD_c] = "SPD_c";
    extNames[FER_c] = "FER_c";
    extNames[Ab] = "Ab";
    extNames[CRPExtracellular] = "CRPExtracellular";
    extNames[Blood_CRP] = "Blood_CRP";
    extNames[pDC_c] = "pDC_c";
    extNames[M1_c] = "M1_c";
    extNames[N_c] = "N_c";
    extNames[Th1_c] = "Th1_c";
    extNames[Th17_c] = "Th17_c";
    extNames[CTL_c] = "CTL_c";
    extNames[Treg_c] = "Treg_c";
    extNames[V_plot] = "V_plot";
    extNames[AT1_plot] = "AT1_plot";
    extNames[AT2_plot] = "AT2_plot";
    extNames[I_plot] = "I_plot";
    extNames[dAT1_plot] = "dAT1_plot";
    extNames[dAT2_plot] = "dAT2_plot";
    extNames[pDC_plot] = "pDC_plot";
    extNames[M1_plot] = "M1_plot";
    extNames[N_plot] = "N_plot";
    extNames[N_tot_plot] = "N_plot";
    extNames[Th1_plot] = "Th1_plot";
    extNames[Th17_plot] = "Th17_plot";
    extNames[CTL_plot] = "CTL_plot";
    extNames[CTL_tot_plot] = "CTL_plot";
    extNames[Treg_plot] = "Treg_plot";
    extNames[TNFa_plot] = "TNFa_plot";
    extNames[IL6_plot] = "IL6_plot";
    extNames[IL1b_plot] = "IL1b_plot";
    extNames[IFNb_plot] = "IFNb_plot";
    extNames[IFNg_plot] = "IFNg_plot";
    extNames[IL2_plot] = "IL2_plot";
    extNames[IL12_plot] = "IL12_plot";
    extNames[IL17_plot] = "IL17_plot";
    extNames[IL10_plot] = "IL10_plot";
    extNames[TGFb_plot] = "TGFb_plot";
    extNames[GMCSF_plot] = "GMCSF_plot";
    extNames[SPD_plot] = "SPD_plot";
    extNames[FER_plot] = "FER_plot";
    extNames[TNFa_c_plot] = "TNFa_c_plot";
    extNames[IL6_c_plot] = "IL6_c_plot";
    extNames[IL1b_c_plot] = "IL1b_c_plot";
    extNames[IFNb_c_plot] = "IFNb_c_plot";
    extNames[IFNg_c_plot] = "IFNg_c_plot";
    extNames[IL2_c_plot] = "IL2_c_plot";
    extNames[IL12_c_plot] = "IL12_c_plot";
    extNames[IL17_c_plot] = "IL17_c_plot";
    extNames[IL10_c_plot] = "IL10_c_plot";
    extNames[TGFb_c_plot] = "TGFb_c_plot";
    extNames[GMCSF_c_plot] = "GMCSF_c_plot";
    extNames[SPD_c_plot] = "SPD_c_plot";
    extNames[FER_c_plot] = "FER_c_plot";
    extNames[Ab_plot] = "Ab_plot";
    extNames[pDC_c_plot] = "pDC_c_plot";
    extNames[M1_c_plot] = "M1_c_plot";
    extNames[N_c_plot] = "N_c_plot";
    extNames[Th1_c_plot] = "Th1_c_plot";
    extNames[Th17_c_plot] = "Th17_c_plot";
    extNames[CTL_c_plot] = "CTL_c_plot";
    extNames[Treg_c_plot] = "Treg_c_plot";
    extNames[CRPExtracellular_plot] = "CRPExtracellular_plot";
    extNames[Blood_CRP_plot] = "Blood_CRP_plot";
    extNames[CD4_c_tot_plot] = "CD4_c_tot_plot";
    extNames[CD3_c_tot_plot] = "CD3_c_tot_plot";
    extNames[CD4_tot_plot] = "CD4_tot_plot";
    extNames[CD3_tot_plot] = "CD3_tot_plot";
    extNames[CD4_lung_plot] = "CD4 lung_plot";
    extNames[CD3_lung_plot] = "CD3 lung_plot";




    // associates the indices of the variables inside the model with the 'official' names/index of variables to be accessed by outside.
    // so, when the model have a different number of variables, they are always accessed with the same 'official' name (ex : 'N::IL2'), from outside, even if they actually have a different indice/order inside the model ('IL2')
    paramNames[A_V] = "A_V";
    paramNames[k_int] = "k_int";
    paramNames[km_int_IFNb] = "km_int_IFNb";
    paramNames[k_V_innate] = "k_V_innate";
    paramNames[b_V] = "b_V";
    paramNames[f_int] = "f_int";
    paramNames[mu_AT2] = "mu_AT2";
    paramNames[k_ROS_AT2] = "k_ROS_AT2";
    paramNames[km_ROS_AT2] = "km_ROS_AT2";
    paramNames[k_AT1_AT2] = "k_AT1_AT2";
    paramNames[km_AT1_AT2] = "km_AT1_AT2";
    paramNames[k_IFNb_kill] = "k_IFNb_kill";
    paramNames[k_kill] = "k_kill";
    paramNames[km_kill] = "km_kill";
    paramNames[km_ROS_AT2] = "km_ROS_AT2";
    paramNames[b_AT2] = "b_AT2";
    paramNames[b_I] = "b_I";
    paramNames[mu_AT1] = "mu_AT1";
    paramNames[k_ROS_AT1] = "k_ROS_AT1";
    paramNames[km_ROS_AT1] = "km_ROS_AT1";
    paramNames[b_AT1] = "b_AT1";
    paramNames[b_dAT1] = "b_dAT1";
    paramNames[k_damage_TNFa] = "k_damage_TNFa";
    paramNames[km_damage_TNFa] = "km_damage_TNFa";
    paramNames[k_damage_IL6] = "k_damage_IL6";
    paramNames[km_damage_IL6] = "km_damage_IL6";
    paramNames[k_damage_IL1b] = "k_damage_IL1b";
    paramNames[km_damage_IL1b] = "km_damage_IL1b";
    paramNames[k_damage_IFNg] = "k_damage_IFNg";
    paramNames[km_damage_IFNg] = "km_damage_IFNg";
    paramNames[k_damage_cyt] = "k_damage_cyt";
    paramNames[a_DC] = "a_DC";
    paramNames[kbasal_DC] = "kbasal_DC";
    paramNames[b_DC] = "b_DC";
    paramNames[k_DC_TNFa] = "k_DC_TNFa";
    paramNames[km_DC_TNFa] = "km_DC_TNFa";
    paramNames[k_DC_IFNg] = "k_DC_IFNg";
    paramNames[km_DC_IFNg] = "km_DC_IFNg";
    paramNames[k_DC_IL6] = "k_DC_IL6";
    paramNames[km_DC_IL6] = "km_DC_IL6";
    paramNames[km_DC_IL10] = "km_DC_IL10";
    paramNames[a_M1] = "a_M1";
    paramNames[kbasal_M1] = "kbasal_M1";
    paramNames[k_v] = "k_v";
    paramNames[k_I] = "k_I";
    paramNames[km_v] = "km_v";
    paramNames[km_I] = "km_I";
    paramNames[k_M1_IL6] = "k_M1_IL6";
    paramNames[km_M1_IL6] = "km_M1_IL6";
    paramNames[b_M1] = "b_M1";
    paramNames[k_M1_TNFa] = "k_M1_TNFa";
    paramNames[km_M1_TNFa] = "km_M1_TNFa";
    paramNames[k_M1_GMCSF] = "k_M1_GMCSF";
    paramNames[km_M1_GMCSF] = "km_M1_GMCSF";
    paramNames[k_M1_IFNg] = "k_M1_IFNg";
    paramNames[km_M1_IFNg] = "km_M1_IFNg";
    paramNames[km_M1_IL10] = "km_M1_IL10";
    paramNames[a_N] = "a_N";
    paramNames[k_N_IFNg] = "k_N_IFNg";
    paramNames[km_N_IFNg] = "km_N_IFNg";
    paramNames[k_N_TNFa] = "k_N_TNFa";
    paramNames[km_N_TNFa] = "km_N_TNFa";
    paramNames[k_N_GMCSF] = "k_N_GMCSF";
    paramNames[km_N_GMCSF] = "km_N_GMCSF";
    paramNames[k_N_IL17c] = "k_N_IL17c";
    paramNames[km_N_IL17c] = "km_N_IL17c";
    paramNames[b_N] = "b_N";
    paramNames[a_Th1] = "a_Th1";
    paramNames[b_Th1] = "b_Th1";
    paramNames[k_Th1_IL2] = "k_Th1_IL2";
    paramNames[K_Th1_IL12] = "K_Th1_IL12";
    paramNames[k_Th1_IL12IL2] = "k_Th1_IL12IL2";
    paramNames[K_Th1_IL12IL2] = "K_Th1_IL12IL2";
    paramNames[K_Th1_IL10] = "K_Th1_IL10";
    paramNames[K_Th1_TGFb] = "K_Th1_TGFb";
    paramNames[k_Th1_IFNg] = "k_Th1_IFNg";
    paramNames[K_IFNg_Th1] = "K_IFNg_Th1";
    paramNames[K_Th1_IL6] = "K_Th1_IL6";
    paramNames[k_Th1_Th17] = "k_Th1_Th17";
    paramNames[K_Th1_Th17] = "K_Th1_Th17";
    paramNames[k_Th1_Treg] = "k_Th1_Treg";
    paramNames[K_Th1_Treg] = "K_Th1_Treg";
    paramNames[a_Th17] = "a_Th17";
    paramNames[b_Th17] = "b_Th17";
    paramNames[k_Th17_TGFb] = "k_Th17_TGFb";
    paramNames[K_Th17_TGFb] = "K_Th17_TGFb";
    paramNames[K_Th17_IL2] = "K_Th17_IL2";
    paramNames[K_Th17_IFNg] = "K_Th17_IFNg";
    paramNames[K_Th17_IL10] = "K_Th17_IL10";
    paramNames[k_Th17_IL6] = "k_Th17_IL6";
    paramNames[km_Th17_IL6] = "km_Th17_IL6";
    paramNames[k_Th17_IL1b] = "k_Th17_IL1b";
    paramNames[km_Th17_IL1b] = "km_Th17_IL1b";
    paramNames[a_CTL] = "a_CTL";
    paramNames[b_CTL] = "b_CTL";
    paramNames[k_CTL_IL2] = "k_CTL_IL2";
    paramNames[K_CTL_IL12] = "K_CTL_IL12";
    paramNames[k_CTL_IL12IL2] = "k_CTL_IL12IL2";
    paramNames[K_CTL_IL12IL2] = "K_CTL_IL12IL2";
    paramNames[K_CTL_IL10] = "K_CTL_IL10";
    paramNames[K_CTL_TGFb] = "K_CTL_TGFb";
    paramNames[K_CTL_IL6] = "K_CTL_IL6";
    paramNames[k_CTL_IFNg] = "k_CTL_IFNg";
    paramNames[K_CTL_IFNg] = "K_CTL_IFNg";
    paramNames[kmax_MHC1] = "kmax_MHC1";
    paramNames[km_MHC1_IFNb] = "km_MHC1_IFNb";
    paramNames[a_Treg] = "a_Treg";
    paramNames[b_Treg] = "b_Treg";
    paramNames[k_Treg_IL2] = "k_Treg_IL2";
    paramNames[K_Treg_IL2] = "K_Treg_IL2";
    paramNames[K_Treg_IL17] = "K_Treg_IL17";
    paramNames[K_Treg_IL6] = "K_Treg_IL6";
    paramNames[k_Treg_TGFb] = "k_Treg_TGFb";
    paramNames[K_Treg_TGFb] = "K_Treg_TGFb";
    paramNames[kbasal_SPD] = "kbasal_SPD";
    paramNames[a_SPD] = "a_SPD";
    paramNames[b_SPD] = "b_SPD";
    paramNames[kbasal_FER] = "kbasal_FER";
    paramNames[a_FER] = "a_FER";
    paramNames[b_FER] = "b_FER";
    paramNames[a_tnf] = "a_tnf";
    paramNames[a_tnf_at1] = "a_tnf_at1";
    paramNames[a_tnf_i] = "a_tnf_i";
    paramNames[a_tnf_at2] = "a_tnf_at2";
    paramNames[a_tnf_m1] = "a_tnf_m1";
    paramNames[a_tnf_th1] = "a_tnf_th1";
    paramNames[a_tnf_th17] = "a_tnf_th17";
    paramNames[b_tnf] = "b_tnf";
    paramNames[a_il6] = "a_il6";
    paramNames[b_il6] = "b_il6";
    paramNames[a_il6_at1] = "a_il6_at1";
    paramNames[a_il6_i] = "a_il6_i";
    paramNames[a_il6_at2] = "a_il6_at2";
    paramNames[a_il6_m1] = "a_il6_m1";
    paramNames[a_il6_th17] = "a_il6_th17";
    paramNames[a_il6_neu] = "a_il6_neu";
    paramNames[a_ifng] = "a_ifng";
    paramNames[b_ifng] = "b_ifng";
    paramNames[a_ifng_dc] = "a_ifng_dc";
    paramNames[a_ifng_th1] = "a_ifng_th1";
    paramNames[a_ifng_ctl] = "a_ifng_ctl";
    paramNames[a_ifnb] = "a_ifnb";
    paramNames[b_ifnb] = "b_ifnb";
    paramNames[a_ifnb_at1] = "a_ifnb_at1";
    paramNames[a_ifnb_i] = "a_ifnb_i";
    paramNames[a_ifnb_d] = "a_ifnb_d";
    paramNames[a_ifnb_dc] = "a_ifnb_dc";
    paramNames[a_il2_dc] = "a_il2_dc";
    paramNames[a_il2_th1] = "a_il2_th1";
    paramNames[b_il2] = "b_il2";
    paramNames[a_il2] = "a_il2";
    paramNames[a_il12_m1] = "a_il12_m1";
    paramNames[a_il12_dc] = "a_il12_dc";
    paramNames[b_il12] = "b_il12";
    paramNames[a_il12] = "a_il12";
    paramNames[a_il17_th17] = "a_il17_th17";
    paramNames[a_il17_ctl] = "a_il17_ctl";
    paramNames[b_il17] = "b_il17";
    paramNames[a_il17] = "a_il17";
    paramNames[a_il10_treg] = "a_il10_treg";
    paramNames[b_il10] = "b_il10";
    paramNames[a_il10] = "a_il10";
    paramNames[a_tgfb_th17] = "a_tgfb_th17";
    paramNames[a_tgfb_treg] = "a_tgfb_treg";
    paramNames[b_tgfb] = "b_tgfb";
    paramNames[a_tgfb] = "a_tgfb";
    paramNames[a_gmcsf_m1] = "a_gmcsf_m1";
    paramNames[a_gmcsf_th1] = "a_gmcsf_th1";
    paramNames[a_gmcsf_th17] = "a_gmcsf_th17";
    paramNames[b_gmcsf] = "b_gmcsf";
    paramNames[a_gmcsf] = "a_gmcsf";
    paramNames[a_il1b] = "a_il1b";
    paramNames[b_il1b] = "b_il1b";
    paramNames[a_il1b_at1] = "a_il1b_at1";
    paramNames[a_il1b_i] = "a_il1b_i";
    paramNames[a_il1b_at2] = "a_il1b_at2";
    paramNames[a_il1b_m1] = "a_il1b_m1";
    paramNames[a_il1b_dc] = "a_il1b_dc";
    paramNames[kbasal_ROS] = "kbasal_ROS";
    paramNames[b_ROS] = "b_ROS";
    paramNames[ktr_TNFa] = "ktr_TNFa";
    paramNames[ktr_IL6] = "ktr_IL6";
    paramNames[ktr_IL1b] = "ktr_IL1b";
    paramNames[ktr_IFNb] = "ktr_IFNb";
    paramNames[ktr_IFNg] = "ktr_IFNg";
    paramNames[ktr_IL2] = "ktr_IL2";
    paramNames[ktr_IL12] = "ktr_IL12";
    paramNames[ktr_IL17] = "ktr_IL17";
    paramNames[ktr_IL10] = "ktr_IL10";
    paramNames[ktr_TGFb] = "ktr_TGFb";
    paramNames[ktr_GMCSF] = "ktr_GMCSF";
    paramNames[ktr_SPD] = "ktr_SPD";
    paramNames[ktr_FER] = "ktr_FER";
    paramNames[k_CTL_I_SPD] = "k_CTL_I_SPD";
    paramNames[k_CTL_I_Fer] = "k_CTL_I_Fer";
    paramNames[kdc] = "kdc";
    paramNames[ktr_pDC] = "ktr_pDC";
    paramNames[ktr_M1] = "ktr_M1";
    paramNames[ktr_N] = "ktr_N";
    paramNames[ktr_Th1] = "ktr_Th1";
    paramNames[ktr_Th17] = "ktr_Th17";
    paramNames[ktr_CTL] = "ktr_CTL";
    paramNames[ktr_Treg] = "ktr_Treg";
    paramNames[k_Ab_V] = "k_Ab_V";
    paramNames[k_mu_AT2] = "k_mu_AT2";
    paramNames[k_diff_AT1] = "k_diff_AT1";
    paramNames[k_dAT] = "k_dAT";
    paramNames[km_dAT] = "km_dAT";
    paramNames[kbasal_N] = "kbasal_N";
    paramNames[k_livercrp] = "k_livercrp";
    paramNames[Liver] = "Liver";
    paramNames[kCRPSecretion] = "kCRPSecretion";
    paramNames[kCRP_BloodtoLiver] = "kCRP_BloodtoLiver";
    paramNames[kCRP_LivertoBlood] = "kCRP_LivertoBlood";
    paramNames[kbasal_CRP] = "kbasal_CRP";
    paramNames[kdeg_CRP] = "kdeg_CRP";
    paramNames[basal_tnfa] = "basal_tnfa";
    paramNames[basalil6] = "basalil6";
    paramNames[basalil1] = "basalil1";
    paramNames[basalifng] = "basalifng";
    paramNames[basalifnb] = "basalifnb";
    paramNames[basalil2] = "basalil2";
    paramNames[basalil12] = "basalil12";
    paramNames[basalil10] = "basalil10";
    paramNames[basaltgfb] = "basaltgfb";
    paramNames[basalgmcsf] = "basalgmcsf";
    paramNames[tau_Ab] = "tau_Ab";
    paramNames[a_Ab] = "a_Ab";
    paramNames[b_Ab] = "b_Ab";
    paramNames[a_FER_c] = "a_FER_c";
    paramNames[vol_plasma] = "vol_plasma";
    paramNames[vol_alv_ml] = "vol_alv_ml";




    paramLowBounds[A_V] = 0;		paramUpBounds[A_V] = 0;
    paramLowBounds[k_int] = 0;		paramUpBounds[k_int] = 0;
    paramLowBounds[km_int_IFNb] = 0;		paramUpBounds[km_int_IFNb] = 0;
    paramLowBounds[k_V_innate] = 0;		paramUpBounds[k_V_innate] = 0;
    paramLowBounds[b_V] = 0;		paramUpBounds[b_V] = 0;
    paramLowBounds[f_int] = 0;		paramUpBounds[f_int] = 0;
    paramLowBounds[mu_AT2] = 0;		paramUpBounds[mu_AT2] = 0;
    paramLowBounds[k_ROS_AT2] = 0;		paramUpBounds[k_ROS_AT2] = 0;
    paramLowBounds[km_ROS_AT2] = 0;		paramUpBounds[km_ROS_AT2] = 0;
    paramLowBounds[k_AT1_AT2] = 0;		paramUpBounds[k_AT1_AT2] = 0;
    paramLowBounds[km_AT1_AT2] = 0;		paramUpBounds[km_AT1_AT2] = 0;
    paramLowBounds[k_IFNb_kill] = 0;		paramUpBounds[k_IFNb_kill] = 0;
    paramLowBounds[k_kill] = 0;		paramUpBounds[k_kill] = 0;
    paramLowBounds[km_kill] = 0;		paramUpBounds[km_kill] = 0;
    paramLowBounds[km_ROS_AT2] = 0;		paramUpBounds[km_ROS_AT2] = 0;
    paramLowBounds[b_AT2] = 0;		paramUpBounds[b_AT2] = 0;
    paramLowBounds[b_I] = 0;		paramUpBounds[b_I] = 0;
    paramLowBounds[mu_AT1] = 0;		paramUpBounds[mu_AT1] = 0;
    paramLowBounds[k_ROS_AT1] = 0;		paramUpBounds[k_ROS_AT1] = 0;
    paramLowBounds[km_ROS_AT1] = 0;		paramUpBounds[km_ROS_AT1] = 0;
    paramLowBounds[b_AT1] = 0;		paramUpBounds[b_AT1] = 0;
    paramLowBounds[b_dAT1] = 0;		paramUpBounds[b_dAT1] = 0;
    paramLowBounds[k_damage_TNFa] = 0;		paramUpBounds[k_damage_TNFa] = 0;
    paramLowBounds[km_damage_TNFa] = 0;		paramUpBounds[km_damage_TNFa] = 0;
    paramLowBounds[k_damage_IL6] = 0;		paramUpBounds[k_damage_IL6] = 0;
    paramLowBounds[km_damage_IL6] = 0;		paramUpBounds[km_damage_IL6] = 0;
    paramLowBounds[k_damage_IL1b] = 0;		paramUpBounds[k_damage_IL1b] = 0;
    paramLowBounds[km_damage_IL1b] = 0;		paramUpBounds[km_damage_IL1b] = 0;
    paramLowBounds[k_damage_IFNg] = 0;		paramUpBounds[k_damage_IFNg] = 0;
    paramLowBounds[km_damage_IFNg] = 0;		paramUpBounds[km_damage_IFNg] = 0;
    paramLowBounds[k_damage_cyt] = 0;		paramUpBounds[k_damage_cyt] = 0;
    paramLowBounds[a_DC] = 0;		paramUpBounds[a_DC] = 0;
    paramLowBounds[kbasal_DC] = 0;		paramUpBounds[kbasal_DC] = 0;
    paramLowBounds[b_DC] = 0;		paramUpBounds[b_DC] = 0;
    paramLowBounds[k_DC_TNFa] = 0;		paramUpBounds[k_DC_TNFa] = 0;
    paramLowBounds[km_DC_TNFa] = 0;		paramUpBounds[km_DC_TNFa] = 0;
    paramLowBounds[k_DC_IFNg] = 0;		paramUpBounds[k_DC_IFNg] = 0;
    paramLowBounds[km_DC_IFNg] = 0;		paramUpBounds[km_DC_IFNg] = 0;
    paramLowBounds[k_DC_IL6] = 0;		paramUpBounds[k_DC_IL6] = 0;
    paramLowBounds[km_DC_IL6] = 0;		paramUpBounds[km_DC_IL6] = 0;
    paramLowBounds[km_DC_IL10] = 0;		paramUpBounds[km_DC_IL10] = 0;
    paramLowBounds[a_M1] = 0;		paramUpBounds[a_M1] = 0;
    paramLowBounds[kbasal_M1] = 0;		paramUpBounds[kbasal_M1] = 0;
    paramLowBounds[k_v] = 0;		paramUpBounds[k_v] = 0;
    paramLowBounds[k_I] = 0;		paramUpBounds[k_I] = 0;
    paramLowBounds[km_v] = 0;		paramUpBounds[km_v] = 0;
    paramLowBounds[km_I] = 0;		paramUpBounds[km_I] = 0;
    paramLowBounds[k_M1_IL6] = 0;		paramUpBounds[k_M1_IL6] = 0;
    paramLowBounds[km_M1_IL6] = 0;		paramUpBounds[km_M1_IL6] = 0;
    paramLowBounds[b_M1] = 0;		paramUpBounds[b_M1] = 0;
    paramLowBounds[k_M1_TNFa] = 0;		paramUpBounds[k_M1_TNFa] = 0;
    paramLowBounds[km_M1_TNFa] = 0;		paramUpBounds[km_M1_TNFa] = 0;
    paramLowBounds[k_M1_GMCSF] = 0;		paramUpBounds[k_M1_GMCSF] = 0;
    paramLowBounds[km_M1_GMCSF] = 0;		paramUpBounds[km_M1_GMCSF] = 0;
    paramLowBounds[k_M1_IFNg] = 0;		paramUpBounds[k_M1_IFNg] = 0;
    paramLowBounds[km_M1_IFNg] = 0;		paramUpBounds[km_M1_IFNg] = 0;
    paramLowBounds[km_M1_IL10] = 0;		paramUpBounds[km_M1_IL10] = 0;
    paramLowBounds[a_N] = 0;		paramUpBounds[a_N] = 0;
    paramLowBounds[k_N_IFNg] = 0;		paramUpBounds[k_N_IFNg] = 0;
    paramLowBounds[km_N_IFNg] = 0;		paramUpBounds[km_N_IFNg] = 0;
    paramLowBounds[k_N_TNFa] = 0;		paramUpBounds[k_N_TNFa] = 0;
    paramLowBounds[km_N_TNFa] = 0;		paramUpBounds[km_N_TNFa] = 0;
    paramLowBounds[k_N_GMCSF] = 0;		paramUpBounds[k_N_GMCSF] = 0;
    paramLowBounds[km_N_GMCSF] = 0;		paramUpBounds[km_N_GMCSF] = 0;
    paramLowBounds[k_N_IL17c] = 0;		paramUpBounds[k_N_IL17c] = 0;
    paramLowBounds[km_N_IL17c] = 0;		paramUpBounds[km_N_IL17c] = 0;
    paramLowBounds[b_N] = 0;		paramUpBounds[b_N] = 0;
    paramLowBounds[a_Th1] = 0;		paramUpBounds[a_Th1] = 0;
    paramLowBounds[b_Th1] = 0;		paramUpBounds[b_Th1] = 0;
    paramLowBounds[k_Th1_IL2] = 0;		paramUpBounds[k_Th1_IL2] = 0;
    paramLowBounds[K_Th1_IL12] = 0;		paramUpBounds[K_Th1_IL12] = 0;
    paramLowBounds[k_Th1_IL12IL2] = 0;		paramUpBounds[k_Th1_IL12IL2] = 0;
    paramLowBounds[K_Th1_IL12IL2] = 0;		paramUpBounds[K_Th1_IL12IL2] = 0;
    paramLowBounds[K_Th1_IL10] = 0;		paramUpBounds[K_Th1_IL10] = 0;
    paramLowBounds[K_Th1_TGFb] = 0;		paramUpBounds[K_Th1_TGFb] = 0;
    paramLowBounds[k_Th1_IFNg] = 0;		paramUpBounds[k_Th1_IFNg] = 0;
    paramLowBounds[K_IFNg_Th1] = 0;		paramUpBounds[K_IFNg_Th1] = 0;
    paramLowBounds[K_Th1_IL6] = 0;		paramUpBounds[K_Th1_IL6] = 0;
    paramLowBounds[k_Th1_Th17] = 0;		paramUpBounds[k_Th1_Th17] = 0;
    paramLowBounds[K_Th1_Th17] = 0;		paramUpBounds[K_Th1_Th17] = 0;
    paramLowBounds[k_Th1_Treg] = 0;		paramUpBounds[k_Th1_Treg] = 0;
    paramLowBounds[K_Th1_Treg] = 0;		paramUpBounds[K_Th1_Treg] = 0;
    paramLowBounds[a_Th17] = 0;		paramUpBounds[a_Th17] = 0;
    paramLowBounds[b_Th17] = 0;		paramUpBounds[b_Th17] = 0;
    paramLowBounds[k_Th17_TGFb] = 0;		paramUpBounds[k_Th17_TGFb] = 0;
    paramLowBounds[K_Th17_TGFb] = 0;		paramUpBounds[K_Th17_TGFb] = 0;
    paramLowBounds[K_Th17_IL2] = 0;		paramUpBounds[K_Th17_IL2] = 0;
    paramLowBounds[K_Th17_IFNg] = 0;		paramUpBounds[K_Th17_IFNg] = 0;
    paramLowBounds[K_Th17_IL10] = 0;		paramUpBounds[K_Th17_IL10] = 0;
    paramLowBounds[k_Th17_IL6] = 0;		paramUpBounds[k_Th17_IL6] = 0;
    paramLowBounds[km_Th17_IL6] = 0;		paramUpBounds[km_Th17_IL6] = 0;
    paramLowBounds[k_Th17_IL1b] = 0;		paramUpBounds[k_Th17_IL1b] = 0;
    paramLowBounds[km_Th17_IL1b] = 0;		paramUpBounds[km_Th17_IL1b] = 0;
    paramLowBounds[a_CTL] = 0;		paramUpBounds[a_CTL] = 0;
    paramLowBounds[b_CTL] = 0;		paramUpBounds[b_CTL] = 0;
    paramLowBounds[k_CTL_IL2] = 0;		paramUpBounds[k_CTL_IL2] = 0;
    paramLowBounds[K_CTL_IL12] = 0;		paramUpBounds[K_CTL_IL12] = 0;
    paramLowBounds[k_CTL_IL12IL2] = 0;		paramUpBounds[k_CTL_IL12IL2] = 0;
    paramLowBounds[K_CTL_IL12IL2] = 0;		paramUpBounds[K_CTL_IL12IL2] = 0;
    paramLowBounds[K_CTL_IL10] = 0;		paramUpBounds[K_CTL_IL10] = 0;
    paramLowBounds[K_CTL_TGFb] = 0;		paramUpBounds[K_CTL_TGFb] = 0;
    paramLowBounds[K_CTL_IL6] = 0;		paramUpBounds[K_CTL_IL6] = 0;
    paramLowBounds[k_CTL_IFNg] = 0;		paramUpBounds[k_CTL_IFNg] = 0;
    paramLowBounds[K_CTL_IFNg] = 0;		paramUpBounds[K_CTL_IFNg] = 0;
    paramLowBounds[kmax_MHC1] = 0;		paramUpBounds[kmax_MHC1] = 0;
    paramLowBounds[km_MHC1_IFNb] = 0;		paramUpBounds[km_MHC1_IFNb] = 0;
    paramLowBounds[a_Treg] = 0;		paramUpBounds[a_Treg] = 0;
    paramLowBounds[b_Treg] = 0;		paramUpBounds[b_Treg] = 0;
    paramLowBounds[k_Treg_IL2] = 0;		paramUpBounds[k_Treg_IL2] = 0;
    paramLowBounds[K_Treg_IL2] = 0;		paramUpBounds[K_Treg_IL2] = 0;
    paramLowBounds[K_Treg_IL17] = 0;		paramUpBounds[K_Treg_IL17] = 0;
    paramLowBounds[K_Treg_IL6] = 0;		paramUpBounds[K_Treg_IL6] = 0;
    paramLowBounds[k_Treg_TGFb] = 0;		paramUpBounds[k_Treg_TGFb] = 0;
    paramLowBounds[K_Treg_TGFb] = 0;		paramUpBounds[K_Treg_TGFb] = 0;
    paramLowBounds[kbasal_SPD] = 0;		paramUpBounds[kbasal_SPD] = 0;
    paramLowBounds[a_SPD] = 0;		paramUpBounds[a_SPD] = 0;
    paramLowBounds[b_SPD] = 0;		paramUpBounds[b_SPD] = 0;
    paramLowBounds[kbasal_FER] = 0;		paramUpBounds[kbasal_FER] = 0;
    paramLowBounds[a_FER] = 0;		paramUpBounds[a_FER] = 0;
    paramLowBounds[b_FER] = 0;		paramUpBounds[b_FER] = 0;
    paramLowBounds[a_tnf] = 0;		paramUpBounds[a_tnf] = 0;
    paramLowBounds[a_tnf_at1] = 0;		paramUpBounds[a_tnf_at1] = 0;
    paramLowBounds[a_tnf_i] = 0;		paramUpBounds[a_tnf_i] = 0;
    paramLowBounds[a_tnf_at2] = 0;		paramUpBounds[a_tnf_at2] = 0;
    paramLowBounds[a_tnf_m1] = 0;		paramUpBounds[a_tnf_m1] = 0;
    paramLowBounds[a_tnf_th1] = 0;		paramUpBounds[a_tnf_th1] = 0;
    paramLowBounds[a_tnf_th17] = 0;		paramUpBounds[a_tnf_th17] = 0;
    paramLowBounds[b_tnf] = 0;		paramUpBounds[b_tnf] = 0;
    paramLowBounds[a_il6] = 0;		paramUpBounds[a_il6] = 0;
    paramLowBounds[b_il6] = 0;		paramUpBounds[b_il6] = 0;
    paramLowBounds[a_il6_at1] = 0;		paramUpBounds[a_il6_at1] = 0;
    paramLowBounds[a_il6_i] = 0;		paramUpBounds[a_il6_i] = 0;
    paramLowBounds[a_il6_at2] = 0;		paramUpBounds[a_il6_at2] = 0;
    paramLowBounds[a_il6_m1] = 0;		paramUpBounds[a_il6_m1] = 0;
    paramLowBounds[a_il6_th17] = 0;		paramUpBounds[a_il6_th17] = 0;
    paramLowBounds[a_il6_neu] = 0;		paramUpBounds[a_il6_neu] = 0;
    paramLowBounds[a_ifng] = 0;		paramUpBounds[a_ifng] = 0;
    paramLowBounds[b_ifng] = 0;		paramUpBounds[b_ifng] = 0;
    paramLowBounds[a_ifng_dc] = 0;		paramUpBounds[a_ifng_dc] = 0;
    paramLowBounds[a_ifng_th1] = 0;		paramUpBounds[a_ifng_th1] = 0;
    paramLowBounds[a_ifng_ctl] = 0;		paramUpBounds[a_ifng_ctl] = 0;
    paramLowBounds[a_ifnb] = 0;		paramUpBounds[a_ifnb] = 0;
    paramLowBounds[b_ifnb] = 0;		paramUpBounds[b_ifnb] = 0;
    paramLowBounds[a_ifnb_at1] = 0;		paramUpBounds[a_ifnb_at1] = 0;
    paramLowBounds[a_ifnb_i] = 0;		paramUpBounds[a_ifnb_i] = 0;
    paramLowBounds[a_ifnb_d] = 0;		paramUpBounds[a_ifnb_d] = 0;
    paramLowBounds[a_ifnb_dc] = 0;		paramUpBounds[a_ifnb_dc] = 0;
    paramLowBounds[a_il2_dc] = 0;		paramUpBounds[a_il2_dc] = 0;
    paramLowBounds[a_il2_th1] = 0;		paramUpBounds[a_il2_th1] = 0;
    paramLowBounds[b_il2] = 0;		paramUpBounds[b_il2] = 0;
    paramLowBounds[a_il2] = 0;		paramUpBounds[a_il2] = 0;
    paramLowBounds[a_il12_m1] = 0;		paramUpBounds[a_il12_m1] = 0;
    paramLowBounds[a_il12_dc] = 0;		paramUpBounds[a_il12_dc] = 0;
    paramLowBounds[b_il12] = 0;		paramUpBounds[b_il12] = 0;
    paramLowBounds[a_il12] = 0;		paramUpBounds[a_il12] = 0;
    paramLowBounds[a_il17_th17] = 0;		paramUpBounds[a_il17_th17] = 0;
    paramLowBounds[a_il17_ctl] = 0;		paramUpBounds[a_il17_ctl] = 0;
    paramLowBounds[b_il17] = 0;		paramUpBounds[b_il17] = 0;
    paramLowBounds[a_il17] = 0;		paramUpBounds[a_il17] = 0;
    paramLowBounds[a_il10_treg] = 0;		paramUpBounds[a_il10_treg] = 0;
    paramLowBounds[b_il10] = 0;		paramUpBounds[b_il10] = 0;
    paramLowBounds[a_il10] = 0;		paramUpBounds[a_il10] = 0;
    paramLowBounds[a_tgfb_th17] = 0;		paramUpBounds[a_tgfb_th17] = 0;
    paramLowBounds[a_tgfb_treg] = 0;		paramUpBounds[a_tgfb_treg] = 0;
    paramLowBounds[b_tgfb] = 0;		paramUpBounds[b_tgfb] = 0;
    paramLowBounds[a_tgfb] = 0;		paramUpBounds[a_tgfb] = 0;
    paramLowBounds[a_gmcsf_m1] = 0;		paramUpBounds[a_gmcsf_m1] = 0;
    paramLowBounds[a_gmcsf_th1] = 0;		paramUpBounds[a_gmcsf_th1] = 0;
    paramLowBounds[a_gmcsf_th17] = 0;		paramUpBounds[a_gmcsf_th17] = 0;
    paramLowBounds[b_gmcsf] = 0;		paramUpBounds[b_gmcsf] = 0;
    paramLowBounds[a_gmcsf] = 0;		paramUpBounds[a_gmcsf] = 0;
    paramLowBounds[a_il1b] = 0;		paramUpBounds[a_il1b] = 0;
    paramLowBounds[b_il1b] = 0;		paramUpBounds[b_il1b] = 0;
    paramLowBounds[a_il1b_at1] = 0;		paramUpBounds[a_il1b_at1] = 0;
    paramLowBounds[a_il1b_i] = 0;		paramUpBounds[a_il1b_i] = 0;
    paramLowBounds[a_il1b_at2] = 0;		paramUpBounds[a_il1b_at2] = 0;
    paramLowBounds[a_il1b_m1] = 0;		paramUpBounds[a_il1b_m1] = 0;
    paramLowBounds[a_il1b_dc] = 0;		paramUpBounds[a_il1b_dc] = 0;
    paramLowBounds[kbasal_ROS] = 0;		paramUpBounds[kbasal_ROS] = 0;
    paramLowBounds[b_ROS] = 0;		paramUpBounds[b_ROS] = 0;
    paramLowBounds[ktr_TNFa] = 0;		paramUpBounds[ktr_TNFa] = 0;
    paramLowBounds[ktr_IL6] = 0;		paramUpBounds[ktr_IL6] = 0;
    paramLowBounds[ktr_IL1b] = 0;		paramUpBounds[ktr_IL1b] = 0;
    paramLowBounds[ktr_IFNb] = 0;		paramUpBounds[ktr_IFNb] = 0;
    paramLowBounds[ktr_IFNg] = 0;		paramUpBounds[ktr_IFNg] = 0;
    paramLowBounds[ktr_IL2] = 0;		paramUpBounds[ktr_IL2] = 0;
    paramLowBounds[ktr_IL12] = 0;		paramUpBounds[ktr_IL12] = 0;
    paramLowBounds[ktr_IL17] = 0;		paramUpBounds[ktr_IL17] = 0;
    paramLowBounds[ktr_IL10] = 0;		paramUpBounds[ktr_IL10] = 0;
    paramLowBounds[ktr_TGFb] = 0;		paramUpBounds[ktr_TGFb] = 0;
    paramLowBounds[ktr_GMCSF] = 0;		paramUpBounds[ktr_GMCSF] = 0;
    paramLowBounds[ktr_SPD] = 0;		paramUpBounds[ktr_SPD] = 0;
    paramLowBounds[ktr_FER] = 0;		paramUpBounds[ktr_FER] = 0;
    paramLowBounds[k_CTL_I_SPD] = 0;		paramUpBounds[k_CTL_I_SPD] = 0;
    paramLowBounds[k_CTL_I_Fer] = 0;		paramUpBounds[k_CTL_I_Fer] = 0;
    paramLowBounds[kdc] = 0;		paramUpBounds[kdc] = 0;
    paramLowBounds[ktr_pDC] = 0;		paramUpBounds[ktr_pDC] = 0;
    paramLowBounds[ktr_M1] = 0;		paramUpBounds[ktr_M1] = 0;
    paramLowBounds[ktr_N] = 0;		paramUpBounds[ktr_N] = 0;
    paramLowBounds[ktr_Th1] = 0;		paramUpBounds[ktr_Th1] = 0;
    paramLowBounds[ktr_Th17] = 0;		paramUpBounds[ktr_Th17] = 0;
    paramLowBounds[ktr_CTL] = 0;		paramUpBounds[ktr_CTL] = 0;
    paramLowBounds[ktr_Treg] = 0;		paramUpBounds[ktr_Treg] = 0;
    paramLowBounds[k_Ab_V] = 0;		paramUpBounds[k_Ab_V] = 0;
    paramLowBounds[k_mu_AT2] = 0;		paramUpBounds[k_mu_AT2] = 0;
    paramLowBounds[k_diff_AT1] = 0;		paramUpBounds[k_diff_AT1] = 0;
    paramLowBounds[k_dAT] = 0;		paramUpBounds[k_dAT] = 0;
    paramLowBounds[km_dAT] = 0;		paramUpBounds[km_dAT] = 0;
    paramLowBounds[kbasal_N] = 0;		paramUpBounds[kbasal_N] = 0;
    paramLowBounds[k_livercrp] = 0;		paramUpBounds[k_livercrp] = 0;
    paramLowBounds[Liver] = 0;		paramUpBounds[Liver] = 0;
    paramLowBounds[kCRPSecretion] = 0;		paramUpBounds[kCRPSecretion] = 0;
    paramLowBounds[kCRP_BloodtoLiver] = 0;		paramUpBounds[kCRP_BloodtoLiver] = 0;
    paramLowBounds[kCRP_LivertoBlood] = 0;		paramUpBounds[kCRP_LivertoBlood] = 0;
    paramLowBounds[kbasal_CRP] = 0;		paramUpBounds[kbasal_CRP] = 0;
    paramLowBounds[kdeg_CRP] = 0;		paramUpBounds[kdeg_CRP] = 0;
    paramLowBounds[basal_tnfa] = 0;		paramUpBounds[basal_tnfa] = 0;
    paramLowBounds[basalil6] = 0;		paramUpBounds[basalil6] = 0;
    paramLowBounds[basalil1] = 0;		paramUpBounds[basalil1] = 0;
    paramLowBounds[basalifng] = 0;		paramUpBounds[basalifng] = 0;
    paramLowBounds[basalifnb] = 0;		paramUpBounds[basalifnb] = 0;
    paramLowBounds[basalil2] = 0;		paramUpBounds[basalil2] = 0;
    paramLowBounds[basalil12] = 0;		paramUpBounds[basalil12] = 0;
    paramLowBounds[basalil10] = 0;		paramUpBounds[basalil10] = 0;
    paramLowBounds[basaltgfb] = 0;		paramUpBounds[basaltgfb] = 0;
    paramLowBounds[basalgmcsf] = 0;		paramUpBounds[basalgmcsf] = 0;
    paramLowBounds[tau_Ab] = 0;		paramUpBounds[tau_Ab] = 0;
    paramLowBounds[a_Ab] = 0;		paramUpBounds[a_Ab] = 0;
    paramLowBounds[b_Ab] = 0;		paramUpBounds[b_Ab] = 0;
    paramLowBounds[a_FER_c] = 0;		paramUpBounds[a_FER_c] = 0;
    paramLowBounds[vol_plasma] = 0;     paramUpBounds[vol_plasma] = 0;
    paramLowBounds[vol_alv_ml] = 0;     paramUpBounds[vol_alv_ml] = 0;



}

void modelDai2021::setBaseParameters(){
    // would like the model to have scaling for more GCs but less antigen and vice-versa
    params.clear();                 // to make sure they are all put to zero
    params.resize(NBPARAM, -1.0);

    params[A_V] = 0.8;
    params[k_int] = 4.73E-12;
    params[km_int_IFNb] = 36;
    params[b_V] = 0.00555;
    params[k_V_innate] = 1.00E-11;
    params[f_int] = 1;
    params[mu_AT2] = 0.000267417;
    params[k_ROS_AT2] = 3.20E-07;
    params[km_ROS_AT2] = 50000000000;
    params[k_AT1_AT2] = 0.000106967;
    params[km_AT1_AT2] = 0.9;
    params[k_IFNb_kill] = 1.1;
    params[k_kill] = 1.84E-09;
    params[km_kill] = 500000;
    params[b_AT2] = 0.00016045;
    params[b_I] = 0.015;
    params[mu_AT1] = 0;
    params[k_ROS_AT1] = 1;
    params[km_ROS_AT1] = 1;
    params[b_AT1] = 0.00016045;
    params[b_dAT1] = 0.05;
    params[k_damage_TNFa] = 0.01;
    params[km_damage_TNFa] = 319334.4;
    params[k_damage_IL6] = 0.01;
    params[km_damage_IL6] = 11592;
    params[k_damage_IL1b] = 0.01;
    params[km_damage_IL1b] = 1727404.56;
    params[k_damage_IFNg] = 0.01;
    params[km_damage_IFNg] = 53222.4;
    params[k_damage_cyt] = 0.05;
    params[k_mu_AT2] = 30;
    params[k_diff_AT1] = 30;
    params[a_DC] = 115000000;
    params[kbasal_DC] = 0;
    params[b_DC] = 0.001041667;
    params[k_DC_TNFa] = 10;
    params[km_DC_TNFa] = 3193.344;
    params[k_DC_IFNg] = 10;
    params[km_DC_IFNg] = 1064.448;
    params[k_DC_IL6] = 10;
    params[km_DC_IL6] = 139.104;
    params[km_DC_IL10] = 46.2672;
    params[a_M1] = 400000000;
    params[kbasal_M1] = 0;
    params[k_v] = 0.3;
    params[k_I] = 0.03;
    params[k_dAT] = 0.3;
    params[km_v] = 5E+11;
    params[km_I] = 5000000000;
    params[km_dAT] = 5000000000;
    params[k_M1_IL6] = 10;
    params[km_M1_IL6] = 139.104;
    params[b_M1] = 0.002791667;
    params[k_M1_TNFa] = 10;
    params[km_M1_TNFa] = 3200.6016;
    params[k_M1_GMCSF] = 10;
    params[km_M1_GMCSF] = 567.3024;
    params[k_M1_IFNg] = 10;
    params[km_M1_IFNg] = 800.1504;
    params[km_M1_IL10] = 167.40864;
    params[kbasal_N] = 0;
    params[a_N] = 79354300000;
    params[k_N_IFNg] = 10;
    params[km_N_IFNg] = 53222.4;
    params[k_N_TNFa] = 0.1;
    params[km_N_TNFa] = 5261.76;
    params[k_N_GMCSF] = 1;
    params[km_N_GMCSF] = 725.76;
    params[k_N_IL17c] = 10;
    params[km_N_IL17c] = 2237.76;
    params[b_N] = 0.618003353;
    params[a_Th1] = 0.02;
    params[b_Th1] = 0.019629167;
    params[k_Th1_IL2] = 1.5;
    params[K_Th1_IL12] = 0.501984;
    params[k_Th1_IL12IL2] = 2;
    params[K_Th1_IL12IL2] = 7.52976;
    params[K_Th1_IL10] = 32516.12903;
    params[K_Th1_TGFb] = 1832727.273;
    params[k_Th1_IFNg] = 1;
    params[K_IFNg_Th1] = 931.392;
    params[K_Th1_IL6] = 267610.6195;
    params[k_Th1_Th17] = 0.01;
    params[K_Th1_Th17] = 0.0328608;
    params[k_Th1_Treg] = 0.005;
    params[K_Th1_Treg] = 20.92608;
    params[a_Th17] = 0.0346;
    params[b_Th17] = 0.0216125;
    params[k_Th17_TGFb] = 4;
    params[K_Th17_TGFb] = 638.372448;
    params[K_Th17_IL2] = 7032558.14;
    params[K_Th17_IFNg] = 6833898.305;
    params[K_Th17_IL10] = 1359101.124;
    params[k_Th17_IL6] = 1;
    params[km_Th17_IL6] = 42.336;
    params[k_Th17_IL1b] = 1;
    params[km_Th17_IL1b] = 20728.85472;
    params[a_CTL] = 0.0007215;
    params[b_CTL] = 0.00245;
    params[k_CTL_IL2] = 3;
    params[K_CTL_IL12] = 7.52976;
    params[k_CTL_IL12IL2] = 4;
    params[K_CTL_IL12IL2] = 7.52976;
    params[K_CTL_IL10] = 32516.12903;
    params[K_CTL_TGFb] = 1832727.273;
    params[K_CTL_IL6] = 535221.2389;
    params[k_CTL_IFNg] = 1;
    params[K_CTL_IFNg] = 931.392;
    params[kmax_MHC1] = 1;
    params[km_MHC1_IFNb] = 5;
    params[a_Treg] = 0.02;
    params[b_Treg] = 0.014791667;
    params[k_Treg_IL2] = 1;
    params[K_Treg_IL2] = 1.796256;
    params[K_Treg_IL17] = 491707.3171;
    params[K_Treg_IL6] = 535221.2389;
    params[k_Treg_TGFb] = 1;
    params[K_Treg_TGFb] = 32.256;
    params[kbasal_SPD] = 40;
    params[a_SPD] = 5.00E-07;
    params[b_SPD] = 0.069315;
    params[kbasal_FER] = 0;
    params[a_FER] = 7.00E-06;
    params[b_FER] = 0.040773;
    params[a_FER_c] = 0.0203865;
    params[k_CTL_I_SPD] = 0.1;
    params[k_CTL_I_Fer] = 0.1;
    //params[VmProtSynth] = 6797.166667;
    //params[KmProtSyn] = 0.55;
    params[kbasal_CRP] = 7.816666667;
    params[kCRP_LivertoBlood] = 0.03685025;
    params[kCRP_BloodtoLiver] = 0.037875;
    params[kCRPSecretion] = 0.019791667;
    params[kdeg_CRP] = 0.12;
    params[k_livercrp] = 520;
    params[a_tnf] = 0.012975984;
    params[a_tnf_at1] = 0.00014;
    params[a_tnf_i] = 5.60E-05;
    params[a_tnf_at2] = 0.00014;
    params[a_tnf_m1] = 4.20E-05;
    params[a_tnf_th1] = 1.13E-09;
    params[a_tnf_th17] = 1.13E-09;
    params[b_tnf] = 3.16;
    params[a_il6] = 0.00056448;
    params[b_il6] = 0.044;
    params[a_il6_at1] = 3.12E-06;
    params[a_il6_i] = 1.25E-06;
    params[a_il6_at2] = 3.12E-06;
    params[a_il6_m1] = 1.45E-06;
    params[a_il6_th17] = 1.25E-06;
    params[a_il6_neu] = 1.08E-07;
    params[a_ifng] = 0.126979181;
    params[b_ifng] = 3.59;
    params[a_ifng_dc] = 2.17E-09;
    params[a_ifng_th1] = 6.15E-07;
    params[a_ifng_ctl] = 2.93E-05;
    params[a_ifnb] = 0.031744795;
    params[b_ifnb] = 3.59;
    params[a_ifnb_at1] = 4.00E-05;
    params[a_ifnb_i] = 7.00E-05;
    params[a_ifnb_d] = 0.0001;
    params[a_ifnb_dc] = 7.00E-05;
    params[a_il2_dc] = 5.60E-08;
    params[a_il2_th1] = 2.00E-08;
    params[b_il2] = 1.913;
    params[a_il2] = 25.96608;
    params[a_il12_m1] = 6.18E-08;
    params[a_il12_dc] = 4.98E-08;
    params[b_il12] = 2.73;
    params[a_il12] = 48.26304;
    params[a_il17_th17] = 1.05E-05;
    params[a_il17_ctl] = 3.45E-09;
    params[b_il17] = 0.0825;
    params[a_il17] = 0.12997152;
    params[a_il10_treg] = 1.15E-05;
    params[b_il10] = 1.22;
    params[a_il10] = 0.272538;
    params[a_tgfb_th17] = 7.42E-06;
    params[a_tgfb_treg] = 7.15E-06;
    params[b_tgfb] = 1.9;
    params[a_tgfb] = 2.1506688;
    params[a_gmcsf_m1] = 1.27E-06;
    params[a_gmcsf_th1] = 4.29E-08;
    params[a_gmcsf_th17] = 1.15E-07;
    params[b_gmcsf] = 1;
    params[a_gmcsf] = 15.5232;
    params[a_il1b] = 0.084672;
    params[b_il1b] = 0.088;
    params[a_il1b_at1] = 5.17E-10;
    params[a_il1b_i] = 2.07E-09;
    params[a_il1b_at2] = 5.17E-09;
    params[a_il1b_m1] = 2.07E-08;
    params[a_il1b_dc] = 0;
    params[kbasal_ROS] = 0;
    params[b_ROS] = 0;
    params[basal_tnfa] = 24;
    params[basalil6] = 40;
    params[basalil1] = 5.714285714;
    params[basalifng] = 20;
    params[basalifnb] = 8;
    params[basalil2] = 4;
    params[basalil12] = 1.2;
    params[basalil10] = 10;
    params[basaltgfb] = 1;
    params[basalgmcsf] = 4;
    params[ktr_TNFa] = 0.1;
    params[ktr_IL6] = 0.1;
    params[ktr_IL1b] = 0.1;
    params[ktr_IFNb] = 0.1;
    params[ktr_IFNg] = 0.1;
    params[ktr_IL2] = 0.2;
    params[ktr_IL12] = 0.1;
    params[ktr_IL17] = 0.1;
    params[ktr_IL10] = 0.1;
    params[ktr_TGFb] = 0.1;
    params[ktr_GMCSF] = 0.1;
    params[ktr_SPD] = 0.1;
    params[ktr_FER] = 0.1;
    params[k_Ab_V] = 1;
    params[ktr_pDC] = 1;
    params[ktr_M1] = 1;
    params[ktr_N] = 1;
    params[ktr_Th1] = 1;
    params[ktr_Th17] = 1;
    params[ktr_CTL] = 1;
    params[ktr_Treg] = 1;
    params[tau_Ab] = 3960;
    params[a_Ab] = 1000000;
    params[b_Ab] = 0.00297619;
    params[Liver] = 1660;
    //params[Blood] = 5000;

    params[kdc] = 1;


    // important lung variables
    double n_alveolar = 480e6; // 480 million alveolars per lung on average 274-790 million; coefficient of variation: 37% (https://www.researchgate.net/publication/9078564_The_Number_of_Alveoli_in_the_Human_Lung)
    double size_alveolar = 4.2e6; //um^3, note 1 um^3 = 1e-15 L
    double conversion_um3toL = 1e-15;
    double vol_alv = n_alveolar*size_alveolar*conversion_um3toL;


    params[vol_alv_ml] = vol_alv * 1000;
    params[vol_plasma] = 5000; //% mL

    for(size_t i = 0; i < getNbParams(); ++i){

        if(params[i] < 0) cerr << "ERR: param " << i << ", name: " << paramNames[i] << " not instanciated with default value" << endl;
    }

	setBaseParametersDone();
}

void modelDai2021::initialise(long long _background){ // don't touch to parameters !
	background = _background;
	val.clear();
	val.resize(NBVAR, 0.0);
	init.clear();
    init.resize(NBVAR, -1.0);


    // other variables are derived

//    double pulmonary_epithelial_cells = 5e10; // assumption, range used in other models
//    double AT1_total = 0.40 * pulmonary_epithelial_cells;
//    double AT2_total = 0.60 * pulmonary_epithelial_cells;
//    init[AT1] = AT1_total; //%/vol_alv;
//    init[AT2] = AT2_total; //%/vol_alv;

    // variables from IBD model needed for parameters
//    double iDC = 1e7;
//    double M0 = 1e7;
//    double Th0 = 1e7;
//    double iCD4 = 300; //% #/uL plasma naive T cells
//    double iCD8 = 200; //% #/uL plasma naive T cells
//    double iMono = 500; //% #/uL plasma monocytes
//    double iN = 2000; //% immature N

    init[V] = 0;
    init[AT1] = 2.00E+10;
    init[AT2] = 3.00E+10;
    init[I] = 0;
    init[dAT1] = 250.26;
    init[dAT2] = 375.4;
    init[pDC] = 1416.8;
    init[M1] = 1869.4;
    init[N] = 2455.6;
    init[Th1] = 1683.7;
    init[Th17] = 0.37168;
    init[CTL] = 2082.6;
    init[Treg] = 454.95;
    init[TNFa] = 0.00024335;
    init[IL6] = 0.00014131;
    init[IL1b] = 0.028005;
    init[IFNb] = 0.0028677;
    init[IFNg] = 0.0018392;
    init[IL2] = 0.35115;
    init[IL12] = 1.2184;
    init[IL17] = 1.13E-05;
    init[IL10] = 0.059298;
    init[TGFb] = 0.07706;
    init[GMCSF] = 0.20209;
    init[SPD] = 440.11;
    init[FER] = 0.21898;
    init[TNFa_c] = 1.54E-09;
    init[IL6_c] = 6.42E-08;
    init[IL1b_c] = 1.27E-05;
    init[IFNb_c] = 1.60E-08;
    init[IFNg_c] = 1.02E-08;
    init[IL2_c] = 7.34E-06;
    init[IL12_c] = 8.93E-06;
    init[IL17_c] = 0;
    init[IL10_c] = 9.72E-07;
    init[TGFb_c] = 8.11E-07;
    init[GMCSF_c] = 4.04E-06;
    init[SPD_c] = 0.12699;
    init[FER_c] = 0.50011;
    init[Ab] = 0;
    init[pDC_c] = 0.70096;
    init[M1_c] = 0.92091;
    init[N_c] = 0.48093;
    init[Th1_c] = 0.7964;
    init[Th17_c] = 0.00017499;
    init[CTL_c] = 1.0268;
    init[Treg_c] = 0.21768;
    init[CRPExtracellular] = 14998;
    init[Blood_CRP] = 6.1428;



    for(size_t i = 0; i < NBVAR; ++i){
        val[i] = init[i];
    }
    t = 0;
    updateDerivedVariables(t);

    for(size_t i = 0; i < NBVAR; ++i){
        if(val[i] < 0) cerr << "ERR: initial value for variable " << i << ", name: " << names[i] << " is not instanciated " << endl;
    }

	initialiseDone();
}









// Hill function, not yet used
double Hill(double x, double K){
    if(x <= 0) return 0;
    if(x+K < 1e-12) return 0;
    return(x / (x+K));
}

