// ------- Automatically generated model -------- //
#include "Dai2021.h"


void modelDai2021::derivatives(const vector<double> &x, vector<double> &dxdt, const double _t){


    enum {pDC_c, M1_c, N_c, Th1_c, Th17_c, CTL_c, Treg_c};

      // LUNG TO CENTRAL TRANSPORT RATES
        double tr_TNFa = params[ktr_TNFa]*x[TNFa];
        double tr_IL6 = params[ktr_IL6]*x[IL6];
        double tr_IL1b = params[ktr_IL1b]*x[IL1b];
        double tr_IFNb = params[ktr_IFNb]*x[IFNb];
        double tr_IFNg = params[ktr_IFNg]*x[IFNg];
        double tr_IL2 = params[ktr_IL2]*x[IL2];
        double tr_IL12 = params[ktr_IL12]*x[IL12];
        double tr_IL17 = params[ktr_IL17]*x[IL17];
        double tr_IL10 = params[ktr_IL10]*x[IL10];
        double tr_TGFb = params[ktr_TGFb]*x[TGFb];
        double tr_GMCSF = params[ktr_GMCSF]*x[GMCSF];
        double tr_SPD = params[ktr_SPD]*x[SPD];
        double tr_FER = params[ktr_FER]*x[FER];


        double tr_pDC = params[ktr_pDC]*(x[pDC]/params[vol_alv_ml] -x[pDC_c]);
        double tr_M1 =  params[ktr_M1]*(x[M1]/params[vol_alv_ml] -x[M1_c]);
        double tr_N = params[ktr_N]*(x[N]/params[vol_alv_ml] -x[N_c]);
        double tr_Th1 = params[ktr_Th1]*(x[Th1]/params[vol_alv_ml] -x[Th1_c]);
        double tr_Th17 = params[ktr_Th17]*(x[Th17]/params[vol_alv_ml] -x[Th17_c]);
        double tr_CTL = params[ktr_CTL]*(x[CTL]/params[vol_alv_ml] -x[CTL_c]);
        double tr_Treg = params[ktr_Treg]*(x[Treg]/params[vol_alv_ml] -x[Treg_c]);

        // viral dynamics

        //  Virus (# viral mRNA/mL)
        double prod_virus_shedding = params[A_V]*x[I];
        double virus_endocytosis = params[k_int]*x[AT2]*V*(1-x[IFNb]/(params[km_int_IFNb]+x[IFNb]));
        double innate_clearance = params[k_V_innate]*x[V]*(x[M1]+x[pDC]);
        double deg_virus = params[b_V]*x[V];

        // not in use for now
        double ab_clearance = 0*params[k_Ab_V]*x[Ab]*x[V];


         if(!over(V))    dxdt[V] = prod_virus_shedding - params[f_int]*virus_endocytosis - deg_virus - innate_clearance - ab_clearance;

        // healthy alveolar type 2 cells (AT2) (# cells)
        double ICAT1 = init[AT1];
        double ICAT2 = init[AT2];
        double damage_cyt_AT = 1*params[k_damage_cyt]*(params[k_damage_TNFa]*(x[TNFa]/(params[km_damage_TNFa]+x[TNFa])) +  params[k_damage_IL6]*(x[IL6]/(params[km_damage_IL6]+x[IL6])) + params[k_damage_IL1b]*(x[IL1b]/(params[km_damage_IL1b] + x[IL1b])) + params[k_damage_IFNg]*(x[IFNg]/(params[km_damage_IFNg]+x[IFNg])));
        double damage_ROS_AT2 = params[k_ROS_AT2]*x[N]/(params[km_ROS_AT2]+x[N]);
        double growth_AT2 = params[mu_AT2]*(1 + params[k_mu_AT2]*(max((ICAT1+ICAT2)-(x[AT2]+x[AT1]),0.)/(params[km_AT1_AT2]*(ICAT1+ICAT2) + max((ICAT1+ICAT2)-(x[AT2]+x[AT1]),0.))))*x[AT2]; // induced differentiation and growth induced by AT1 decrease from baseline
        double deg_AT2 = params[b_AT2]*x[AT2];
        double diff_AT2 = params[k_AT1_AT2] * x[AT2] * (1 + params[k_diff_AT1]*max(ICAT1-x[AT1],0.)/(params[km_AT1_AT2]*ICAT1 + max(ICAT1-x[AT1],0.)));
         if(!over(AT2))    dxdt[AT2] = growth_AT2 - virus_endocytosis - damage_ROS_AT2*x[AT2] - deg_AT2 - damage_cyt_AT*x[AT2] - diff_AT2;

        // Infected AT2 (# cells)
        double kill_CTL_I = params[k_kill]*(1+params[k_IFNb_kill]*x[IFNb]/(params[km_kill]+x[IFNb]))*x[I]*x[CTL];
        double deg_I = params[b_I]*x[I];

         if(!over(I))    dxdt[I] = virus_endocytosis - deg_I - kill_CTL_I - params[k_V_innate]*x[I]*(x[M1]+x[pDC]) - damage_ROS_AT2*x[I];

        // health alveolar type 1 cells (AT1) (# cells)
        double growth_AT1 = params[mu_AT1]*(ICAT1-x[AT1]); // no growth, no division, source is from AT2
        double damage_ROS_AT1 = damage_ROS_AT2*x[AT1];
        double deg_AT1 = params[b_AT1]*x[AT1];

        if(!over(AT1))    dxdt[AT1] = growth_AT1 - damage_ROS_AT1 - deg_AT1 - damage_cyt_AT*x[AT1] + diff_AT2 ;

        // damaged AT1 cells (# cells)
        double deg_dAT1 = params[b_dAT1]*x[dAT1];
         if(!over(dAT1))    dxdt[dAT1]= damage_ROS_AT1 - deg_dAT1 + damage_cyt_AT*x[AT1];

        // damaged AT2 cells (# cells)
        double deg_dAT2 = params[b_dAT1]*x[dAT2];
         if(!over(dAT2))    dxdt[dAT2] = damage_ROS_AT2*(x[I]+x[AT2])  - deg_dAT2 + damage_cyt_AT*x[AT2];



        // % % % % % Immune cells. % % % % %

        // lung pulmonary dendritic cells (# cells)
        double act_pDC_TNFa = params[k_DC_TNFa]*(x[TNFa]/(params[km_DC_TNFa]+x[TNFa]));
        double act_pDC_IFNg = params[k_DC_IFNg]*(x[IFNg]/(params[km_DC_IFNg]+x[IFNg]));
        ////////////// Problem, one variable is not read => I addeed it in the formula
        double act_pDC_IL6 = params[k_DC_IL6]*(x[IL6]/(params[km_DC_IL6]+x[IL6]));
        double act_pDC_GMCSF = params[k_M1_GMCSF]*(x[GMCSF]/(params[km_M1_GMCSF]+x[GMCSF]));
        double inh_pDC_IL10 = (params[km_DC_IL10]/(params[km_DC_IL10]+x[IL10]));

         if(!over(pDC))    dxdt[pDC] = params[a_DC]*(params[kbasal_DC]+ params[k_v]*x[V]/(params[km_v] + x[V]) + params[k_I]*x[I]/(params[km_I] + x[I]) + params[k_dAT]*(x[dAT1]+x[dAT2])/(params[km_dAT] + x[dAT1]+x[dAT2]))*(1+act_pDC_TNFa + act_pDC_IFNg + act_pDC_GMCSF + act_pDC_IL6)* inh_pDC_IL10 - params[b_DC]*x[pDC] - tr_pDC*params[vol_alv_ml] ;

        // lung M1 macrophages (# cells)
        double act_M1_TNFa = params[k_M1_TNFa]*(x[TNFa]/(params[km_M1_TNFa]+x[TNFa]));
        double act_M1_GMCSF = params[k_M1_GMCSF]*(x[GMCSF]/(params[km_M1_GMCSF]+x[GMCSF]));
        double act_M1_IFNg = params[k_M1_IFNg]*(x[IFNg]/(params[km_M1_IFNg]+x[IFNg]));
        double inh_M1_IL10 = (params[km_M1_IL10]/(params[km_M1_IL10]+x[IL10]));



         if(!over(M1))    dxdt[M1] = params[a_M1]*(params[kbasal_M1]+ params[k_v]*x[V]/(params[km_v]/1 + x[V]) + params[k_I]*x[I]/(params[km_I] + x[I]) + params[k_dAT]*(x[dAT1]+x[dAT2])/(params[km_dAT] + x[dAT1]+x[dAT2]))*(1+ act_M1_TNFa + act_M1_GMCSF + act_M1_IFNg)*inh_M1_IL10 - params[b_M1]*x[M1] - tr_M1*params[vol_alv_ml] ;

        // lung Neutrophils (# cells)
        double act_N_IFNg = params[k_N_IFNg]*x[IFNg]/(x[IFNg]+params[km_N_IFNg]);
        double act_N_TNFa= params[k_N_TNFa]*x[TNFa]/(x[TNFa]+params[km_N_TNFa]);
        double act_N_GMCSF = params[k_N_GMCSF]*x[GMCSF]/(x[GMCSF]+params[km_N_GMCSF]);
        double rec_N_IL17c = params[k_N_IL17c]*x[IL17_c]/(x[IL17_c] + params[km_N_IL17c]);

         if(!over(N))    dxdt[N] = params[a_N]*(params[kbasal_N]+  params[k_v]*x[V]/(params[km_v]/1 + x[V]) +  params[k_I]*x[I]/(params[km_I] + x[I]) + params[k_dAT]*(x[dAT1]+x[dAT2])/(params[km_dAT] + x[dAT1]+x[dAT2]))*(1+act_N_IFNg + act_N_TNFa + act_N_GMCSF) + rec_N_IL17c - params[b_N]*x[N] - tr_N*params[vol_alv_ml] ;

        // lung Th1 Cells (# cells)
        double act_Th1_IL12 = params[k_Th1_IL2]*(x[IL12]/(params[K_Th1_IL12]+x[IL12]))*(1+params[k_Th1_IL12IL2]*(x[IL2]/(params[K_Th1_IL12IL2]+x[IL2])));
        double act_Th1_IFNg = params[k_Th1_IFNg]*(x[IFNg]/(params[K_IFNg_Th1] + x[IFNg])) * (params[K_Th1_IL6]/(params[K_Th1_IL6]+x[IL6]));
        double inh_Th1_IL10_TGFb = (params[K_Th1_IL10]/(params[K_Th1_IL10]+x[IL10])) * (params[K_Th1_TGFb]/(params[K_Th1_TGFb]+x[TGFb]));
        double diff_Th1_Th17 = params[k_Th1_Th17]*x[Th17]*(x[IL12]/(params[K_Th1_Th17]+x[IL12]))*(params[K_Th1_TGFb]/(params[K_Th1_TGFb]+x[TGFb]));
        double diff_Th1_Treg = params[k_Th1_Treg]*x[Treg]*(x[IL12]/(params[K_Th1_Treg]+x[IL12]));

         if(!over(Th1))    dxdt[Th1] = params[a_Th1]*x[pDC]*(act_Th1_IL12 + act_Th1_IFNg)*inh_Th1_IL10_TGFb + diff_Th1_Th17 + 1*diff_Th1_Treg - params[b_Th1]*x[Th1] - tr_Th1*params[vol_alv_ml];

        // lung TH17 cells (# cells)
        double inh_Th17 = (params[K_Th17_IL2]/(params[K_Th17_IL2] + x[IL2])) * (params[K_Th17_IFNg]/(params[K_Th17_IFNg] + x[IFNg])) * (params[K_Th17_IL10]/(params[K_Th17_IL10] + x[IL10]));
        double act_TH17_TGFb = params[k_Th17_TGFb]*(x[TGFb]/(params[K_Th17_TGFb] + x[TGFb])) * inh_Th17;
        double act_Th17_IL6 = params[k_Th17_IL6]*(x[IL6]/(params[km_Th17_IL6] + x[IL6])) * inh_Th17;
        double act_Th17_IL1b = params[k_Th17_IL1b]*(x[IL1b]/(params[km_Th17_IL1b] + x[IL1b])) * inh_Th17;
         if(!over(Th17))    dxdt[Th17] = params[a_Th17]*pDC*(act_TH17_TGFb + act_Th17_IL6 + act_Th17_IL1b) - diff_Th1_Th17 - params[b_Th17]*Th17 - tr_Th17*params[vol_alv_ml];

        // lung CTL (# cells)
        double act_CTL_IFNb = params[kmax_MHC1]*(x[IFNb]/(params[km_MHC1_IFNb]+x[IFNb]));
        double act_CTL_IL12 = params[k_CTL_IL2]*(x[IL12]/(params[K_CTL_IL12]+x[IL12]))*(1+params[k_CTL_IL12IL2]*(x[IL12]/(params[K_CTL_IL12IL2]+x[IL12]))) * (params[K_CTL_IL10]/(params[K_CTL_IL10]+x[IL10])) * (params[K_CTL_TGFb]/(params[K_CTL_TGFb]+x[TGFb]));
        double act_CTL_IFNg = params[k_CTL_IFNg]*(x[IFNg]/(params[K_CTL_IFNg] + x[IFNg])) * (params[K_CTL_IL10]/(params[K_CTL_IL10]+x[IL10])) * (params[K_CTL_TGFb]/(params[K_CTL_TGFb]+x[TGFb])) * (params[K_CTL_IL6]/(params[K_CTL_IL6]+x[IL6]));

         if(!over(CTL))    dxdt[CTL] = params[a_CTL]*x[pDC]*(1+act_CTL_IFNb)*(1+ act_CTL_IL12 + act_CTL_IFNg)  - params[b_CTL]*CTL - tr_CTL*params[vol_alv_ml];//% + ktr_CTL*CTL_c;

        // lung Treg (# cells)
        double act_Treg_IL2 = params[k_Treg_IL2]*(x[IL2]/(params[K_Treg_IL2] + x[IL2])) * (params[K_Treg_IL17]/(params[K_Treg_IL17] + x[IL17])) * (params[K_Treg_IL6]/(params[K_Treg_IL6] + x[IL6]));
        double act_Treg_TGFb = params[k_Treg_TGFb]*(x[TGFb]/(params[K_Treg_TGFb] + x[TGFb])) * (params[K_Treg_IL17]/(params[K_Treg_IL17] + x[IL17])) * (params[K_Treg_IL6]/(params[K_Treg_IL6] + x[IL6]));

         if(!over(Treg))    dxdt[Treg] = params[a_Treg]*x[pDC]*(act_Treg_IL2 + act_Treg_TGFb) - diff_Th1_Treg - params[b_Treg]*x[Treg] - tr_Treg*params[vol_alv_ml];


        // Biomarkers

        // lung surfactant protein D (pmol)
         if(!over(SPD))    dxdt[SPD] = params[kbasal_SPD] + params[a_SPD] * (x[dAT1] + x[dAT2] + params[k_CTL_I_SPD]*kill_CTL_I) - tr_SPD ;

        // lung Ferritin (pmol)
         if(!over(FER))    dxdt[FER] = params[kbasal_FER] + params[a_FER] * (x[dAT1] + x[dAT2] + params[k_CTL_I_Fer]*kill_CTL_I) - tr_FER;

        // Liver CRP Production (pmol)
        double Liver_CRP = params[k_livercrp]*params[Liver]*((x[IL6_c]*params[Liver]));
        double prod_CRP_liver = params[kCRPSecretion]*Liver_CRP;
        double tr_CRP = params[kCRP_BloodtoLiver]*x[Blood_CRP]-params[kCRP_LivertoBlood]*Liver_CRP/params[Liver];
        double prod_CRP_blood = params[kbasal_CRP];
         if(!over(CRPExtracellular))    dxdt[CRPExtracellular] = prod_CRP_liver + tr_CRP*params[Liver] - params[kdeg_CRP]*x[CRPExtracellular];
         if(!over(Blood_CRP))    dxdt[Blood_CRP] = (-tr_CRP + prod_CRP_blood - params[kdeg_CRP]*x[Blood_CRP]);


        // Cytokine Dynamics

        // TNF-A (pmol)
        double prod_tnf_dat1 = params[a_tnf_at1]*x[dAT1];
        double prod_tnf_i = params[a_tnf_i]*x[I];
        double prod_tnf_dat2 = params[a_tnf_at2]*x[dAT2];
        double prod_tnf_m1 = params[a_tnf_m1]*x[M1];
        double prod_tnf_th1 = params[a_tnf_th1]*x[Th1];
        double prod_tnf_th17 = params[a_tnf_th17]*x[Th17];
        double deg_tnf = params[b_tnf]*x[TNFa];


         if(!over(TNFa))    dxdt[TNFa] = params[a_tnf]*(params[basal_tnfa] + prod_tnf_i + prod_tnf_dat2 + prod_tnf_dat1 + prod_tnf_m1 + prod_tnf_th1 + prod_tnf_th17) - deg_tnf - tr_TNFa;


        // IL-6 (pmol)
        double prod_il6_dat1 = params[a_il6_at1]*x[dAT1];
        double prod_il6_i = params[a_il6_i]*x[I];
        double prod_il6_dat2 = params[a_il6_at2]*x[dAT2];
        double prod_il6_m1 = params[a_il6_m1]*x[M1];
        double prod_il6_th17 = params[a_il6_th17]*x[Th17];
        double prod_il6_neu =params[a_il6_neu]*x[N];
        double deg_il6 = params[b_il6]*x[IL6];

         if(!over(IL6))    dxdt[IL6] = params[a_il6]*(params[basalil6] + prod_il6_dat1 + prod_il6_i + prod_il6_dat2 + prod_il6_m1 + prod_il6_th17 + prod_il6_neu) - deg_il6 - tr_IL6;


        // IL1-B (pmol)
        double prod_il1b_dat1 = params[a_il1b_at2]*x[dAT1];
        double prod_il1b_i = params[a_il1b_i]*x[I];
        double prod_il1b_dat2 = params[a_il1b_at2]*x[dAT2];
        double prod_il1b_m1 = params[a_il1b_m1]*x[M1];
        double a_il1b_dc = params[a_il1b_m1];
        double prod_il1b_dc = params[a_il1b_dc]*x[pDC];
        double deg_il1b = params[b_il1b]*x[IL1b];

         if(!over(IL1b))    dxdt[IL1b] = params[a_il1b]*(params[basalil1] + prod_il1b_dat1 + prod_il1b_i + prod_il1b_dat2 + prod_il1b_m1 + prod_il1b_dc) - deg_il1b - tr_IL1b;

        // IFN-G (pmol)
        double prod_ifng_dc = params[a_ifng_dc]*x[pDC];
        double prod_ifng_th1 = params[a_ifng_th1]*x[Th1];
        double prod_ifng_ctl = params[a_ifng_ctl]*x[CTL];
        double del_ifng = params[b_ifng]*x[IFNg];

         if(!over(IFNg))    dxdt[IFNg] = params[a_ifng]*(params[basalifng] + prod_ifng_dc + prod_ifng_th1 + prod_ifng_ctl) - del_ifng - tr_IFNg;

        // IFN-B (pmol)
        double prod_ifnb_i = params[a_ifnb_i]*x[I];
        double prod_ifnb_dc = params[a_ifnb_dc]*x[pDC];
        double del_ifnb = params[b_ifnb]*x[IFNb];

         if(!over(IFNb))    dxdt[IFNb] = params[a_ifnb]*(params[basalifnb] + prod_ifnb_i + prod_ifnb_dc) - del_ifnb - tr_IFNb;

        // IL-2 (pmol)
        double prod_il2_dc = params[a_il2_dc]*x[pDC];
        double prod_il2_th1 = params[a_il2_th1]*x[Th1];
        double deg_il2 = params[b_il2]*x[IL2];

         if(!over(IL2))    dxdt[IL2] = params[a_il2]*(params[basalil2] + prod_il2_dc + prod_il2_th1) - deg_il2 - tr_IL2;

        // IL-12 (pmol)
        double prod_il12_m1 = params[a_il12_m1]*x[M1];
        double prod_il12_dc = params[a_il12_dc]*x[pDC];
        double deg_il12 = params[b_il12]*x[IL12];

         if(!over(IL12))    dxdt[IL12] = params[a_il12]*(params[basalil12] + prod_il12_m1 + prod_il12_dc) - deg_il12 - tr_IL12;

        // IL-17 (pmol)
        double prod_il17_th17 = params[a_il17_th17]*x[Th17];
        double prod_il17_ctl = params[a_il17_ctl]*x[CTL];
        double deg_il17 = params[b_il17]*x[IL17];

         if(!over(IL17))    dxdt[IL17] = params[a_il17]*(prod_il17_th17 + prod_il17_ctl) - deg_il17 - tr_IL17;

        // IL-10 (pmol)
        double prod_il10_treg = params[a_il10_treg]*x[Treg];
        double deg_il10 = params[b_il10]*x[IL10];

         if(!over(IL10))    dxdt[IL10] = params[a_il10]*(params[basalil10]+ prod_il10_treg) - deg_il10 - tr_IL10;

        // TGF-B (pmol)
        double prod_tgfb_th17 = params[a_tgfb_th17]*x[Th17];
        double prod_tgfb_treg = params[a_tgfb_treg]*x[Treg];
        double deg_tgfb = params[b_tgfb]*x[TGFb];

         if(!over(TGFb))    dxdt[TGFb] = params[a_tgfb]*(params[basaltgfb] + prod_tgfb_th17 + prod_tgfb_treg) - deg_tgfb - tr_TGFb;

        // GM-CSF (pmol)
        double prod_gmcsf_m1 = params[a_gmcsf_m1]*x[M1];
        double prod_gmcsf_th1 = params[a_gmcsf_th1]*x[Th1];
        double prod_gmcsf_th17 = params[a_gmcsf_th17]*x[Th17];
        double deg_gmcsf = params[b_gmcsf]*x[GMCSF];

         if(!over(GMCSF))    dxdt[GMCSF] = params[a_gmcsf]*(params[basalgmcsf] + prod_gmcsf_m1 + prod_gmcsf_th1 + prod_gmcsf_th17) - deg_gmcsf - tr_GMCSF;

        // neutralizing antibody Don't get, this equation works at any time, no need to have the time constraint
        dxdt[Ab] = 0;
        if(_t>=params[tau_Ab]){
            if(!over(Ab))    dxdt[Ab] = params[a_Ab] - x[Ab]*params[b_Ab] - ab_clearance;
        }



        // Central compartment cytokines (pmol/mL)
         if(!over(TNFa_c))    dxdt[TNFa_c] = tr_TNFa/vol_plasma - params[b_tnf]*x[TNFa_c];
         if(!over(IL6_c))    dxdt[IL6_c] = tr_IL6/vol_plasma - params[b_il6]*x[IL6_c];
         if(!over(IL1b_c))    dxdt[IL1b_c] = tr_IL1b/vol_plasma - params[b_il1b]*x[IL1b_c];
         if(!over(IFNb_c))    dxdt[IFNb_c] = tr_IFNb/vol_plasma - params[b_ifnb]*x[IFNb_c];
         if(!over(IFNg_c))    dxdt[IFNg_c] = tr_IFNg/vol_plasma - params[b_ifng]*x[IFNg_c];
         if(!over(IL2_c))    dxdt[IL2_c] = tr_IL2/vol_plasma - params[b_il2]*x[IL2_c];
         if(!over(IL12_c))    dxdt[IL12_c] = tr_IL12/vol_plasma - params[b_il12]*x[IL12_c];
         if(!over(IL17_c))    dxdt[IL17_c] = tr_IL17/vol_plasma - params[b_il17]*x[IL17_c];
         if(!over(IL10_c))    dxdt[IL10_c] = tr_IL10/vol_plasma - params[b_il10]*x[IL10_c];
         if(!over(TGFb_c))    dxdt[TGFb_c] = tr_TGFb/vol_plasma - params[b_tgfb]*x[TGFb_c];
         if(!over(GMCSF_c))    dxdt[GMCSF_c] = tr_GMCSF/vol_plasma - params[b_gmcsf]*x[GMCSF_c];
         if(!over(SPD_c))    dxdt[SPD_c] = tr_SPD/vol_plasma - params[b_SPD]*x[SPD_c];
         if(!over(FER_c))    dxdt[FER_c] = params[a_FER_c] + tr_FER/vol_plasma - params[b_FER]*x[FER_c];

        // Central compartment cells (# cells/uL)
         if(!over(pDC_c))    dxdt[pDC_c] =  tr_pDC*params[vol_alv_ml]/vol_plasma - x[pDC_c]*params[b_DC];
         if(!over(M1_c))    dxdt[M1_c] =  tr_M1*params[vol_alv_ml]/vol_plasma - x[M1_c]*params[b_M1];
         if(!over(N_c))    dxdt[N_c] = tr_N*params[vol_alv_ml]/vol_plasma -  x[N_c]*params[b_N];
         if(!over(Th1_c))    dxdt[Th1_c] =  tr_Th1*params[vol_alv_ml]/vol_plasma - x[Th1_c]*params[b_Th1];
         if(!over(Th17_c))    dxdt[Th17_c] =  tr_Th17*params[vol_alv_ml]/vol_plasma - x[Th17_c]*params[b_Th17];
         if(!over(CTL_c))    dxdt[CTL_c] =  tr_CTL*params[vol_alv_ml]/vol_plasma - x[CTL_c]*params[b_CTL];
         if(!over(Treg_c))    dxdt[Treg_c] =  tr_Treg*params[vol_alv_ml]/vol_plasma - x[Treg_c]*params[b_Treg];

}

void modelDai2021::updateDerivedVariables(double _t){

}

modelDai2021::modelDai2021() : Model(NBVAR, NBPARAM), background(0) {
    name = string("Erwin 2017");    // name of this particular model
    dt = 0.001;   print_every_dt = 0.1;              // initial values for dt of simulation and for following the kinetics. Can be modified by user later.

    names[V] = "V";
    names[AT1] = "AT1";
    names[AT2] = "AT2";
    names[I] = "I";
    names[dAT1] = "dAT1";
    names[dAT2] = "dAT2";
    names[pDC] = "pDC";
    names[M1] = "M1";
    names[N] = "N";
    names[Th1] = "Th1";
    names[Th17] = "Th17";
    names[CTL] = "CTL";
    names[Treg] = "Treg";
    names[TNFa] = "TNFa";
    names[IL6] = "IL6";
    names[IL1b] = "IL1b";
    names[IFNb] = "IFNb";
    names[IFNg] = "IFNg";
    names[IL2] = "IL2";
    names[IL12] = "IL12";
    names[IL17] = "IL17";
    names[IL10] = "IL10";
    names[TGFb] = "TGFb";
    names[GMCSF] = "GMCSF";
    names[SPD] = "SPD";
    names[FER] = "FER";
    names[TNFa_c] = "TNFa_c";
    names[IL6_c] = "IL6_c";
    names[IL1b_c] = "IL1b_c";
    names[IFNb_c] = "IFNb_c";
    names[IFNg_c] = "IFNg_c";
    names[IL2_c] = "IL2_c";
    names[IL12_c] = "IL12_c";
    names[IL17_c] = "IL17_c";
    names[IL10_c] = "IL10_c";
    names[TGFb_c] = "TGFb_c";
    names[GMCSF_c] = "GMCSF_c";
    names[SPD_c] = "SPD_c";
    names[FER_c] = "FER_c";
    names[Ab] = "Ab";
    names[CRPExtracellular] = "CRPExtracellular";
    names[Blood_CRP] = "Blood_CRP";
    names[pDC_c] = "pDC_c";
    names[M1_c] = "M1_c";
    names[N_c] = "N_c";
    names[Th1_c] = "Th1_c";
    names[Th17_c] = "Th17_c";
    names[CTL_c] = "CTL_c";
    names[Treg_c] = "Treg_c";


    extNames[V] = "V";
    extNames[AT1] = "AT1";
    extNames[AT2] = "AT2";
    extNames[I] = "I";
    extNames[dAT1] = "dAT1";
    extNames[dAT2] = "dAT2";
    extNames[pDC] = "pDC";
    extNames[M1] = "M1";
    extNames[N] = "N";
    extNames[Th1] = "Th1";
    extNames[Th17] = "Th17";
    extNames[CTL] = "CTL";
    extNames[Treg] = "Treg";
    extNames[TNFa] = "TNFa";
    extNames[IL6] = "IL6";
    extNames[IL1b] = "IL1b";
    extNames[IFNb] = "IFNb";
    extNames[IFNg] = "IFNg";
    extNames[IL2] = "IL2";
    extNames[IL12] = "IL12";
    extNames[IL17] = "IL17";
    extNames[IL10] = "IL10";
    extNames[TGFb] = "TGFb";
    extNames[GMCSF] = "GMCSF";
    extNames[SPD] = "SPD";
    extNames[FER] = "FER";
    extNames[TNFa_c] = "TNFa_c";
    extNames[IL6_c] = "IL6_c";
    extNames[IL1b_c] = "IL1b_c";
    extNames[IFNb_c] = "IFNb_c";
    extNames[IFNg_c] = "IFNg_c";
    extNames[IL2_c] = "IL2_c";
    extNames[IL12_c] = "IL12_c";
    extNames[IL17_c] = "IL17_c";
    extNames[IL10_c] = "IL10_c";
    extNames[TGFb_c] = "TGFb_c";
    extNames[GMCSF_c] = "GMCSF_c";
    extNames[SPD_c] = "SPD_c";
    extNames[FER_c] = "FER_c";
    extNames[Ab] = "Ab";
    extNames[CRPExtracellular] = "CRPExtracellular";
    extNames[Blood_CRP] = "Blood_CRP";
    extNames[pDC_c] = "pDC_c";
    extNames[M1_c] = "M1_c";
    extNames[N_c] = "N_c";
    extNames[Th1_c] = "Th1_c";
    extNames[Th17_c] = "Th17_c";
    extNames[CTL_c] = "CTL_c";
    extNames[Treg_c] = "Treg_c";




    // associates the indices of the variables inside the model with the 'official' names/index of variables to be accessed by outside.
    // so, when the model have a different number of variables, they are always accessed with the same 'official' name (ex : 'N::IL2'), from outside, even if they actually have a different indice/order inside the model ('IL2')
    paramNames[A_V] = "A_V";
    paramNames[k_int] = "k_int";
    paramNames[km_int_IFNb] = "km_int_IFNb";
    paramNames[k_V_innate] = "k_V_innate";
    paramNames[b_V] = "b_V";
    paramNames[f_int] = "f_int";
    paramNames[mu_AT2] = "mu_AT2";
    paramNames[k_ROS_AT2] = "k_ROS_AT2";
    paramNames[km_ROS_AT2] = "km_ROS_AT2";
    paramNames[k_AT1_AT2] = "k_AT1_AT2";
    paramNames[km_AT1_AT2] = "km_AT1_AT2";
    paramNames[k_IFNb_kill] = "k_IFNb_kill";
    paramNames[k_kill] = "k_kill";
    paramNames[km_kill] = "km_kill";
    paramNames[km_ROS_AT2] = "km_ROS_AT2";
    paramNames[b_AT2] = "b_AT2";
    paramNames[b_I] = "b_I";
    paramNames[mu_AT1] = "mu_AT1";
    paramNames[k_ROS_AT1] = "k_ROS_AT1";
    paramNames[km_ROS_AT1] = "km_ROS_AT1";
    paramNames[b_AT1] = "b_AT1";
    paramNames[b_dAT1] = "b_dAT1";
    paramNames[k_damage_TNFa] = "k_damage_TNFa";
    paramNames[km_damage_TNFa] = "km_damage_TNFa";
    paramNames[k_damage_IL6] = "k_damage_IL6";
    paramNames[km_damage_IL6] = "km_damage_IL6";
    paramNames[k_damage_IL1b] = "k_damage_IL1b";
    paramNames[km_damage_IL1b] = "km_damage_IL1b";
    paramNames[k_damage_IFNg] = "k_damage_IFNg";
    paramNames[km_damage_IFNg] = "km_damage_IFNg";
    paramNames[k_damage_cyt] = "k_damage_cyt";
    paramNames[a_DC] = "a_DC";
    paramNames[kbasal_DC] = "kbasal_DC";
    paramNames[b_DC] = "b_DC";
    paramNames[k_DC_TNFa] = "k_DC_TNFa";
    paramNames[km_DC_TNFa] = "km_DC_TNFa";
    paramNames[k_DC_IFNg] = "k_DC_IFNg";
    paramNames[km_DC_IFNg] = "km_DC_IFNg";
    paramNames[k_DC_IL6] = "k_DC_IL6";
    paramNames[km_DC_IL6] = "km_DC_IL6";
    paramNames[km_DC_IL10] = "km_DC_IL10";
    paramNames[a_M1] = "a_M1";
    paramNames[kbasal_M1] = "kbasal_M1";
    paramNames[k_v] = "k_v";
    paramNames[k_I] = "k_I";
    paramNames[km_v] = "km_v";
    paramNames[km_I] = "km_I";
    paramNames[k_M1_IL6] = "k_M1_IL6";
    paramNames[km_M1_IL6] = "km_M1_IL6";
    paramNames[b_M1] = "b_M1";
    paramNames[k_M1_TNFa] = "k_M1_TNFa";
    paramNames[km_M1_TNFa] = "km_M1_TNFa";
    paramNames[k_M1_GMCSF] = "k_M1_GMCSF";
    paramNames[km_M1_GMCSF] = "km_M1_GMCSF";
    paramNames[k_M1_IFNg] = "k_M1_IFNg";
    paramNames[km_M1_IFNg] = "km_M1_IFNg";
    paramNames[km_M1_IL10] = "km_M1_IL10";
    paramNames[a_N] = "a_N";
    paramNames[k_N_IFNg] = "k_N_IFNg";
    paramNames[km_N_IFNg] = "km_N_IFNg";
    paramNames[k_N_TNFa] = "k_N_TNFa";
    paramNames[km_N_TNFa] = "km_N_TNFa";
    paramNames[k_N_GMCSF] = "k_N_GMCSF";
    paramNames[km_N_GMCSF] = "km_N_GMCSF";
    paramNames[k_N_IL17c] = "k_N_IL17c";
    paramNames[km_N_IL17c] = "km_N_IL17c";
    paramNames[b_N] = "b_N";
    paramNames[a_Th1] = "a_Th1";
    paramNames[b_Th1] = "b_Th1";
    paramNames[k_Th1_IL2] = "k_Th1_IL2";
    paramNames[K_Th1_IL12] = "K_Th1_IL12";
    paramNames[k_Th1_IL12IL2] = "k_Th1_IL12IL2";
    paramNames[K_Th1_IL12IL2] = "K_Th1_IL12IL2";
    paramNames[K_Th1_IL10] = "K_Th1_IL10";
    paramNames[K_Th1_TGFb] = "K_Th1_TGFb";
    paramNames[k_Th1_IFNg] = "k_Th1_IFNg";
    paramNames[K_IFNg_Th1] = "K_IFNg_Th1";
    paramNames[K_Th1_IL6] = "K_Th1_IL6";
    paramNames[k_Th1_Th17] = "k_Th1_Th17";
    paramNames[K_Th1_Th17] = "K_Th1_Th17";
    paramNames[k_Th1_Treg] = "k_Th1_Treg";
    paramNames[K_Th1_Treg] = "K_Th1_Treg";
    paramNames[a_Th17] = "a_Th17";
    paramNames[b_Th17] = "b_Th17";
    paramNames[k_Th17_TGFb] = "k_Th17_TGFb";
    paramNames[K_Th17_TGFb] = "K_Th17_TGFb";
    paramNames[K_Th17_IL2] = "K_Th17_IL2";
    paramNames[K_Th17_IFNg] = "K_Th17_IFNg";
    paramNames[K_Th17_IL10] = "K_Th17_IL10";
    paramNames[k_Th17_IL6] = "k_Th17_IL6";
    paramNames[km_Th17_IL6] = "km_Th17_IL6";
    paramNames[k_Th17_IL1b] = "k_Th17_IL1b";
    paramNames[km_Th17_IL1b] = "km_Th17_IL1b";
    paramNames[a_CTL] = "a_CTL";
    paramNames[b_CTL] = "b_CTL";
    paramNames[k_CTL_IL2] = "k_CTL_IL2";
    paramNames[K_CTL_IL12] = "K_CTL_IL12";
    paramNames[k_CTL_IL12IL2] = "k_CTL_IL12IL2";
    paramNames[K_CTL_IL12IL2] = "K_CTL_IL12IL2";
    paramNames[K_CTL_IL10] = "K_CTL_IL10";
    paramNames[K_CTL_TGFb] = "K_CTL_TGFb";
    paramNames[K_CTL_IL6] = "K_CTL_IL6";
    paramNames[k_CTL_IFNg] = "k_CTL_IFNg";
    paramNames[K_CTL_IFNg] = "K_CTL_IFNg";
    paramNames[kmax_MHC1] = "kmax_MHC1";
    paramNames[km_MHC1_IFNb] = "km_MHC1_IFNb";
    paramNames[a_Treg] = "a_Treg";
    paramNames[b_Treg] = "b_Treg";
    paramNames[k_Treg_IL2] = "k_Treg_IL2";
    paramNames[K_Treg_IL2] = "K_Treg_IL2";
    paramNames[K_Treg_IL17] = "K_Treg_IL17";
    paramNames[K_Treg_IL6] = "K_Treg_IL6";
    paramNames[k_Treg_TGFb] = "k_Treg_TGFb";
    paramNames[K_Treg_TGFb] = "K_Treg_TGFb";
    paramNames[kbasal_SPD] = "kbasal_SPD";
    paramNames[a_SPD] = "a_SPD";
    paramNames[b_SPD] = "b_SPD";
    paramNames[kbasal_FER] = "kbasal_FER";
    paramNames[a_FER] = "a_FER";
    paramNames[b_FER] = "b_FER";
    paramNames[a_tnf] = "a_tnf";
    paramNames[a_tnf_at1] = "a_tnf_at1";
    paramNames[a_tnf_i] = "a_tnf_i";
    paramNames[a_tnf_at2] = "a_tnf_at2";
    paramNames[a_tnf_m1] = "a_tnf_m1";
    paramNames[a_tnf_th1] = "a_tnf_th1";
    paramNames[a_tnf_th17] = "a_tnf_th17";
    paramNames[b_tnf] = "b_tnf";
    paramNames[a_il6] = "a_il6";
    paramNames[b_il6] = "b_il6";
    paramNames[a_il6_at1] = "a_il6_at1";
    paramNames[a_il6_i] = "a_il6_i";
    paramNames[a_il6_at2] = "a_il6_at2";
    paramNames[a_il6_m1] = "a_il6_m1";
    paramNames[a_il6_th17] = "a_il6_th17";
    paramNames[a_il6_neu] = "a_il6_neu";
    paramNames[a_ifng] = "a_ifng";
    paramNames[b_ifng] = "b_ifng";
    paramNames[a_ifng_dc] = "a_ifng_dc";
    paramNames[a_ifng_th1] = "a_ifng_th1";
    paramNames[a_ifng_ctl] = "a_ifng_ctl";
    paramNames[a_ifnb] = "a_ifnb";
    paramNames[b_ifnb] = "b_ifnb";
    paramNames[a_ifnb_at1] = "a_ifnb_at1";
    paramNames[a_ifnb_i] = "a_ifnb_i";
    paramNames[a_ifnb_d] = "a_ifnb_d";
    paramNames[a_ifnb_dc] = "a_ifnb_dc";
    paramNames[a_il2_dc] = "a_il2_dc";
    paramNames[a_il2_th1] = "a_il2_th1";
    paramNames[b_il2] = "b_il2";
    paramNames[a_il2] = "a_il2";
    paramNames[a_il12_m1] = "a_il12_m1";
    paramNames[a_il12_dc] = "a_il12_dc";
    paramNames[b_il12] = "b_il12";
    paramNames[a_il12] = "a_il12";
    paramNames[a_il17_th17] = "a_il17_th17";
    paramNames[a_il17_ctl] = "a_il17_ctl";
    paramNames[b_il17] = "b_il17";
    paramNames[a_il17] = "a_il17";
    paramNames[a_il10_treg] = "a_il10_treg";
    paramNames[b_il10] = "b_il10";
    paramNames[a_il10] = "a_il10";
    paramNames[a_tgfb_th17] = "a_tgfb_th17";
    paramNames[a_tgfb_treg] = "a_tgfb_treg";
    paramNames[b_tgfb] = "b_tgfb";
    paramNames[a_tgfb] = "a_tgfb";
    paramNames[a_gmcsf_m1] = "a_gmcsf_m1";
    paramNames[a_gmcsf_th1] = "a_gmcsf_th1";
    paramNames[a_gmcsf_th17] = "a_gmcsf_th17";
    paramNames[b_gmcsf] = "b_gmcsf";
    paramNames[a_gmcsf] = "a_gmcsf";
    paramNames[a_il1b] = "a_il1b";
    paramNames[b_il1b] = "b_il1b";
    paramNames[a_il1b_at1] = "a_il1b_at1";
    paramNames[a_il1b_i] = "a_il1b_i";
    paramNames[a_il1b_at2] = "a_il1b_at2";
    paramNames[a_il1b_m1] = "a_il1b_m1";
    paramNames[a_il1b_dc] = "a_il1b_dc";
    paramNames[kbasal_ROS] = "kbasal_ROS";
    paramNames[b_ROS] = "b_ROS";
    paramNames[ktr_TNFa] = "ktr_TNFa";
    paramNames[ktr_IL6] = "ktr_IL6";
    paramNames[ktr_IL1b] = "ktr_IL1b";
    paramNames[ktr_IFNb] = "ktr_IFNb";
    paramNames[ktr_IFNg] = "ktr_IFNg";
    paramNames[ktr_IL2] = "ktr_IL2";
    paramNames[ktr_IL12] = "ktr_IL12";
    paramNames[ktr_IL17] = "ktr_IL17";
    paramNames[ktr_IL10] = "ktr_IL10";
    paramNames[ktr_TGFb] = "ktr_TGFb";
    paramNames[ktr_GMCSF] = "ktr_GMCSF";
    paramNames[ktr_SPD] = "ktr_SPD";
    paramNames[ktr_FER] = "ktr_FER";
    paramNames[k_CTL_I_SPD] = "k_CTL_I_SPD";
    paramNames[k_CTL_I_Fer] = "k_CTL_I_Fer";
    paramNames[kdc] = "kdc";
    paramNames[ktr_pDC] = "ktr_pDC";
    paramNames[ktr_M1] = "ktr_M1";
    paramNames[ktr_N] = "ktr_N";
    paramNames[ktr_Th1] = "ktr_Th1";
    paramNames[ktr_Th17] = "ktr_Th17";
    paramNames[ktr_CTL] = "ktr_CTL";
    paramNames[ktr_Treg] = "ktr_Treg";
    paramNames[k_Ab_V] = "k_Ab_V";
    paramNames[k_mu_AT2] = "k_mu_AT2";
    paramNames[k_diff_AT1] = "k_diff_AT1";
    paramNames[k_dAT] = "k_dAT";
    paramNames[km_dAT] = "km_dAT";
    paramNames[kbasal_N] = "kbasal_N";
    paramNames[k_livercrp] = "k_livercrp";
    paramNames[Liver] = "Liver";
    paramNames[kCRPSecretion] = "kCRPSecretion";
    paramNames[kCRP_BloodtoLiver] = "kCRP_BloodtoLiver";
    paramNames[kCRP_LivertoBlood] = "kCRP_LivertoBlood";
    paramNames[kbasal_CRP] = "kbasal_CRP";
    paramNames[kdeg_CRP] = "kdeg_CRP";
    paramNames[basal_tnfa] = "basal_tnfa";
    paramNames[basalil6] = "basalil6";
    paramNames[basalil1] = "basalil1";
    paramNames[basalifng] = "basalifng";
    paramNames[basalifnb] = "basalifnb";
    paramNames[basalil2] = "basalil2";
    paramNames[basalil12] = "basalil12";
    paramNames[basalil10] = "basalil10";
    paramNames[basaltgfb] = "basaltgfb";
    paramNames[basalgmcsf] = "basalgmcsf";
    paramNames[tau_Ab] = "tau_Ab";
    paramNames[a_Ab] = "a_Ab";
    paramNames[b_Ab] = "b_Ab";
    paramNames[a_FER_c] = "a_FER_c";
    paramNames[vol_plasma] = "vol_plasma";
    paramNames[vol_alv_ml] = "vol_alv_ml";




    paramLowBounds[A_V] = 0;		paramUpBounds[A_V] = 0;
    paramLowBounds[k_int] = 0;		paramUpBounds[k_int] = 0;
    paramLowBounds[km_int_IFNb] = 0;		paramUpBounds[km_int_IFNb] = 0;
    paramLowBounds[k_V_innate] = 0;		paramUpBounds[k_V_innate] = 0;
    paramLowBounds[b_V] = 0;		paramUpBounds[b_V] = 0;
    paramLowBounds[f_int] = 0;		paramUpBounds[f_int] = 0;
    paramLowBounds[mu_AT2] = 0;		paramUpBounds[mu_AT2] = 0;
    paramLowBounds[k_ROS_AT2] = 0;		paramUpBounds[k_ROS_AT2] = 0;
    paramLowBounds[km_ROS_AT2] = 0;		paramUpBounds[km_ROS_AT2] = 0;
    paramLowBounds[k_AT1_AT2] = 0;		paramUpBounds[k_AT1_AT2] = 0;
    paramLowBounds[km_AT1_AT2] = 0;		paramUpBounds[km_AT1_AT2] = 0;
    paramLowBounds[k_IFNb_kill] = 0;		paramUpBounds[k_IFNb_kill] = 0;
    paramLowBounds[k_kill] = 0;		paramUpBounds[k_kill] = 0;
    paramLowBounds[km_kill] = 0;		paramUpBounds[km_kill] = 0;
    paramLowBounds[km_ROS_AT2] = 0;		paramUpBounds[km_ROS_AT2] = 0;
    paramLowBounds[b_AT2] = 0;		paramUpBounds[b_AT2] = 0;
    paramLowBounds[b_I] = 0;		paramUpBounds[b_I] = 0;
    paramLowBounds[mu_AT1] = 0;		paramUpBounds[mu_AT1] = 0;
    paramLowBounds[k_ROS_AT1] = 0;		paramUpBounds[k_ROS_AT1] = 0;
    paramLowBounds[km_ROS_AT1] = 0;		paramUpBounds[km_ROS_AT1] = 0;
    paramLowBounds[b_AT1] = 0;		paramUpBounds[b_AT1] = 0;
    paramLowBounds[b_dAT1] = 0;		paramUpBounds[b_dAT1] = 0;
    paramLowBounds[k_damage_TNFa] = 0;		paramUpBounds[k_damage_TNFa] = 0;
    paramLowBounds[km_damage_TNFa] = 0;		paramUpBounds[km_damage_TNFa] = 0;
    paramLowBounds[k_damage_IL6] = 0;		paramUpBounds[k_damage_IL6] = 0;
    paramLowBounds[km_damage_IL6] = 0;		paramUpBounds[km_damage_IL6] = 0;
    paramLowBounds[k_damage_IL1b] = 0;		paramUpBounds[k_damage_IL1b] = 0;
    paramLowBounds[km_damage_IL1b] = 0;		paramUpBounds[km_damage_IL1b] = 0;
    paramLowBounds[k_damage_IFNg] = 0;		paramUpBounds[k_damage_IFNg] = 0;
    paramLowBounds[km_damage_IFNg] = 0;		paramUpBounds[km_damage_IFNg] = 0;
    paramLowBounds[k_damage_cyt] = 0;		paramUpBounds[k_damage_cyt] = 0;
    paramLowBounds[a_DC] = 0;		paramUpBounds[a_DC] = 0;
    paramLowBounds[kbasal_DC] = 0;		paramUpBounds[kbasal_DC] = 0;
    paramLowBounds[b_DC] = 0;		paramUpBounds[b_DC] = 0;
    paramLowBounds[k_DC_TNFa] = 0;		paramUpBounds[k_DC_TNFa] = 0;
    paramLowBounds[km_DC_TNFa] = 0;		paramUpBounds[km_DC_TNFa] = 0;
    paramLowBounds[k_DC_IFNg] = 0;		paramUpBounds[k_DC_IFNg] = 0;
    paramLowBounds[km_DC_IFNg] = 0;		paramUpBounds[km_DC_IFNg] = 0;
    paramLowBounds[k_DC_IL6] = 0;		paramUpBounds[k_DC_IL6] = 0;
    paramLowBounds[km_DC_IL6] = 0;		paramUpBounds[km_DC_IL6] = 0;
    paramLowBounds[km_DC_IL10] = 0;		paramUpBounds[km_DC_IL10] = 0;
    paramLowBounds[a_M1] = 0;		paramUpBounds[a_M1] = 0;
    paramLowBounds[kbasal_M1] = 0;		paramUpBounds[kbasal_M1] = 0;
    paramLowBounds[k_v] = 0;		paramUpBounds[k_v] = 0;
    paramLowBounds[k_I] = 0;		paramUpBounds[k_I] = 0;
    paramLowBounds[km_v] = 0;		paramUpBounds[km_v] = 0;
    paramLowBounds[km_I] = 0;		paramUpBounds[km_I] = 0;
    paramLowBounds[k_M1_IL6] = 0;		paramUpBounds[k_M1_IL6] = 0;
    paramLowBounds[km_M1_IL6] = 0;		paramUpBounds[km_M1_IL6] = 0;
    paramLowBounds[b_M1] = 0;		paramUpBounds[b_M1] = 0;
    paramLowBounds[k_M1_TNFa] = 0;		paramUpBounds[k_M1_TNFa] = 0;
    paramLowBounds[km_M1_TNFa] = 0;		paramUpBounds[km_M1_TNFa] = 0;
    paramLowBounds[k_M1_GMCSF] = 0;		paramUpBounds[k_M1_GMCSF] = 0;
    paramLowBounds[km_M1_GMCSF] = 0;		paramUpBounds[km_M1_GMCSF] = 0;
    paramLowBounds[k_M1_IFNg] = 0;		paramUpBounds[k_M1_IFNg] = 0;
    paramLowBounds[km_M1_IFNg] = 0;		paramUpBounds[km_M1_IFNg] = 0;
    paramLowBounds[km_M1_IL10] = 0;		paramUpBounds[km_M1_IL10] = 0;
    paramLowBounds[a_N] = 0;		paramUpBounds[a_N] = 0;
    paramLowBounds[k_N_IFNg] = 0;		paramUpBounds[k_N_IFNg] = 0;
    paramLowBounds[km_N_IFNg] = 0;		paramUpBounds[km_N_IFNg] = 0;
    paramLowBounds[k_N_TNFa] = 0;		paramUpBounds[k_N_TNFa] = 0;
    paramLowBounds[km_N_TNFa] = 0;		paramUpBounds[km_N_TNFa] = 0;
    paramLowBounds[k_N_GMCSF] = 0;		paramUpBounds[k_N_GMCSF] = 0;
    paramLowBounds[km_N_GMCSF] = 0;		paramUpBounds[km_N_GMCSF] = 0;
    paramLowBounds[k_N_IL17c] = 0;		paramUpBounds[k_N_IL17c] = 0;
    paramLowBounds[km_N_IL17c] = 0;		paramUpBounds[km_N_IL17c] = 0;
    paramLowBounds[b_N] = 0;		paramUpBounds[b_N] = 0;
    paramLowBounds[a_Th1] = 0;		paramUpBounds[a_Th1] = 0;
    paramLowBounds[b_Th1] = 0;		paramUpBounds[b_Th1] = 0;
    paramLowBounds[k_Th1_IL2] = 0;		paramUpBounds[k_Th1_IL2] = 0;
    paramLowBounds[K_Th1_IL12] = 0;		paramUpBounds[K_Th1_IL12] = 0;
    paramLowBounds[k_Th1_IL12IL2] = 0;		paramUpBounds[k_Th1_IL12IL2] = 0;
    paramLowBounds[K_Th1_IL12IL2] = 0;		paramUpBounds[K_Th1_IL12IL2] = 0;
    paramLowBounds[K_Th1_IL10] = 0;		paramUpBounds[K_Th1_IL10] = 0;
    paramLowBounds[K_Th1_TGFb] = 0;		paramUpBounds[K_Th1_TGFb] = 0;
    paramLowBounds[k_Th1_IFNg] = 0;		paramUpBounds[k_Th1_IFNg] = 0;
    paramLowBounds[K_IFNg_Th1] = 0;		paramUpBounds[K_IFNg_Th1] = 0;
    paramLowBounds[K_Th1_IL6] = 0;		paramUpBounds[K_Th1_IL6] = 0;
    paramLowBounds[k_Th1_Th17] = 0;		paramUpBounds[k_Th1_Th17] = 0;
    paramLowBounds[K_Th1_Th17] = 0;		paramUpBounds[K_Th1_Th17] = 0;
    paramLowBounds[k_Th1_Treg] = 0;		paramUpBounds[k_Th1_Treg] = 0;
    paramLowBounds[K_Th1_Treg] = 0;		paramUpBounds[K_Th1_Treg] = 0;
    paramLowBounds[a_Th17] = 0;		paramUpBounds[a_Th17] = 0;
    paramLowBounds[b_Th17] = 0;		paramUpBounds[b_Th17] = 0;
    paramLowBounds[k_Th17_TGFb] = 0;		paramUpBounds[k_Th17_TGFb] = 0;
    paramLowBounds[K_Th17_TGFb] = 0;		paramUpBounds[K_Th17_TGFb] = 0;
    paramLowBounds[K_Th17_IL2] = 0;		paramUpBounds[K_Th17_IL2] = 0;
    paramLowBounds[K_Th17_IFNg] = 0;		paramUpBounds[K_Th17_IFNg] = 0;
    paramLowBounds[K_Th17_IL10] = 0;		paramUpBounds[K_Th17_IL10] = 0;
    paramLowBounds[k_Th17_IL6] = 0;		paramUpBounds[k_Th17_IL6] = 0;
    paramLowBounds[km_Th17_IL6] = 0;		paramUpBounds[km_Th17_IL6] = 0;
    paramLowBounds[k_Th17_IL1b] = 0;		paramUpBounds[k_Th17_IL1b] = 0;
    paramLowBounds[km_Th17_IL1b] = 0;		paramUpBounds[km_Th17_IL1b] = 0;
    paramLowBounds[a_CTL] = 0;		paramUpBounds[a_CTL] = 0;
    paramLowBounds[b_CTL] = 0;		paramUpBounds[b_CTL] = 0;
    paramLowBounds[k_CTL_IL2] = 0;		paramUpBounds[k_CTL_IL2] = 0;
    paramLowBounds[K_CTL_IL12] = 0;		paramUpBounds[K_CTL_IL12] = 0;
    paramLowBounds[k_CTL_IL12IL2] = 0;		paramUpBounds[k_CTL_IL12IL2] = 0;
    paramLowBounds[K_CTL_IL12IL2] = 0;		paramUpBounds[K_CTL_IL12IL2] = 0;
    paramLowBounds[K_CTL_IL10] = 0;		paramUpBounds[K_CTL_IL10] = 0;
    paramLowBounds[K_CTL_TGFb] = 0;		paramUpBounds[K_CTL_TGFb] = 0;
    paramLowBounds[K_CTL_IL6] = 0;		paramUpBounds[K_CTL_IL6] = 0;
    paramLowBounds[k_CTL_IFNg] = 0;		paramUpBounds[k_CTL_IFNg] = 0;
    paramLowBounds[K_CTL_IFNg] = 0;		paramUpBounds[K_CTL_IFNg] = 0;
    paramLowBounds[kmax_MHC1] = 0;		paramUpBounds[kmax_MHC1] = 0;
    paramLowBounds[km_MHC1_IFNb] = 0;		paramUpBounds[km_MHC1_IFNb] = 0;
    paramLowBounds[a_Treg] = 0;		paramUpBounds[a_Treg] = 0;
    paramLowBounds[b_Treg] = 0;		paramUpBounds[b_Treg] = 0;
    paramLowBounds[k_Treg_IL2] = 0;		paramUpBounds[k_Treg_IL2] = 0;
    paramLowBounds[K_Treg_IL2] = 0;		paramUpBounds[K_Treg_IL2] = 0;
    paramLowBounds[K_Treg_IL17] = 0;		paramUpBounds[K_Treg_IL17] = 0;
    paramLowBounds[K_Treg_IL6] = 0;		paramUpBounds[K_Treg_IL6] = 0;
    paramLowBounds[k_Treg_TGFb] = 0;		paramUpBounds[k_Treg_TGFb] = 0;
    paramLowBounds[K_Treg_TGFb] = 0;		paramUpBounds[K_Treg_TGFb] = 0;
    paramLowBounds[kbasal_SPD] = 0;		paramUpBounds[kbasal_SPD] = 0;
    paramLowBounds[a_SPD] = 0;		paramUpBounds[a_SPD] = 0;
    paramLowBounds[b_SPD] = 0;		paramUpBounds[b_SPD] = 0;
    paramLowBounds[kbasal_FER] = 0;		paramUpBounds[kbasal_FER] = 0;
    paramLowBounds[a_FER] = 0;		paramUpBounds[a_FER] = 0;
    paramLowBounds[b_FER] = 0;		paramUpBounds[b_FER] = 0;
    paramLowBounds[a_tnf] = 0;		paramUpBounds[a_tnf] = 0;
    paramLowBounds[a_tnf_at1] = 0;		paramUpBounds[a_tnf_at1] = 0;
    paramLowBounds[a_tnf_i] = 0;		paramUpBounds[a_tnf_i] = 0;
    paramLowBounds[a_tnf_at2] = 0;		paramUpBounds[a_tnf_at2] = 0;
    paramLowBounds[a_tnf_m1] = 0;		paramUpBounds[a_tnf_m1] = 0;
    paramLowBounds[a_tnf_th1] = 0;		paramUpBounds[a_tnf_th1] = 0;
    paramLowBounds[a_tnf_th17] = 0;		paramUpBounds[a_tnf_th17] = 0;
    paramLowBounds[b_tnf] = 0;		paramUpBounds[b_tnf] = 0;
    paramLowBounds[a_il6] = 0;		paramUpBounds[a_il6] = 0;
    paramLowBounds[b_il6] = 0;		paramUpBounds[b_il6] = 0;
    paramLowBounds[a_il6_at1] = 0;		paramUpBounds[a_il6_at1] = 0;
    paramLowBounds[a_il6_i] = 0;		paramUpBounds[a_il6_i] = 0;
    paramLowBounds[a_il6_at2] = 0;		paramUpBounds[a_il6_at2] = 0;
    paramLowBounds[a_il6_m1] = 0;		paramUpBounds[a_il6_m1] = 0;
    paramLowBounds[a_il6_th17] = 0;		paramUpBounds[a_il6_th17] = 0;
    paramLowBounds[a_il6_neu] = 0;		paramUpBounds[a_il6_neu] = 0;
    paramLowBounds[a_ifng] = 0;		paramUpBounds[a_ifng] = 0;
    paramLowBounds[b_ifng] = 0;		paramUpBounds[b_ifng] = 0;
    paramLowBounds[a_ifng_dc] = 0;		paramUpBounds[a_ifng_dc] = 0;
    paramLowBounds[a_ifng_th1] = 0;		paramUpBounds[a_ifng_th1] = 0;
    paramLowBounds[a_ifng_ctl] = 0;		paramUpBounds[a_ifng_ctl] = 0;
    paramLowBounds[a_ifnb] = 0;		paramUpBounds[a_ifnb] = 0;
    paramLowBounds[b_ifnb] = 0;		paramUpBounds[b_ifnb] = 0;
    paramLowBounds[a_ifnb_at1] = 0;		paramUpBounds[a_ifnb_at1] = 0;
    paramLowBounds[a_ifnb_i] = 0;		paramUpBounds[a_ifnb_i] = 0;
    paramLowBounds[a_ifnb_d] = 0;		paramUpBounds[a_ifnb_d] = 0;
    paramLowBounds[a_ifnb_dc] = 0;		paramUpBounds[a_ifnb_dc] = 0;
    paramLowBounds[a_il2_dc] = 0;		paramUpBounds[a_il2_dc] = 0;
    paramLowBounds[a_il2_th1] = 0;		paramUpBounds[a_il2_th1] = 0;
    paramLowBounds[b_il2] = 0;		paramUpBounds[b_il2] = 0;
    paramLowBounds[a_il2] = 0;		paramUpBounds[a_il2] = 0;
    paramLowBounds[a_il12_m1] = 0;		paramUpBounds[a_il12_m1] = 0;
    paramLowBounds[a_il12_dc] = 0;		paramUpBounds[a_il12_dc] = 0;
    paramLowBounds[b_il12] = 0;		paramUpBounds[b_il12] = 0;
    paramLowBounds[a_il12] = 0;		paramUpBounds[a_il12] = 0;
    paramLowBounds[a_il17_th17] = 0;		paramUpBounds[a_il17_th17] = 0;
    paramLowBounds[a_il17_ctl] = 0;		paramUpBounds[a_il17_ctl] = 0;
    paramLowBounds[b_il17] = 0;		paramUpBounds[b_il17] = 0;
    paramLowBounds[a_il17] = 0;		paramUpBounds[a_il17] = 0;
    paramLowBounds[a_il10_treg] = 0;		paramUpBounds[a_il10_treg] = 0;
    paramLowBounds[b_il10] = 0;		paramUpBounds[b_il10] = 0;
    paramLowBounds[a_il10] = 0;		paramUpBounds[a_il10] = 0;
    paramLowBounds[a_tgfb_th17] = 0;		paramUpBounds[a_tgfb_th17] = 0;
    paramLowBounds[a_tgfb_treg] = 0;		paramUpBounds[a_tgfb_treg] = 0;
    paramLowBounds[b_tgfb] = 0;		paramUpBounds[b_tgfb] = 0;
    paramLowBounds[a_tgfb] = 0;		paramUpBounds[a_tgfb] = 0;
    paramLowBounds[a_gmcsf_m1] = 0;		paramUpBounds[a_gmcsf_m1] = 0;
    paramLowBounds[a_gmcsf_th1] = 0;		paramUpBounds[a_gmcsf_th1] = 0;
    paramLowBounds[a_gmcsf_th17] = 0;		paramUpBounds[a_gmcsf_th17] = 0;
    paramLowBounds[b_gmcsf] = 0;		paramUpBounds[b_gmcsf] = 0;
    paramLowBounds[a_gmcsf] = 0;		paramUpBounds[a_gmcsf] = 0;
    paramLowBounds[a_il1b] = 0;		paramUpBounds[a_il1b] = 0;
    paramLowBounds[b_il1b] = 0;		paramUpBounds[b_il1b] = 0;
    paramLowBounds[a_il1b_at1] = 0;		paramUpBounds[a_il1b_at1] = 0;
    paramLowBounds[a_il1b_i] = 0;		paramUpBounds[a_il1b_i] = 0;
    paramLowBounds[a_il1b_at2] = 0;		paramUpBounds[a_il1b_at2] = 0;
    paramLowBounds[a_il1b_m1] = 0;		paramUpBounds[a_il1b_m1] = 0;
    paramLowBounds[a_il1b_dc] = 0;		paramUpBounds[a_il1b_dc] = 0;
    paramLowBounds[kbasal_ROS] = 0;		paramUpBounds[kbasal_ROS] = 0;
    paramLowBounds[b_ROS] = 0;		paramUpBounds[b_ROS] = 0;
    paramLowBounds[ktr_TNFa] = 0;		paramUpBounds[ktr_TNFa] = 0;
    paramLowBounds[ktr_IL6] = 0;		paramUpBounds[ktr_IL6] = 0;
    paramLowBounds[ktr_IL1b] = 0;		paramUpBounds[ktr_IL1b] = 0;
    paramLowBounds[ktr_IFNb] = 0;		paramUpBounds[ktr_IFNb] = 0;
    paramLowBounds[ktr_IFNg] = 0;		paramUpBounds[ktr_IFNg] = 0;
    paramLowBounds[ktr_IL2] = 0;		paramUpBounds[ktr_IL2] = 0;
    paramLowBounds[ktr_IL12] = 0;		paramUpBounds[ktr_IL12] = 0;
    paramLowBounds[ktr_IL17] = 0;		paramUpBounds[ktr_IL17] = 0;
    paramLowBounds[ktr_IL10] = 0;		paramUpBounds[ktr_IL10] = 0;
    paramLowBounds[ktr_TGFb] = 0;		paramUpBounds[ktr_TGFb] = 0;
    paramLowBounds[ktr_GMCSF] = 0;		paramUpBounds[ktr_GMCSF] = 0;
    paramLowBounds[ktr_SPD] = 0;		paramUpBounds[ktr_SPD] = 0;
    paramLowBounds[ktr_FER] = 0;		paramUpBounds[ktr_FER] = 0;
    paramLowBounds[k_CTL_I_SPD] = 0;		paramUpBounds[k_CTL_I_SPD] = 0;
    paramLowBounds[k_CTL_I_Fer] = 0;		paramUpBounds[k_CTL_I_Fer] = 0;
    paramLowBounds[kdc] = 0;		paramUpBounds[kdc] = 0;
    paramLowBounds[ktr_pDC] = 0;		paramUpBounds[ktr_pDC] = 0;
    paramLowBounds[ktr_M1] = 0;		paramUpBounds[ktr_M1] = 0;
    paramLowBounds[ktr_N] = 0;		paramUpBounds[ktr_N] = 0;
    paramLowBounds[ktr_Th1] = 0;		paramUpBounds[ktr_Th1] = 0;
    paramLowBounds[ktr_Th17] = 0;		paramUpBounds[ktr_Th17] = 0;
    paramLowBounds[ktr_CTL] = 0;		paramUpBounds[ktr_CTL] = 0;
    paramLowBounds[ktr_Treg] = 0;		paramUpBounds[ktr_Treg] = 0;
    paramLowBounds[k_Ab_V] = 0;		paramUpBounds[k_Ab_V] = 0;
    paramLowBounds[k_mu_AT2] = 0;		paramUpBounds[k_mu_AT2] = 0;
    paramLowBounds[k_diff_AT1] = 0;		paramUpBounds[k_diff_AT1] = 0;
    paramLowBounds[k_dAT] = 0;		paramUpBounds[k_dAT] = 0;
    paramLowBounds[km_dAT] = 0;		paramUpBounds[km_dAT] = 0;
    paramLowBounds[kbasal_N] = 0;		paramUpBounds[kbasal_N] = 0;
    paramLowBounds[k_livercrp] = 0;		paramUpBounds[k_livercrp] = 0;
    paramLowBounds[Liver] = 0;		paramUpBounds[Liver] = 0;
    paramLowBounds[kCRPSecretion] = 0;		paramUpBounds[kCRPSecretion] = 0;
    paramLowBounds[kCRP_BloodtoLiver] = 0;		paramUpBounds[kCRP_BloodtoLiver] = 0;
    paramLowBounds[kCRP_LivertoBlood] = 0;		paramUpBounds[kCRP_LivertoBlood] = 0;
    paramLowBounds[kbasal_CRP] = 0;		paramUpBounds[kbasal_CRP] = 0;
    paramLowBounds[kdeg_CRP] = 0;		paramUpBounds[kdeg_CRP] = 0;
    paramLowBounds[basal_tnfa] = 0;		paramUpBounds[basal_tnfa] = 0;
    paramLowBounds[basalil6] = 0;		paramUpBounds[basalil6] = 0;
    paramLowBounds[basalil1] = 0;		paramUpBounds[basalil1] = 0;
    paramLowBounds[basalifng] = 0;		paramUpBounds[basalifng] = 0;
    paramLowBounds[basalifnb] = 0;		paramUpBounds[basalifnb] = 0;
    paramLowBounds[basalil2] = 0;		paramUpBounds[basalil2] = 0;
    paramLowBounds[basalil12] = 0;		paramUpBounds[basalil12] = 0;
    paramLowBounds[basalil10] = 0;		paramUpBounds[basalil10] = 0;
    paramLowBounds[basaltgfb] = 0;		paramUpBounds[basaltgfb] = 0;
    paramLowBounds[basalgmcsf] = 0;		paramUpBounds[basalgmcsf] = 0;
    paramLowBounds[tau_Ab] = 0;		paramUpBounds[tau_Ab] = 0;
    paramLowBounds[a_Ab] = 0;		paramUpBounds[a_Ab] = 0;
    paramLowBounds[b_Ab] = 0;		paramUpBounds[b_Ab] = 0;
    paramLowBounds[a_FER_c] = 0;		paramUpBounds[a_FER_c] = 0;
    paramLowBounds[vol_plasma] = 0;     paramUpBounds[vol_plasma] = 0;
    paramLowBounds[vol_alv_ml] = 0;     paramUpBounds[vol_alv_ml] = 0;



}

void modelDai2021::setBaseParameters(){
    // would like the model to have scaling for more GCs but less antigen and vice-versa
    params.clear();                 // to make sure they are all put to zero
	params.resize(NBPARAM, 0.0);
    params[A_V] = 0;init[V] = 0;
    params[k_int] = 0;init[AT1] = 0;
    params[km_int_IFNb] = 0;init[AT2] = 0;
    params[k_V_innate] = 0;init[I] = 0;
    params[b_V] = 0;init[dAT1] = 0;
    params[f_int] = 0;init[dAT2] = 0;
    params[mu_AT2] = 0;init[pDC] = 0;
    params[k_ROS_AT2] = 0;init[M1] = 0;
    params[km_ROS_AT2] = 0;init[N] = 0;
    params[k_AT1_AT2] = 0;init[Th1] = 0;
    params[km_AT1_AT2] = 0;init[Th17] = 0;
    params[k_IFNb_kill] = 0;init[CTL] = 0;
    params[k_kill] = 0;init[Treg] = 0;
    params[km_kill] = 0;init[TNFa] = 0;
    params[km_ROS_AT2] = 0;init[IL6] = 0;
    params[b_AT2] = 0;init[IL1b] = 0;
    params[b_I] = 0;init[IFNb] = 0;
    params[mu_AT1] = 0;init[IFNg] = 0;
    params[k_ROS_AT1] = 0;init[IL2] = 0;
    params[km_ROS_AT1] = 0;init[IL12] = 0;
    params[b_AT1] = 0;init[IL17] = 0;
    params[b_dAT1] = 0;init[IL10] = 0;
    params[k_damage_TNFa] = 0;init[TGFb] = 0;
    params[km_damage_TNFa] = 0;init[GMCSF] = 0;
    params[k_damage_IL6] = 0;init[SPD] = 0;
    params[km_damage_IL6] = 0;init[FER] = 0;
    params[k_damage_IL1b] = 0;init[TNFa_c] = 0;
    params[km_damage_IL1b] = 0;init[IL6_c] = 0;
    params[k_damage_IFNg] = 0;init[IL1b_c] = 0;
    params[km_damage_IFNg] = 0;init[IFNb_c] = 0;
    params[k_damage_cyt] = 0;init[IFNg_c] = 0;
    params[a_DC] = 0;init[IL2_c] = 0;
    params[kbasal_DC] = 0;init[IL12_c] = 0;
    params[b_DC] = 0;init[IL17_c] = 0;
    params[k_DC_TNFa] = 0;init[IL10_c] = 0;
    params[km_DC_TNFa] = 0;init[TGFb_c] = 0;
    params[k_DC_IFNg] = 0;init[GMCSF_c] = 0;
    params[km_DC_IFNg] = 0;init[SPD_c] = 0;
    params[k_DC_IL6] = 0;init[FER_c] = 0;
    params[km_DC_IL6] = 0;init[Ab] = 0;
    params[km_DC_IL10] = 0;init[CRPExtracellular] = 0;
    params[a_M1] = 0;init[Blood_CRP] = 0;
    params[kbasal_M1] = 0;
    params[k_v] = 0;
    params[k_I] = 0;
    params[km_v] = 0;
    params[km_I] = 0;
    params[k_M1_IL6] = 0;
    params[km_M1_IL6] = 0;
    params[b_M1] = 0;
    params[k_M1_TNFa] = 0;
    params[km_M1_TNFa] = 0;
    params[k_M1_GMCSF] = 0;
    params[km_M1_GMCSF] = 0;
    params[k_M1_IFNg] = 0;
    params[km_M1_IFNg] = 0;
    params[km_M1_IL10] = 0;
    params[a_N] = 0;
    params[k_N_IFNg] = 0;
    params[km_N_IFNg] = 0;
    params[k_N_TNFa] = 0;
    params[km_N_TNFa] = 0;
    params[k_N_GMCSF] = 0;
    params[km_N_GMCSF] = 0;
    params[k_N_IL17c] = 0;
    params[km_N_IL17c] = 0;
    params[b_N] = 0;
    params[a_Th1] = 0;
    params[b_Th1] = 0;
    params[k_Th1_IL2] = 0;
    params[K_Th1_IL12] = 0;
    params[k_Th1_IL12IL2] = 0;
    params[K_Th1_IL12IL2] = 0;
    params[K_Th1_IL10] = 0;
    params[K_Th1_TGFb] = 0;
    params[k_Th1_IFNg] = 0;
    params[K_IFNg_Th1] = 0;
    params[K_Th1_IL6] = 0;
    params[k_Th1_Th17] = 0;
    params[K_Th1_Th17] = 0;
    params[k_Th1_Treg] = 0;
    params[K_Th1_Treg] = 0;
    params[a_Th17] = 0;
    params[b_Th17] = 0;
    params[k_Th17_TGFb] = 0;
    params[K_Th17_TGFb] = 0;
    params[K_Th17_IL2] = 0;
    params[K_Th17_IFNg] = 0;
    params[K_Th17_IL10] = 0;
    params[k_Th17_IL6] = 0;
    params[km_Th17_IL6] = 0;
    params[k_Th17_IL1b] = 0;
    params[km_Th17_IL1b] = 0;
    params[a_CTL] = 0;
    params[b_CTL] = 0;
    params[k_CTL_IL2] = 0;
    params[K_CTL_IL12] = 0;
    params[k_CTL_IL12IL2] = 0;
    params[K_CTL_IL12IL2] = 0;
    params[K_CTL_IL10] = 0;
    params[K_CTL_TGFb] = 0;
    params[K_CTL_IL6] = 0;
    params[k_CTL_IFNg] = 0;
    params[K_CTL_IFNg] = 0;
    params[kmax_MHC1] = 0;
    params[km_MHC1_IFNb] = 0;
    params[a_Treg] = 0;
    params[b_Treg] = 0;
    params[k_Treg_IL2] = 0;
    params[K_Treg_IL2] = 0;
    params[K_Treg_IL17] = 0;
    params[K_Treg_IL6] = 0;
    params[k_Treg_TGFb] = 0;
    params[K_Treg_TGFb] = 0;
    params[kbasal_SPD] = 0;
    params[a_SPD] = 0;
    params[b_SPD] = 0;
    params[kbasal_FER] = 0;
    params[a_FER] = 0;
    params[b_FER] = 0;
    params[a_tnf] = 0;
    params[a_tnf_at1] = 0;
    params[a_tnf_i] = 0;
    params[a_tnf_at2] = 0;
    params[a_tnf_m1] = 0;
    params[a_tnf_th1] = 0;
    params[a_tnf_th17] = 0;
    params[b_tnf] = 0;
    params[a_il6] = 0;
    params[b_il6] = 0;
    params[a_il6_at1] = 0;
    params[a_il6_i] = 0;
    params[a_il6_at2] = 0;
    params[a_il6_m1] = 0;
    params[a_il6_th17] = 0;
    params[a_il6_neu] = 0;
    params[a_ifng] = 0;
    params[b_ifng] = 0;
    params[a_ifng_dc] = 0;
    params[a_ifng_th1] = 0;
    params[a_ifng_ctl] = 0;
    params[a_ifnb] = 0;
    params[b_ifnb] = 0;
    params[a_ifnb_at1] = 0;
    params[a_ifnb_i] = 0;
    params[a_ifnb_d] = 0;
    params[a_ifnb_dc] = 0;
    params[a_il2_dc] = 0;
    params[a_il2_th1] = 0;
    params[b_il2] = 0;
    params[a_il2] = 0;
    params[a_il12_m1] = 0;
    params[a_il12_dc] = 0;
    params[b_il12] = 0;
    params[a_il12] = 0;
    params[a_il17_th17] = 0;
    params[a_il17_ctl] = 0;
    params[b_il17] = 0;
    params[a_il17] = 0;
    params[a_il10_treg] = 0;
    params[b_il10] = 0;
    params[a_il10] = 0;
    params[a_tgfb_th17] = 0;
    params[a_tgfb_treg] = 0;
    params[b_tgfb] = 0;
    params[a_tgfb] = 0;
    params[a_gmcsf_m1] = 0;
    params[a_gmcsf_th1] = 0;
    params[a_gmcsf_th17] = 0;
    params[b_gmcsf] = 0;
    params[a_gmcsf] = 0;
    params[a_il1b] = 0;
    params[b_il1b] = 0;
    params[a_il1b_at1] = 0;
    params[a_il1b_i] = 0;
    params[a_il1b_at2] = 0;
    params[a_il1b_m1] = 0;
    params[a_il1b_dc] = 0;
    params[kbasal_ROS] = 0;
    params[b_ROS] = 0;
    params[ktr_TNFa] = 0;
    params[ktr_IL6] = 0;
    params[ktr_IL1b] = 0;
    params[ktr_IFNb] = 0;
    params[ktr_IFNg] = 0;
    params[ktr_IL2] = 0;
    params[ktr_IL12] = 0;
    params[ktr_IL17] = 0;
    params[ktr_IL10] = 0;
    params[ktr_TGFb] = 0;
    params[ktr_GMCSF] = 0;
    params[ktr_SPD] = 0;
    params[ktr_FER] = 0;
    params[k_CTL_I_SPD] = 0;
    params[k_CTL_I_Fer] = 0;
    params[kdc] = 0;
    params[ktr_pDC] = 0;
    params[ktr_M1] = 0;
    params[ktr_N] = 0;
    params[ktr_Th1] = 0;
    params[ktr_Th17] = 0;
    params[ktr_CTL] = 0;
    params[ktr_Treg] = 0;
    params[k_Ab_V] = 0;
    params[k_mu_AT2] = 0;
    params[k_diff_AT1] = 0;
    params[k_dAT] = 0;
    params[km_dAT] = 0;
    params[kbasal_N] = 0;
    params[k_livercrp] = 0;
    params[Liver] = 0;
    params[kCRPSecretion] = 0;
    params[kCRP_BloodtoLiver] = 0;
    params[kCRP_LivertoBlood] = 0;
    params[kbasal_CRP] = 0;
    params[kdeg_CRP] = 0;
    params[basal_tnfa] = 0;
    params[basalil6] = 0;
    params[basalil1] = 0;
    params[basalifng] = 0;
    params[basalifnb] = 0;
    params[basalil2] = 0;
    params[basalil12] = 0;
    params[basalil10] = 0;
    params[basaltgfb] = 0;
    params[basalgmcsf] = 0;
    params[tau_Ab] = 0;
    params[a_Ab] = 0;
    params[b_Ab] = 0;
    params[a_FER_c] = 0;
    params[vol_plasma] = 0;
    params[vol_alv_ml] = 0;


	setBaseParametersDone();
}

void modelDai2021::initialise(long long _background){ // don't touch to parameters !
	background = _background;
	val.clear();
	val.resize(NBVAR, 0.0);
	init.clear();
	init.resize(NBVAR, 0.0);       

    init[V] = 0;
    init[AT1] = 0;
    init[AT2] = 0;
    init[I] = 0;
    init[dAT1] = 0;
    init[dAT2] = 0;
    init[pDC] = 0;
    init[M1] = 0;
    init[N] = 0;
    init[Th1] = 0;
    init[Th17] = 0;
    init[CTL] = 0;
    init[Treg] = 0;
    init[TNFa] = 0;
    init[IL6] = 0;
    init[IL1b] = 0;
    init[IFNb] = 0;
    init[IFNg] = 0;
    init[IL2] = 0;
    init[IL12] = 0;
    init[IL17] = 0;
    init[IL10] = 0;
    init[TGFb] = 0;
    init[GMCSF] = 0;
    init[SPD] = 0;
    init[FER] = 0;
    init[TNFa_c] = 0;
    init[IL6_c] = 0;
    init[IL1b_c] = 0;
    init[IFNb_c] = 0;
    init[IFNg_c] = 0;
    init[IL2_c] = 0;
    init[IL12_c] = 0;
    init[IL17_c] = 0;
    init[IL10_c] = 0;
    init[TGFb_c] = 0;
    init[GMCSF_c] = 0;
    init[SPD_c] = 0;
    init[FER_c] = 0;
    init[Ab] = 0;
    init[CRPExtracellular] = 0;
    init[Blood_CRP] = 0;
    init[pDC_c] = 0;
    init[M1_c] = 0;
    init[N_c] = 0;
    init[Th1_c] = 0;
    init[Th17_c] = 0;
    init[CTL_c] = 0;
    init[Treg_c] = 0;
    // other variables are derived


    for(size_t i = 0; i < NBVAR; ++i){
        val[i] = init[i];}
	t = 0;
	initialiseDone();
}









// Hill function, not yet used
double Hill(double x, double K){
    if(x <= 0) return 0;
    if(x+K < 1e-12) return 0;
    return(x / (x+K));
}

