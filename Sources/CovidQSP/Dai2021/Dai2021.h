// ------- Automatically generated model -------- //
#ifndef modeleDai2021_H
#define modeleDai2021_H
#include "../Moonfit/moonfit.h"



// Note, the ETA parameter is nonphysio (decrease of Tfh if more B cells)
// and it assumes plasma are only produced at the end, don't like
struct modelDai2021 : public Model {
    modelDai2021();
    enum {V, AT1, AT2, I, dAT1, dAT2, pDC, M1, N, Th1, Th17, CTL, Treg, TNFa, IL6, IL1b, IFNb, IFNg, IL2, IL12, IL17, IL10, TGFb, GMCSF, SPD, FER, TNFa_c, IL6_c, IL1b_c, IFNb_c, IFNg_c, IL2_c, IL12_c, IL17_c, IL10_c, TGFb_c, GMCSF_c, SPD_c, FER_c, Ab, CRPExtracellular, Blood_CRP, pDC_c, M1_c, N_c, Th1_c, Th17_c, CTL_c, Treg_c, V_plot, AT1_plot, AT2_plot, I_plot, dAT1_plot, dAT2_plot, pDC_plot, M1_plot, N_plot, Th1_plot, Th17_plot, CTL_plot, Treg_plot, TNFa_plot, IL6_plot, IL1b_plot, IFNb_plot, IFNg_plot, IL2_plot, IL12_plot, IL17_plot, IL10_plot, TGFb_plot, GMCSF_plot, SPD_plot, FER_plot, TNFa_c_plot, IL6_c_plot, IL1b_c_plot, IFNb_c_plot, IFNg_c_plot, IL2_c_plot, IL12_c_plot, IL17_c_plot, IL10_c_plot, TGFb_c_plot, GMCSF_c_plot, SPD_c_plot, FER_c_plot, Ab_plot, CRPExtracellular_plot, Blood_CRP_plot, pDC_c_plot, M1_c_plot, N_c_plot, Th1_c_plot, Th17_c_plot, CTL_c_plot, Treg_c_plot, CD4_c_tot_plot, CD3_c_tot_plot, CD4_tot_plot, CD3_tot_plot, CD4_lung_plot, CD3_lung_plot, N_tot_plot, CTL_tot_plot, NBVAR};
    enum {A_V, k_int, km_int_IFNb, k_V_innate, b_V, f_int, mu_AT2, k_ROS_AT2, km_ROS_AT2, k_AT1_AT2, km_AT1_AT2, k_IFNb_kill, k_kill, km_kill, b_AT2, b_I, mu_AT1, k_ROS_AT1, km_ROS_AT1, b_AT1, b_dAT1, k_damage_TNFa, km_damage_TNFa, k_damage_IL6, km_damage_IL6, k_damage_IL1b, km_damage_IL1b, k_damage_IFNg, km_damage_IFNg, k_damage_cyt, a_DC, kbasal_DC, b_DC, k_DC_TNFa, km_DC_TNFa, k_DC_IFNg, km_DC_IFNg, k_DC_IL6, km_DC_IL6, km_DC_IL10, a_M1, kbasal_M1, k_v, k_I, km_v, km_I, k_M1_IL6, km_M1_IL6, b_M1, k_M1_TNFa, km_M1_TNFa, k_M1_GMCSF, km_M1_GMCSF, k_M1_IFNg, km_M1_IFNg, km_M1_IL10, a_N, k_N_IFNg, km_N_IFNg, k_N_TNFa, km_N_TNFa, k_N_GMCSF, km_N_GMCSF, k_N_IL17c, km_N_IL17c, b_N, a_Th1, b_Th1, k_Th1_IL2, K_Th1_IL12, k_Th1_IL12IL2, K_Th1_IL12IL2, K_Th1_IL10, K_Th1_TGFb, k_Th1_IFNg, K_IFNg_Th1, K_Th1_IL6, k_Th1_Th17, K_Th1_Th17, k_Th1_Treg, K_Th1_Treg, a_Th17, b_Th17, k_Th17_TGFb, K_Th17_TGFb, K_Th17_IL2, K_Th17_IFNg, K_Th17_IL10, k_Th17_IL6, km_Th17_IL6, k_Th17_IL1b, km_Th17_IL1b, a_CTL, b_CTL, k_CTL_IL2, K_CTL_IL12, k_CTL_IL12IL2, K_CTL_IL12IL2, K_CTL_IL10, K_CTL_TGFb, K_CTL_IL6, k_CTL_IFNg, K_CTL_IFNg, kmax_MHC1, km_MHC1_IFNb, a_Treg, b_Treg, k_Treg_IL2, K_Treg_IL2, K_Treg_IL17, K_Treg_IL6, k_Treg_TGFb, K_Treg_TGFb, kbasal_SPD, a_SPD, b_SPD, kbasal_FER, a_FER, b_FER, a_tnf, a_tnf_at1, a_tnf_i, a_tnf_at2, a_tnf_m1, a_tnf_th1, a_tnf_th17, b_tnf, a_il6, b_il6, a_il6_at1, a_il6_i, a_il6_at2, a_il6_m1, a_il6_th17, a_il6_neu, a_ifng, b_ifng, a_ifng_dc, a_ifng_th1, a_ifng_ctl, a_ifnb, b_ifnb, a_ifnb_at1, a_ifnb_i, a_ifnb_d, a_ifnb_dc, a_il2_dc, a_il2_th1, b_il2, a_il2, a_il12_m1, a_il12_dc, b_il12, a_il12, a_il17_th17, a_il17_ctl, b_il17, a_il17, a_il10_treg, b_il10, a_il10, a_tgfb_th17, a_tgfb_treg, b_tgfb, a_tgfb, a_gmcsf_m1, a_gmcsf_th1, a_gmcsf_th17, b_gmcsf, a_gmcsf, a_il1b, b_il1b, a_il1b_at1, a_il1b_i, a_il1b_at2, a_il1b_m1, a_il1b_dc, kbasal_ROS, b_ROS, ktr_TNFa, ktr_IL6, ktr_IL1b, ktr_IFNb, ktr_IFNg, ktr_IL2, ktr_IL12, ktr_IL17, ktr_IL10, ktr_TGFb, ktr_GMCSF, ktr_SPD, ktr_FER, k_CTL_I_SPD, k_CTL_I_Fer, kdc, kCRPSecretion, kCRP_BloodtoLiver, kCRP_LivertoBlood, kbasal_CRP, kdeg_CRP, basal_tnfa, basalil6, basalil1, basalifng, basalifnb, basalil2, basalil12, basalil10, basaltgfb, basalgmcsf, tau_Ab, a_Ab, b_Ab, a_FER_c, vol_plasma, vol_alv_ml, ktr_pDC, ktr_M1, ktr_N, ktr_Th1, ktr_Th17, ktr_CTL, ktr_Treg, k_Ab_V, k_mu_AT2, k_diff_AT1, k_dAT, km_dAT, kbasal_N, k_livercrp, Liver, NBPARAM};

    long long background;
    void derivatives(const vector<double> &x, vector<double> &dxdt, const double _t);

    void updateDerivedVariables(double _t = 0);
    void initialise(long long _background = 0);
	void setBaseParameters();

    void action(string _name, double parameter){
        if(!_name.compare("nameAction")){
            //params[] = 0;
        }
        cerr << "ERR: modelDai2021, no define action for '" << name << "'\n";
    }
};
#endif

