include("../Moonfit/moonfit.pri")

# This is the version without librarires
#include("../Moonfit/moonfitNoLib.pri")
#QT -= opengl
#CONFIG -= QT
#DEFINES += "WITHOUT_QT"

#name of the executable file generated
TARGET = MiniCov


HEADERS += \
	Dai2021/Dai2021.h \
    expCov.h
	
SOURCES += \
    expCov.cpp \
	Dai2021/Dai2021.cpp \
	miniMain.cpp


