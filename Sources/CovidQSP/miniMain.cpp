#include "../Moonfit/moonfit.h"


#include "expCov.h"
#include "Dai2021/Dai2021.h"

static string folder = "C:/Users/pprobert/Desktop/Softwares/NewArchaeropteryx/Sources/CovidQSP/";
static string folderBaseResults = "C:/Users/pprobert/Desktop/Softwares/NewArchaeropteryx/Sources/CovidQSP/Results/";

#define nRep 10

int main(int argc, char *argv[]){
    if(argc < 2) {cerr << "MiniMain takes only 1 argument (which combination)" << endl; return 0;}
    int IDcomb = -1;
    if(argc == 2){IDcomb = atoi(argv[1]);}
    cout << "Will process combination " << IDcomb << endl;

    createFolder(folderBaseResults);

    //TableCourse* TTh1   = new TableCourse(folder + string("DATA/2022_Kin_Th1_All.txt"));


    //TableCourse* TTh1Std   = new TableCourse(folder + string("DATA/2022_Kin_Th1_All_STD.txt"));

    //overrider* OverTh1 = new overrider(TTh1);                                               // Note : never create overrider as a non pointer, to be used by the graphical interface because they will be erased when function closes and gives control to the interface --> use a pointer and new ...

    //    case SQUARE_COST: res << "RSS"; break;
    //    case SQUARE_COST_STD: res << "RSS + StdDev"; break;
    //    case LOG_COST: res << "Log"; break;
    //    case PROPORTION_COST: res << "Ratios"; break;

    //    case NO_NORM: res << " (No Norm)"; break;
    //    case NORM_AVERAGE: res << " Norm/Avg vars"; break;
    //    case NORM_NB_PTS: res << " Norm/Nb Points"; break;
    //    case NORM_AVG_AND_NB_PTS: res << " Norm/Avg and Nb Pts "; break;
    //    case NORM_MAX: res << " Norm/Max vars"; break;
    //    case NORM_MAX_AND_NB_PTS: res << " Norm/Max and Nb Pts"; break;
    setTypeCost(SQUARE_COST);
    setTypeNorm(NORM_AVERAGE);

    string configFile = string("ConfigForGlobal.txt");
    Model* currentModel = new modelDai2021(); //modelErwinA();
    expCov* currentExperiment = new expCov(currentModel);

    //currentExperiment->giveData(TTh1, HEALTHY, TTh1Std);
    //currentExperiment->loadEvaluators();
    //currentExperiment->setOverrider(HEALTHY,    OverTh1);
    cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";


    if(IDcomb >= 0){

        manageSims* msi = new manageSims(currentExperiment);
        msi->loadConfig(folder + configFile);
        vector<string> listGeneratedFilesSets;

        for(int i = 0; i < msi->nbCombs; ++i){
            if(IDcomb < 0 || IDcomb == i){

                stringstream headerOptimizer;                                                   // each further script might use different optimizer options, will be stored in the following stringstream
                if(IDcomb == 0) headerOptimizer << optFileHeader(Genetic1M);
                else headerOptimizer << optFileHeader(Genetic50k);

                for(int j = 0; j < nRep; ++j){
                    stringstream codeSimu;      codeSimu << "CombNr" << i << "-" << codeTime();               // generates a text code for this particular optimization, in case parallel optimizations are running
                    stringstream folderComb;    folderComb << folderBaseResults << codeSimu.str() << "/";        // creates a folder for this particular optimization, to create figures etc ...
                    createFolder(folderComb.str());

                    cout << "   -> Optimizing combination (" << i << ") with ID: " << codeSimu.str() << "\n";
                    msi->resetParamSetFromConfig(folder + configFile);
                    currentExperiment->m->setPrintMode(false, 5000./3600.);
                    currentExperiment->m->dt = 10./3600.;

                    string optOptions = msi->motherCreateOptimizerFile(i, headerOptimizer.str());       // for each combination, will need to re-create an optimizer file
                    ofstream f1((folderComb.str() + string("Optimizer.txt")).c_str(), ios::out); if(f1) {f1 << optOptions << "\n"; f1.close();}

                    msi->motherOverrideUsingComb(i);                                                    // chose the variables to simulate and to replace by data according to this combination
                    msi->motherOptimize(folderComb.str() + string("Optimizer.txt"), 1000);              // DOES THE OPTIMIZATION !!!, and records the 1000 best sets

                    msi->saveHistory(folderComb.str() + string("History.txt"));                         // SAVES all the best parameter sets. by default, 10 000, can be modified by             msi->history.resize(max_nb_sets_to_record);
                    listGeneratedFilesSets.push_back(folderComb.str() + string("History.txt"));         // list[i] = historyFile for comb i

                    msi->useParamSetFromHistory(0);                                                     // takes the first set of parameters (the best), also possible to use msi->useParamSetFromHistory(0, i); for overriding only parameters from this combination,
                    msi->simulate();
                    ofstream f2((folderComb.str() + string("FitnessBestSetOf") + codeSimu.str() + string(".txt")).c_str(), ios::out); if(f2) {f2 << currentExperiment->costReport() << "\n"; f2.close();}
                    ofstream f3((folderComb.str() + string("CostEvolutionDuringOptimization.txt") + codeSimu.str()).c_str(), ios::out); if(f3) {f3 << msi->costRecords.print() << "\n"; f3.close();}

                }
            }
        }
    } else {
        #ifndef WITHOUT_QT
        QApplication b(argc, argv);
        simuWin* p = new simuWin(currentExperiment);
        cout << "Launching Graphical Interface ..." << endl;
        //p->loadConfig(folder + configFile);
        p->show();
        b.exec();
        #else
        cout << "Script finished (without qt, because WITHOUT_QT was defined)\n";
        #endif
    }

    cout << "END";
    return 0;
}

