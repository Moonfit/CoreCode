#ifndef EXPERIMENTST_COV_H
#define EXPERIMENTST_COV_H


#include "../Moonfit/moonfit.h"

#include <vector>
#include <string>
#include <iostream>

using namespace std;
enum  {HEALTHY, INFECTED, N_COVEXP};
namespace Back {
    enum {UNTREATED};
}

struct expCov : public Experiment {

    expCov(Model* _m) : Experiment(_m, N_COVEXP){
        Identification = string("ODE GC");
        names_exp[HEALTHY] = string("Healthy");
        names_exp[INFECTED] = string("Infected");
        m->setBaseParameters();
    }

    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                default: {m->initialise(Back::UNTREATED); break;}
            }

            // m->setPrintMode(true, 10800);

            switch(IdExp){
                case HEALTHY:{ // we are in hours, loosers
                m->t = 0; //-20000;
                m->simulate(23960, E);
                m->setValue("V", 1e6);
                m->simulate(10000, E);
                break;}
                case INFECTED:{
                m->t = 0; //-20000;
                m->simulate(23960, E);
                m->simulate(10000, E);
                break;}
            }
            m->setOverrider(nullptr);
        }
    }
};



#endif // EXPERIMENTSTHALL_H
