
#include "expThs.h"
#include "../Moonfit/moonfit.h"
#include "namesTh.h"

#include <iostream>
#include <fstream>
#include <sstream>

expThs::expThs(Model* _m) : Experiment(_m, NB_EXP){
    Identification = string("Canonical Differentiation");
    //Overs.resize(NB_EXP, NULL);
    //names_exp.resize(NB_EXP);
    names_exp[TH0] = string("Th0"); // activation (TCR Only)");
    names_exp[TH1] = string("Th1"); // activation (TCR + IL12 + anti-IL4)");
    names_exp[TH2] = string("Th2"); // differentiation (TCR + IL4 + anti-IFNg");
    names_exp[TREG] = string("Treg"); // differentiation (TCR + TGFb + IL2)");
    names_exp[TH17] = string("Th17"); // differentiation (TCR + TGFb + IL6 + IL1b + anti-IL2 + anti-IFNg)");
    m->setBaseParameters();
}


#define DAY *24*3600
#define HR *3600

//bool isIn(double Val, double vmin, double vmax){return ((vmin <= Val) && (Val <= vmax));}


////// gné force is missing in simulate
void expThs::simulate(int IdExp, Evaluator* E, bool force){
    //if(!E) E = VTG[IdExp]; // seems not necessary ( I was afraid of destruction of E)
    if(motherPrepareSimulate(IdExp, E, force)){
        switch(IdExp){
            /*case TH1_IFNGRKO: case IFNG_IFNGRKO: {
                m->initialise(Back::IFNGRKO); break;}*/
            default: {m->initialise(Back::WT); break;}
        }
        // here, no need to stabilization - m.stabilize(2*3600);
        //m->setPrintMode(true, 10800);
        #define Ldiff 3.1
        switch(IdExp){
        case TH0:{
                m->simulate(Ldiff*24*3600, E); break;}
        case TH1: {
                m->setValue("IL12", 10.0);
                m->setValue("antiIL4", 10000.0);
                m->simulate(Ldiff*24*3600, E); break;}
        case TH2: {
                m->setValue("IL4", 14.3);
                m->setValue("antiIFNg", 10000.0);
                //cout << "IL4 = " << m->getParam(149) << endl << endl;
                m->simulate(Ldiff*24*3600, E); break;}
        case TREG: {
                m->setValue("TGFB", 0.5);
                m->setValue("IL2", 13.4);
                m->simulate(Ldiff*24*3600, E); break;}
        case TH17: {
                m->setValue("IL6", 30.0);
                m->setValue("TGFB", 0.2);
                m->setValue("antiIL2", 5000.0);
                m->setValue("antiIFNg", 10000.0);
                m->simulate(Ldiff*24*3600, E); break;}
        }
        m->setOverrider(nullptr);
    }
}


void expThsHours::simulate(int IdExp, Evaluator* E, bool force){
    //if(!E) E = VTG[IdExp]; // seems not necessary ( I was afraid of destruction of E)
    if(motherPrepareSimulate(IdExp, E, force)){
        switch(IdExp){
            /*case TH1_IFNGRKO: case IFNG_IFNGRKO: {
                m->initialise(Back::IFNGRKO); break;}*/
            default: {m->initialise(Back::WT); break;}
        }
        // here, no need to stabilization - m.stabilize(2*3600);
        //m->setPrintMode(true, 10800);
        #define Ldiff 3.1
        switch(IdExp){
        case TH0:{
                m->simulate(Ldiff*24, E); break;}
        case TH1: {
                m->setValue("IL12", 10.0);
                m->setValue("antiIL4", 10000.0);
                m->simulate(Ldiff*24, E); break;}
        case TH2: {
                m->setValue("IL4", 14.3);
                m->setValue("antiIFNg", 10000.0);
                //cout << "IL4 = " << m->getParam(149) << endl << endl;
                m->simulate(Ldiff*24, E); break;}
        case TREG: {
                m->setValue("TGFB", 0.5);
                m->setValue("IL2", 13.4);
                m->simulate(Ldiff*24, E); break;}
        case TH17: {
                m->setValue("IL6", 30.0);
                m->setValue("TGFB", 0.2);
                m->setValue("antiIL2", 5000.0);
                m->setValue("antiIFNg", 10000.0);
                m->simulate(Ldiff*24, E); break;}
        }
        m->setOverrider(nullptr);
    }
}


/*
names_exp[TH1_IFNG] = string("Th1 + IFNg activation on WT background (TCR + IL12 + IFNg)");
names_exp[IFNG_ALONE] = string("IFNg alone on WT background (TCR + IFNg)");
names_exp[TH1_IFNGRKO] = string("Th1 activation on IFNGR-/- background (TCR + IL12)");
names_exp[TH1_STAT4KO] = string("Th1 activation on STAT4-/- background (TCR + IL12)");
names_exp[TH1_TBETKO] = string("Th1 activation on Tbet-/- background (TCR + IL12)");
names_exp[IFNG_IFNGRKO] = string("Th0 activation with IFNg on IFNGR-/- background (TCR + IFNG)");
names_exp[IL4_IFNG_TGFB] = string("Activation with IL4 IFNG and TGFB on WT Background");
names_exp[TGFB_ALONE] = string("Th activation with TGFb on WT Background");
names_exp[TH1_IL2] = string("Th1 activation with IL2");
names_exp[MIXEDTH1_TH2] = string("Th1-Th2 activation");
names_exp[TH2IL2] = string("Th2 differentiation with IL2");
names_exp[TH17IL2] = string("Th17 differentiation with IL2"); */


/* doable[TH1_IFNG]    = minTh1 && m->isVarKnown(N::IFNG);
 doable[IFNG_ALONE]  = minTh1 && m->isVarKnown(N::IFNG);
 doable[TH1_IFNGRKO] = minTh1 && m->isSimuBack(Back::IFNGRKO);
 doable[TH1_STAT4KO] = minTh1 && m->isSimuBack(Back::STAT4KO);
 doable[TH1_TBETKO]  = minTh1 && m->isSimuBack(Back::TBETKO);
 doable[IFNG_IFNGRKO]= minTh1 && m->isVarKnown(N::IFNG) && m->isSimuBack(Back::IFNGRKO);*/



/*    doable[IL4_IFNG_TGFB]   = m->isVarKnown(N::IFNG) && m->isVarKnown(N::IL4) && m->isVarKnown(N::TGFB);
    doable[TGFB_ALONE]      = m->isVarKnown(N::TGFB);
    doable[TH1_IL2]         = minTh1 && m->isVarKnown(N::IL2);
    doable[MIXEDTH1_TH2]    = minTh1 && minTh2;*/

//    doable[TH2IL2]          = minTh2 && minTh2;

//    doable[TH17IL2]         = minTh17 && m->isVarKnown(N::IL2);

/*case TH1_IFNG: {
        m->setValue(GlobalName(N::IL12), 1.0);
        m->setValue(N::IFNG, 1.0);
        m->simulate(Ldiff*24*3600, E); break;}
case IFNG_ALONE: case IFNG_IFNGRKO: {
        m->setValue(N::IFNG, 1.0);
        m->simulate(Ldiff*24*3600, E); break;}
case IL4_IFNG_TGFB: {
        m->setValue(GlobalName(N::IL4), 1.0);
        m->setValue(N::IFNG, 1.0);
        m->setValue(GlobalName(N::TGFB), 1.0);
        m->simulate(Ldiff*24*3600, E); break;}
case TGFB_ALONE: {
        m->setValue(GlobalName(N::TGFB), 1.0);
        m->simulate(Ldiff*24*3600, E); break;}
case TH1_IL2: {
        m->setValue(GlobalName(N::IL12), 1.0);
        m->setValue(GlobalName(N::IL2), 1.0);
        m->simulate(Ldiff*24*3600, E); break;}
case MIXEDTH1_TH2: {
        m->setValue(GlobalName(N::IL4), 1.0);
        m->setValue(GlobalName(N::IL12), 1.0);
        m->simulate(Ldiff*24*3600, E); break;}*/
/*
 *         case TH2IL2: {
                m->setValue(GlobalName(N::IL4), 1.0);
                m->setValue(GlobalName(N::IL2), 1.0);
                m->simulate(Ldiff*24*3600, E); break;}

        case TH17IL2: {
                m->setValue(GlobalName(N::IL6), 1.0);
                m->setValue(GlobalName(N::TGFB), 1.0);
                m->setValue(GlobalName(N::IL2), 1.0);
                m->simulate(Ldiff*24*3600, E); break;}
                */
