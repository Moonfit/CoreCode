// ------- Automatically generated model -------- //

//#ifdef COMPILE_AUTOGEN, ok, trustable
#include "modeleThTot5custom.h"

ModeleThTot5::ModeleThTot5() : Model(NBVAR, NBPARAM), background(Back::WT) {
    name = string("modeleThTot5");
    dt = 0.2;
    print_every_dt = 1200;
    names[IL2] = string("IL2");
    names[IL4] = string("IL4");
    names[IL6] = string("IL6");
    names[IL10] = string("IL10");
    names[IL12] = string("IL12");
    names[IL17] = string("IL17");
    names[IL21] = string("IL21");
    names[IFNG] = string("IFNG");
    names[TGFB] = string("TGFB");
    names[TBET] = string("TBET");
    names[GATA3] = string("GATA3");
    names[RORGT] = string("RORGT");
    names[FOXP3] = string("FOXP3");
    names[TCR] = string("TCR");
    // the names of variables that can be accessed by outside (by setValue and getValue)
    extNames[IL2] = GlobalName(N::IL2);
    extNames[IL4] = GlobalName(N::IL4);
    extNames[IL6] = GlobalName(N::IL6);
    extNames[IL10] = GlobalName(N::IL10);
    extNames[IL12] = GlobalName(N::IL12);
    extNames[IL17] = GlobalName(N::IL17);
    extNames[IL21] = GlobalName(N::IL21);
    extNames[IFNG] = GlobalName(N::IFNG);
    extNames[TGFB] = GlobalName(N::TGFB);
    extNames[TBET] = GlobalName(N::TBET);
    extNames[GATA3] = GlobalName(N::GATA3);
    extNames[RORGT] = GlobalName(N::RORGT);
    extNames[FOXP3] = GlobalName(N::FOXP3);
    extNames[TCR] = GlobalName(N::TCR);

    paramNames[TCRPEAK] = "XTCRPEAK";
    paramNames[TCRCOEFF] = "TCRCOEFF";
    paramNames[KTCRGATA3] = "KTCRGATA3";
    paramNames[KDIL2] = "KDIL2";
    paramNames[KDIL4] = "KDIL4";
    paramNames[KDIL6] = "KDIL6";
    paramNames[KDIL10] = "KDIL10";
    paramNames[KDIL12] = "KDIL12";
    paramNames[KDIL17] = "KDIL17";
    paramNames[KDIL21] = "KDIL21";
    paramNames[KDIFNG] = "KDIFNG";
    paramNames[KDTGFB] = "KDTGFB";
    paramNames[KDTBET] = "KDTBET";
    paramNames[KDGATA3] = "KDGATA3";
    paramNames[KDRORGT] = "KDRORGT";
    paramNames[KDFOXP3] = "KDFOXP3";
    paramNames[IL2EQ] = "IL2EQ";
    paramNames[IL4EQ] = "IL4EQ";
    paramNames[IL10EQ] = "IL10EQ";
    paramNames[IL17EQ] = "IL17EQ";
    paramNames[IL21EQ] = "IL21EQ";
    paramNames[IFNGEQ] = "IFNGEQ";
    paramNames[TGFBEQ] = "TGFBEQ";
    paramNames[TBETEQ] = "TBETEQ";
    paramNames[GATA3EQ] = "GATA3EQ";
    paramNames[RORGTEQ] = "RORGTEQ";
    paramNames[FOXP3EQ] = "FOXP3EQ";
    paramNames[CIL2] = "CIL2";
    paramNames[KIL2_TO_IL2] = "KIL2_TO_IL2";
    paramNames[NIL2_TO_IL2] = "NIL2_TO_IL2";
    paramNames[CIL4] = "CIL4";
    paramNames[KGATA3_TO_IL4] = "KGATA3_TO_IL4";
    paramNames[NGATA3_TO_IL4] = "NGATA3_TO_IL4";
    paramNames[CIL10] = "CIL10";
    paramNames[KTBET_TO_IL10] = "KTBET_TO_IL10";
    paramNames[NTBET_TO_IL10] = "NTBET_TO_IL10";
    paramNames[KGATA3_TO_IL10] = "KGATA3_TO_IL10";
    paramNames[NGATA3_TO_IL10] = "NGATA3_TO_IL10";
    paramNames[KRORGT_TO_IL10] = "KRORGT_TO_IL10";
    paramNames[NRORGT_TO_IL10] = "NRORGT_TO_IL10";
    paramNames[KFOXP3_TO_IL10] = "KFOXP3_TO_IL10";
    paramNames[NFOXP3_TO_IL10] = "NFOXP3_TO_IL10";
    paramNames[CIL17] = "CIL17";
    paramNames[KRORGT_TO_IL17] = "KRORGT_TO_IL17";
    paramNames[NRORGT_TO_IL17] = "NRORGT_TO_IL17";
    paramNames[CIL21] = "CIL21";
    paramNames[KRORGT_TO_IL21] = "KRORGT_TO_IL21";
    paramNames[NRORGT_TO_IL21] = "NRORGT_TO_IL21";
    paramNames[CIFNG] = "CIFNG";
    paramNames[KTBET_TO_IFNG] = "KTBET_TO_IFNG";
    paramNames[NTBET_TO_IFNG] = "NTBET_TO_IFNG";
    paramNames[CTGFB] = "CTGFB";
    paramNames[KRORGT_TO_TGFB] = "KRORGT_TO_TGFB";
    paramNames[NRORGT_TO_TGFB] = "NRORGT_TO_TGFB";
    paramNames[KFOXP3_TO_TGFB] = "KFOXP3_TO_TGFB";
    paramNames[NFOXP3_TO_TGFB] = "NFOXP3_TO_TGFB";
    paramNames[CTBET] = "CTBET";
    paramNames[KIL10_TO_TBET] = "KIL10_TO_TBET";
    paramNames[NIL10_TO_TBET] = "NIL10_TO_TBET";
    paramNames[KIL12_TO_TBET] = "KIL12_TO_TBET";
    paramNames[NIL12_TO_TBET] = "NIL12_TO_TBET";
    paramNames[KIFNG_TO_TBET] = "KIFNG_TO_TBET";
    paramNames[NIFNG_TO_TBET] = "NIFNG_TO_TBET";
    paramNames[KGATA3_TO_TBET] = "KGATA3_TO_TBET";
    paramNames[NGATA3_TO_TBET] = "NGATA3_TO_TBET";
    paramNames[KRORGT_TO_TBET] = "KRORGT_TO_TBET";
    paramNames[NRORGT_TO_TBET] = "NRORGT_TO_TBET";
    paramNames[KFOXP3_TO_TBET] = "KFOXP3_TO_TBET";
    paramNames[NFOXP3_TO_TBET] = "NFOXP3_TO_TBET";
    paramNames[CGATA3] = "CGATA3";
    paramNames[KIL2_TO_GATA3] = "KIL2_TO_GATA3";
    paramNames[NIL2_TO_GATA3] = "NIL2_TO_GATA3";
    paramNames[KIL4_TO_GATA3] = "KIL4_TO_GATA3";
    paramNames[NIL4_TO_GATA3] = "NIL4_TO_GATA3";
    paramNames[KIL10_TO_GATA3] = "KIL10_TO_GATA3";
    paramNames[NIL10_TO_GATA3] = "NIL10_TO_GATA3";
    paramNames[KTBET_TO_GATA3] = "KTBET_TO_GATA3";
    paramNames[NTBET_TO_GATA3] = "NTBET_TO_GATA3";
    paramNames[KGATA3_TO_GATA3] = "KGATA3_TO_GATA3";
    paramNames[NGATA3_TO_GATA3] = "NGATA3_TO_GATA3";
    paramNames[KRORGT_TO_GATA3] = "KRORGT_TO_GATA3";
    paramNames[NRORGT_TO_GATA3] = "NRORGT_TO_GATA3";
    paramNames[KFOXP3_TO_GATA3] = "KFOXP3_TO_GATA3";
    paramNames[NFOXP3_TO_GATA3] = "NFOXP3_TO_GATA3";
    paramNames[CRORGT] = "CRORGT";
    paramNames[KIL6_TO_RORGT] = "KIL6_TO_RORGT";
    paramNames[NIL6_TO_RORGT] = "NIL6_TO_RORGT";
    paramNames[KIL10_TO_RORGT] = "KIL10_TO_RORGT";
    paramNames[NIL10_TO_RORGT] = "NIL10_TO_RORGT";
    paramNames[KIL21_TO_RORGT] = "KIL21_TO_RORGT";
    paramNames[NIL21_TO_RORGT] = "NIL21_TO_RORGT";
    paramNames[KTGFB_TO_RORGT] = "KTGFB_TO_RORGT";
    paramNames[NTGFB_TO_RORGT] = "NTGFB_TO_RORGT";
    paramNames[KTBET_TO_RORGT] = "KTBET_TO_RORGT";
    paramNames[NTBET_TO_RORGT] = "NTBET_TO_RORGT";
    paramNames[KGATA3_TO_RORGT] = "KGATA3_TO_RORGT";
    paramNames[NGATA3_TO_RORGT] = "NGATA3_TO_RORGT";
    paramNames[KFOXP3_TO_RORGT] = "KFOXP3_TO_RORGT";
    paramNames[NFOXP3_TO_RORGT] = "NFOXP3_TO_RORGT";
    paramNames[CFOXP3] = "CFOXP3";
    paramNames[KIL2_TO_FOXP3] = "KIL2_TO_FOXP3";
    paramNames[NIL2_TO_FOXP3] = "NIL2_TO_FOXP3";
    paramNames[KIL10_TO_FOXP3] = "KIL10_TO_FOXP3";
    paramNames[NIL10_TO_FOXP3] = "NIL10_TO_FOXP3";
    paramNames[KTGFB_TO_FOXP3] = "KTGFB_TO_FOXP3";
    paramNames[NTGFB_TO_FOXP3] = "NTGFB_TO_FOXP3";
    paramNames[KTBET_TO_FOXP3] = "KTBET_TO_FOXP3";
    paramNames[NTBET_TO_FOXP3] = "NTBET_TO_FOXP3";
    paramNames[KGATA3_TO_FOXP3] = "KGATA3_TO_FOXP3";
    paramNames[NGATA3_TO_FOXP3] = "NGATA3_TO_FOXP3";
    paramNames[KRORGT_TO_FOXP3] = "KRORGT_TO_FOXP3";
    paramNames[NRORGT_TO_FOXP3] = "NRORGT_TO_FOXP3";
    paramNames[BIL2] = "BIL2";
    paramNames[BIL4] = "BIL4";
    paramNames[BIL10] = "BIL10";
    paramNames[BIL17] = "BIL17";
    paramNames[BIL21] = "BIL21";
    paramNames[BIFNG] = "BIFNG";
    paramNames[BTGFB] = "BTGFB";
    paramNames[BTBET] = "BTBET";
    paramNames[BGATA3] = "BGATA3";
    paramNames[BRORGT] = "BRORGT";
    paramNames[BFOXP3] = "BFOXP3";
    paramLowBounds[TCRPEAK] = 0.01;         paramUpBounds[TCRPEAK] 	= 20;
    paramLowBounds[TCRCOEFF]= 0.1;          paramUpBounds[TCRCOEFF] = 100;
    paramLowBounds[KTCRGATA3]= 0.01;        paramUpBounds[KTCRGATA3] = 50;
    paramLowBounds[KDIL2] 	= 1e-005;		paramUpBounds[KDIL2] 	= 0.1;
    paramLowBounds[KDIL4] 	= 1e-005;		paramUpBounds[KDIL4] 	= 0.1;
    paramLowBounds[KDIL6] 	= 1e-005;		paramUpBounds[KDIL6] 	= 0.1;
    paramLowBounds[KDIL10] 	= 1e-005;		paramUpBounds[KDIL10] 	= 0.1;
    paramLowBounds[KDIL12] 	= 1e-005;		paramUpBounds[KDIL12] 	= 0.1;
    paramLowBounds[KDIL17] 	= 1e-005;		paramUpBounds[KDIL17] 	= 0.1;
    paramLowBounds[KDIL21] 	= 1e-005;		paramUpBounds[KDIL21] 	= 0.1;
    paramLowBounds[KDIFNG] 	= 1e-005;		paramUpBounds[KDIFNG] 	= 0.1;
    paramLowBounds[KDTGFB] 	= 1e-005;		paramUpBounds[KDTGFB] 	= 0.1;
    paramLowBounds[KDTBET] 	= 1e-006;		paramUpBounds[KDTBET] 	= 0.1;
    paramLowBounds[KDGATA3] 	= 1e-006;		paramUpBounds[KDGATA3] 	= 0.1;
    paramLowBounds[KDRORGT] 	= 1e-006;		paramUpBounds[KDRORGT] 	= 0.1;
    paramLowBounds[KDFOXP3] 	= 1e-006;		paramUpBounds[KDFOXP3] 	= 0.1;
    paramLowBounds[IL2EQ] 	= 0.01;		paramUpBounds[IL2EQ] 	= 0.25;
    paramLowBounds[IL4EQ] 	= 0.01;		paramUpBounds[IL4EQ] 	= 0.25;
    paramLowBounds[IL10EQ] 	= 0.01;		paramUpBounds[IL10EQ] 	= 0.25;
    paramLowBounds[IL17EQ] 	= 0.01;		paramUpBounds[IL17EQ] 	= 0.25;
    paramLowBounds[IL21EQ] 	= 0.01;		paramUpBounds[IL21EQ] 	= 0.25;
    paramLowBounds[IFNGEQ] 	= 0.01;		paramUpBounds[IFNGEQ] 	= 0.25;
    paramLowBounds[TGFBEQ] 	= 0.01;		paramUpBounds[TGFBEQ] 	= 0.25;
    paramLowBounds[TBETEQ] 	= 0.01;		paramUpBounds[TBETEQ] 	= 0.25;
    paramLowBounds[GATA3EQ] 	= 0.01;		paramUpBounds[GATA3EQ] 	= 0.25;
    paramLowBounds[RORGTEQ] 	= 0.01;		paramUpBounds[RORGTEQ] 	= 0.25;
    paramLowBounds[FOXP3EQ] 	= 0.01;		paramUpBounds[FOXP3EQ] 	= 0.25;
    paramLowBounds[CIL2] 	= 1e-005;		paramUpBounds[CIL2] 	= 1;
    paramLowBounds[KIL2_TO_IL2] 	= 0.01;		paramUpBounds[KIL2_TO_IL2] 	= 1;
    paramLowBounds[NIL2_TO_IL2] 	= 0.25;		paramUpBounds[NIL2_TO_IL2] 	= 4;
    paramLowBounds[CIL4] 	= 1e-005;		paramUpBounds[CIL4] 	= 1;
    paramLowBounds[KGATA3_TO_IL4] 	= 0.01;		paramUpBounds[KGATA3_TO_IL4] 	= 1;
    paramLowBounds[NGATA3_TO_IL4] 	= 0.25;		paramUpBounds[NGATA3_TO_IL4] 	= 4;
    paramLowBounds[CIL10] 	= 1e-005;		paramUpBounds[CIL10] 	= 1;
    paramLowBounds[KTBET_TO_IL10] 	= 0.01;		paramUpBounds[KTBET_TO_IL10] 	= 1;
    paramLowBounds[NTBET_TO_IL10] 	= 0.25;		paramUpBounds[NTBET_TO_IL10] 	= 4;
    paramLowBounds[KGATA3_TO_IL10] 	= 0.01;		paramUpBounds[KGATA3_TO_IL10] 	= 1;
    paramLowBounds[NGATA3_TO_IL10] 	= 0.25;		paramUpBounds[NGATA3_TO_IL10] 	= 4;
    paramLowBounds[KRORGT_TO_IL10] 	= 0.01;		paramUpBounds[KRORGT_TO_IL10] 	= 1;
    paramLowBounds[NRORGT_TO_IL10] 	= 0.25;		paramUpBounds[NRORGT_TO_IL10] 	= 4;
    paramLowBounds[KFOXP3_TO_IL10] 	= 0.01;		paramUpBounds[KFOXP3_TO_IL10] 	= 1;
    paramLowBounds[NFOXP3_TO_IL10] 	= 0.25;		paramUpBounds[NFOXP3_TO_IL10] 	= 4;
    paramLowBounds[CIL17] 	= 1e-005;		paramUpBounds[CIL17] 	= 1;
    paramLowBounds[KRORGT_TO_IL17] 	= 0.01;		paramUpBounds[KRORGT_TO_IL17] 	= 1;
    paramLowBounds[NRORGT_TO_IL17] 	= 0.25;		paramUpBounds[NRORGT_TO_IL17] 	= 4;
    paramLowBounds[CIL21] 	= 1e-005;		paramUpBounds[CIL21] 	= 1;
    paramLowBounds[KRORGT_TO_IL21] 	= 0.01;		paramUpBounds[KRORGT_TO_IL21] 	= 1;
    paramLowBounds[NRORGT_TO_IL21] 	= 0.25;		paramUpBounds[NRORGT_TO_IL21] 	= 4;
    paramLowBounds[CIFNG] 	= 1e-005;		paramUpBounds[CIFNG] 	= 1;
    paramLowBounds[KTBET_TO_IFNG] 	= 0.01;		paramUpBounds[KTBET_TO_IFNG] 	= 1;
    paramLowBounds[NTBET_TO_IFNG] 	= 0.25;		paramUpBounds[NTBET_TO_IFNG] 	= 4;
    paramLowBounds[CTGFB] 	= 1e-005;		paramUpBounds[CTGFB] 	= 1;
    paramLowBounds[KRORGT_TO_TGFB] 	= 0.01;		paramUpBounds[KRORGT_TO_TGFB] 	= 1;
    paramLowBounds[NRORGT_TO_TGFB] 	= 0.25;		paramUpBounds[NRORGT_TO_TGFB] 	= 4;
    paramLowBounds[KFOXP3_TO_TGFB] 	= 0.01;		paramUpBounds[KFOXP3_TO_TGFB] 	= 1;
    paramLowBounds[NFOXP3_TO_TGFB] 	= 0.25;		paramUpBounds[NFOXP3_TO_TGFB] 	= 4;
    paramLowBounds[CTBET] 	= 1e-005;		paramUpBounds[CTBET] 	= 1;
    paramLowBounds[KIL10_TO_TBET] 	= 0.01;		paramUpBounds[KIL10_TO_TBET] 	= 1;
    paramLowBounds[NIL10_TO_TBET] 	= 0.25;		paramUpBounds[NIL10_TO_TBET] 	= 4;
    paramLowBounds[KIL12_TO_TBET] 	= 0.01;		paramUpBounds[KIL12_TO_TBET] 	= 1;
    paramLowBounds[NIL12_TO_TBET] 	= 0.25;		paramUpBounds[NIL12_TO_TBET] 	= 4;
    paramLowBounds[KIFNG_TO_TBET] 	= 0.01;		paramUpBounds[KIFNG_TO_TBET] 	= 1;
    paramLowBounds[NIFNG_TO_TBET] 	= 0.25;		paramUpBounds[NIFNG_TO_TBET] 	= 4;
    paramLowBounds[KGATA3_TO_TBET] 	= 0.01;		paramUpBounds[KGATA3_TO_TBET] 	= 1;
    paramLowBounds[NGATA3_TO_TBET] 	= 0.25;		paramUpBounds[NGATA3_TO_TBET] 	= 4;
    paramLowBounds[KRORGT_TO_TBET] 	= 0.01;		paramUpBounds[KRORGT_TO_TBET] 	= 1;
    paramLowBounds[NRORGT_TO_TBET] 	= 0.25;		paramUpBounds[NRORGT_TO_TBET] 	= 4;
    paramLowBounds[KFOXP3_TO_TBET] 	= 0.01;		paramUpBounds[KFOXP3_TO_TBET] 	= 1;
    paramLowBounds[NFOXP3_TO_TBET] 	= 0.25;		paramUpBounds[NFOXP3_TO_TBET] 	= 4;
    paramLowBounds[CGATA3] 	= 1e-005;		paramUpBounds[CGATA3] 	= 1;
    paramLowBounds[KIL2_TO_GATA3] 	= 0.01;		paramUpBounds[KIL2_TO_GATA3] 	= 1;
    paramLowBounds[NIL2_TO_GATA3] 	= 0.25;		paramUpBounds[NIL2_TO_GATA3] 	= 4;
    paramLowBounds[KIL4_TO_GATA3] 	= 0.01;		paramUpBounds[KIL4_TO_GATA3] 	= 1;
    paramLowBounds[NIL4_TO_GATA3] 	= 0.25;		paramUpBounds[NIL4_TO_GATA3] 	= 4;
    paramLowBounds[KIL10_TO_GATA3] 	= 0.01;		paramUpBounds[KIL10_TO_GATA3] 	= 1;
    paramLowBounds[NIL10_TO_GATA3] 	= 0.25;		paramUpBounds[NIL10_TO_GATA3] 	= 4;
    paramLowBounds[KTBET_TO_GATA3] 	= 0.01;		paramUpBounds[KTBET_TO_GATA3] 	= 1;
    paramLowBounds[NTBET_TO_GATA3] 	= 0.25;		paramUpBounds[NTBET_TO_GATA3] 	= 4;
    paramLowBounds[KGATA3_TO_GATA3] 	= 0.01;		paramUpBounds[KGATA3_TO_GATA3] 	= 1;
    paramLowBounds[NGATA3_TO_GATA3] 	= 0.25;		paramUpBounds[NGATA3_TO_GATA3] 	= 4;
    paramLowBounds[KRORGT_TO_GATA3] 	= 0.01;		paramUpBounds[KRORGT_TO_GATA3] 	= 1;
    paramLowBounds[NRORGT_TO_GATA3] 	= 0.25;		paramUpBounds[NRORGT_TO_GATA3] 	= 4;
    paramLowBounds[KFOXP3_TO_GATA3] 	= 0.01;		paramUpBounds[KFOXP3_TO_GATA3] 	= 1;
    paramLowBounds[NFOXP3_TO_GATA3] 	= 0.25;		paramUpBounds[NFOXP3_TO_GATA3] 	= 4;
    paramLowBounds[CRORGT] 	= 1e-005;		paramUpBounds[CRORGT] 	= 1;
    paramLowBounds[KIL6_TO_RORGT] 	= 0.01;		paramUpBounds[KIL6_TO_RORGT] 	= 1;
    paramLowBounds[NIL6_TO_RORGT] 	= 0.25;		paramUpBounds[NIL6_TO_RORGT] 	= 4;
    paramLowBounds[KIL10_TO_RORGT] 	= 0.01;		paramUpBounds[KIL10_TO_RORGT] 	= 1;
    paramLowBounds[NIL10_TO_RORGT] 	= 0.25;		paramUpBounds[NIL10_TO_RORGT] 	= 4;
    paramLowBounds[KIL21_TO_RORGT] 	= 0.01;		paramUpBounds[KIL21_TO_RORGT] 	= 1;
    paramLowBounds[NIL21_TO_RORGT] 	= 0.25;		paramUpBounds[NIL21_TO_RORGT] 	= 4;
    paramLowBounds[KTGFB_TO_RORGT] 	= 0.01;		paramUpBounds[KTGFB_TO_RORGT] 	= 1;
    paramLowBounds[NTGFB_TO_RORGT] 	= 0.25;		paramUpBounds[NTGFB_TO_RORGT] 	= 4;
    paramLowBounds[KTBET_TO_RORGT] 	= 0.01;		paramUpBounds[KTBET_TO_RORGT] 	= 1;
    paramLowBounds[NTBET_TO_RORGT] 	= 0.25;		paramUpBounds[NTBET_TO_RORGT] 	= 4;
    paramLowBounds[KGATA3_TO_RORGT] 	= 0.01;		paramUpBounds[KGATA3_TO_RORGT] 	= 1;
    paramLowBounds[NGATA3_TO_RORGT] 	= 0.25;		paramUpBounds[NGATA3_TO_RORGT] 	= 4;
    paramLowBounds[KFOXP3_TO_RORGT] 	= 0.01;		paramUpBounds[KFOXP3_TO_RORGT] 	= 1;
    paramLowBounds[NFOXP3_TO_RORGT] 	= 0.25;		paramUpBounds[NFOXP3_TO_RORGT] 	= 4;
    paramLowBounds[CFOXP3] 	= 1e-005;		paramUpBounds[CFOXP3] 	= 1;
    paramLowBounds[KIL2_TO_FOXP3] 	= 0.01;		paramUpBounds[KIL2_TO_FOXP3] 	= 1;
    paramLowBounds[NIL2_TO_FOXP3] 	= 0.25;		paramUpBounds[NIL2_TO_FOXP3] 	= 4;
    paramLowBounds[KIL10_TO_FOXP3] 	= 0.01;		paramUpBounds[KIL10_TO_FOXP3] 	= 1;
    paramLowBounds[NIL10_TO_FOXP3] 	= 0.25;		paramUpBounds[NIL10_TO_FOXP3] 	= 4;
    paramLowBounds[KTGFB_TO_FOXP3] 	= 0.01;		paramUpBounds[KTGFB_TO_FOXP3] 	= 1;
    paramLowBounds[NTGFB_TO_FOXP3] 	= 0.25;		paramUpBounds[NTGFB_TO_FOXP3] 	= 4;
    paramLowBounds[KTBET_TO_FOXP3] 	= 0.01;		paramUpBounds[KTBET_TO_FOXP3] 	= 1;
    paramLowBounds[NTBET_TO_FOXP3] 	= 0.25;		paramUpBounds[NTBET_TO_FOXP3] 	= 4;
    paramLowBounds[KGATA3_TO_FOXP3] 	= 0.01;		paramUpBounds[KGATA3_TO_FOXP3] 	= 1;
    paramLowBounds[NGATA3_TO_FOXP3] 	= 0.25;		paramUpBounds[NGATA3_TO_FOXP3] 	= 4;
    paramLowBounds[KRORGT_TO_FOXP3] 	= 0.01;		paramUpBounds[KRORGT_TO_FOXP3] 	= 1;
    paramLowBounds[NRORGT_TO_FOXP3] 	= 0.25;		paramUpBounds[NRORGT_TO_FOXP3] 	= 4;


}

void ModeleThTot5::setBaseParameters(){
    params.clear();     // to make sure they are all put to zero
    params.resize(NBPARAM, 0.0);
    params[TCRPEAK] = 1.5;
    params[TCRCOEFF] = 10;
    params[KTCRGATA3] = 1;
    params[KDIL2] = 1e-1;
    params[KDIL4] = 1e-1;
    params[KDIL6] = 1e-1;
    params[KDIL10] = 1e-1;
    params[KDIL12] = 1e-1;
    params[KDIL17] = 1e-1;
    params[KDIL21] = 1e-1;
    params[KDIFNG] = 1e-1;
    params[KDTGFB] = 1e-1;
    params[KDTBET] = 1e-1;
    params[KDGATA3] = 1e-1;
    params[KDRORGT] = 1e-1;
    params[KDFOXP3] = 1e-1;
    params[IL2EQ] = 1e-1;
    params[IL4EQ] = 1e-1;
    params[IL10EQ] = 1e-1;
    params[IL17EQ] = 1e-1;
    params[IL21EQ] = 1e-1;
    params[IFNGEQ] = 1e-1;
    params[TGFBEQ] = 1e-1;
    params[TBETEQ] = 1e-1;
    params[GATA3EQ] = 1e-1;
    params[RORGTEQ] = 1e-1;
    params[FOXP3EQ] = 1e-1;
    params[CIL2] = 1e-1;
    params[KIL2_TO_IL2] = 1e-1;
    params[NIL2_TO_IL2] = 1e-1;
    params[CIL4] = 1e-1;
    params[KGATA3_TO_IL4] = 1e-1;
    params[NGATA3_TO_IL4] = 1e-1;
    params[CIL10] = 1e-1;
    params[KTBET_TO_IL10] = 1e-1;
    params[NTBET_TO_IL10] = 1e-1;
    params[KGATA3_TO_IL10] = 1e-1;
    params[NGATA3_TO_IL10] = 1e-1;
    params[KRORGT_TO_IL10] = 1e-1;
    params[NRORGT_TO_IL10] = 1e-1;
    params[KFOXP3_TO_IL10] = 1e-1;
    params[NFOXP3_TO_IL10] = 1e-1;
    params[CIL17] = 1e-1;
    params[KRORGT_TO_IL17] = 1e-1;
    params[NRORGT_TO_IL17] = 1e-1;
    params[CIL21] = 1e-1;
    params[KRORGT_TO_IL21] = 1e-1;
    params[NRORGT_TO_IL21] = 1e-1;
    params[CIFNG] = 1e-1;
    params[KTBET_TO_IFNG] = 1e-1;
    params[NTBET_TO_IFNG] = 1e-1;
    params[CTGFB] = 1e-1;
    params[KRORGT_TO_TGFB] = 1e-1;
    params[NRORGT_TO_TGFB] = 1e-1;
    params[KFOXP3_TO_TGFB] = 1e-1;
    params[NFOXP3_TO_TGFB] = 1e-1;
    params[CTBET] = 1e-1;
    params[KIL10_TO_TBET] = 1e-1;
    params[NIL10_TO_TBET] = 1e-1;
    params[KIL12_TO_TBET] = 1e-1;
    params[NIL12_TO_TBET] = 1e-1;
    params[KIFNG_TO_TBET] = 1e-1;
    params[NIFNG_TO_TBET] = 1e-1;
    params[KGATA3_TO_TBET] = 1e-1;
    params[NGATA3_TO_TBET] = 1e-1;
    params[KRORGT_TO_TBET] = 1e-1;
    params[NRORGT_TO_TBET] = 1e-1;
    params[KFOXP3_TO_TBET] = 1e-1;
    params[NFOXP3_TO_TBET] = 1e-1;
    params[CGATA3] = 1e-1;
    params[KIL2_TO_GATA3] = 1e-1;
    params[NIL2_TO_GATA3] = 1e-1;
    params[KIL4_TO_GATA3] = 1e-1;
    params[NIL4_TO_GATA3] = 1e-1;
    params[KIL10_TO_GATA3] = 1e-1;
    params[NIL10_TO_GATA3] = 1e-1;
    params[KTBET_TO_GATA3] = 1e-1;
    params[NTBET_TO_GATA3] = 1e-1;
    params[KGATA3_TO_GATA3] = 1e-1;
    params[NGATA3_TO_GATA3] = 1e-1;
    params[KRORGT_TO_GATA3] = 1e-1;
    params[NRORGT_TO_GATA3] = 1e-1;
    params[KFOXP3_TO_GATA3] = 1e-1;
    params[NFOXP3_TO_GATA3] = 1e-1;
    params[CRORGT] = 1e-1;
    params[KIL6_TO_RORGT] = 1e-1;
    params[NIL6_TO_RORGT] = 1e-1;
    params[KIL10_TO_RORGT] = 1e-1;
    params[NIL10_TO_RORGT] = 1e-1;
    params[KIL21_TO_RORGT] = 1e-1;
    params[NIL21_TO_RORGT] = 1e-1;
    params[KTGFB_TO_RORGT] = 1e-1;
    params[NTGFB_TO_RORGT] = 1e-1;
    params[KTBET_TO_RORGT] = 1e-1;
    params[NTBET_TO_RORGT] = 1e-1;
    params[KGATA3_TO_RORGT] = 1e-1;
    params[NGATA3_TO_RORGT] = 1e-1;
    params[KFOXP3_TO_RORGT] = 1e-1;
    params[NFOXP3_TO_RORGT] = 1e-1;
    params[CFOXP3] = 1e-1;
    params[KIL2_TO_FOXP3] = 1e-1;
    params[NIL2_TO_FOXP3] = 1e-1;
    params[KIL10_TO_FOXP3] = 1e-1;
    params[NIL10_TO_FOXP3] = 1e-1;
    params[KTGFB_TO_FOXP3] = 1e-1;
    params[NTGFB_TO_FOXP3] = 1e-1;
    params[KTBET_TO_FOXP3] = 1e-1;
    params[NTBET_TO_FOXP3] = 1e-1;
    params[KGATA3_TO_FOXP3] = 1e-1;
    params[NGATA3_TO_FOXP3] = 1e-1;
    params[KRORGT_TO_FOXP3] = 1e-1;
    params[NRORGT_TO_FOXP3] = 1e-1;
    params[BIL2] = 1e-1;
    params[BIL4] = 1e-1;
    params[BIL10] = 1e-1;
    params[BIL17] = 1e-1;
    params[BIL21] = 1e-1;
    params[BIFNG] = 1e-1;
    params[BTGFB] = 1e-1;
    params[BTBET] = 1e-1;
    params[BGATA3] = 1e-1;
    params[BRORGT] = 1e-1;
    params[BFOXP3] = 1e-1;
    setBaseParametersDone();
}

void ModeleThTot5::initialise(long long _background){ // don't touch to parameters !
    background = _background;
    val.clear();
    val.resize(NBVAR, 0.0);
    init.clear();
    init.resize(NBVAR, 0.0);
    params[BIL2] = params[KDIL2] * params[IL2EQ]  - params[CIL2] * Activ0Inhib1(params[IL2EQ],params[KIL2_TO_IL2],params[NIL2_TO_IL2]);
    params[BIL4] = params[KDIL4] * params[IL4EQ]  - params[CIL4] * Activ1Inhib0(params[GATA3EQ],params[KGATA3_TO_IL4],params[NGATA3_TO_IL4]);
    params[BIL10] = params[KDIL10] * params[IL10EQ]  - params[CIL10] * Activ4Inhib0(params[TBETEQ],params[KTBET_TO_IL10],params[NTBET_TO_IL10],params[GATA3EQ],params[KGATA3_TO_IL10],params[NGATA3_TO_IL10],params[RORGTEQ],params[KRORGT_TO_IL10],params[NRORGT_TO_IL10],params[FOXP3EQ],params[KFOXP3_TO_IL10],params[NFOXP3_TO_IL10]);
    params[BIL17] = params[KDIL17] * params[IL17EQ]  - params[CIL17] * Activ1Inhib0(params[RORGTEQ],params[KRORGT_TO_IL17],params[NRORGT_TO_IL17]);
    params[BIL21] = params[KDIL21] * params[IL21EQ]  - params[CIL21] * Activ1Inhib0(params[RORGTEQ],params[KRORGT_TO_IL21],params[NRORGT_TO_IL21]);
    params[BIFNG] = params[KDIFNG] * params[IFNGEQ]  - params[CIFNG] * Activ1Inhib0(params[TBETEQ],params[KTBET_TO_IFNG],params[NTBET_TO_IFNG]);
    params[BTGFB] = params[KDTGFB] * params[TGFBEQ]  - params[CTGFB] * Activ2Inhib0(params[RORGTEQ],params[KRORGT_TO_TGFB],params[NRORGT_TO_TGFB],params[FOXP3EQ],params[KFOXP3_TO_TGFB],params[NFOXP3_TO_TGFB]);
    params[BTBET] = params[KDTBET] * params[TBETEQ]  - params[CTBET] * Activ2Inhib4(init[IL12], /* ERR : no equilibrium parameter for it */ params[KIL12_TO_TBET],params[NIL12_TO_TBET],params[IFNGEQ],params[KIFNG_TO_TBET],params[NIFNG_TO_TBET],params[IL10EQ],params[KIL10_TO_TBET],params[NIL10_TO_TBET],params[GATA3EQ],params[KGATA3_TO_TBET],params[NGATA3_TO_TBET],params[RORGTEQ],params[KRORGT_TO_TBET],params[NRORGT_TO_TBET],params[FOXP3EQ],params[KFOXP3_TO_TBET],params[NFOXP3_TO_TBET]);
    params[BGATA3] = params[KDGATA3] * params[GATA3EQ]  - params[CGATA3] * Activ3Inhib4(params[IL2EQ],params[KIL2_TO_GATA3],params[NIL2_TO_GATA3],params[IL4EQ],params[KIL4_TO_GATA3],params[NIL4_TO_GATA3],params[GATA3EQ],params[KGATA3_TO_GATA3],params[NGATA3_TO_GATA3],params[IL10EQ],params[KIL10_TO_GATA3],params[NIL10_TO_GATA3],params[TBETEQ],params[KTBET_TO_GATA3],params[NTBET_TO_GATA3],params[RORGTEQ],params[KRORGT_TO_GATA3],params[NRORGT_TO_GATA3],params[FOXP3EQ],params[KFOXP3_TO_GATA3],params[NFOXP3_TO_GATA3]);
    params[BRORGT] = params[KDRORGT] * params[RORGTEQ]  - params[CRORGT] * Activ3Inhib4(init[IL6], /* ERR : no equilibrium parameter for it */ params[KIL6_TO_RORGT],params[NIL6_TO_RORGT],params[IL21EQ],params[KIL21_TO_RORGT],params[NIL21_TO_RORGT],params[TGFBEQ],params[KTGFB_TO_RORGT],params[NTGFB_TO_RORGT],params[IL10EQ],params[KIL10_TO_RORGT],params[NIL10_TO_RORGT],params[TBETEQ],params[KTBET_TO_RORGT],params[NTBET_TO_RORGT],params[GATA3EQ],params[KGATA3_TO_RORGT],params[NGATA3_TO_RORGT],params[FOXP3EQ],params[KFOXP3_TO_RORGT],params[NFOXP3_TO_RORGT]);
    params[BFOXP3] = params[KDFOXP3] * params[FOXP3EQ]  - params[CFOXP3] * Activ3Inhib3(params[IL2EQ],params[KIL2_TO_FOXP3],params[NIL2_TO_FOXP3],params[IL10EQ],params[KIL10_TO_FOXP3],params[NIL10_TO_FOXP3],params[TGFBEQ],params[KTGFB_TO_FOXP3],params[NTGFB_TO_FOXP3],params[TBETEQ],params[KTBET_TO_FOXP3],params[NTBET_TO_FOXP3],params[GATA3EQ],params[KGATA3_TO_FOXP3],params[NGATA3_TO_FOXP3],params[RORGTEQ],params[KRORGT_TO_FOXP3],params[NRORGT_TO_FOXP3]);
    if(params[BIL2] < 0) params[BIL2] = 0;
    if(params[BIL4] < 0) params[BIL4] = 0;
    if(params[BIL10] < 0) params[BIL10] = 0;
    if(params[BIL17] < 0) params[BIL17] = 0;
    if(params[BIL21] < 0) params[BIL21] = 0;
    if(params[BIFNG] < 0) params[BIFNG] = 0;
    if(params[BTGFB] < 0) params[BTGFB] = 0;
    if(params[BTBET] < 0) params[BTBET] = 0;
    if(params[BGATA3] < 0) params[BGATA3] = 0;
    if(params[BRORGT] < 0) params[BRORGT] = 0;
    if(params[BFOXP3] < 0) params[BFOXP3] = 0;
    init[IL2] = params[IL2EQ];
    init[IL4] = params[IL4EQ];
    init[IL10] = params[IL10EQ];
    init[IL17] = params[IL17EQ];
    init[IL21] = params[IL21EQ];
    init[IFNG] = params[IFNGEQ];
    init[TGFB] = params[TGFBEQ];
    init[TBET] = params[TBETEQ];
    init[GATA3] = params[GATA3EQ];
    init[RORGT] = params[RORGTEQ];
    init[FOXP3] = params[FOXP3EQ];
    init[TCR] = 0;
    for(int i = 0; i < NBVAR; ++i){
        val[i] = init[i];}
    t = 0;
    initialiseDone();
}
#define TCRBASAL 1.0

void ModeleThTot5::derivatives(const vector<double> &x, vector<double> &dxdt, const double t){
    double LAMBDA = params[TCRCOEFF] / (params[TCRPEAK] * params[TCRCOEFF] - TCRBASAL);
    double tcrval = (-TCRBASAL + params[TCRCOEFF] * (t/3600.0)) * exp(-(t/3600.0) * LAMBDA) + TCRBASAL;
    if(!over(IL2))	dxdt[IL2] = (- params[KDIL2] * x[IL2] + params[BIL2] + params[CIL2] * tcrval * Activ0Inhib1(x[IL2],params[KIL2_TO_IL2],params[NIL2_TO_IL2]));
    if(!over(IL4))	dxdt[IL4] = (- params[KDIL4] * x[IL4] + params[BIL4] + params[CIL4] * Activ1Inhib0(x[GATA3],params[KGATA3_TO_IL4],params[NGATA3_TO_IL4]));
    if(!over(IL6))	dxdt[IL6] = (- params[KDIL6] * x[IL6] );
    if(!over(IL10))	dxdt[IL10] = (- params[KDIL10] * x[IL10] + params[BIL10] + params[CIL10] * Activ4Inhib0(x[TBET],params[KTBET_TO_IL10],params[NTBET_TO_IL10],x[GATA3],params[KGATA3_TO_IL10],params[NGATA3_TO_IL10],x[RORGT],params[KRORGT_TO_IL10],params[NRORGT_TO_IL10],x[FOXP3],params[KFOXP3_TO_IL10],params[NFOXP3_TO_IL10]));
    if(!over(IL12))	dxdt[IL12] = (- params[KDIL12] * x[IL12] );
    if(!over(IL17))	dxdt[IL17] = (- params[KDIL17] * x[IL17] + params[BIL17] + params[CIL17] * Activ1Inhib0(x[RORGT],params[KRORGT_TO_IL17],params[NRORGT_TO_IL17]));
    if(!over(IL21))	dxdt[IL21] = (- params[KDIL21] * x[IL21] + params[BIL21] + params[CIL21] * Activ1Inhib0(x[RORGT],params[KRORGT_TO_IL21],params[NRORGT_TO_IL21]));
    if(!over(IFNG))	dxdt[IFNG] = (- params[KDIFNG] * x[IFNG] + params[BIFNG] + params[CIFNG] * Activ1Inhib0(x[TBET],params[KTBET_TO_IFNG],params[NTBET_TO_IFNG]));
    if(!over(TGFB))	dxdt[TGFB] = (- params[KDTGFB] * x[TGFB] + params[BTGFB] + params[CTGFB] * tcrval * Activ2Inhib0(x[RORGT],params[KRORGT_TO_TGFB],params[NRORGT_TO_TGFB],x[FOXP3],params[KFOXP3_TO_TGFB],params[NFOXP3_TO_TGFB]));
    if(!over(TBET))	dxdt[TBET] = (- params[KDTBET] * x[TBET] + params[BTBET] + params[CTBET] * tcrval * Activ2Inhib4(x[IL12],params[KIL12_TO_TBET],params[NIL12_TO_TBET],x[IFNG],params[KIFNG_TO_TBET],params[NIFNG_TO_TBET],x[IL10],params[KIL10_TO_TBET],params[NIL10_TO_TBET],x[GATA3],params[KGATA3_TO_TBET],params[NGATA3_TO_TBET],x[RORGT],params[KRORGT_TO_TBET],params[NRORGT_TO_TBET],x[FOXP3],params[KFOXP3_TO_TBET],params[NFOXP3_TO_TBET]));
    if(!over(GATA3))	dxdt[GATA3] = (- params[KDGATA3] * x[GATA3] + params[BGATA3] * (params[KTCRGATA3] / (params[KTCRGATA3] + tcrval)) + params[CGATA3] * Activ2Inhib4(x[IL2],params[KIL2_TO_GATA3],params[NIL2_TO_GATA3],x[IL4],params[KIL4_TO_GATA3],params[NIL4_TO_GATA3],x[IL10],params[KIL10_TO_GATA3],params[NIL10_TO_GATA3],x[TBET],params[KTBET_TO_GATA3],params[NTBET_TO_GATA3],x[RORGT],params[KRORGT_TO_GATA3],params[NRORGT_TO_GATA3],x[FOXP3],params[KFOXP3_TO_GATA3],params[NFOXP3_TO_GATA3]));
    if(!over(RORGT))	dxdt[RORGT] = (- params[KDRORGT] * x[RORGT] + params[BRORGT] + params[CRORGT] * Activ3Inhib4(x[IL6],params[KIL6_TO_RORGT],params[NIL6_TO_RORGT],x[IL21],params[KIL21_TO_RORGT],params[NIL21_TO_RORGT],x[TGFB],params[KTGFB_TO_RORGT],params[NTGFB_TO_RORGT],x[IL10],params[KIL10_TO_RORGT],params[NIL10_TO_RORGT],x[TBET],params[KTBET_TO_RORGT],params[NTBET_TO_RORGT],x[GATA3],params[KGATA3_TO_RORGT],params[NGATA3_TO_RORGT],x[FOXP3],params[KFOXP3_TO_RORGT],params[NFOXP3_TO_RORGT]));
    if(!over(FOXP3))	dxdt[FOXP3] = (- params[KDFOXP3] * x[FOXP3] + params[BFOXP3] + params[CFOXP3] * Activ3Inhib3(x[IL2],params[KIL2_TO_FOXP3],params[NIL2_TO_FOXP3],x[IL10],params[KIL10_TO_FOXP3],params[NIL10_TO_FOXP3],x[TGFB],params[KTGFB_TO_FOXP3],params[NTGFB_TO_FOXP3],x[TBET],params[KTBET_TO_FOXP3],params[NTBET_TO_FOXP3],x[GATA3],params[KGATA3_TO_FOXP3],params[NGATA3_TO_FOXP3],x[RORGT],params[KRORGT_TO_FOXP3],params[NRORGT_TO_FOXP3]));
    if(!over(TCR))	dxdt[TCR] = -LAMBDA * (-TCRBASAL + params[TCRCOEFF] * (t /3600)) * exp(-(t /3600) * LAMBDA) + params[TCRCOEFF] * exp(-(t /3600) * LAMBDA);
    //question : est-ce qu'on met TCR->gata3 dans le calcul à l'équilibre ?
}
//#endif
