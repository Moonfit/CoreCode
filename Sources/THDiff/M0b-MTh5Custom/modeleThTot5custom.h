// ------- Automatically generated model -------- //
#ifndef MODELEThTot5_H
#define MODELEThTot5_H
#include "../Moonfit/moonfit.h"

//#ifdef COMPILE_AUTOGEN, ok, this one is trustable
#include "../namesTh.h"


/* Automatically generated modele */
struct ModeleThTot5 : public Model {
    ModeleThTot5();
    enum {IL2, IL4, IL6, IL10, IL12, IL17, IL21, IFNG, TGFB, TBET, GATA3, RORGT, FOXP3, TCR, NBVAR};
    enum {TCRPEAK, TCRCOEFF, KTCRGATA3, KDIL2, KDIL4, KDIL6, KDIL10, KDIL12, KDIL17, KDIL21, KDIFNG, KDTGFB, KDTBET, KDGATA3, KDRORGT, KDFOXP3, IL2EQ, IL4EQ, IL10EQ, IL17EQ, IL21EQ, IFNGEQ, TGFBEQ, TBETEQ, GATA3EQ, RORGTEQ, FOXP3EQ, CIL2, KIL2_TO_IL2, NIL2_TO_IL2, CIL4, KGATA3_TO_IL4, NGATA3_TO_IL4, CIL10, KTBET_TO_IL10, NTBET_TO_IL10, KGATA3_TO_IL10, NGATA3_TO_IL10, KRORGT_TO_IL10, NRORGT_TO_IL10, KFOXP3_TO_IL10, NFOXP3_TO_IL10, CIL17, KRORGT_TO_IL17, NRORGT_TO_IL17, CIL21, KRORGT_TO_IL21, NRORGT_TO_IL21, CIFNG, KTBET_TO_IFNG, NTBET_TO_IFNG, CTGFB, KRORGT_TO_TGFB, NRORGT_TO_TGFB, KFOXP3_TO_TGFB, NFOXP3_TO_TGFB, CTBET, KIL10_TO_TBET, NIL10_TO_TBET, KIL12_TO_TBET, NIL12_TO_TBET, KIFNG_TO_TBET, NIFNG_TO_TBET, KGATA3_TO_TBET, NGATA3_TO_TBET, KRORGT_TO_TBET, NRORGT_TO_TBET, KFOXP3_TO_TBET, NFOXP3_TO_TBET, CGATA3, KIL2_TO_GATA3, NIL2_TO_GATA3, KIL4_TO_GATA3, NIL4_TO_GATA3, KIL10_TO_GATA3, NIL10_TO_GATA3, KTBET_TO_GATA3, NTBET_TO_GATA3, KGATA3_TO_GATA3, NGATA3_TO_GATA3, KRORGT_TO_GATA3, NRORGT_TO_GATA3, KFOXP3_TO_GATA3, NFOXP3_TO_GATA3, CRORGT, KIL6_TO_RORGT, NIL6_TO_RORGT, KIL10_TO_RORGT, NIL10_TO_RORGT, KIL21_TO_RORGT, NIL21_TO_RORGT, KTGFB_TO_RORGT, NTGFB_TO_RORGT, KTBET_TO_RORGT, NTBET_TO_RORGT, KGATA3_TO_RORGT, NGATA3_TO_RORGT, KFOXP3_TO_RORGT, NFOXP3_TO_RORGT, CFOXP3, KIL2_TO_FOXP3, NIL2_TO_FOXP3, KIL10_TO_FOXP3, NIL10_TO_FOXP3, KTGFB_TO_FOXP3, NTGFB_TO_FOXP3, KTBET_TO_FOXP3, NTBET_TO_FOXP3, KGATA3_TO_FOXP3, NGATA3_TO_FOXP3, KRORGT_TO_FOXP3, NRORGT_TO_FOXP3, BIL2, BIL4, BIL10, BIL17, BIL21, BIFNG, BTGFB, BTBET, BGATA3, BRORGT, BFOXP3, NBPARAM};

    long long background;
    void derivatives(const vector<double> &x, vector<double> &dxdt, const double t);
    void initialise(long long _background = Back::WT);
    void setBaseParameters();
    double Activ0Inhib1(double Val1, double K1, double N1){
        if((Val1 <= 0.0)) return 0.0;
        else return (pow(K1, N1) / ( pow(K1, N1) + pow(Val1, N1) )); }
    double Activ1Inhib0(double Val1, double K1, double N1){
        if((Val1 <= 0.0)) return 0.0;
        else return (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) *1); }
    double Activ2Inhib0(double Val1, double K1, double N1,double Val2, double K2, double N2){
        if((Val1 <= 0.0) || (Val2 <= 0.0)) return 0.0;
        else return (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) *pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) *1); }
    double Activ2Inhib4(double Val1, double K1, double N1,double Val2, double K2, double N2,double Val3, double K3, double N3,double Val4, double K4, double N4,double Val5, double K5, double N5,double Val6, double K6, double N6){
        if((Val1 <= 0.0) || (Val2 <= 0.0) || (Val3 <= 0.0) || (Val4 <= 0.0) || (Val5 <= 0.0) || (Val6 <= 0.0)) return 0.0;
        else return (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) *pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) *pow(K3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) * pow(K4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) * pow(K5, N5) / ( pow(K5, N5) + pow(Val5, N5) ) * pow(K6, N6) / ( pow(K6, N6) + pow(Val6, N6) )); }
    double Activ3Inhib3(double Val1, double K1, double N1,double Val2, double K2, double N2,double Val3, double K3, double N3,double Val4, double K4, double N4,double Val5, double K5, double N5,double Val6, double K6, double N6){
        if((Val1 <= 0.0) || (Val2 <= 0.0) || (Val3 <= 0.0) || (Val4 <= 0.0) || (Val5 <= 0.0) || (Val6 <= 0.0)) return 0.0;
        else return (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) *pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) *pow(Val3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) *pow(K4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) * pow(K5, N5) / ( pow(K5, N5) + pow(Val5, N5) ) * pow(K6, N6) / ( pow(K6, N6) + pow(Val6, N6) )); }
    double Activ3Inhib4(double Val1, double K1, double N1,double Val2, double K2, double N2,double Val3, double K3, double N3,double Val4, double K4, double N4,double Val5, double K5, double N5,double Val6, double K6, double N6,double Val7, double K7, double N7){
        if((Val1 <= 0.0) || (Val2 <= 0.0) || (Val3 <= 0.0) || (Val4 <= 0.0) || (Val5 <= 0.0) || (Val6 <= 0.0) || (Val7 <= 0.0)) return 0.0;
        else return (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) *pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) *pow(Val3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) *pow(K4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) * pow(K5, N5) / ( pow(K5, N5) + pow(Val5, N5) ) * pow(K6, N6) / ( pow(K6, N6) + pow(Val6, N6) ) * pow(K7, N7) / ( pow(K7, N7) + pow(Val7, N7) )); }
    double Activ4Inhib0(double Val1, double K1, double N1,double Val2, double K2, double N2,double Val3, double K3, double N3,double Val4, double K4, double N4){
        if((Val1 <= 0.0) || (Val2 <= 0.0) || (Val3 <= 0.0) || (Val4 <= 0.0)) return 0.0;
        else return (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) *pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) *pow(Val3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) *pow(Val4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) *1); }
};
#endif
//#endif
