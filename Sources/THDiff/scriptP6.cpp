#include "scriptP6.h"
#include "../Moonfit/moonfit.h"
#include "../Moonfit/Framework/generate.h"

#include "namesTh.h"
#include "expThs.h"
//#include <QApplication>
#include <QString>
#include "fstream"
#include <sstream>

#define ReCreate


// generated here
#ifndef ReCreate
#include "modeleThTot5custom.h"
#endif

//#define ReCreate
enum typesVariables {PROTEINE, CYTOKINE, RNA, NBTPVARS};
void scriptP6()
{

    // 1 - ================ Creating network (ThTot5) from its topology =============== //

    string name = string("ThTot5");
    string folder = string("C:/Users/Philippe/Desktop/Restart/RV2/R1/");

#ifdef ReCreate

    Generate a = Generate(name.c_str());
    a.addVar("IL2",     PROP_DEG, CST_FROM_EQ, CodingName(N::IL2),  CYTOKINE);
    a.addVar("IL4",     PROP_DEG, CST_FROM_EQ, CodingName(N::IL4),  CYTOKINE);
    a.addVar("IL6",     PROP_DEG, NO_B,        CodingName(N::IL6),  CYTOKINE);
    a.addVar("IL10",    PROP_DEG, CST_FROM_EQ, CodingName(N::IL10), CYTOKINE);
    a.addVar("IL12",    PROP_DEG, NO_B,        CodingName(N::IL12), CYTOKINE);
    a.addVar("IL17",    PROP_DEG, CST_FROM_EQ, CodingName(N::IL17), CYTOKINE);
    a.addVar("IL21",    PROP_DEG, CST_FROM_EQ, CodingName(N::IL21), CYTOKINE);
    a.addVar("IFNG",    PROP_DEG, CST_FROM_EQ, CodingName(N::IFNG), CYTOKINE);
    a.addVar("TGFB",    PROP_DEG, CST_FROM_EQ, CodingName(N::TGFB), CYTOKINE);
    a.addVar("TBET",    PROP_DEG, CST_FROM_EQ, CodingName(N::TBET), PROTEINE);
    a.addVar("GATA3",   PROP_DEG, CST_FROM_EQ, CodingName(N::GATA3),PROTEINE);
    a.addVar("RORGT",   PROP_DEG, CST_FROM_EQ, CodingName(N::RORGT),PROTEINE);
    a.addVar("FOXP3",   PROP_DEG, CST_FROM_EQ, CodingName(N::FOXP3),PROTEINE);
    a.addVar("TCR",     NO_D,     NO_B,        CodingName(N::TCR));

    a.addReaction("IL12", "TBET",  +1);
    a.addReaction("TBET", "IFNG",  +1);
    a.addReaction("IFNG", "TBET",  +1);
    a.addReaction("IL4",  "GATA3", +1);
    a.addReaction("GATA3","IL4",   +1);
    //a.addReaction("TBET", "IL2",   +1); //???
    a.addReaction("IL2",  "GATA3", +1);
    a.addReaction("IL2",  "FOXP3", +1);
    a.addReaction("IL2",  "IL2",   -1);
    a.addReaction("TGFB", "FOXP3", +1);
    a.addReaction("TGFB", "RORGT", +1);
    a.addReaction("RORGT","TGFB",  +1);
    a.addReaction("FOXP3","TGFB",  +1);
    a.addReaction("RORGT","IL17",  +1);
    a.addReaction("RORGT","IL21",  +1);
    a.addReaction("IL21", "RORGT", +1);
    a.addReaction("IL6",  "RORGT", +1);
    //a.addReaction("IL6",  "GATA3", +1); // controversial

    //a.addReaction("TCR", "TBET",   +1);
    //a.addReaction("TCR", "GATA3",  +1);
    //a.addReaction("TCR", "IL2",    +1);

    a.addReaction("IL10", "RORGT", -1);
    a.addReaction("IL10", "FOXP3", +1);
    a.addReaction("IL10", "GATA3", -1); // controversial as well
    a.addReaction("IL10", "TBET",  -1);
    a.addReaction("RORGT","IL10",  +1);
    a.addReaction("FOXP3","IL10",  +1);
    a.addReaction("GATA3","IL10",  +1);
    a.addReaction("TBET", "IL10",  +1);

    a.addReaction("TBET", "GATA3",  -1);
    a.addReaction("TBET", "FOXP3",  -1);
    a.addReaction("TBET", "RORGT",  -1);
    a.addReaction("GATA3", "TBET",  -1);
    a.addReaction("GATA3", "FOXP3", -1);
    a.addReaction("GATA3", "RORGT", -1);
    a.addReaction("FOXP3", "TBET",  -1);
    a.addReaction("FOXP3", "GATA3", -1);
    a.addReaction("FOXP3", "RORGT", -1);
    a.addReaction("RORGT", "TBET",  -1);
    a.addReaction("RORGT", "FOXP3", -1);
    a.addReaction("RORGT", "GATA3", -1);

    a.addReaction("GATA3", "GATA3",  +1);

    a.addInclude(string("../GlobalExperiments/namesTh.h"));



    vector<typeVar> store;
    {
        typeVar vx = typeVar();
        vx.name = string("Protein");
        vx.Deg_min   = 1e-6;
        vx.Basal_Cst_min = 1e-6;
        vx.Basal_prop_min= 1e-6;
        vx.Trans_min     = 1e-5;
        vx.ValueEQ_min   = 0.01;
        vx.Khill_min     = 0.01;
        vx.Nhill_min     = 0.25;
        vx.KfKv_min      = 0.01;

        vx.Deg_max   = 0.1;
        vx.Basal_Cst_max = 0.1;
        vx.Basal_prop_max= 1.0;
        vx.Trans_max     = 1.0;
        vx.ValueEQ_max   = 0.25; // supposedly EQ close to zero ...
        vx.Khill_max     = 1.0;
        vx.Nhill_max     = 4.0;
        vx.KfKv_max      = 100.0;
        store.push_back(vx); //should do a copy, right !

        vx.name = string("Cytokine");
        vx.Deg_min   = 1e-5;
        vx.Deg_max   = 0.1;
        store.push_back(vx);
    }

    a.setBorderPolicy(store);

    ofstream fichier((folder + string("modele") + name + string(".h")).c_str(), ios::out);
    fichier << a.generateCodeHeader();
    fichier.close();

    ofstream fichier2((folder + string("modele") + name + string(".cpp")).c_str(), ios::out);
    fichier2 << a.generateCodeSource();
    fichier2.close();


    string optAsk =
       string("geneticAlgorithm 14		# New syntax : IdOptimizer TypeScaling NbArgsForScaling ArgsScaling NbArgstoOptimizer   \n") +
       string("0	# 0 = CEP 	1 = SSGA 	2 = GGA		+10 = test Mut/Cross	+20 = test Pop / %mut                           \n") +
       string("7	# Selection parents	1Rank	2Random	3456Tournament5710725750	7-12Prop                                    \n") +
       string("9	-1	# Cross-Over                                                                                            \n") +
       string("1	# Mutation                                                                                                  \n") +
       string("0	# Replacement policy                                                                                        \n") +
       string("0	# selection policy                                                                                          \n") +
       string("7	0.005	# Strategy                                                                                          \n") +
       string("1	# Num tries                                                                                                 \n") +
       string("80000	# Num Calls                                                                                             \n") +
       string("80	# Population size                                                                                           \n") +
       string("0.1	# x=Proportion of cross-over. for CEP,  x cross-over and 1-x mutations. SSGA or GGA: popSize*x cross-overs  \n") +
       string("1.0	# fork coefficient (lambda/mu)                                                                              \n");

    ofstream fichier3((folder + string("Opt") + name + string(".txt")).c_str(), ios::out);
    fichier3 << optAsk << "\n" << a.generateParamBorders()  << "\n0\n\nNoScaling\t0\n";
    fichier3.close();

    cerr << "FIN" << endl;


    // 2 ------------- Extracting the time courses from Carbo, with the desired time points & Variables --------------------------
/*
    // initialise Carbo
    vector<string> listeCarboFiles;
    vector<int> IDCorrespondingExperiments;
    listeCarboFiles.push_back(string("resultIFNGAlone.txt"));   IDCorrespondingExperiments.push_back(IFNG_ALONE);
    listeCarboFiles.push_back(string("resultIL4IFNgTGFB.txt")); IDCorrespondingExperiments.push_back(IL4_IFNG_TGFB);
    listeCarboFiles.push_back(string("resultTGFbOnly.txt"));    IDCorrespondingExperiments.push_back(TGFB_ALONE);
    listeCarboFiles.push_back(string("resultTh0.txt"));         IDCorrespondingExperiments.push_back(TH0);
    listeCarboFiles.push_back(string("resultTh1.txt"));         IDCorrespondingExperiments.push_back(TH1);
    listeCarboFiles.push_back(string("resultTh1IL2.txt"));      IDCorrespondingExperiments.push_back(TH1_IL2);
    listeCarboFiles.push_back(string("resultTh1Th2.txt"));      IDCorrespondingExperiments.push_back(MIXEDTH1_TH2);
    listeCarboFiles.push_back(string("resultTh2.txt"));         IDCorrespondingExperiments.push_back(TH2);
    listeCarboFiles.push_back(string("resultTh2IL2.txt"));      IDCorrespondingExperiments.push_back(TH2IL2);
    listeCarboFiles.push_back(string("resultTh17.txt"));        IDCorrespondingExperiments.push_back(TH17);
    listeCarboFiles.push_back(string("resultTh17IL2.txt"));     IDCorrespondingExperiments.push_back(TH17IL2);
    listeCarboFiles.push_back(string("resultTreg(IL2).txt"));   IDCorrespondingExperiments.push_back(TREG);

    string baseCarbo = "C:/Users/parobert/Desktop/Optimisation/ThModelesV3/FromPublished/Carbo/";
    int nbFiles = listeCarboFiles.size();
    for(int i = 0; i < nbFiles; ++i){
        putVariableNamesToCarboFile(baseCarbo + listeCarboFiles[i], baseCarbo + string("ListeSpecies.txt"), baseCarbo + string("treated/") +  listeCarboFiles[i]);
    }

    // prepares the file for generating cost data,
    // should be in the following format :
    //
    //  NB_EXPS
    //  ID_EXP  NameEXP(txt)
    //
    //  NbLg    NbLig
    //  Time    NameV1  NameV2  ...
    //  data    ...     ...)
    //
    //  ID_EXP  NameExp ...


    vector<int> TimePoints;
    TimePoints.push_back(3);
    TimePoints.push_back(6);
    TimePoints.push_back(9);
    TimePoints.push_back(12);
    TimePoints.push_back(18);
    TimePoints.push_back(24);
    TimePoints.push_back(36);
    TimePoints.push_back(48);
    TimePoints.push_back(72);

    vector<string> listeVars; // = getCodingNames();
    ModeleThTot5 MK;
    for(int i = 0; i < MK.getNbVars(); ++i){
        listeVars.push_back(CodingName(MK.getGlobalID(i)));
    }

    TableCourse Tc(0);
    stringstream rcp;
    rcp << nbFiles << "\n";
    for(int i = 0; i < nbFiles; ++i){
        rcp << IDCorrespondingExperiments[i] << "\t" << CodingNameExp(IDCorrespondingExperiments[i]) << "\n";
        Tc.read(baseCarbo + string("treated/") + listeCarboFiles[i]);
        TableCourse Tc2 = Tc.subKinetics(TimePoints, listeVars);
        rcp << Tc2.print();
    }

    ofstream fichier4((baseCarbo + string("treated/RecapAllExps.txt")).c_str(), ios::out);
    fichier4 << rcp.str() << endl;
    fichier4.close();

    string costs = generateCostCodeFromData(rcp.str());
    ofstream fichier5((baseCarbo + string("treated/CostCodeCarbo.txt")).c_str(), ios::out);
    fichier5 << costs << endl;
    fichier5.close();

    ofstream fichier6((folder + string("CostCarbo.h")).c_str(), ios::out);
    fichier6 << "// -------- Automatically generated code -----------\n";
    fichier6 << "#ifndef COSTCARBO_H\n";
    fichier6 << "#define COSTCARBO_H\n";
    fichier6 << "#include \"../common.h\"\n";
    fichier6 << "#ifdef COMPILE_AUTOGEN\n";
    fichier6 << "#define DAY *24*3600\n";
    fichier6 << "#include \"../GlobalExperiments/experimentsThAll.h\"\n";
    fichier6 << "struct CostCarbo : public ExpTHALL {\n";
    fichier6 << "\tCostCarbo(Model* _m) : ExpTHALL(_m) {}\n";
    fichier6 << costs << "\n";
    fichier6 << "};\n";
    fichier6 << "#endif\n";
    fichier6 << "#endif //COSTCARBO_H\n";
    fichier6.close();
*/
    // --> Now, the generated code was used to make ExpThAL
#else




    // 3 - preparing the Th5tot model for optimisation


    //ofstream fichier3((folder + string("Opt") + name + string(".txt")).c_str(), ios::out);

    Model* currentModel = new ModeleThTot5();
    Experiment* currentExperiment = new expThs(currentModel);
    //Optimize(folder + string("Opt") + name + string(".txt"), currentExperiment);

    vector<int> TimePoints2;
    TimePoints2.push_back(3  *3600);
    TimePoints2.push_back(6  *3600);
    TimePoints2.push_back(9  *3600);
    TimePoints2.push_back(12 *3600);
    TimePoints2.push_back(18 *3600);
    TimePoints2.push_back(24 *3600);
    TimePoints2.push_back(36 *3600);
    TimePoints2.push_back(48 *3600);
    TimePoints2.push_back(72 *3600);

    currentModel->setBaseParameters();
    //currentModel->loadParameters((folder + string("Sim2/ParamSets.txt")).c_str());
    currentModel->setPrintMode(true, 1200);
    currentModel->dt = 1.0;
    currentExperiment->simulate(TH1, NULL, true);
    TableCourse Tc3 = currentModel->getCinetique();
    TableCourse Tc4 = Tc3.subKinetics(TimePoints2);
    Tc4.print();
    for(int i = 0; i < 10; ++i) cout << "Yeah\n";
    Tc3.print();




#endif
}


void scriptP7()
{

    // 1 - ================ Creating network (ThTot5) from its topology =============== //

    string name = string("ThTot7");
    string folder = string("C:/Users/Philippe/Desktop/Restart/RV2/R1/");


    enum typesVariables {PROTEINE, CYTOKINE, NBTPVARS};
    Generate a = Generate(name.c_str());
    a.addVar("IL2",     PROP_DEG, CST_FROM_EQ, CodingName(N::IL2),  CYTOKINE);
    a.addVar("IL4",     PROP_DEG, CST_FROM_EQ, CodingName(N::IL4),  CYTOKINE);
    a.addVar("IL6",     PROP_DEG, NO_B,        CodingName(N::IL6),  CYTOKINE);
    //a.addVar("IL10",    PROP_DEG, CST_FROM_EQ, CodingName(N::IL10), CYTOKINE);
    a.addVar("IL12",    PROP_DEG, NO_B,        CodingName(N::IL12), CYTOKINE);
    a.addVar("IL17",    PROP_DEG, CST_FROM_EQ, CodingName(N::IL17), CYTOKINE);
    a.addVar("IL21",    PROP_DEG, CST_FROM_EQ, CodingName(N::IL21), CYTOKINE);
    a.addVar("IFNG",    PROP_DEG, CST_FROM_EQ, CodingName(N::IFNG), CYTOKINE);
    a.addVar("TGFB",    PROP_DEG, CST_FROM_EQ, CodingName(N::TGFB), CYTOKINE);
    a.addVar("TBET",    PROP_DEG, CST_FROM_EQ, CodingName(N::TBET), PROTEINE);
    a.addVar("GATA3",   PROP_DEG, CST_FROM_EQ, CodingName(N::GATA3),PROTEINE);
    a.addVar("RORGT",   PROP_DEG, CST_FROM_EQ, CodingName(N::RORGT),PROTEINE);
    a.addVar("FOXP3",   PROP_DEG, CST_FROM_EQ, CodingName(N::FOXP3),PROTEINE);

    a.addVar("IL2mRNA",     PROP_DEG, CST_FROM_EQ, CodingName(N::IL2mRNA),  RNA);
    a.addVar("IL4mRNA",     PROP_DEG, CST_FROM_EQ, CodingName(N::IL4mRNA),  RNA);
    //a.addVar("IL10mRNA",    PROP_DEG, CST_FROM_EQ, CodingName(N::IL10mRNA), RNA);
    a.addVar("IL17mRNA",    PROP_DEG, CST_FROM_EQ, CodingName(N::IL17mRNA), RNA);
    a.addVar("IL21mRNA",    PROP_DEG, CST_FROM_EQ, CodingName(N::IL21mRNA), RNA);
    a.addVar("IFNGmRNA",    PROP_DEG, CST_FROM_EQ, CodingName(N::IFNGmRNA), RNA);
    a.addVar("TGFBmRNA",    PROP_DEG, CST_FROM_EQ, CodingName(N::TGFBmRNA), RNA);
    a.addVar("TBETmRNA",    PROP_DEG, CST_FROM_EQ, CodingName(N::TBETmRNA), RNA);
    a.addVar("GATA3mRNA",   PROP_DEG, CST_FROM_EQ, CodingName(N::GATA3mRNA),RNA);
    a.addVar("RORGTmRNA",   PROP_DEG, CST_FROM_EQ, CodingName(N::RORGTmRNA),RNA);
    a.addVar("FOXP3mRNA",   PROP_DEG, CST_FROM_EQ, CodingName(N::FOXP3mRNA),RNA);

    a.addVar("TCR",     NO_D,     NO_B,        CodingName(N::TCR));

    a.addReaction("IL12", "TBETmRNA",  +1);
    a.addReaction("TBET", "IFNGmRNA",  +1);
    a.addReaction("IFNG", "TBETmRNA",  +1);
    a.addReaction("IL4",  "GATA3mRNA", +1);
    a.addReaction("GATA3","IL4mRNA",   +1);
    //a.addReaction("TBET", "IL2mRNA",   +1); //???
    a.addReaction("IL2",  "GATA3mRNA", +1);
    a.addReaction("IL2",  "FOXP3mRNA", +1);
    a.addReaction("IL2",  "IL2mRNA",   -1);
    a.addReaction("TGFB", "FOXP3mRNA", +1);
    a.addReaction("TGFB", "RORGTmRNA", +1);
    //a.addReaction("RORGT","TGFBmRNA",  +1);
    //a.addReaction("FOXP3","TGFBmRNA",  +1);
    a.addReaction("RORGT","IL17mRNA",  +1);
    a.addReaction("RORGT","IL21mRNA",  +1);
    a.addReaction("IL21", "RORGTmRNA", +1);
    a.addReaction("IL6",  "RORGTmRNA", +1);
    a.addReaction("IL6",  "TBETmRNA", +1);
    //a.addReaction("IL6",  "GATA3mRNA", +1); // controversial

    //a.addReaction("TCR", "TBETmRNA",   +1);
    //a.addReaction("TCR", "GATA3mRNA",  +1);
    //a.addReaction("TCR", "IL2mRNA",    +1);

    /*a.addReaction("IL10", "RORGTmRNA", -1);
    a.addReaction("IL10", "FOXP3mRNA", +1);
    a.addReaction("IL10", "GATA3mRNA", -1); // controversial as well
    a.addReaction("IL10", "TBETmRNA",  -1);
    a.addReaction("RORGT","IL10mRNA",  +1);
    a.addReaction("FOXP3","IL10mRNA",  +1);
    a.addReaction("GATA3","IL10mRNA",  +1);
    a.addReaction("TBET", "IL10mRNA",  +1);*/

    a.addReaction("TBET", "GATA3mRNA",  -1);
    a.addReaction("TBET", "FOXP3mRNA",  -1);
    a.addReaction("TBET", "RORGTmRNA",  -1);
    a.addReaction("GATA3", "TBETmRNA",  -1);
    a.addReaction("GATA3", "FOXP3mRNA", -1);
    a.addReaction("GATA3", "RORGTmRNA", -1);
    a.addReaction("FOXP3", "TBETmRNA",  -1);
    a.addReaction("FOXP3", "GATA3mRNA", -1);
    a.addReaction("FOXP3", "RORGTmRNA", -1);
    a.addReaction("RORGT", "TBETmRNA",  -1);
    a.addReaction("RORGT", "FOXP3mRNA", -1);
    a.addReaction("RORGT", "GATA3mRNA", -1);

    a.addReaction("GATA3", "GATA3mRNA",  +1);

    a.addInclude(string("../GlobalExperiments/namesTh.h"));



    vector<typeVar> store;
    {
        typeVar vx = typeVar();
        vx.name = string("Protein");
        vx.Deg_min   = 1e-6;
        vx.Basal_Cst_min = 1e-6;
        vx.Basal_prop_min= 1e-6;
        vx.Trans_min     = 1e-5;
        vx.ValueEQ_min   = 0.01;
        vx.Khill_min     = 0.01;
        vx.Nhill_min     = 0.25;
        vx.KfKv_min      = 0.01;

        vx.Deg_max   = 0.1;
        vx.Basal_Cst_max = 0.1;
        vx.Basal_prop_max= 1.0;
        vx.Trans_max     = 1.0;
        vx.ValueEQ_max   = 0.25; // supposedly EQ close to zero ...
        vx.Khill_max     = 1.0;
        vx.Nhill_max     = 4.0;
        vx.KfKv_max      = 100.0;
        store.push_back(vx); //should do a copy, right !

        vx.name = string("Cytokine");
        vx.Deg_min   = 1e-5;
        vx.Deg_max   = 0.1;
        store.push_back(vx);

        vx.name = string("RNA");
        vx.Deg_min   = 1e-5;
        vx.Deg_max   = 0.1;
        store.push_back(vx);


    }

    a.setBorderPolicy(store);

    ofstream fichier((folder + string("modele") + name + string(".h")).c_str(), ios::out);
    fichier << a.generateCodeHeader();
    fichier.close();

    ofstream fichier2((folder + string("modele") + name + string(".cpp")).c_str(), ios::out);
    fichier2 << a.generateCodeSource();
    fichier2.close();

    ofstream fichier2b((folder + string("modele") + name + string(".tex")).c_str(), ios::out);
    fichier2b << a.generateLatex();
    fichier2b.close();

    string optAsk =
       string("geneticAlgorithm 14		# New syntax : IdOptimizer TypeScaling NbArgsForScaling ArgsScaling NbArgstoOptimizer   \n") +
       string("0	# 0 = CEP 	1 = SSGA 	2 = GGA		+10 = test Mut/Cross	+20 = test Pop / %mut                           \n") +
       string("7	# Selection parents	1Rank	2Random	3456Tournament5710725750	7-12Prop                                    \n") +
       string("9	-1	# Cross-Over                                                                                            \n") +
       string("1	# Mutation                                                                                                  \n") +
       string("0	# Replacement policy                                                                                        \n") +
       string("0	# selection policy                                                                                          \n") +
       string("7	0.005	# Strategy                                                                                          \n") +
       string("1	# Num tries                                                                                                 \n") +
       string("80000	# Num Calls                                                                                             \n") +
       string("80	# Population size                                                                                           \n") +
       string("0.1	# x=Proportion of cross-over. for CEP,  x cross-over and 1-x mutations. SSGA or GGA: popSize*x cross-overs  \n") +
       string("1.0	# fork coefficient (lambda/mu)                                                                              \n");

    ofstream fichier3((folder + string("Opt") + name + string(".txt")).c_str(), ios::out);
    fichier3 << optAsk << "\n" << a.generateParamBorders()  << "\n0\n\nNoScaling\t0\n";
    fichier3.close();

}


