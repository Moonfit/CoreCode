#include "namesTh.h"
#include <iostream>
#include <cmath>
#include <sstream>
using namespace std;

///////// ARCHTUNG !! the NoName in the first variable is still necesary. There is a condition inside Modele::Over() that checks if NoName is called. There are things elsewhere.

static vector<string> GNames;
static vector<string> codingNames;
static vector<string> expCodingNames;
static vector<long long> backgrounds;

long long getBackgroundNr(int i){
    static bool loaded = false;
    if(!loaded){
        backgrounds.push_back(Back::WT);
      backgrounds.push_back(Back::TCRGATA3NEGKO);
      backgrounds.push_back(Back::TCRIL2KO);
      backgrounds.push_back(Back::TCRGARA3POSKO);
      backgrounds.push_back(Back::TCRTGFBKO);
      backgrounds.push_back(Back::TCRTBETKO);
      backgrounds.push_back(Back::TCRIFNGKO);          // removing such arrows
      backgrounds.push_back(Back::IL2KO);
      backgrounds.push_back(Back::IL4KO);
      backgrounds.push_back(Back::IL17KO);
      backgrounds.push_back(Back::IL21KO);
      backgrounds.push_back(Back::IFNGKO);
      backgrounds.push_back(Back::TGFBKO);
      backgrounds.push_back(Back::TBETKO);
      backgrounds.push_back(Back::GATA3KO);
      backgrounds.push_back(Back::RORGTKO);
      backgrounds.push_back(Back::FOXP3KO);  // not producing the protein at all (only the added one). The CmRNA = 0 and EQ(init) = 0
      backgrounds.push_back(Back::STAT1KO);       // = IFNGRKO no any effects of IFNG         SIFNG_TO_TBETmRNA   SIFNG_TO_GATA3mRNA
      backgrounds.push_back(Back::STAT3KO);       // FIL6_TO_IL21mRNA = 1?    FIL6_TO_TBETmRNA FIL6_TO_RORGTmRNA SIL21_TO_RORGTmRNA
      backgrounds.push_back(Back::STAT4KO);       // = IL12RKO no IL12 activates IFNG   FIL12_TO_IFNGmRNA = 1 or 0 ??? SIL12_TO_TBETmRNA  FIL12_TO_GATA3mRNA
      backgrounds.push_back(Back::STAT5KO);      // = IL2KO, no effect of IL2          SIL2_TO_IL2mRNA = 1 SIL2_TO_GATA3mRNA SIL2_TO_FOXP3mRNA
      backgrounds.push_back(Back::STAT6KO);      // = IL4RKO, no effect of IL4         SIL4_TO_IL2mRNA = 1   SIL4_TO_GATA3mRNA
      backgrounds.push_back(Back::IL6RKO);       // FIL6_TO_IL21mRNA = 1 ??  FIL6_TO_TBETmRNA    FIL6_TO_RORGTmRNA
      backgrounds.push_back(Back::IL21RKO);      // SIL21_TO_RORGTmRNA
      backgrounds.push_back(Back::TGFBRKO);     // STGFB_TO_RORGTmRNA   STGFB_TO_FOXP3mRNA
      backgrounds.push_back(Back::NOIL2INHIBIL2);       // SIL2_TO_IL2mRNA = 1
      backgrounds.push_back(Back::NOIL4INHIBIL);       // SIL4_TO_IL2mRNA = 1
      backgrounds.push_back(Back::NOGATA3ACTIVIL4);    // SGATA3_TO_IL4mRNA = 1??
      backgrounds.push_back(Back::NOIL6ACTIVIL21);     // FIL6_TO_IL21mRNA = 1??
      backgrounds.push_back(Back::NORORGTACTIVIL21);   // SRORGT_TO_IL21mRNA = 1
      backgrounds.push_back(Back::NOIL12ACTIVIFN);    // FIL12_TO_IFNGmRNA
      backgrounds.push_back(Back::NOTBETACTIVIFNG);   // STBET_TO_IFNGmRNA = 1? // 2^31 attention, int stops here
      backgrounds.push_back(Back::NOIL6ACTIVTBET);    // FIL6_TO_TBETmRNA
      backgrounds.push_back(Back::NOIL12ACTIVTBET);   // SIL12_TO_TBETmRNA
      backgrounds.push_back(Back::NOIFNGACTIVTBET);  // SIFNG_TO_TBETmRNA
      backgrounds.push_back(Back::NOGATA3INHIBTBET); // SGATA3_TO_TBETmRNA
      backgrounds.push_back(Back::NORORGTINHIBTBET); // SRORGT_TO_TBETmRNA
      backgrounds.push_back(Back::NOFOXP3INHIBTBET); // SFOXP3_TO_TBETmRNA
      backgrounds.push_back(Back::NOIL12INHIBGATA3); // FIL12_TO_GATA3mRNA
      backgrounds.push_back(Back::NOIL2ACTIVGATA3);  // SIL2_TO_GATA3mRNA
      backgrounds.push_back(Back::NOIL4ACTIVGATA3);  // SIL4_TO_GATA3mRNA
      backgrounds.push_back(Back::NOIFNGINHIBGATA3); // SIFNG_TO_GATA3mRNA
      backgrounds.push_back(Back::NOTBETINHIBGATA3); // STBET_TO_GATA3mRNA
      backgrounds.push_back(Back::GATA3FEEDBACKKO);  // SGATA3_TO_GATA3mRNA
      backgrounds.push_back(Back::NORORGTINHIBGATA3);// SRORGT_TO_GATA3mRNA
      backgrounds.push_back(Back::NOFOXP3INHIBGATA3);// SFOXP3_TO_GATA3mRNA
      backgrounds.push_back(Back::NOIL6ACTIVRORGT);  // FIL6_TO_RORGTmRNA
      backgrounds.push_back(Back::NOIL21ACTIVRORGT); // SIL21_TO_RORGTmRNA
      backgrounds.push_back(Back::NOTGFBACTIVRORGT); // STGFB_TO_RORGTmRNA
      backgrounds.push_back(Back::NOTBETINHIBRORGT); // STBET_TO_RORGTmRNA
      backgrounds.push_back(Back::NOGATA3INHIBRORGT);// SGATA3_TO_RORGTmRNA
      backgrounds.push_back(Back::NOFOXP3INHIBRORGT);// SFOXP3_TO_RORGTmRNA
      backgrounds.push_back(Back::NOIL2ACTIVFOXP3);  // SIL2_TO_FOXP3mRNA
      backgrounds.push_back(Back::NOTGFBACTIVFOXP3); // STGFB_TO_FOXP3mRNA
      backgrounds.push_back(Back::NOTBETINHIBFOXP3); // STBET_TO_FOXP3mRNA
      backgrounds.push_back(Back::NOGATA3INHIBFOXP3);// SGATA3_TO_FOXP3mRNA
      backgrounds.push_back(Back::NORORGTINHIBFOXP3);// SRORGT_TO_FOXP3mRNA
      backgrounds.push_back(Back::NODELAYIL2);       // FORCEIL2 = 10??
      backgrounds.push_back(Back::NODELAYIL21); // FORCEIL21
      backgrounds.push_back(Back::NODELAYFOXP3);     // FORCEFOXP3
      backgrounds.push_back(Back::NODELAYRORGT);     // FORCERORGT
      backgrounds.push_back(Back::NODELAYTRANSL);    // FORCETRANSL
      backgrounds.push_back(Back::NODELAYSECRET);    // FORCESECRET
    }
    if(i < 0)  return backgrounds.size();
    if((i < 0) || (i >= (int) backgrounds.size())){
        cerr << "ERR:  backgrounds(" << i << ") is not defined, only " << backgrounds.size() << " backgrounds" << endl;
        return -1;
    }
    return backgrounds[i];
}

string getBackgroundName(long long background){
    stringstream res;
    if(background & Back::WT)	res << "-WT";
    if(background & Back::TCRGATA3NEGKO)	res << "-TCRGATA3NEGKO";
    if(background & Back::TCRIL2KO)	res << "-TCRIL2KO";
    if(background & Back::TCRGARA3POSKO)	res << "-TCRGARA3POSKO";
    if(background & Back::TCRTGFBKO)	res << "-TCRTGFBKO";
    if(background & Back::TCRTBETKO)	res << "-TCRTBETKO";
    if(background & Back::TCRIFNGKO)	res << "-TCRIFNGKO";
    if(background & Back::IL2KO)	res << "-IL2KO";
    if(background & Back::IL4KO)	res << "-IL4KO";
    if(background & Back::IL17KO)	res << "-IL17KO";
    if(background & Back::IL21KO)	res << "-IL21KO";
    if(background & Back::IFNGKO)	res << "-IFNGKO";
    if(background & Back::TGFBKO)	res << "-TGFBKO";
    if(background & Back::TBETKO)	res << "-TBETKO";
    if(background & Back::GATA3KO)	res << "-GATA3KO";
    if(background & Back::RORGTKO)	res << "-RORGTKO";
    if(background & Back::FOXP3KO)	res << "-FOXP3KO";
    if(background & Back::STAT1KO)	res << "-STAT1KO";
    if(background & Back::STAT3KO)	res << "-STAT3KO";
    if(background & Back::STAT4KO)	res << "-STAT4KO";
    if(background & Back::STAT5KO)	res << "-STAT5KO";
    if(background & Back::STAT6KO)	res << "-STAT6KO";
    if(background & Back::IL6RKO)	res << "-IL6RKO";
    if(background & Back::IL21RKO)	res << "-IL21RKO";
    if(background & Back::TGFBRKO)	res << "-TGFBRKO";
    if(background & Back::NOIL2INHIBIL2)	res << "-NOIL2INHIBIL2";
    if(background & Back::NOIL4INHIBIL)	res << "-NOIL4INHIBIL";
    if(background & Back::NOGATA3ACTIVIL4)	res << "-NOGATA3ACTIVIL4";
    if(background & Back::NOIL6ACTIVIL21)	res << "-NOIL6ACTIVIL21";
    if(background & Back::NORORGTACTIVIL21)	res << "-NORORGTACTIVIL21";
    if(background & Back::NOIL12ACTIVIFN)	res << "-NOIL12ACTIVIFN";
    if(background & Back::NOTBETACTIVIFNG)	res << "-NOTBETACTIVIFNG";
    if(background & Back::NOIL6ACTIVTBET)	res << "-NOIL6ACTIVTBET";
    if(background & Back::NOIL12ACTIVTBET)	res << "-NOIL12ACTIVTBET";
    if(background & Back::NOIFNGACTIVTBET)	res << "-NOIFNGACTIVTBET";
    if(background & Back::NOGATA3INHIBTBET)	res << "-NOGATA3INHIBTBET";
    if(background & Back::NORORGTINHIBTBET)	res << "-NORORGTINHIBTBET";
    if(background & Back::NOFOXP3INHIBTBET)	res << "-NOFOXP3INHIBTBET";
    if(background & Back::NOIL12INHIBGATA3)	res << "-NOIL12INHIBGATA3";
    if(background & Back::NOIL2ACTIVGATA3)	res << "-NOIL2ACTIVGATA3";
    if(background & Back::NOIL4ACTIVGATA3)	res << "-NOIL4ACTIVGATA3";
    if(background & Back::NOIFNGINHIBGATA3)	res << "-NOIFNGINHIBGATA3";
    if(background & Back::NOTBETINHIBGATA3)	res << "-NOTBETINHIBGATA3";
    if(background & Back::GATA3FEEDBACKKO)	res << "-GATA3FEEDBACKKO";
    if(background & Back::NORORGTINHIBGATA3)	res << "-NORORGTINHIBGATA3";
    if(background & Back::NOFOXP3INHIBGATA3)	res << "-NOFOXP3INHIBGATA3";
    if(background & Back::NOIL6ACTIVRORGT)	res << "-NOIL6ACTIVRORGT";
    if(background & Back::NOIL21ACTIVRORGT)	res << "-NOIL21ACTIVRORGT";
    if(background & Back::NOTGFBACTIVRORGT)	res << "-NOTGFBACTIVRORGT";
    if(background & Back::NOTBETINHIBRORGT)	res << "-NOTBETINHIBRORGT";
    if(background & Back::NOGATA3INHIBRORGT)	res << "-NOGATA3INHIBRORGT";
    if(background & Back::NOFOXP3INHIBRORGT)	res << "-NOFOXP3INHIBRORGT";
    if(background & Back::NOIL2ACTIVFOXP3)	res << "-NOIL2ACTIVFOXP3";
    if(background & Back::NOTGFBACTIVFOXP3)	res << "-NOTGFBACTIVFOXP3";
    if(background & Back::NOTBETINHIBFOXP3)	res << "-NOTBETINHIBFOXP3";
    if(background & Back::NOGATA3INHIBFOXP3)	res << "-NOGATA3INHIBFOXP3";
    if(background & Back::NORORGTINHIBFOXP3)	res << "-NORORGTINHIBFOXP3";
    if(background & Back::NODELAYIL2)	res << "-NODELAYIL2";
    if(background & Back::NODELAYIL21)	res << "-NODELAYIL21";
    if(background & Back::NODELAYFOXP3)	res << "-NODELAYFOXP3";
    if(background & Back::NODELAYRORGT)	res << "-NODELAYRORGT";
    if(background & Back::NODELAYTRANSL)	res << "-NODELAYTRANSL";
    if(background & Back::NODELAYSECRET)	res << "-NODELAYSECRET";
    return res.str();
}


void testBackgroundSystem(){
    long a1 = pow(2,10);
    long b1 = pow(2,5);
    long c1 = a1 | b1;  // adding backgrounds
    cerr << a1 << "," << b1 << "," << (a1 | b1) << endl;
    // recovering backgrounds
    cerr << (c1 & ((long) pow(2,1))) << "\t";
    cerr << (c1 & ((long) pow(2,2))) << "\t";
    cerr << (c1 & ((long) pow(2,3))) << "\t";
    cerr << (c1 & ((long) pow(2,4))) << "\t";
    cerr << (c1 & ((long) pow(2,5))) << "\t";
    cerr << (c1 & ((long) pow(2,6))) << "\t";
    cerr << (c1 & ((long) pow(2,7))) << "\t";
    cerr << (c1 & ((long) pow(2,8))) << "\t";
    cerr << (c1 & ((long) pow(2,9))) << "\t";
    cerr << (c1 & ((long) pow(2,10))) << "\t";
}


string CodingNameExp(int id){
    static bool loaded = false;
    if(!loaded){
        expCodingNames.push_back(string("TH1"));
        expCodingNames.push_back(string("TH2"));
        expCodingNames.push_back(string("TREG"));
        expCodingNames.push_back(string("TH17"));
        expCodingNames.push_back(string("TH0"));
    }
    if((id < 0) || (id >= (int)  expCodingNames.size())) {
          return string("Not Found");
    }
    else return expCodingNames[id];
}

string GlobalName(int id){
    static bool loaded = false;

    if(!loaded){
        GNames.push_back(string("gNONAME"));
        GNames.push_back(string("gIL12"));
        GNames.push_back(string("gSTAT4"));
        GNames.push_back(string("gSTAT4P"));
        GNames.push_back(string("gTBET"));
        GNames.push_back(string("gIFNG"));
        GNames.push_back(string("gIL12RB2"));
        GNames.push_back(string("gRL12"));
        GNames.push_back(string("gRIFN"));
        GNames.push_back(string("gIFNGR"));
        GNames.push_back(string("gSTAT1"));
        GNames.push_back(string("gSTAT1P"));
        GNames.push_back(string("gIL4"));
        GNames.push_back(string("gIL4R"));
        GNames.push_back(string("gRL4"));
        GNames.push_back(string("gIL13"));
        GNames.push_back(string("gIL13R"));
        GNames.push_back(string("gRL13"));
        GNames.push_back(string("gIL5"));
        GNames.push_back(string("gSTAT6"));
        GNames.push_back(string("gSTAT6P"));
        GNames.push_back(string("gGATA3"));
        GNames.push_back(string("gGATA3TBET"));
        GNames.push_back(string("gIL2"));
        GNames.push_back(string("gIL2RA"));
        GNames.push_back(string("gRL2"));
        GNames.push_back(string("gSTAT5"));
        GNames.push_back(string("gSTAT5P"));
        GNames.push_back(string("gTGFB"));
        GNames.push_back(string("gTGFBR"));
        GNames.push_back(string("gRTGFB"));
        GNames.push_back(string("gSMAD23"));
        GNames.push_back(string("gSMAD23P"));
        GNames.push_back(string("gFOXP3"));
        GNames.push_back(string("gFOXP3GATA3"));
        GNames.push_back(string("gFOXP3RORGT"));
        GNames.push_back(string("gIL6"));
        GNames.push_back(string("gIL6R"));
        GNames.push_back(string("gRL6"));
        GNames.push_back(string("gIL21"));
        GNames.push_back(string("gIL21R"));
        GNames.push_back(string("gRL21"));
        GNames.push_back(string("gIL10"));
        GNames.push_back(string("gIL10R"));
        GNames.push_back(string("gRL10"));
        GNames.push_back(string("gSTAT3"));
        GNames.push_back(string("gSTAT3P"));
        GNames.push_back(string("gSTAT3SERP"));
        GNames.push_back(string("gRORGT"));
        GNames.push_back(string("gIL17"));
        GNames.push_back(string("gIL22"));
        GNames.push_back(string("gIL17F"));
        GNames.push_back(string("gSOCS1"));
        GNames.push_back(string("gSOCS2"));
        GNames.push_back(string("gSOCS3"));
        GNames.push_back(string("gSMAD7"));
        GNames.push_back(string("gTCR"));
        GNames.push_back(string("gSTAT4mRNA"));
        GNames.push_back(string("gTBETmRNA"));
        GNames.push_back(string("gIFNGmRNA"));
        GNames.push_back(string("gIL12RB2mRNA"));
        GNames.push_back(string("gIFNGRmRNA"));
        GNames.push_back(string("gSTAT1mRNA"));
        GNames.push_back(string("gIL4mRNA"));
        GNames.push_back(string("gIL4RmRNA"));
        GNames.push_back(string("gIL13mRNA"));
        GNames.push_back(string("gIL13RmRNA"));
        GNames.push_back(string("gIL5mRNA"));
        GNames.push_back(string("gSTAT6mRNA"));
        GNames.push_back(string("gGATA3mRNA"));
        GNames.push_back(string("gIL2mRNA"));
        GNames.push_back(string("gIL2RAmRNA"));
        GNames.push_back(string("gSTAT5mRNA"));
        GNames.push_back(string("gTGFBmRNA"));
        GNames.push_back(string("gTGFBRmRNA"));
        GNames.push_back(string("gSMAD23mRNA"));
        GNames.push_back(string("gFOXP3mRNA"));
        GNames.push_back(string("gIL6mRNA"));
        GNames.push_back(string("gIL6RmRNA"));
        GNames.push_back(string("gIL21mRNA"));
        GNames.push_back(string("gIL21RmRNA"));
        GNames.push_back(string("gIL10mRNA"));
        GNames.push_back(string("gIL10RmRNA"));
        GNames.push_back(string("gSTAT3mRNA"));
        GNames.push_back(string("gRORGTmRNA"));
        GNames.push_back(string("gIL17mRNA"));
        GNames.push_back(string("gIL22mRNA"));
        GNames.push_back(string("gIL17FmRNA"));
        GNames.push_back(string("gAntiIL4"));
        GNames.push_back(string("gAntiIFNg"));
        GNames.push_back(string("gAntiIL2"));
        GNames.push_back(string("gOpenIL2"));
        GNames.push_back(string("gOpenIL21"));
        GNames.push_back(string("gOpenFOXP3"));
        GNames.push_back(string("gOpenRORGT"));
        GNames.push_back(string("gOpenTBET"));
        GNames.push_back(string("gTransl"));
        GNames.push_back(string("gSecret"));
    }
    if((id < 0) || (id >= (int) GNames.size())) {
          return string("Not Found");
    }
    else return GNames[id];
}

string CodingName(int id){
    static bool loaded = false;

    if(!loaded){
        codingNames.push_back(string("N::NONAME"));
        codingNames.push_back(string("N::IL12"));
        codingNames.push_back(string("N::STAT4"));
        codingNames.push_back(string("N::STAT4P"));
        codingNames.push_back(string("N::TBET"));
        codingNames.push_back(string("N::IFNG"));
        codingNames.push_back(string("N::IL12RB2"));
        codingNames.push_back(string("N::RL12"));
        codingNames.push_back(string("N::RIFN"));
        codingNames.push_back(string("N::IFNGR"));
        codingNames.push_back(string("N::STAT1"));
        codingNames.push_back(string("N::STAT1P"));
        codingNames.push_back(string("N::IL4"));
        codingNames.push_back(string("N::IL4R"));
        codingNames.push_back(string("N::RL4"));
        codingNames.push_back(string("N::IL13"));
        codingNames.push_back(string("N::IL13R"));
        codingNames.push_back(string("N::RL13"));
        codingNames.push_back(string("N::IL5"));
        codingNames.push_back(string("N::STAT6"));
        codingNames.push_back(string("N::STAT6P"));
        codingNames.push_back(string("N::GATA3"));
        codingNames.push_back(string("N::GATA3TBET"));
        codingNames.push_back(string("N::IL2"));
        codingNames.push_back(string("N::IL2RA"));
        codingNames.push_back(string("N::RL2"));
        codingNames.push_back(string("N::STAT5"));
        codingNames.push_back(string("N::STAT5P"));
        codingNames.push_back(string("N::TGFB"));
        codingNames.push_back(string("N::TGFBR"));
        codingNames.push_back(string("N::RTGFB"));
        codingNames.push_back(string("N::SMAD23"));
        codingNames.push_back(string("N::SMAD23P"));
        codingNames.push_back(string("N::FOXP3"));
        codingNames.push_back(string("N::FOXP3GATA3"));
        codingNames.push_back(string("N::FOXP3RORGT"));
        codingNames.push_back(string("N::IL6"));
        codingNames.push_back(string("N::IL6R"));
        codingNames.push_back(string("N::RL6"));
        codingNames.push_back(string("N::IL21"));
        codingNames.push_back(string("N::IL21R"));
        codingNames.push_back(string("N::RL21"));
        codingNames.push_back(string("N::IL10"));
        codingNames.push_back(string("N::IL10R"));
        codingNames.push_back(string("N::RL10"));
        codingNames.push_back(string("N::STAT3"));
        codingNames.push_back(string("N::STAT3P"));
        codingNames.push_back(string("N::STAT3SERP"));
        codingNames.push_back(string("N::RORGT"));
        codingNames.push_back(string("N::IL17"));
        codingNames.push_back(string("N::IL22"));
        codingNames.push_back(string("N::IL17F"));
        codingNames.push_back(string("N::SOCS1"));
        codingNames.push_back(string("N::SOCS2"));
        codingNames.push_back(string("N::SOCS3"));
        codingNames.push_back(string("N::SMAD7"));
        codingNames.push_back(string("N::TCR"));
        codingNames.push_back(string("N::STAT4mRNA"));
        codingNames.push_back(string("N::TBETmRNA"));
        codingNames.push_back(string("N::IFNGmRNA"));
        codingNames.push_back(string("N::IL12RB2mRNA"));
        codingNames.push_back(string("N::IFNGRmRNA"));
        codingNames.push_back(string("N::STAT1mRNA"));
        codingNames.push_back(string("N::IL4mRNA"));
        codingNames.push_back(string("N::IL4RmRNA"));
        codingNames.push_back(string("N::IL13mRNA"));
        codingNames.push_back(string("N::IL13RmRNA"));
        codingNames.push_back(string("N::IL5mRNA"));
        codingNames.push_back(string("N::STAT6mRNA"));
        codingNames.push_back(string("N::GATA3mRNA"));
        codingNames.push_back(string("N::IL2mRNA"));
        codingNames.push_back(string("N::IL2RAmRNA"));
        codingNames.push_back(string("N::STAT5mRNA"));
        codingNames.push_back(string("N::TGFBmRNA"));
        codingNames.push_back(string("N::TGFBRmRNA"));
        codingNames.push_back(string("N::SMAD23mRNA"));
        codingNames.push_back(string("N::FOXP3mRNA"));
        codingNames.push_back(string("N::IL6mRNA"));
        codingNames.push_back(string("N::IL6RmRNA"));
        codingNames.push_back(string("N::IL21mRNA"));
        codingNames.push_back(string("N::IL21RmRNA"));
        codingNames.push_back(string("N::IL10mRNA"));
        codingNames.push_back(string("N::IL10RmRNA"));
        codingNames.push_back(string("N::STAT3mRNA"));
        codingNames.push_back(string("N::RORGTmRNA"));
        codingNames.push_back(string("N::IL17mRNA"));
        codingNames.push_back(string("N::IL22mRNA"));
        codingNames.push_back(string("N::IL17FmRNA"));
        codingNames.push_back(string("N::antiIL4"));
        codingNames.push_back(string("N::antiIFNg"));
        codingNames.push_back(string("N::antiIL2"));
        codingNames.push_back(string("N::openIL2"));
        codingNames.push_back(string("N::openIL21"));
        codingNames.push_back(string("N::openFOXP3"));
        codingNames.push_back(string("N::openRORGT"));
        codingNames.push_back(string("N::openTBET"));
        codingNames.push_back(string("N::transl"));
        codingNames.push_back(string("N::secret"));
        loaded = true;
        }
    if((id < 0) || (id >= (int) codingNames.size())) {
          return string("Not Found");
    }
    else return codingNames[id];
}

vector<string> getCodingNames(){
    CodingName(0); // to make sure it's loaded
    return codingNames;
}

vector<string> getGlobalNames(){
    GlobalName(0); // to make sure it's loaded
    return GNames;
}




/*  expCodingNames.push_back(string("TH17IL2"));
    expCodingNames.push_back(string("TH2IL2"));
    expCodingNames.push_back(string("TH1_IFNG"));
    expCodingNames.push_back(string("IFNG_ALONE"));
    expCodingNames.push_back(string("TH1_IFNGRKO"));
    expCodingNames.push_back(string("TH1_STAT4KO"));
    expCodingNames.push_back(string("TH1_TBETKO"));
    expCodingNames.push_back(string("IFNG_IFNGRKO"));
    expCodingNames.push_back(string("IL4_IFNG_TGFB"));
    expCodingNames.push_back(string("TGFB_ALONE"));
    expCodingNames.push_back(string("TH1_IL2"));
    expCodingNames.push_back(string("MIXEDTH1_TH2"));*/
/*
 *     if(background & Back::WT)
    if(background & Back::TCRGATA3NEGKO)
    if(background & Back::TCRIL2KO)
    if(background & Back::TCRGARA3POSKO)
    if(background & Back::TCRTGFBKO)
    if(background & Back::TCRTBETKO)
    if(background & Back::TCRIFNGKO)
    if(background & Back::IL2KO)
    if(background & Back::IL4KO)
    if(background & Back::IL17KO)
    if(background & Back::IL21KO)
    if(background & Back::IFNGKO)
    if(background & Back::TGFBKO)
    if(background & Back::TBETKO)
    if(background & Back::GATA3KO)
    if(background & Back::RORGTKO)
    if(background & Back::FOXP3KO)
    if(background & Back::STAT1KO)
    if(background & Back::STAT3KO)
    if(background & Back::STAT4KO)
    if(background & Back::STAT5KO)
    if(background & Back::STAT6KO)
    if(background & Back::IL6RKO)
    if(background & Back::IL21RKO)
    if(background & Back::TGFBRKO)
    if(background & Back::NOIL2INHIBIL2)
    if(background & Back::NOIL4INHIBIL)
    if(background & Back::NOGATA3ACTIVIL4)
    if(background & Back::NOIL6ACTIVIL21)
    if(background & Back::NORORGTACTIVIL21)
    if(background & Back::NOIL12ACTIVIFN)
    if(background & Back::NOTBETACTIVIFNG)
    if(background & Back::NOIL6ACTIVTBET)
    if(background & Back::NOIL12ACTIVTBET)
    if(background & Back::NOIFNGACTIVTBET)
    if(background & Back::NOGATA3INHIBTBET)
    if(background & Back::NORORGTINHIBTBET)
    if(background & Back::NOFOXP3INHIBTBET)
    if(background & Back::NOIL12INHIBGATA3)
    if(background & Back::NOIL2ACTIVGATA3)
    if(background & Back::NOIL4ACTIVGATA3)
    if(background & Back::NOIFNGINHIBGATA3)
    if(background & Back::NOTBETINHIBGATA3)
    if(background & Back::GATA3FEEDBACKKO)
    if(background & Back::NORORGTINHIBGATA3)
    if(background & Back::NOFOXP3INHIBGATA3)
    if(background & Back::NOIL6ACTIVRORGT)
    if(background & Back::NOIL21ACTIVRORGT)
    if(background & Back::NOTGFBACTIVRORGT)
    if(background & Back::NOTBETINHIBRORGT)
    if(background & Back::NOGATA3INHIBRORGT)
    if(background & Back::NOFOXP3INHIBRORGT)
    if(background & Back::NOIL2ACTIVFOXP3)
    if(background & Back::NOTGFBACTIVFOXP3)
    if(background & Back::NOTBETINHIBFOXP3)
    if(background & Back::NOGATA3INHIBFOXP3)
    if(background & Back::NORORGTINHIBFOXP3)
    if(background & Back::NODELAYIL2)
    if(background & Back::NODELAYIL21)
    if(background & Back::NODELAYFOXP3)
    if(background & Back::NODELAYRORGT)
    if(background & Back::NODELAYTRANSL)
    if(background & Back::NODELAYSECRET)

    */
