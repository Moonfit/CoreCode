#ifndef EXPERIMENTSTHALL_H
#define EXPERIMENTSTHALL_H



#include "namesTh.h"
#include "../Moonfit/moonfit.h"

#include <vector>
#include <string>
#include <iostream>

using namespace std;

struct expThs : public Experiment {
    //vector<Evaluator*> VTG; // Values to get
    //double totalPenalities;
    expThs(Model* _m);

    /*vector<string> NamesVariablesInTheirIndex;
    vector<TableCourse*> ExpData;
    vector<TableCourse*> ExpStdDevs;
    void giveData(TableCourse* kineticData, int IdExp);
    void giveStdDevs(TableCourse* kineticStds, int IdExp);
    void giveHowToReadNamesInKinetics(vector<string> _NamesVariablesInTheirIndex);
    void loadEvaluators();

    double V(int IDexp, int Species, int time_sec){return VTG[IDexp]->getVal(m->internValName(Species), time_sec);}
*/
    //void init();
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false); // if no E is given, VTG[i] is used
    //void simulateAll();                 // simulates everything
    //virtual double costPart();
    //void reset();

    /*void setOverrider(int IdExp, overrider* _ov = NULL);
    vector<overrider*> Overs;
    void overrideVariable(int IdGlobalName, bool override = true, int IdExp = -1);*/
};


struct expThsHours : public expThs {
    expThsHours(Model* _m) : expThs(_m){};
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false); // if no E is given, VTG[i] is used
};

/*string generateCostFromKinetics(int ExpID, vector<double> timePoints, vector<int> IDGlobVariables = vector<int>()){



     double c1a[] = {V(TH1, N::TBET,0 DAY), V(TH1, N::TBET,1 DAY), V(TH1, N::TBET,2 DAY), V(TH1, N::TBET,3 DAY), V(TH1, N::TBET,4 DAY), V(TH1, N::TBET,5 DAY), V(TH1, N::TBET,6 DAY)};
    double c1b[] = {0.033,	0.5,	0.21,	0.18,	0.31,	0.51,	0.28};
    res += functCost(c1a, c1b, 7);*/






#endif // EXPERIMENTSTHALL_H
