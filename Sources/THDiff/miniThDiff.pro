include("../Moonfit/moonfitNoLib.pri")

QT -= opengl
CONFIG -= QT
DEFINES += "WITHOUT_QT"

#name of the executable file generated
TARGET = MiniTHDiff


HEADERS += \
    M6a-LatentTbet2/modeleLatentTbet2Hours.h \
    expPred.h \
    expThs.h \
    namesTh.h 
	
SOURCES += \
    M6a-LatentTbet2/modeleLatentTbet2Hours.cpp \
    expPred.cpp \
    expThs.cpp \
    namesTh.cpp \
	miniMain.cpp


