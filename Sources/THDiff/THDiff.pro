include("../Moonfit/moonfit.pri")

QT += opengl
QT += svg

#name of the executable file generated
TARGET = THDiffExe

CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    M0a-MTh5/modeleThTot5.h \
    M0b-MTh5Custom/modeleThTot5custom.h \
    M1-MTh6/modeleThTot6.h \
    M2-MTh7/modeleThTot7.h \
    M3a-SimpleNoIL10/modeleSimpleNoIL10.h \
    M3c-MinNoIL10/modeleMinNoIL10.h \
    M4-MinLatent/modeleMinLatent.h \
    M5-MinLatentTbet/modeleLatentTbet.h \
    M6a-LatentTbet2/modeleLatentTbet2.h \
    M6b-LatentTbet2Simplified/modeleLatentTbetSimplified.h \
    expPred.h \
    expThs.h \
    namesTh.h \
    ScriptMai2015.h \
    scriptP6.h \
    scriptP7.h \
    M6c-LatentTbet2Gata3Mult/modeleLatentTbet2Gata3Mult.h \
    M6a-LatentTbet2/modeleLatentTbet2BigFraction.h \
    M6a-LatentTbet2/modeleLatentTbet2Gangs.h \
    M6a-LatentTbet2/modeleLatentTbet2MassAction.h \
    M6a-LatentTbet2/modeleLatentTbet2MassActionOne.h \
    M6a-LatentTbet2/modeleLatentTbet2Polygon.h \
    StartingInterface/starter.h

SOURCES += \
    M0a-MTh5/modeleThTot5.cpp \
    M0b-MTh5Custom/modeleThTot5custom.cpp \
    M1-MTh6/modeleThTot6.cpp \
    M2-MTh7/modeleThTot7.cpp \
    M3a-SimpleNoIL10/modeleSimpleNoIL10.cpp \
    M3c-MinNoIL10/modeleMinNoIL10.cpp \
    M4-MinLatent/modeleMinLatent.cpp \
    M5-MinLatentTbet/modeleLatentTbet.cpp \
    M6b-LatentTbet2Simplified/modeleLatentTbetSimplified.cpp \
    expPred.cpp \
    expThs.cpp \
    namesTh.cpp \
    scriptP6.cpp \
    Script13Jan2017.cpp \
    M6a-LatentTbet2/modeleLatentTbet2.cpp \
    M6c-LatentTbet2Gata3Mult/modeleLatentTbet2Gata3Mult.cpp \
    M6a-LatentTbet2/modeleLatentTbet2BigFraction.cpp \
    M6a-LatentTbet2/modeleLatentTbet2Gangs.cpp \
    M6a-LatentTbet2/modeleLatentTbet2MassAction.cpp \
    M6a-LatentTbet2/modeleLatentTbet2MassActionOne.cpp \
    M6a-LatentTbet2/modeleLatentTbet2Polygon.cpp \
    StartingInterface/starter.cpp

FORMS += \
    StartingInterface/starter.ui


