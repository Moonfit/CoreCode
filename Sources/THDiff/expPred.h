#ifndef EXPERIMENTSPRED_H
#define EXPERIMENTSPRED_H

#include "namesTh.h"
#include "../Moonfit/moonfit.h"
#include <vector>
#include <string>
#include <iostream>

#ifndef WITHOUT_QT
#include <QString>
#endif



using namespace std;



#define DAY *24*3600
#define HR *3600
#define LdiffPert 5.1

enum TH_EXTENDED   {EXT_TH1,    EXT_TH2,    EXT_ITREG,    EXT_TH17,     EXT_TH0,    TH1_NO_aIL4, TH2_NO_aIFNg, TREG_NO_IL2, TREG_TGFB_aIL2, TH17_NO_aIL2, TH17_NO_aIFNG, TH17_NO_AB, TH0_aIL4, TH0_aIFNg, TH0_aIL2, TH0_aIFNg_aIL4, NB_EXTENDED};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};

struct expCanoExtended : public Experiment {
    expCanoExtended(Model* _m) : Experiment(_m, NB_EXTENDED){
        Identification = string("Canonical differentiation + more conditions");
        names_exp[EXT_TH1] =     string("Th1");
        names_exp[EXT_TH2] =     string("Th2");
        names_exp[EXT_ITREG] =   string("iTreg");
        names_exp[EXT_TH17] =    string("Th17");
        names_exp[EXT_TH0] =     string("Th0");
        names_exp[TH1_NO_aIL4] =        string("Th1 No a-IL4");
        names_exp[TH2_NO_aIFNg] =       string("Th2 No a-IFNg");
        names_exp[TREG_NO_IL2] =        string("Treg No IL2");
        names_exp[TREG_TGFB_aIL2] =     string("Treg TGFb + a-IL2");
        names_exp[TH17_NO_aIL2] =       string("Th17 No a-IL2");
        names_exp[TH17_NO_aIFNG] =      string("Th17 No a-IFNg");
        names_exp[TH17_NO_AB] =         string("Th17 No Blocks");
        names_exp[TH0_aIL4] =           string("Th0 + aIL4");
        names_exp[TH0_aIFNg] =          string("Th0 + aIFNg");
        names_exp[TH0_aIL2] =           string("Th0 + aIL2");
        names_exp[TH0_aIFNg_aIL4] =     string("Th0 + aIFNg & aIL4");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();

            switch(IdExp){
            case TH0:{ break;}
            case TH1: {
                    m->setValue("IL12", 10.0);
                    m->setValue("antiIL4", 10000.0); break;}
            case TH2: {
                    m->setValue("IL4", 14.6);
                    m->setValue("antiIFNg", 10000.0); break;}
            case TREG: {
                    m->setValue("TGFB", 0.5);
                    m->setValue("IL2", 19.05); break;}
            case TH17: {
                    m->setValue("IL6", 30.0);
                    m->setValue("TGFB", 0.2);
                    m->setValue("antiIL2", 5000.0);
                    m->setValue("antiIFNg", 10000.0); break;}
            case TH1_NO_aIL4: {
                    m->setValue("IL12", 10.0); break;}
            case TH2_NO_aIFNg: {
                    m->setValue("IL4", 14.6); break;}
            case TREG_NO_IL2: {
                    m->setValue("TGFB", 0.5); break;}
            case TREG_TGFB_aIL2: {
                    m->setValue("TGFB", 0.5);
                    m->setValue("antiIL2", 5000.0); break;}
            case TH17_NO_aIL2: {
                    m->setValue("IL6", 30.0);
                    m->setValue("TGFB", 0.2);
                    // m->setValue("antiIL2", 5000.0);
                    m->setValue("antiIFNg", 10000.0); break;}
            case TH17_NO_aIFNG: {
                    m->setValue("IL6", 30.0);
                    m->setValue("TGFB", 0.2);
                    m->setValue("antiIL2", 5000.0); break;}
            case TH17_NO_AB: {
                    m->setValue("IL6", 30.0);
                    m->setValue("TGFB", 0.2); break;}
            case TH0_aIL4: {
                    m->setValue("antiIL4", 10000.0); break;}
            case TH0_aIFNg: {
                    m->setValue("antiIFNg", 10000.0); break;}
            case TH0_aIL2: {
                    m->setValue("antiIL2", 10000.0); break;}
            case TH0_aIFNg_aIL4: {
                    m->setValue("antiIFNg", 10000.0);
                    m->setValue("antiIL4", 10000.0); break;}
            }
            m->simulate(LdiffPert * 24 * 3600, E);
            m->setOverrider(nullptr);
        }
    }
};











enum FROM_TH1   {TH1_TO_TH1,    TH1_TO_TH2,     TH1_TO_ITREG,    TH1_TO_TH17,    TH1_TO_TH0,     TH1_UNTOUCHED,     NB_FROM_TH1};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};
enum FROM_TH2   {TH2_TO_TH1,    TH2_TO_TH2,     TH2_TO_ITREG,    TH2_TO_TH17,    TH2_TO_TH0,     TH2_UNTOUCHED,     NB_FROM_TH2};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};
enum FROM_ITREG {ITREG_TO_TH1,  ITREG_TO_TH2,   ITREG_TO_ITREG,  ITREG_TO_TH17,  ITREG_TO_TH0,   ITREG_UNTOUCHED,   NB_FROM_ITREG};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};
enum FROM_TH17  {TH17_TO_TH1,   TH17_TO_TH2,    TH17_TO_ITREG,   TH17_TO_TH17,   TH17_TO_TH0,    TH17_UNTOUCHED,    NB_FROM_TH17};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};
enum FROM_TH0   {TH0_TO_TH1,    TH0_TO_TH2,     TH0_TO_ITREG,    TH0_TO_TH17,    TH0_TO_TH0,     TH0_UNTOUCHED,     NB_FROM_TH0};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};

// Problem regarding the washing of antibodies !
#define washingEfficiency 1.0




struct exp20Hours : public Experiment {
    double parameter;
    exp20Hours(Model* _m, double _parameter) : Experiment(_m, 5*NB_FROM_TH1), parameter(_parameter) {
        stringstream res; res << "Condition switching (all) at " << parameter << " hours"; Identification = res.str();
        names_exp[TH1_TO_TH1] =     string("Th1 -> Th1");
        names_exp[TH1_TO_TH2] =     string("Th1 -> Th2");
        names_exp[TH1_TO_ITREG] =   string("Th1 -> iTreg");
        names_exp[TH1_TO_TH17] =    string("Th1 -> Th17");
        names_exp[TH1_TO_TH0] =     string("Th1 -> Th0");
        names_exp[TH1_UNTOUCHED] =     string("Th1 untouched");
        names_exp[6+TH2_TO_TH1] =     string("Th2 -> Th1");
        names_exp[6+TH2_TO_TH2] =     string("Th2 -> Th2");
        names_exp[6+TH2_TO_ITREG] =   string("Th2 -> iTreg");
        names_exp[6+6+TH2_TO_TH17] =    string("Th2 -> Th17");
        names_exp[6+TH2_TO_TH0] =     string("Th2 -> Th0");
        names_exp[6+TH2_UNTOUCHED] =  string("Th2 untouched");
        names_exp[12+ITREG_TO_TH1] =     string("iTreg -> Th1");
        names_exp[12+ITREG_TO_TH2] =     string("iTreg -> Th2");
        names_exp[12+ITREG_TO_ITREG] =   string("iTreg -> iTreg");
        names_exp[12+ITREG_TO_TH17] =    string("iTreg -> Th17");
        names_exp[12+ITREG_TO_TH0] =     string("iTreg -> Th0");
        names_exp[12+ITREG_UNTOUCHED] =  string("iTreg untouched");
        names_exp[18+TH17_TO_TH1] =     string("Th17 -> Th1");
        names_exp[18+TH17_TO_TH2] =     string("Th17 -> Th2");
        names_exp[18+TH17_TO_ITREG] =   string("Th17 -> iTreg");
        names_exp[18+TH17_TO_TH17] =    string("Th17 -> Th17");
        names_exp[18+TH17_TO_TH0] =     string("Th17 -> Th0");
        names_exp[18+TH17_UNTOUCHED] =  string("Th17 untouched");
        names_exp[24+TH0_TO_TH1] =     string("Th0 -> Th1");
        names_exp[24+TH0_TO_TH2] =     string("Th0 -> Th2");
        names_exp[24+TH0_TO_ITREG] =   string("Th0 -> iTreg");
        names_exp[24+TH0_TO_TH17] =    string("Th0 -> Th17");
        names_exp[24+TH0_TO_TH0] =     string("Th0 -> Th0");
        names_exp[24+TH0_UNTOUCHED] =  string("Th0 untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expFromTh1, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            if((IdExp < NB_FROM_TH1) && (IdExp >= 0)){
                m->setValue("IL12", 10.0);
                m->setValue("antiIL4", 10000.0);
                m->simulate(parameter*3600, E);
                if(IdExp != TH1_UNTOUCHED) m->action(string("wash"), washingEfficiency);
                switch(IdExp){
                    case TH1_TO_TH1: {
                        m->addValue("IL12", 10.0);
                        m->addValue("antiIL4", 10000.0);break;}
                    case TH1_TO_TH2: {
                        m->addValue("IL4", 14.6);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case TH1_TO_ITREG: {
                        m->addValue("TGFB", 0.5);
                        m->addValue("IL2", 19.05);break;}
                    case TH1_TO_TH17: {
                        m->addValue("IL6", 30.0);
                        m->addValue("TGFB", 0.2);
                        m->addValue("antiIL2", 5000.0);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case TH1_TO_TH0: { break;}
                }
            }
            if((IdExp < 2*NB_FROM_TH1) && (IdExp >= NB_FROM_TH1)){
                m->setValue("IL4", 14.6);
                m->setValue("antiIFNg", 10000.0);
                m->simulate(parameter*3600, E);
                if(IdExp != 6+TH2_UNTOUCHED) m->action(string("wash"), washingEfficiency);

                switch(IdExp){
                    case 6+TH2_TO_TH1: {
                        m->addValue("IL12", 10.0);
                        m->addValue("antiIL4", 10000.0);break;}
                    case 6+TH2_TO_TH2: {
                        m->addValue("IL4", 14.6);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case 6+TH2_TO_ITREG: {
                        m->addValue("TGFB", 0.5);
                        m->addValue("IL2", 19.05);break;}
                    case 6+TH2_TO_TH17: {
                        m->addValue("IL6", 30.0);
                        m->addValue("TGFB", 0.2);
                        m->addValue("antiIL2", 5000.0);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case 6+TH2_TO_TH0: { break;}
                }
            }
            if((IdExp < 3*NB_FROM_TH1) && (IdExp >= 2*NB_FROM_TH1)){
                m->setValue("TGFB", 0.5);
                m->setValue("IL2", 19.05);
                m->simulate(parameter*3600, E);
                if(IdExp != 12+ITREG_UNTOUCHED) m->action(string("wash"), washingEfficiency);
                switch(IdExp){
                    case 12+ITREG_TO_TH1: {
                        m->addValue("IL12", 10.0);
                        m->addValue("antiIL4", 10000.0);break;}
                    case 12+ITREG_TO_TH2: {
                        m->addValue("IL4", 14.6);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case 12+ITREG_TO_ITREG: {
                        m->addValue("TGFB", 0.5);
                        m->addValue("IL2", 19.05);break;}
                    case 12+ITREG_TO_TH17: {
                        m->addValue("IL6", 30.0);
                        m->addValue("TGFB", 0.2);
                        m->addValue("antiIL2", 5000.0);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case 12+ITREG_TO_TH0: { break;}
                }
            }
            if((IdExp < 4*NB_FROM_TH1) && (IdExp >= 3*NB_FROM_TH1)){
                m->setValue("IL6", 30.0);
                m->setValue("TGFB", 0.2);
                m->setValue("antiIL2", 5000.0);
                m->setValue("antiIFNg", 10000.0);
                m->simulate(parameter*3600, E);
                if(IdExp != 18+TH17_UNTOUCHED) m->action(string("wash"), washingEfficiency);
                switch(IdExp){
                    case 18+TH17_TO_TH1: {
                        m->addValue("IL12", 10.0);
                        m->addValue("antiIL4", 10000.0);break;}
                    case 18+TH17_TO_TH2: {
                        m->addValue("IL4", 14.6);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case 18+TH17_TO_ITREG: {
                        m->addValue("TGFB", 0.5);
                        m->addValue("IL2", 19.05);break;}
                    case 18+TH17_TO_TH17: {
                        m->addValue("IL6", 30.0);
                        m->addValue("TGFB", 0.2);
                        m->addValue("antiIL2", 5000.0);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case 18+TH17_TO_TH0: { break;}
                }
            }
            if((IdExp < 5*NB_FROM_TH1) && (IdExp >= 4*NB_FROM_TH1)){
                m->simulate(parameter*3600, E);
                if(IdExp != 24+TH0_UNTOUCHED) m->action(string("wash"), washingEfficiency);
                switch(IdExp){
                    case 24+TH0_TO_TH1: {
                        m->addValue("IL12", 10.0);
                        m->addValue("antiIL4", 10000.0);break;}
                    case 24+TH0_TO_TH2: {
                        m->addValue("IL4", 14.6);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case 24+TH0_TO_ITREG: {
                        m->addValue("TGFB", 0.5);
                        m->addValue("IL2", 19.05);break;}
                    case 24+TH0_TO_TH17: {
                        m->addValue("IL6", 30.0);
                        m->addValue("TGFB", 0.2);
                        m->addValue("antiIL2", 5000.0);
                        m->addValue("antiIFNg", 10000.0); break;}
                    case 24+TH0_TO_TH0: { break;}
                }
            }
            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};















struct expFromTh1 : public Experiment {
    double parameter;
    expFromTh1(Model* _m, double _parameter) : Experiment(_m, NB_FROM_TH1), parameter(_parameter) {
        stringstream res; res << "Switching from Th1 at " << parameter << " hours"; Identification = res.str();
        names_exp[TH1_TO_TH1] =     string("Th1 -> Th1");
        names_exp[TH1_TO_TH2] =     string("Th1 -> Th2");
        names_exp[TH1_TO_ITREG] =   string("Th1 -> iTreg");
        names_exp[TH1_TO_TH17] =    string("Th1 -> Th17");
        names_exp[TH1_TO_TH0] =     string("Th1 -> Th0");
        names_exp[TH1_UNTOUCHED] =     string("Th1 untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expFromTh1, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            m->setValue("IL12", 10.0);
            m->setValue("antiIL4", 10000.0);
            m->simulate(parameter*3600, E);
            if(IdExp != TH1_UNTOUCHED) m->action(string("wash"), washingEfficiency);

            switch(IdExp){
                case TH1_TO_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case TH1_TO_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH1_TO_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case TH1_TO_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH1_TO_TH0: { break;}
            }
            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};

struct expFromTh2 : public Experiment {
    double parameter;
    expFromTh2(Model* _m, double _parameter) : Experiment(_m, NB_FROM_TH2), parameter(_parameter) {
        stringstream res; res << "Switching from Th2 at " << parameter << " hours"; Identification = res.str();
        names_exp[TH2_TO_TH1] =     string("Th2 -> Th1");
        names_exp[TH2_TO_TH2] =     string("Th2 -> Th2");
        names_exp[TH2_TO_ITREG] =   string("Th2 -> iTreg");
        names_exp[TH2_TO_TH17] =    string("Th2 -> Th17");
        names_exp[TH2_TO_TH0] =     string("Th2 -> Th0");
        names_exp[TH2_UNTOUCHED] =  string("Th2 untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expFromTh2, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            m->setValue("IL4", 14.6);
            m->setValue("antiIFNg", 10000.0);
            m->simulate(parameter*3600, E);
            if(IdExp != TH2_UNTOUCHED) m->action(string("wash"), washingEfficiency);

            switch(IdExp){
                case TH2_TO_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case TH2_TO_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH2_TO_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case TH2_TO_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH2_TO_TH0: { break;}
            }
            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};

struct expFromTreg : public Experiment {
    double parameter;
    expFromTreg(Model* _m, double _parameter) : Experiment(_m, NB_FROM_ITREG), parameter(_parameter) {
        stringstream res; res << "Switching from iTreg at " << parameter << " hours"; Identification = res.str();
        names_exp[ITREG_TO_TH1] =     string("iTreg -> Th1");
        names_exp[ITREG_TO_TH2] =     string("iTreg -> Th2");
        names_exp[ITREG_TO_ITREG] =   string("iTreg -> iTreg");
        names_exp[ITREG_TO_TH17] =    string("iTreg -> Th17");
        names_exp[ITREG_TO_TH0] =     string("iTreg -> Th0");
        names_exp[ITREG_UNTOUCHED] =  string("iTreg untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expFromTreg, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            m->setValue("TGFB", 0.5);
            m->setValue("IL2", 19.05);
            m->simulate(parameter*3600, E);
            if(IdExp != ITREG_UNTOUCHED) m->action(string("wash"), washingEfficiency);

            switch(IdExp){
                case ITREG_TO_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case ITREG_TO_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case ITREG_TO_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case ITREG_TO_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case ITREG_TO_TH0: { break;}
            }
            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};

struct expFromTh17 : public Experiment {
    double parameter;
    expFromTh17(Model* _m, double _parameter) : Experiment(_m, NB_FROM_TH17), parameter(_parameter) {
        stringstream res; res << "Switching from Th17 at " << parameter << " hours"; Identification = res.str();
        names_exp[TH17_TO_TH1] =     string("Th17 -> Th1");
        names_exp[TH17_TO_TH2] =     string("Th17 -> Th2");
        names_exp[TH17_TO_ITREG] =   string("Th17 -> iTreg");
        names_exp[TH17_TO_TH17] =    string("Th17 -> Th17");
        names_exp[TH17_TO_TH0] =     string("Th17 -> Th0");
        names_exp[TH17_UNTOUCHED] =  string("Th17 untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){ // does the setoverrider ??
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expFromTh17, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            m->setValue("IL6", 30.0);
            m->setValue("TGFB", 0.2);
            m->setValue("antiIL2", 5000.0);
            m->setValue("antiIFNg", 10000.0);
            m->simulate(parameter*3600, E);
            if(IdExp != TH17_UNTOUCHED) m->action(string("wash"), washingEfficiency);

            switch(IdExp){
                case TH17_TO_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case TH17_TO_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH17_TO_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case TH17_TO_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH17_TO_TH0: { break;}
            }
            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};

struct expFromTh0 : public Experiment {
    double parameter;   // here, the parameter is the length before changing the differentiation cocktail
    expFromTh0(Model* _m, double _parameter) : Experiment(_m, NB_FROM_TH0), parameter(_parameter) {
        stringstream res; res << "Switching from Th0 at " << parameter << " hours"; Identification = res.str();
        names_exp[TH0_TO_TH1] =     string("Th0 -> Th1");
        names_exp[TH0_TO_TH2] =     string("Th0 -> Th2");
        names_exp[TH0_TO_ITREG] =   string("Th0 -> iTreg");
        names_exp[TH0_TO_TH17] =    string("Th0 -> Th17");
        names_exp[TH0_TO_TH0] =     string("Th0 -> Th0");
        names_exp[TH0_UNTOUCHED] =  string("Th0 untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expFromTh0, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            m->simulate(parameter*3600, E);
            if(IdExp != TH0_UNTOUCHED) m->action(string("wash"), washingEfficiency);

            switch(IdExp){
                case TH0_TO_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case TH0_TO_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH0_TO_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case TH0_TO_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH0_TO_TH0: { break;}
            }
            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};
















enum TO_TH1   {TH1_FROM_TH1,    TH1_FROM_TH2,     TH1_FROM_ITREG,    TH1_FROM_TH17,    TH1_FROM_TH0,     TH1_UNTOUCHED_BIS,     NB_TO_TH1};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};
enum TO_TH2   {TH2_FROM_TH1,    TH2_FROM_TH2,     TH2_FROM_ITREG,    TH2_FROM_TH17,    TH2_FROM_TH0,     TH2_UNTOUCHED_BIS,     NB_TO_TH2};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};
enum TO_ITREG {ITREG_FROM_TH1,  ITREG_FROM_TH2,   ITREG_FROM_ITREG,  ITREG_FROM_TH17,  ITREG_FROM_TH0,   ITREG_UNTOUCHED_BIS,   NB_TO_ITREG};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};
enum TO_TH17  {TH17_FROM_TH1,   TH17_FROM_TH2,    TH17_FROM_ITREG,   TH17_FROM_TH17,   TH17_FROM_TH0,    TH17_UNTOUCHED_BIS,    NB_TO_TH17};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};
enum TO_TH0   {TH0_FROM_TH1,    TH0_FROM_TH2,     TH0_FROM_ITREG,    TH0_FROM_TH17,    TH0_FROM_TH0,     TH0_UNTOUCHED_BIS,     NB_TO_TH0};// TH1_IFNG, IFNG_ALONE, TH1_IFNGRKO, TH1_STAT4KO, TH1_TBETKO, IFNG_IFNGRKO, IL4_IFNG_TGFB, TGFB_ALONE, TH1_IL2, MIXEDTH1_TH2, TH2, TH2IL2, TH17, TH17IL2, TREG, NB_EXP};








struct expToTh1 : public Experiment {
    double parameter;
    expToTh1(Model* _m, double _parameter) : Experiment(_m, NB_TO_TH1), parameter(_parameter) {
        stringstream res; res << "Switching To Th1 at " << parameter << " hours"; Identification = res.str();
        names_exp[TH1_FROM_TH1] =     string("Th1  -> Th1");
        names_exp[TH1_FROM_TH2] =     string("Th2  -> Th1");
        names_exp[TH1_FROM_ITREG] =   string("iTreg-> Th1");
        names_exp[TH1_FROM_TH17] =    string("Th17 -> Th1");
        names_exp[TH1_FROM_TH0] =     string("Th0  -> Th1");
        names_exp[TH1_UNTOUCHED_BIS] =     string("Th1 untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expToTh1, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            switch(IdExp){
                case TH1_FROM_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case TH1_FROM_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH1_FROM_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case TH1_FROM_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH1_FROM_TH0: { break;}
            }

            m->simulate(parameter*3600, E);
            if(IdExp != TH1_UNTOUCHED_BIS) m->action(string("wash"), washingEfficiency);

            m->setValue("IL12", 10.0);
            m->setValue("antiIL4", 10000.0);

            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};

struct expToTh2 : public Experiment {
    double parameter;
    expToTh2(Model* _m, double _parameter) : Experiment(_m, NB_TO_TH2), parameter(_parameter) {
        stringstream res; res << "Switching to Th2 at " << parameter << " hours"; Identification = res.str();
        names_exp[TH2_FROM_TH1] =     string("Th1  -> Th2");
        names_exp[TH2_FROM_TH2] =     string("Th2  -> Th2");
        names_exp[TH2_FROM_ITREG] =   string("iTreg-> Th2");
        names_exp[TH2_FROM_TH17] =    string("Th17 -> Th2");
        names_exp[TH2_FROM_TH0] =     string("Th0  -> Th2");
        names_exp[TH2_UNTOUCHED_BIS] =  string("Th2 untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expToTh2, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;


            switch(IdExp){
                case TH2_FROM_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case TH2_FROM_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH2_FROM_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case TH2_FROM_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH2_FROM_TH0: { break;}
            }
            m->simulate(parameter*3600, E);
            if(IdExp != TH2_UNTOUCHED_BIS) m->action(string("wash"), washingEfficiency);

            m->setValue("IL4", 14.6);
            m->setValue("antiIFNg", 10000.0);

            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};

struct expToTreg : public Experiment {
    double parameter;
    expToTreg(Model* _m, double _parameter) : Experiment(_m, NB_TO_ITREG), parameter(_parameter) {
        stringstream res; res << "Switching to iTreg at " << parameter << " hours"; Identification = res.str();
        names_exp[ITREG_FROM_TH1] =     string("Th1   -> iTreg");
        names_exp[ITREG_FROM_TH2] =     string("Th2   -> iTreg");
        names_exp[ITREG_FROM_ITREG] =   string("iTreg -> iTreg");
        names_exp[ITREG_FROM_TH17] =    string("Th17  -> iTreg");
        names_exp[ITREG_FROM_TH0] =     string("Th0   -> iTreg");
        names_exp[ITREG_UNTOUCHED_BIS] =  string("iTreg untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expToTreg, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            switch(IdExp){
                case ITREG_FROM_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case ITREG_FROM_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case ITREG_FROM_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case ITREG_FROM_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case ITREG_FROM_TH0: { break;}
            }

            m->simulate(parameter*3600, E);
            if(IdExp != ITREG_UNTOUCHED_BIS) m->action(string("wash"), washingEfficiency);

            m->setValue("TGFB", 0.5);
            m->setValue("IL2", 19.05);


            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};

struct expToTh17 : public Experiment {
    double parameter;
    expToTh17(Model* _m, double _parameter) : Experiment(_m, NB_TO_TH17), parameter(_parameter) {
        stringstream res; res << "Switching to Th17 at " << parameter << " hours"; Identification = res.str();
        names_exp[TH17_FROM_TH1] =     string("Th1  -> Th17");
        names_exp[TH17_FROM_TH2] =     string("Th2  -> Th17");
        names_exp[TH17_FROM_ITREG] =   string("iTreg-> Th17");
        names_exp[TH17_FROM_TH17] =    string("Th17 -> Th17");
        names_exp[TH17_FROM_TH0] =     string("Th0  -> Th17");
        names_exp[TH17_UNTOUCHED_BIS] =  string("Th17 untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){ // does the setoverrider ??
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expToTh17, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            switch(IdExp){
                case TH17_FROM_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case TH17_FROM_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH17_FROM_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case TH17_FROM_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH17_FROM_TH0: { break;}
            }

            m->simulate(parameter*3600, E);
            if(IdExp != TH17_UNTOUCHED_BIS) m->action(string("wash"), washingEfficiency);

            m->setValue("IL6", 30.0);
            m->setValue("TGFB", 0.2);
            m->setValue("antiIL2", 5000.0);
            m->setValue("antiIFNg", 10000.0);


            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};

struct expToTh0 : public Experiment {
    double parameter;   // here, the parameter is the length before changing the differentiation cocktail
    expToTh0(Model* _m, double _parameter) : Experiment(_m, NB_TO_TH0), parameter(_parameter) {
        stringstream res; res << "Switching to Th0 at " << parameter << " hours"; Identification = res.str();
        names_exp[TH0_FROM_TH1] =     string("Th1  -> Th0");
        names_exp[TH0_FROM_TH2] =     string("Th2  -> Th0");
        names_exp[TH0_FROM_ITREG] =   string("iTreg-> Th0");
        names_exp[TH0_FROM_TH17] =    string("Th17 -> Th0");
        names_exp[TH0_FROM_TH0] =     string("Th0  -> Th0");
        names_exp[TH0_UNTOUCHED_BIS] =  string("Th0 untouched");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expToTh0, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            switch(IdExp){
                case TH0_FROM_TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case TH0_FROM_TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH0_FROM_ITREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case TH0_FROM_TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH0_FROM_TH0: { break;}
            }

            m->simulate(parameter*3600, E);
            if(IdExp != TH0_UNTOUCHED_BIS) m->action(string("wash"), washingEfficiency);

            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};






















enum DOSES{DOSE0, DOSE0p01, DOSE0p1, DOSE0p8, DOSE1, DOSE5, DOSE10, DOSE20, DOSE40, /*DOSE60, */DOSE80, DOSE100, NB_DOSES};

struct expDosesTH1 : public Experiment {
    int parameter;  // this time the parameter is the (global !!) index of the input variable.
    expDosesTH1(Model* _m, int _parameter) : Experiment(_m, NB_DOSES), parameter(_parameter) {
        stringstream res; res << "Th1 + doses of " << m->getExternalName(parameter) << " from the beginning"; Identification = res.str();
        names_exp[DOSE0] =          string("TH1-DOSE0");
        names_exp[DOSE0p01] =       string("TH1-DOSE0p01");
        names_exp[DOSE0p1] =        string("TH1-DOSE0p1");
        names_exp[DOSE0p8] =        string("TH1-DOSE0p8");
        names_exp[DOSE1] =          string("TH1-DOSE1");
        names_exp[DOSE5] =          string("TH1-DOSE5");
        names_exp[DOSE10] =         string("TH1-DOSE10");
        names_exp[DOSE20] =         string("TH1-DOSE20");
        names_exp[DOSE40] =         string("TH1-DOSE40");
        //names_exp[DOSE60] =         string("TH1-DOSE60");
        names_exp[DOSE80] =         string("TH1-DOSE80");
        names_exp[DOSE100] =        string("TH1-DOSE100");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            if(parameter < 0) cerr << "ERR: expDoses, parameter should be the index of the global variable whose dose should be fixed. Out of bounds (nb of global variables :" << N::NB_GLOB_VARS << ")" << endl;

            m->addValue("IL12", 10.0);
            m->addValue("antiIL4", 10000.0);

            switch(IdExp){
            case DOSE0:  { m->setValue(m->getExternalName(parameter), 0.0); break;}
            case DOSE0p01:{m->setValue(m->getExternalName(parameter), 0.01); break;}
            case DOSE0p1:{ m->setValue(m->getExternalName(parameter), 0.1); break;}
            case DOSE0p8:{ m->setValue(m->getExternalName(parameter), 0.8); break;}
            case DOSE1:  { m->setValue(m->getExternalName(parameter), 1.0); break;}
            case DOSE5:  { m->setValue(m->getExternalName(parameter), 5.0); break;}
            case DOSE10: { m->setValue(m->getExternalName(parameter), 10.0); break;}
            case DOSE20: { m->setValue(m->getExternalName(parameter), 20.0); break;}
            case DOSE40: { m->setValue(m->getExternalName(parameter), 40.0); break;}
            //case DOSE60: { m->setValue(m->getExternalName(parameter), 60.0); break;}
            case DOSE80: { m->setValue(m->getExternalName(parameter), 80.0); break;}
            case DOSE100:{ m->setValue(m->getExternalName(parameter), 100.0); break;}
            }
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};


struct expDosesTH2 : public Experiment {
    int parameter;  // this time the parameter is the (global !!) index of the input variable.
    expDosesTH2(Model* _m, int _parameter) : Experiment(_m, NB_DOSES), parameter(_parameter) {
        stringstream res; res << "Th2 + doses of " << m->getExternalName(parameter) << " from the beginning"; Identification = res.str();
        names_exp[DOSE0] =          string("TH2-DOSE0");
        names_exp[DOSE0p01] =       string("TH2-DOSE0p01");
        names_exp[DOSE0p1] =        string("TH2-DOSE0p1");
        names_exp[DOSE0p8] =        string("TH1-DOSE0p8");
        names_exp[DOSE1] =          string("TH2-DOSE1");
        names_exp[DOSE5] =          string("TH2-DOSE5");
        names_exp[DOSE10] =         string("TH2-DOSE10");
        names_exp[DOSE20] =         string("TH2-DOSE20");
        names_exp[DOSE40] =         string("TH2-DOSE40");
        //names_exp[DOSE60] =         string("TH2-DOSE60");
        names_exp[DOSE80] =         string("TH2-DOSE80");
        names_exp[DOSE100] =        string("TH2-DOSE100");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            if(parameter < 0) cerr << "ERR: expDoses, parameter should be the index of the global variable whose dose should be fixed. Out of bounds (nb of global variables :" << N::NB_GLOB_VARS << ")" << endl;

            m->addValue("IL4", 14.6);
            m->addValue("antiIFNg", 10000.0);

            switch(IdExp){
            case DOSE0:  { m->setValue(m->getExternalName(parameter), 0.0); break;}
            case DOSE0p01:{m->setValue(m->getExternalName(parameter), 0.01); break;}
            case DOSE0p1:{ m->setValue(m->getExternalName(parameter), 0.1); break;}
            case DOSE0p8:{ m->setValue(m->getExternalName(parameter), 0.8); break;}
            case DOSE1:  { m->setValue(m->getExternalName(parameter), 1.0); break;}
            case DOSE5:  { m->setValue(m->getExternalName(parameter), 5.0); break;}
            case DOSE10: { m->setValue(m->getExternalName(parameter), 10.0); break;}
            case DOSE20: { m->setValue(m->getExternalName(parameter), 20.0); break;}
            case DOSE40: { m->setValue(m->getExternalName(parameter), 40.0); break;}
            //case DOSE60: { m->setValue(m->getExternalName(parameter), 60.0); break;}
            case DOSE80: { m->setValue(m->getExternalName(parameter), 80.0); break;}
            case DOSE100:{ m->setValue(m->getExternalName(parameter), 100.0); break;}
            }
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};


struct expDosesTreg : public Experiment {
    int parameter;  // this time the parameter is the (global !!) index of the input variable.
    expDosesTreg(Model* _m, int _parameter) : Experiment(_m, NB_DOSES), parameter(_parameter) {
        stringstream res; res << "iTreg + doses of " << m->getExternalName(parameter) << " from the beginning"; Identification = res.str();
        names_exp[DOSE0] =          string("TREG-DOSE0");
        names_exp[DOSE0p01] =       string("TREG-DOSE0p01");
        names_exp[DOSE0p1] =        string("TREG-DOSE0p1");
        names_exp[DOSE0p8] =        string("TH1-DOSE0p8");
        names_exp[DOSE1] =          string("TREG-DOSE1");
        names_exp[DOSE5] =          string("TREG-DOSE5");
        names_exp[DOSE10] =         string("TREG-DOSE10");
        names_exp[DOSE20] =         string("TREG-DOSE20");
        names_exp[DOSE40] =         string("TREG-DOSE40");
        //names_exp[DOSE60] =         string("TREG-DOSE60");
        names_exp[DOSE80] =         string("TREG-DOSE80");
        names_exp[DOSE100] =        string("TREG-DOSE100");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            if(parameter < 0) cerr << "ERR: expDoses, parameter should be the index of the global variable whose dose should be fixed. Out of bounds (nb of global variables :" << N::NB_GLOB_VARS << ")" << endl;

            m->addValue("TGFB", 0.5);
            m->addValue("IL2", 19.05);

            switch(IdExp){
            case DOSE0:  { m->setValue(m->getExternalName(parameter), 0.0); break;}
            case DOSE0p01:{m->setValue(m->getExternalName(parameter), 0.01); break;}
            case DOSE0p1:{ m->setValue(m->getExternalName(parameter), 0.1); break;}
            case DOSE0p8:{ m->setValue(m->getExternalName(parameter), 0.8); break;}
            case DOSE1:  { m->setValue(m->getExternalName(parameter), 1.0); break;}
            case DOSE5:  { m->setValue(m->getExternalName(parameter), 5.0); break;}
            case DOSE10: { m->setValue(m->getExternalName(parameter), 10.0); break;}
            case DOSE20: { m->setValue(m->getExternalName(parameter), 20.0); break;}
            case DOSE40: { m->setValue(m->getExternalName(parameter), 40.0); break;}
            //case DOSE60: { m->setValue(m->getExternalName(parameter), 60.0); break;}
            case DOSE80: { m->setValue(m->getExternalName(parameter), 80.0); break;}
            case DOSE100:{ m->setValue(m->getExternalName(parameter), 100.0); break;}
            }

            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};


struct expDosesTH17 : public Experiment {
    int parameter;  // this time the parameter is the (global !!) index of the input variable.
    expDosesTH17(Model* _m, int _parameter) : Experiment(_m, NB_DOSES), parameter(_parameter) {
        stringstream res; res << "Th17 + doses of " << m->getExternalName(parameter) << " from the beginning"; Identification = res.str();
        names_exp[DOSE0] =          string("TH17-DOSE0");
        names_exp[DOSE0p01] =       string("TH17-DOSE0p01");
        names_exp[DOSE0p1] =        string("TH17-DOSE0p1");
        names_exp[DOSE0p8] =        string("TH1-DOSE0p8");
        names_exp[DOSE1] =          string("TH17-DOSE1");
        names_exp[DOSE5] =          string("TH17-DOSE5");
        names_exp[DOSE10] =         string("TH17-DOSE10");
        names_exp[DOSE20] =         string("TH17-DOSE20");
        names_exp[DOSE40] =         string("TH17-DOSE40");
        //names_exp[DOSE60] =         string("TH17-DOSE60");
        names_exp[DOSE80] =         string("TH17-DOSE80");
        names_exp[DOSE100] =        string("TH17-DOSE100");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            if(parameter < 0) cerr << "ERR: expDoses, parameter should be the index of the global variable whose dose should be fixed. Out of bounds (nb of global variables :" << N::NB_GLOB_VARS << ")" << endl;

            m->addValue("IL6", 30.0);
            m->addValue("TGFB", 0.2);
            m->addValue("antiIL2", 5000.0);
            m->addValue("antiIFNg", 10000.0);

            switch(IdExp){
            case DOSE0:  { m->setValue(m->getExternalName(parameter), 0.0); break;}
            case DOSE0p01:{m->setValue(m->getExternalName(parameter), 0.01); break;}
            case DOSE0p1:{ m->setValue(m->getExternalName(parameter), 0.1); break;}
            case DOSE0p8:{ m->setValue(m->getExternalName(parameter), 0.8); break;}
            case DOSE1:  { m->setValue(m->getExternalName(parameter), 1.0); break;}
            case DOSE5:  { m->setValue(m->getExternalName(parameter), 5.0); break;}
            case DOSE10: { m->setValue(m->getExternalName(parameter), 10.0); break;}
            case DOSE20: { m->setValue(m->getExternalName(parameter), 20.0); break;}
            case DOSE40: { m->setValue(m->getExternalName(parameter), 40.0); break;}
            //case DOSE60: { m->setValue(m->getExternalName(parameter), 60.0); break;}
            case DOSE80: { m->setValue(m->getExternalName(parameter), 80.0); break;}
            case DOSE100:{ m->setValue(m->getExternalName(parameter), 100.0); break;}
            }
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};


struct expDosesTH0 : public Experiment {
    int parameter;  // this time the parameter is the (global !!) index of the input variable.
    expDosesTH0(Model* _m, int _parameter) : Experiment(_m, NB_DOSES), parameter(_parameter) {
        stringstream res; res << "Th0 + doses of " << m->getExternalName(parameter) << " from the beginning"; Identification = res.str();
        names_exp[DOSE0] =          string("TH0-DOSE0");
        names_exp[DOSE0p01] =       string("TH0-DOSE0p01");
        names_exp[DOSE0p1] =        string("TH0-DOSE0p1");
        names_exp[DOSE0p8] =        string("TH1-DOSE0p8");
        names_exp[DOSE1] =          string("TH0-DOSE1");
        names_exp[DOSE5] =          string("TH0-DOSE5");
        names_exp[DOSE10] =         string("TH0-DOSE10");
        names_exp[DOSE20] =         string("TH0-DOSE20");
        names_exp[DOSE40] =         string("TH0-DOSE40");
        //names_exp[DOSE60] =         string("TH0-DOSE60");
        names_exp[DOSE80] =         string("TH0-DOSE80");
        names_exp[DOSE100] =        string("TH0-DOSE100");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            if(parameter < 0) cerr << "ERR: expDoses, parameter should be the index of the global variable whose dose should be fixed. Out of bounds (nb of global variables :" << N::NB_GLOB_VARS << ")" << endl;

            switch(IdExp){
            case DOSE0:  { m->setValue(m->getExternalName(parameter), 0.0); break;}
            case DOSE0p01:{m->setValue(m->getExternalName(parameter), 0.01); break;}
            case DOSE0p1:{ m->setValue(m->getExternalName(parameter), 0.1); break;}
            case DOSE0p8:{ m->setValue(m->getExternalName(parameter), 0.8); break;}
            case DOSE1:  { m->setValue(m->getExternalName(parameter), 1.0); break;}
            case DOSE5:  { m->setValue(m->getExternalName(parameter), 5.0); break;}
            case DOSE10: { m->setValue(m->getExternalName(parameter), 10.0); break;}
            case DOSE20: { m->setValue(m->getExternalName(parameter), 20.0); break;}
            case DOSE40: { m->setValue(m->getExternalName(parameter), 40.0); break;}
            //case DOSE60: { m->setValue(m->getExternalName(parameter), 60.0); break;}
            case DOSE80: { m->setValue(m->getExternalName(parameter), 80.0); break;}
            case DOSE100:{ m->setValue(m->getExternalName(parameter), 100.0); break;}
            }
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};







enum DENSITIES {DENS10k, DENS40k, DENS100k, DENS200k, DENS400k, DENS700k, DENS1000k, DENS1800k, NB_DENS};

struct expDensityTh1 : public Experiment {
    expDensityTh1(Model* _m) : Experiment(_m, NB_DENS) {
        Identification = string("Th1, titration of cell density");
        names_exp[DENS10k] =        string("TH1-DENS10k");
        names_exp[DENS40k] =        string("TH1-DENS40k");
        names_exp[DENS100k] =       string("TH1-DENS100k");
        names_exp[DENS200k] =       string("TH1-DENS200k");
        names_exp[DENS400k] =       string("TH1-DENS400k");
        names_exp[DENS700k] =       string("TH1-DENS700k");
        names_exp[DENS1000k] =      string("TH1-DENS1000k");
        names_exp[DENS1800k] =      string("TH1-DENS1800k");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);

            double densityNbCells = 0;
            switch(IdExp){
            case DENS10k:  {densityNbCells = 10.0; break;}
            case DENS40k:  {densityNbCells = 40.0; break;}
            case DENS100k: {densityNbCells = 100.0; break;}
            case DENS200k: {densityNbCells = 200.0; break;}
            case DENS400k: {densityNbCells = 400.0; break;}
            case DENS700k: {densityNbCells = 700.0; break;}
            case DENS1000k:{densityNbCells = 1000.0; break;}
            case DENS1800k:{densityNbCells = 1800.0; break;}
            }
            m->action(string("correctProductionByFactor"), densityNbCells / 700.0);

            m->addValue("IL12", 10.0);
            m->addValue("antiIL4", 10000.0);

            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};



struct expDensityTh2 : public Experiment {
    expDensityTh2(Model* _m) : Experiment(_m, NB_DENS){
        Identification = string("Th2, titration of cell density");
        names_exp[DENS10k] =        string("TH2-DENS10k");
        names_exp[DENS40k] =        string("TH2-DENS40k");
        names_exp[DENS100k] =       string("TH2-DENS100k");
        names_exp[DENS200k] =       string("TH2-DENS200k");
        names_exp[DENS400k] =       string("TH2-DENS400k");
        names_exp[DENS700k] =       string("TH2-DENS700k");
        names_exp[DENS1000k] =      string("TH2-DENS1000k");
        names_exp[DENS1800k] =      string("TH2-DENS1800k");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);

            double densityNbCells = 0;
            switch(IdExp){
            case DENS10k:  {densityNbCells = 10.0; break;}
            case DENS40k:  {densityNbCells = 40.0; break;}
            case DENS100k: {densityNbCells = 100.0; break;}
            case DENS200k: {densityNbCells = 200.0; break;}
            case DENS400k: {densityNbCells = 400.0; break;}
            case DENS700k: {densityNbCells = 700.0; break;}
            case DENS1000k:{densityNbCells = 1000.0; break;}
            case DENS1800k:{densityNbCells = 1800.0; break;}
            }
            m->action(string("correctProductionByFactor"), densityNbCells / 700.0);

            m->addValue("IL4", 14.6);
            m->addValue("antiIFNg", 10000.0);

            m->simulate(LdiffPert *24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};




struct expDensityTreg : public Experiment {
    expDensityTreg(Model* _m) : Experiment(_m, NB_DENS)  {
        Identification = string("iTreg, titration of cell density");
        names_exp[DENS10k] =        string("TREG-DENS10k");
        names_exp[DENS40k] =        string("TREG-DENS40k");
        names_exp[DENS100k] =       string("TREG-DENS100k");
        names_exp[DENS200k] =       string("TREG-DENS200k");
        names_exp[DENS400k] =       string("TREG-DENS400k");
        names_exp[DENS700k] =       string("TREG-DENS700k");
        names_exp[DENS1000k] =      string("TREG-DENS1000k");
        names_exp[DENS1800k] =      string("TREG-DENS1800k");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);

            double densityNbCells = 0;
            switch(IdExp){
            case DENS10k:  {densityNbCells = 10.0; break;}
            case DENS40k:  {densityNbCells = 40.0; break;}
            case DENS100k: {densityNbCells = 100.0; break;}
            case DENS200k: {densityNbCells = 200.0; break;}
            case DENS400k: {densityNbCells = 400.0; break;}
            case DENS700k: {densityNbCells = 700.0; break;}
            case DENS1000k:{densityNbCells = 1000.0; break;}
            case DENS1800k:{densityNbCells = 1800.0; break;}
            }
            m->action(string("correctProductionByFactor"), densityNbCells / 700.0);

            m->addValue("TGFB", 0.5);
            m->addValue("IL2", 19.05);

            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};



struct expDensityTh17 : public Experiment {
    expDensityTh17(Model* _m) : Experiment(_m, NB_DENS)  {
        Identification = string("Th17, titration of cell density");
        names_exp[DENS10k] =        string("TH17-DENS10k");
        names_exp[DENS40k] =        string("TH17-DENS40k");
        names_exp[DENS100k] =       string("TH17-DENS100k");
        names_exp[DENS200k] =       string("TH17-DENS200k");
        names_exp[DENS400k] =       string("TH17-DENS400k");
        names_exp[DENS700k] =       string("TH17-DENS700k");
        names_exp[DENS1000k] =      string("TH17-DENS1000k");
        names_exp[DENS1800k] =      string("TH17-DENS1800k");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);

            double densityNbCells = 0;
            switch(IdExp){
            case DENS10k:  {densityNbCells = 10.0; break;}
            case DENS40k:  {densityNbCells = 40.0; break;}
            case DENS100k: {densityNbCells = 100.0; break;}
            case DENS200k: {densityNbCells = 200.0; break;}
            case DENS400k: {densityNbCells = 400.0; break;}
            case DENS700k: {densityNbCells = 700.0; break;}
            case DENS1000k:{densityNbCells = 1000.0; break;}
            case DENS1800k:{densityNbCells = 1800.0; break;}
            }
            m->action(string("correctProductionByFactor"), densityNbCells / 700.0);

            m->addValue("IL6", 30.0);
            m->addValue("TGFB", 0.2);
            m->addValue("antiIL2", 5000.0);
            m->addValue("antiIFNg", 10000.0);

            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};



struct expDensityTh0 : public Experiment {
    expDensityTh0(Model* _m) : Experiment(_m, NB_DENS)  {
        Identification = string("Th0, titration of cell density");
        names_exp[DENS10k] =        string("TH0-DENS10k");
        names_exp[DENS40k] =        string("TH0-DENS40k");
        names_exp[DENS100k] =       string("TH0-DENS100k");
        names_exp[DENS200k] =       string("TH0-DENS200k");
        names_exp[DENS400k] =       string("TH0-DENS400k");
        names_exp[DENS700k] =       string("TH0-DENS700k");
        names_exp[DENS1000k] =      string("TH0-DENS1000k");
        names_exp[DENS1800k] =      string("TH0-DENS1800k");


    }
    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);

            double densityNbCells = 0;
            switch(IdExp){
            case DENS10k:  {densityNbCells = 10.0; break;}
            case DENS40k:  {densityNbCells = 40.0; break;}
            case DENS100k: {densityNbCells = 100.0; break;}
            case DENS200k: {densityNbCells = 200.0; break;}
            case DENS400k: {densityNbCells = 400.0; break;}
            case DENS700k: {densityNbCells = 700.0; break;}
            case DENS1000k:{densityNbCells = 1000.0; break;}
            case DENS1800k:{densityNbCells = 1800.0; break;}
            }
            m->action(string("correctProductionByFactor"), densityNbCells / 700.0);

            m->simulate(LdiffPert *24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};



struct expParametersAllCond : public Experiment {
    int parameterID;  // this time the parameter is the index of the input variable.
    double value;
    expParametersAllCond(Model* _m, int _parameterID, double _value) : Experiment(_m, NB_EXP), parameterID(_parameterID), value(_value) {
        stringstream res; res << "All subsets, with " << m->getParamName(parameterID) << " = " << value; Identification = res.str();
        if((parameterID < 0) || (parameterID >= m->getNbParams())){cerr << "ERR: expParameters, parameter ID(" << parameterID << ") incorrect, only " << m->getNbParams() << " parameters in the model" << endl;}
        if(value < 0) cerr << "ERR: expParameters, values are not supposed to be negative ..." << endl;
        names_exp[TH0] = string("Th0"); // activation (TCR Only)");
        names_exp[TH1] = string("Th1"); // activation (TCR + IL12 + anti-IL4)");
        names_exp[TH2] = string("Th2"); // differentiation (TCR + IL4 + anti-IFNg");
        names_exp[TREG] = string("Treg"); // differentiation (TCR + TGFb + IL2)");
        names_exp[TH17] = string("Th17"); // differentiation (TCR + TGFb + IL6 + IL1b + anti-IL2 + anti-IFNg)");


    }
    void simulate(int IdExp, Evaluator* E, bool force){
        m->setParam(parameterID, value);
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                default: {m->initialise(Back::WT); break;}
            }
            switch(IdExp){
            case TH0:{
                     break;}
            case TH1: {
                    m->setValue("IL12", 10.0);
                    m->setValue("antiIL4", 10000.0); break;}
            case TH2: {
                    m->setValue("IL4", 14.6);
                    m->setValue("antiIFNg", 10000.0); break;}
            case TREG: {
                    m->setValue("TGFB", 0.5);
                    m->setValue("IL2", 19.05); break;}
            case TH17: {
                    m->setValue("IL6", 30.0);
                    m->setValue("TGFB", 0.2);
                    m->setValue("antiIL2", 5000.0);
                    m->setValue("antiIFNg", 10000.0); break;}
            }
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};




enum AROUNDPARAMETERS {PARAM0, PARAM0P1, PARAM1, PARAM5, PARAM20, PARAM50, PARAM80, PARAM100, PARAM120, PARAM150, PARAM200, PARAM500, PARAM1000, NB_AROUNDPARAMETERS};
struct expParametersTh1 : public Experiment {
    int parameterID;  // this time the parameter is the index of the input variable.
    bool aroundInitialValue; // if yes, samples % variations around the initial value. If not, then samples the whole range of the configuration.
    vector<double> listPoints;
    bool initialized;

    expParametersTh1(Model* _m, int _parameterID, bool _aroundInitialValue) : Experiment(_m, NB_AROUNDPARAMETERS), parameterID(_parameterID), aroundInitialValue(_aroundInitialValue), listPoints(NB_AROUNDPARAMETERS, 0.0), initialized(false) {
        stringstream res; res << "Th1, change parameter " << m->getParamName(parameterID) << " around " << aroundInitialValue; Identification = res.str();
        if((parameterID < 0) || (parameterID >= m->getNbParams())){cerr << "ERR: expParameters, parameter ID(" << parameterID << ") incorrect, only " << m->getNbParams() << " parameters in the model" << endl;}
        names_exp.clear();
        names_exp.resize(NB_AROUNDPARAMETERS, string(""));

    }
    void simulate(int IdExp, Evaluator* E, bool force){
        // Initializing the names of experiments and parameter values to check - everything that depends on parameter values should be done inside simulate and not in the constructor.
        if(!initialized){
            listPoints = cutSpace(13, true, 2, m->getLowerBound(parameterID), m->getUpperBound(parameterID));
            if(aroundInitialValue){
                double initVal = m->getParam(parameterID);
                listPoints[PARAM0] = 0.0;
                listPoints[PARAM0P1] = 0.001*initVal;
                listPoints[PARAM1] = 0.01*initVal;
                listPoints[PARAM5] = 0.05*initVal;
                listPoints[PARAM20] = 0.2*initVal;
                listPoints[PARAM50] = 0.5*initVal;
                listPoints[PARAM80] = 0.8*initVal;
                listPoints[PARAM100] = 1.0*initVal;
                listPoints[PARAM120] = 1.2*initVal;
                listPoints[PARAM150] = 1.5*initVal;
                listPoints[PARAM200] = 2.0*initVal;
                listPoints[PARAM500] = 5.0*initVal;
                listPoints[PARAM1000] = 10.0*initVal;
                names_exp[PARAM0]   = string("(0%)  ");
                names_exp[PARAM0P1] = string("(0.1%)");
                names_exp[PARAM1]   = string("(1%)  ");
                names_exp[PARAM5]   = string("(5%)  ");
                names_exp[PARAM20]  = string("(20%) ");
                names_exp[PARAM50]  = string("(50%) ");
                names_exp[PARAM80]  = string("(80%) ");
                names_exp[PARAM100] = string("(100%)");
                names_exp[PARAM120] = string("(120%)");
                names_exp[PARAM150] = string("(150%)");
                names_exp[PARAM200] = string("(2X)  ");
                names_exp[PARAM500] = string("(5X)  ");
                names_exp[PARAM1000] = string("(10X) ");
            }
            for(int i = 0; i < getNbCond(); ++i){
                #ifndef WITHOUT_QT
                names_exp[i] += string("") + QString::number(listPoints[i]).toStdString();
                cerr << "WRN : expParametersTh1, can not label the names of Experiment in the non-graphical mode (WITHOUT_QT)" << endl;
                #endif
            }
            initialized = true;
        }



        switch(IdExp){
            case PARAM0:   {m->setParam(parameterID, listPoints[0]); break;}
            case PARAM0P1: {m->setParam(parameterID, listPoints[1]); break;}
            case PARAM1:   {m->setParam(parameterID, listPoints[2]); break;}
            case PARAM5:   {m->setParam(parameterID, listPoints[3]); break;}
            case PARAM20:  {m->setParam(parameterID, listPoints[4]); break;}
            case PARAM50:  {m->setParam(parameterID, listPoints[5]); break;}
            case PARAM80:  {m->setParam(parameterID, listPoints[6]); break;}
            case PARAM100: {m->setParam(parameterID, listPoints[7]); break;}
            case PARAM120: {m->setParam(parameterID, listPoints[8]); break;}
            case PARAM150: {m->setParam(parameterID, listPoints[9]); break;}
            case PARAM200: {m->setParam(parameterID, listPoints[10]); break;}
            case PARAM500: {m->setParam(parameterID, listPoints[11]); break;}
            case PARAM1000:{m->setParam(parameterID, listPoints[12]); break;}
        }
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                default: {m->initialise(Back::WT); break;}
            }
            m->setValue("IL12", 10.0);
            m->setValue("antiIL4", 10000.0);
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};


struct expParametersTh2 : public Experiment {
    int parameterID;  // this time the parameter is the index of the input variable.
    bool aroundInitialValue; // if yes, samples % variations around the initial value. If not, then samples the whole range of the configuration.
    vector<double> listPoints;
    bool initialized;

    expParametersTh2(Model* _m, int _parameterID, bool _aroundInitialValue) : Experiment(_m, NB_AROUNDPARAMETERS), parameterID(_parameterID), aroundInitialValue(_aroundInitialValue), listPoints(NB_AROUNDPARAMETERS, 0.0), initialized(false)  {
        stringstream res; res << "Th2, change parameter " << m->getParamName(parameterID) << " around " << aroundInitialValue; Identification = res.str();
        if((parameterID < 0) || (parameterID >= m->getNbParams())){cerr << "ERR: expParameters, parameter ID(" << parameterID << ") incorrect, only " << m->getNbParams() << " parameters in the model" << endl;}
        names_exp.clear();
        names_exp.resize(NB_AROUNDPARAMETERS, string(""));
    }
    void simulate(int IdExp, Evaluator* E, bool force){
        // Initializing the names of experiments and parameter values to check - everything that depends on parameter values should be done inside simulate and not in the constructor.
        if(!initialized){
            listPoints = cutSpace(13, true, 2, m->getLowerBound(parameterID), m->getUpperBound(parameterID));
            if(aroundInitialValue){
                double initVal = m->getParam(parameterID);
                listPoints[PARAM0] = 0.0;
                listPoints[PARAM0P1] = 0.001*initVal;
                listPoints[PARAM1] = 0.01*initVal;
                listPoints[PARAM5] = 0.05*initVal;
                listPoints[PARAM20] = 0.2*initVal;
                listPoints[PARAM50] = 0.5*initVal;
                listPoints[PARAM80] = 0.8*initVal;
                listPoints[PARAM100] = 1.0*initVal;
                listPoints[PARAM120] = 1.2*initVal;
                listPoints[PARAM150] = 1.5*initVal;
                listPoints[PARAM200] = 2.0*initVal;
                listPoints[PARAM500] = 5.0*initVal;
                listPoints[PARAM1000] = 10.0*initVal;
                names_exp[PARAM0]   = string("(0%)  ");
                names_exp[PARAM0P1] = string("(0.1%)");
                names_exp[PARAM1]   = string("(1%)  ");
                names_exp[PARAM5]   = string("(5%)  ");
                names_exp[PARAM20]  = string("(20%) ");
                names_exp[PARAM50]  = string("(50%) ");
                names_exp[PARAM80]  = string("(80%) ");
                names_exp[PARAM100] = string("(100%)");
                names_exp[PARAM120] = string("(120%)");
                names_exp[PARAM150] = string("(150%)");
                names_exp[PARAM200] = string("(2X)  ");
                names_exp[PARAM500] = string("(5X)  ");
                names_exp[PARAM1000] = string("(10X) ");
            }
            for(int i = 0; i < getNbCond(); ++i){
                #ifndef WITHOUT_QT
                names_exp[i] += string("") + QString::number(listPoints[i]).toStdString();
                cerr << "WRN : expParametersTh1, can not label the names of Experiment in the non-graphical mode (WITHOUT_QT)" << endl;
                #endif
            }
            initialized = true;
        }

        switch(IdExp){
            case PARAM0:   {m->setParam(parameterID, listPoints[0]); break;}
            case PARAM0P1: {m->setParam(parameterID, listPoints[1]); break;}
            case PARAM1:   {m->setParam(parameterID, listPoints[2]); break;}
            case PARAM5:   {m->setParam(parameterID, listPoints[3]); break;}
            case PARAM20:  {m->setParam(parameterID, listPoints[4]); break;}
            case PARAM50:  {m->setParam(parameterID, listPoints[5]); break;}
            case PARAM80:  {m->setParam(parameterID, listPoints[6]); break;}
            case PARAM100: {m->setParam(parameterID, listPoints[7]); break;}
            case PARAM120: {m->setParam(parameterID, listPoints[8]); break;}
            case PARAM150: {m->setParam(parameterID, listPoints[9]); break;}
            case PARAM200: {m->setParam(parameterID, listPoints[10]); break;}
            case PARAM500: {m->setParam(parameterID, listPoints[11]); break;}
            case PARAM1000:{m->setParam(parameterID, listPoints[12]); break;}
        }
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                default: {m->initialise(Back::WT); break;}
            }
            m->setValue("IL4", 14.6);
            m->setValue("antiIFNg", 10000.0);
            //cout << "IL4 = " << m->getParam(149) << endl << endl;
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};

struct expParametersiTreg : public Experiment {
    int parameterID;  // this time the parameter is the index of the input variable.
    bool aroundInitialValue; // if yes, samples % variations around the initial value. If not, then samples the whole range of the configuration.
    vector<double> listPoints;
    bool initialized;

    expParametersiTreg(Model* _m, int _parameterID, bool _aroundInitialValue) : Experiment(_m, NB_AROUNDPARAMETERS), parameterID(_parameterID), aroundInitialValue(_aroundInitialValue), listPoints(NB_AROUNDPARAMETERS, 0.0), initialized(false)  {
        stringstream res; res << "iTreg, change parameter " << m->getParamName(parameterID) << " around " << aroundInitialValue; Identification = res.str();
        if((parameterID < 0) || (parameterID >= m->getNbParams())){cerr << "ERR: expParameters, parameter ID(" << parameterID << ") incorrect, only " << m->getNbParams() << " parameters in the model" << endl;}
        names_exp.clear();
        names_exp.resize(NB_AROUNDPARAMETERS, string(""));
    }
    void simulate(int IdExp, Evaluator* E, bool force){
        // Initializing the names of experiments and parameter values to check - everything that depends on parameter values should be done inside simulate and not in the constructor.
        if(!initialized){
            listPoints = cutSpace(13, true, 2, m->getLowerBound(parameterID), m->getUpperBound(parameterID));
            if(aroundInitialValue){
                double initVal = m->getParam(parameterID);
                listPoints[PARAM0] = 0.0;
                listPoints[PARAM0P1] = 0.001*initVal;
                listPoints[PARAM1] = 0.01*initVal;
                listPoints[PARAM5] = 0.05*initVal;
                listPoints[PARAM20] = 0.2*initVal;
                listPoints[PARAM50] = 0.5*initVal;
                listPoints[PARAM80] = 0.8*initVal;
                listPoints[PARAM100] = 1.0*initVal;
                listPoints[PARAM120] = 1.2*initVal;
                listPoints[PARAM150] = 1.5*initVal;
                listPoints[PARAM200] = 2.0*initVal;
                listPoints[PARAM500] = 5.0*initVal;
                listPoints[PARAM1000] = 10.0*initVal;
                names_exp[PARAM0]   = string("(0%)  ");
                names_exp[PARAM0P1] = string("(0.1%)");
                names_exp[PARAM1]   = string("(1%)  ");
                names_exp[PARAM5]   = string("(5%)  ");
                names_exp[PARAM20]  = string("(20%) ");
                names_exp[PARAM50]  = string("(50%) ");
                names_exp[PARAM80]  = string("(80%) ");
                names_exp[PARAM100] = string("(100%)");
                names_exp[PARAM120] = string("(120%)");
                names_exp[PARAM150] = string("(150%)");
                names_exp[PARAM200] = string("(2X)  ");
                names_exp[PARAM500] = string("(5X)  ");
                names_exp[PARAM1000] = string("(10X) ");
            }
            for(int i = 0; i < getNbCond(); ++i){
                #ifndef WITHOUT_QT
                names_exp[i] += string("") + QString::number(listPoints[i]).toStdString();
                cerr << "WRN : expParametersTh1, can not label the names of Experiment in the non-graphical mode (WITHOUT_QT)" << endl;
                #endif
            }
            initialized = true;
        }

        switch(IdExp){
            case PARAM0:   {m->setParam(parameterID, listPoints[0]); break;}
            case PARAM0P1: {m->setParam(parameterID, listPoints[1]); break;}
            case PARAM1:   {m->setParam(parameterID, listPoints[2]); break;}
            case PARAM5:   {m->setParam(parameterID, listPoints[3]); break;}
            case PARAM20:  {m->setParam(parameterID, listPoints[4]); break;}
            case PARAM50:  {m->setParam(parameterID, listPoints[5]); break;}
            case PARAM80:  {m->setParam(parameterID, listPoints[6]); break;}
            case PARAM100: {m->setParam(parameterID, listPoints[7]); break;}
            case PARAM120: {m->setParam(parameterID, listPoints[8]); break;}
            case PARAM150: {m->setParam(parameterID, listPoints[9]); break;}
            case PARAM200: {m->setParam(parameterID, listPoints[10]); break;}
            case PARAM500: {m->setParam(parameterID, listPoints[11]); break;}
            case PARAM1000:{m->setParam(parameterID, listPoints[12]); break;}
        }
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                default: {m->initialise(Back::WT); break;}
            }
            m->setValue("TGFB", 0.5);
            m->setValue("IL2", 19.05);
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};



struct expParametersTh17 : public Experiment {
    int parameterID;  // this time the parameter is the index of the input variable.
    bool aroundInitialValue; // if yes, samples % variations around the initial value. If not, then samples the whole range of the configuration.
    vector<double> listPoints;
    bool initialized;

    expParametersTh17(Model* _m, int _parameterID, bool _aroundInitialValue) : Experiment(_m, NB_AROUNDPARAMETERS), parameterID(_parameterID), aroundInitialValue(_aroundInitialValue), listPoints(NB_AROUNDPARAMETERS, 0.0), initialized(false)  {
        stringstream res; res << "Th17, change parameter " << m->getParamName(parameterID) << " around " << aroundInitialValue; Identification = res.str();
        if((parameterID < 0) || (parameterID >= m->getNbParams())){cerr << "ERR: expParameters, parameter ID(" << parameterID << ") incorrect, only " << m->getNbParams() << " parameters in the model" << endl;}
        names_exp.clear();
        names_exp.resize(NB_AROUNDPARAMETERS, string(""));
    }
    void simulate(int IdExp, Evaluator* E, bool force){
        // Initializing the names of experiments and parameter values to check - everything that depends on parameter values should be done inside simulate and not in the constructor.
        if(!initialized){
            listPoints = cutSpace(13, true, 2, m->getLowerBound(parameterID), m->getUpperBound(parameterID));
            if(aroundInitialValue){
                double initVal = m->getParam(parameterID);
                listPoints[PARAM0] = 0.0;
                listPoints[PARAM0P1] = 0.001*initVal;
                listPoints[PARAM1] = 0.01*initVal;
                listPoints[PARAM5] = 0.05*initVal;
                listPoints[PARAM20] = 0.2*initVal;
                listPoints[PARAM50] = 0.5*initVal;
                listPoints[PARAM80] = 0.8*initVal;
                listPoints[PARAM100] = 1.0*initVal;
                listPoints[PARAM120] = 1.2*initVal;
                listPoints[PARAM150] = 1.5*initVal;
                listPoints[PARAM200] = 2.0*initVal;
                listPoints[PARAM500] = 5.0*initVal;
                listPoints[PARAM1000] = 10.0*initVal;
                names_exp[PARAM0]   = string("(0%)  ");
                names_exp[PARAM0P1] = string("(0.1%)");
                names_exp[PARAM1]   = string("(1%)  ");
                names_exp[PARAM5]   = string("(5%)  ");
                names_exp[PARAM20]  = string("(20%) ");
                names_exp[PARAM50]  = string("(50%) ");
                names_exp[PARAM80]  = string("(80%) ");
                names_exp[PARAM100] = string("(100%)");
                names_exp[PARAM120] = string("(120%)");
                names_exp[PARAM150] = string("(150%)");
                names_exp[PARAM200] = string("(2X)  ");
                names_exp[PARAM500] = string("(5X)  ");
                names_exp[PARAM1000] = string("(10X) ");
            }
            for(int i = 0; i < getNbCond(); ++i){
                #ifndef WITHOUT_QT
                names_exp[i] += string("") + QString::number(listPoints[i]).toStdString();
                cerr << "WRN : expParametersTh1, can not label the names of Experiment in the non-graphical mode (WITHOUT_QT)" << endl;
                #endif
            }
            initialized = true;
        }

        switch(IdExp){
            case PARAM0:   {m->setParam(parameterID, listPoints[0]); break;}
            case PARAM0P1: {m->setParam(parameterID, listPoints[1]); break;}
            case PARAM1:   {m->setParam(parameterID, listPoints[2]); break;}
            case PARAM5:   {m->setParam(parameterID, listPoints[3]); break;}
            case PARAM20:  {m->setParam(parameterID, listPoints[4]); break;}
            case PARAM50:  {m->setParam(parameterID, listPoints[5]); break;}
            case PARAM80:  {m->setParam(parameterID, listPoints[6]); break;}
            case PARAM100: {m->setParam(parameterID, listPoints[7]); break;}
            case PARAM120: {m->setParam(parameterID, listPoints[8]); break;}
            case PARAM150: {m->setParam(parameterID, listPoints[9]); break;}
            case PARAM200: {m->setParam(parameterID, listPoints[10]); break;}
            case PARAM500: {m->setParam(parameterID, listPoints[11]); break;}
            case PARAM1000:{m->setParam(parameterID, listPoints[12]); break;}
        }
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                default: {m->initialise(Back::WT); break;}
            }
            m->setValue("IL6", 30.0);
            m->setValue("TGFB", 0.2);
            m->setValue("antiIL2", 5000.0);
            m->setValue("antiIFNg", 10000.0);
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};



struct expParametersTh0 : public Experiment {
    int parameterID;  // this time the parameter is the index of the input variable.
    bool aroundInitialValue; // if yes, samples % variations around the initial value. If not, then samples the whole range of the configuration.
    vector<double> listPoints;
    bool initialized;

    expParametersTh0(Model* _m, int _parameterID, bool _aroundInitialValue) : Experiment(_m, NB_AROUNDPARAMETERS), parameterID(_parameterID), aroundInitialValue(_aroundInitialValue), listPoints(NB_AROUNDPARAMETERS, 0.0), initialized(false)  {
        stringstream res; res << "Th0, change parameter " << m->getParamName(parameterID) << " around " << aroundInitialValue; Identification = res.str();
        if((parameterID < 0) || (parameterID >= m->getNbParams())){cerr << "ERR: expParameters, parameter ID(" << parameterID << ") incorrect, only " << m->getNbParams() << " parameters in the model" << endl;}
        names_exp.clear();
        names_exp.resize(NB_AROUNDPARAMETERS, string(""));
    }
    void simulate(int IdExp, Evaluator* E, bool force){
        // Initializing the names of experiments and parameter values to check - everything that depends on parameter values should be done inside simulate and not in the constructor.
        if(!initialized){
            listPoints = cutSpace(13, true, 2, m->getLowerBound(parameterID), m->getUpperBound(parameterID));
            if(aroundInitialValue){
                double initVal = m->getParam(parameterID);
                listPoints[PARAM0] = 0.0;
                listPoints[PARAM0P1] = 0.001*initVal;
                listPoints[PARAM1] = 0.01*initVal;
                listPoints[PARAM5] = 0.05*initVal;
                listPoints[PARAM20] = 0.2*initVal;
                listPoints[PARAM50] = 0.5*initVal;
                listPoints[PARAM80] = 0.8*initVal;
                listPoints[PARAM100] = 1.0*initVal;
                listPoints[PARAM120] = 1.2*initVal;
                listPoints[PARAM150] = 1.5*initVal;
                listPoints[PARAM200] = 2.0*initVal;
                listPoints[PARAM500] = 5.0*initVal;
                listPoints[PARAM1000] = 10.0*initVal;
                names_exp[PARAM0]   = string("(0%)  ");
                names_exp[PARAM0P1] = string("(0.1%)");
                names_exp[PARAM1]   = string("(1%)  ");
                names_exp[PARAM5]   = string("(5%)  ");
                names_exp[PARAM20]  = string("(20%) ");
                names_exp[PARAM50]  = string("(50%) ");
                names_exp[PARAM80]  = string("(80%) ");
                names_exp[PARAM100] = string("(100%)");
                names_exp[PARAM120] = string("(120%)");
                names_exp[PARAM150] = string("(150%)");
                names_exp[PARAM200] = string("(2X)  ");
                names_exp[PARAM500] = string("(5X)  ");
                names_exp[PARAM1000] = string("(10X) ");
            }
            for(int i = 0; i < getNbCond(); ++i){
                #ifndef WITHOUT_QT
                names_exp[i] += string("") + QString::number(listPoints[i]).toStdString();
                cerr << "WRN : expParametersTh1, can not label the names of Experiment in the non-graphical mode (WITHOUT_QT)" << endl;
                #endif
            }
            initialized = true;
        }

        switch(IdExp){
            case PARAM0:   {m->setParam(parameterID, listPoints[0]); break;}
            case PARAM0P1: {m->setParam(parameterID, listPoints[1]); break;}
            case PARAM1:   {m->setParam(parameterID, listPoints[2]); break;}
            case PARAM5:   {m->setParam(parameterID, listPoints[3]); break;}
            case PARAM20:  {m->setParam(parameterID, listPoints[4]); break;}
            case PARAM50:  {m->setParam(parameterID, listPoints[5]); break;}
            case PARAM80:  {m->setParam(parameterID, listPoints[6]); break;}
            case PARAM100: {m->setParam(parameterID, listPoints[7]); break;}
            case PARAM120: {m->setParam(parameterID, listPoints[8]); break;}
            case PARAM150: {m->setParam(parameterID, listPoints[9]); break;}
            case PARAM200: {m->setParam(parameterID, listPoints[10]); break;}
            case PARAM500: {m->setParam(parameterID, listPoints[11]); break;}
            case PARAM1000:{m->setParam(parameterID, listPoints[12]); break;}
        }
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                default: {m->initialise(Back::WT); break;}
            }
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};





#define coeffCyclohex 0.05
struct expCycloheximidine : public Experiment {
    double parameter; // time treatment
    expCycloheximidine(Model* _m, double _parameter) : Experiment(_m, NB_EXP), parameter(_parameter) {
        stringstream res; res << "Cycloheximidine at t=" << parameter << " hours"; Identification = res.str();
        if(parameter< 0) cerr << "ERR: expCycloheximidine, values are not supposed to be negative ..." << endl;
        names_exp[TH0] = string("Th0  CHX"); // activation (TCR Only)");
        names_exp[TH1] = string("Th1  CHX"); // activation (TCR + IL12 + anti-IL4)");
        names_exp[TH2] = string("Th2  CHX"); // differentiation (TCR + IL4 + anti-IFNg");
        names_exp[TREG] = string("Treg CHX"); // differentiation (TCR + TGFb + IL2)");
        names_exp[TH17] = string("Th17 CHX"); // differentiation (TCR + TGFb + IL6 + IL1b + anti-IL2 + anti-IFNg)");


    }
    void simulate(int IdExp, Evaluator* E, bool force){
        //m->setParam(parameterID, value);
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            //m->setBaseParameters();
            if((parameter > LdiffPert * 24) || (parameter < 0)) cerr << "ERR: expFromTh1, parameter should be a time in hours, inferior to the max time of differentiation " << LdiffPert << endl;

            switch(IdExp){
                case TH1: {
                    m->addValue("IL12", 10.0);
                    m->addValue("antiIL4", 10000.0);break;}
                case TH2: {
                    m->addValue("IL4", 14.6);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TREG: {
                    m->addValue("TGFB", 0.5);
                    m->addValue("IL2", 19.05);break;}
                case TH17: {
                    m->addValue("IL6", 30.0);
                    m->addValue("TGFB", 0.2);
                    m->addValue("antiIL2", 5000.0);
                    m->addValue("antiIFNg", 10000.0); break;}
                case TH0: { break;}
            }
            m->simulate(parameter*3600, E);
            m->action(string("cycloheximidine"), coeffCyclohex);
            m->simulate((LdiffPert*24 - parameter) *3600, E);
            m->setOverrider(nullptr);
        }
    }
};

enum DEFICIENTS   {/*TH1_WT, TH2_WT, TREG_WT, TH17_WT, TH0_WT, */TH1_KO, TH2_KO, TREG_KO, TH17_KO, TH0_KO, NB_DEFICIENT};
struct expDeficientAllCond : public Experiment {
    long long backgroundID;  // this time the parameter is the index of the input variable.
    expDeficientAllCond(Model* _m, long long _backgroundID) : Experiment(_m, NB_DEFICIENT), backgroundID(_backgroundID) {
        stringstream res; res << "All subsets, Background=" << getBackgroundName(backgroundID); Identification = res.str();
        if(backgroundID < 0){cerr << "ERR: expDeficientAllCond, background ID(" << backgroundID << ") incorrect" << endl;}
        /*names_exp[TH0_WT] = string("Th0, WT"); // activation (TCR Only)");
        names_exp[TH1_WT] = string("Th1, WT"); // activation (TCR + IL12 + anti-IL4)");
        names_exp[TH2_WT] = string("Th2, WT"); // differentiation (TCR + IL4 + anti-IFNg");
        names_exp[TREG_WT] = string("Treg, WT"); // differentiation (TCR + TGFb + IL2)");
        names_exp[TH17_WT] = string("Th17, WT"); // differentiation (TCR + TGFb + IL6 + IL1b + anti-IL2 + anti-IFNg)");*/
        names_exp[TH0_KO] = string("Th0, KO"); // activation (TCR Only)");
        names_exp[TH1_KO] = string("Th1, KO"); // activation (TCR + IL12 + anti-IL4)");
        names_exp[TH2_KO] = string("Th2, KO"); // differentiation (TCR + IL4 + anti-IFNg");
        names_exp[TREG_KO] = string("Treg, KO"); // differentiation (TCR + TGFb + IL2)");
        names_exp[TH17_KO] = string("Th17, KO"); // differentiation (TCR + TGFb + IL6 + IL1b + anti-IL2 + anti-IFNg)");
    }
    void simulate(int IdExp, Evaluator* E, bool force){
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
            /*case TH0_WT:{
                m->initialise(Back::WT); break;}
            case TH1_WT: {
                m->initialise(Back::WT);
                m->setValue("IL12", 10.0);
                m->setValue("antiIL4", 10000.0); break;}
            case TH2_WT: {
                m->initialise(Back::WT);
                m->setValue("IL4", 14.6);
                m->setValue("antiIFNg", 10000.0); break;}
            case TREG_WT: {
                m->initialise(Back::WT);
                m->setValue("TGFB", 0.5);
                m->setValue("IL2", 19.05); break;}
            case TH17_WT: {
                m->initialise(Back::WT);
                m->setValue("IL6", 30.0);
                m->setValue("TGFB", 0.2);
                m->setValue("antiIL2", 5000.0);
                m->setValue("antiIFNg", 10000.0); break;}*/
            case TH0_KO:{
                m->initialise(backgroundID); break;}
            case TH1_KO: {
                m->initialise(backgroundID);
                m->setValue("IL12", 10.0);
                m->setValue("antiIL4", 10000.0); break;}
            case TH2_KO: {
                m->initialise(backgroundID);
                m->setValue("IL4", 14.6);
                m->setValue("antiIFNg", 10000.0); break;}
            case TREG_KO: {
                m->initialise(backgroundID);
                m->setValue("TGFB", 0.5);
                m->setValue("IL2", 19.05); break;}
            case TH17_KO: {
                m->initialise(backgroundID);
                m->setValue("IL6", 30.0);
                m->setValue("TGFB", 0.2);
                m->setValue("antiIL2", 5000.0);
                m->setValue("antiIFNg", 10000.0); break;}
            }
            m->simulate(LdiffPert*24*3600, E);
            m->setOverrider(nullptr);
        }
    }
};
/*
struct expInduceDeficiencyAllCond : public Experiment {
    long long backgroundID;  // this time the parameter is the index of the input variable.
    double efficiency;
    double timeInduce;
    expInduceDeficiencyAllCond(Model* _m, long long _backgroundID, double _efficiency, double _timeInduce) : Experiment(_m, NB_DEFICIENT), backgroundID(_backgroundID), efficiency(_efficiency), timeInduce(_timeInduce) {
        if(backgroundID < 0){cerr << "ERR: expInduceDeficiencyAllCond, background ID(" << backgroundID << ") incorrect" << endl;}
        if((efficiency < 0) || (efficiency > 10.0)){cerr << "ERR: expInduceDeficiencyAllCond, efficiency(" << efficiency << ") is incorrect" << endl;}
        if((timeInduce < 0) || (timeInduce > LdiffPert)){cerr << "ERR: expInduceDeficiencyAllCond, timeInduce(" << timeInduce<< ") is incorrect" << endl;}
        /// philippe, checl if timeInduce is in hours ????
        names_exp[TH0_WT] = string("Th0, WT"); // activation (TCR Only)");
        names_exp[TH1_WT] = string("Th1, WT"); // activation (TCR + IL12 + anti-IL4)");
        names_exp[TH2_WT] = string("Th2, WT"); // differentiation (TCR + IL4 + anti-IFNg");
        names_exp[TREG_WT] = string("Treg, WT"); // differentiation (TCR + TGFb + IL2)");
        names_exp[TH17_WT] = string("Th17, WT"); // differentiation (TCR + TGFb + IL6 + IL1b + anti-IL2 + anti-IFNg)");
        names_exp[TH0_KO] = string("Th0, KO"); // activation (TCR Only)");
        names_exp[TH1_KO] = string("Th1, KO"); // activation (TCR + IL12 + anti-IL4)");
        names_exp[TH2_KO] = string("Th2, KO"); // differentiation (TCR + IL4 + anti-IFNg");
        names_exp[TREG_KO] = string("Treg, KO"); // differentiation (TCR + TGFb + IL2)");
        names_exp[TH17_KO] = string("Th17, KO"); // differentiation (TCR + TGFb + IL6 + IL1b + anti-IL2 + anti-IFNg)");

        doable.resize(NB_DEFICIENT, true);
    }
    void simulate(int IdExp, Evaluator* E, bool force){
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(Back::WT);
            switch(IdExp){
            case TH0_WT:{ break;}
            case TH1_WT: {
                m->setValue("IL12", 10.0);
                m->setValue("antiIL4", 10000.0); break;}
            case TH2_WT: {
                m->setValue("IL4", 14.6);
                m->setValue("antiIFNg", 10000.0); break;}
            case TREG_WT: {
                m->setValue("TGFB", 0.5);
                m->setValue("IL2", 19.05); break;}
            case TH17_WT: {
                m->setValue("IL6", 30.0);
                m->setValue("TGFB", 0.2);
                m->setValue("antiIL2", 5000.0);
                m->setValue("antiIFNg", 10000.0); break;}
            case TH0_KO:{ break;}
            case TH1_KO: {
                m->setValue("IL12", 10.0);
                m->setValue("antiIL4", 10000.0); break;}
            case TH2_KO: {
                m->setValue("IL4", 14.6);
                m->setValue("antiIFNg", 10000.0); break;}
            case TREG_KO: {
                m->setValue("TGFB", 0.5);
                m->setValue("IL2", 19.05); break;}
            case TH17_KO: {
                m->setValue("IL6", 30.0);
                m->setValue("TGFB", 0.2);
                m->setValue("antiIL2", 5000.0);
                m->setValue("antiIFNg", 10000.0); break;}
            }            
            m->simulate(timeInduce*3600, E);
            vector<double> compact;
            compact.push_back(efficiency);
            compact.push_back((double) backgroundID);
            m->action(string("induceBackground"), compact);
            m->simulate((LdiffPert*24 - timeInduce) *3600, E);
            m->setOverrider(NULL);
        }
    }
};
*/

#endif

