#-------------------------------------------------
#
# Project created by QtCreator 2014-09-01T15:12:07
#
#-------------------------------------------------

DEFINES += WITHOUT_QT

#QT += printsupport
#QT += opengl
#QT += svg

# QT       += core gui

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# windows sytem
#INCLUDEPATH += C:\Qt530\boost_1_58_0\boost_1_58_0
#INCLUDEPATH += C:\Qt530\boost_1_58_0

INCLUDEPATH += /usr/include/boost
#INCLUDEPATH += C:\Qt530\boost_1_58_0

#QT       += core
#QT       -= gui

# to avoid warnings from boost library files (wtf !)
QMAKE_CXXFLAGS += -std=c++11 -Wno-unused-local-typedefs -Wreorder -Wno-unused-parameter

TARGET = fitte
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

HEADERS += \
    Framework/evaluator.h \
    Framework/experiments.h \
    Framework/generate.h \
    Framework/modele.h \
    Framework/overrider.h \
    Framework/spline.h \
    Framework/tableCourse.h \
    Interface/simuwin.h \
    GlobalExperiments/expThs.h \
    GlobalExperiments/namesTh.h \
    R1/modeleThTot5custom.h \
    R1/modeleThTot6.h \
    common.h \
    R1/modeleMinNoIL10.h \
    R1/modeleSimpleNoIL10.h \ 
    Extreminator/common/myFiles.h \
    Extreminator/common/myRandom.h \
    Extreminator/common/myTimes.h \
    Extreminator/common/statistiques.h \
    Extreminator/cost/baseProblem.h \
    Extreminator/include/multimedia/AudioDefs.hpp \
    Extreminator/include/multimedia/VideoDefs.hpp \
    Extreminator/include/blas.h \
    Extreminator/include/blascompat32.h \
    Extreminator/include/covrt.h \
    Extreminator/include/emlrt.h \
    Extreminator/include/engine.h \
    Extreminator/include/fintrf.h \
    Extreminator/include/io64.h \
    Extreminator/include/lapack.h \
    Extreminator/include/mat.h \
    Extreminator/include/matrix.h \
    Extreminator/include/mex.h \
    Extreminator/include/mwmathutil.h \
    Extreminator/include/tmwtypes.h \
    Extreminator/optimizers/SRES/ESES.h \
    Extreminator/optimizers/SRES/ESSRSort.h \
    Extreminator/optimizers/SRES/sharefunc.h \
    Extreminator/optimizers/baseOptimizer.h \
    Extreminator/optimizers/CMAES.h \
    Extreminator/optimizers/Config.h \
    Extreminator/optimizers/Genetic.h \
    Extreminator/optimizers/GeneticGeneral.h \
    Extreminator/optimizers/GradientDescent.h \
    Extreminator/optimizers/individual.h \
    Extreminator/optimizers/MultipleGradientDescent.h \
    Extreminator/optimizers/Orthogonalise.h \
    Extreminator/optimizers/population.h \
    Extreminator/optimizers/scalingFunctions.h \
    Extreminator/optimizers/simulatedAnnealing.h \
    Extreminator/optimizers/SRES.h \
    R1/ScriptMai2015.h \
    Interface/optselect.h \
    R1/modeleLatentTbet.h \
    R1/modeleLatentTbet2.h
    Framework/parameterSets.h

SOURCES += \
    Framework/evaluator.cpp \
    Framework/experiments.cpp \
    Framework/generate.cpp \
    Framework/modele.cpp \
    Framework/overrider.cpp \
    Framework/spline.cpp \
    Framework/tableCourse.cpp \
    Interface/mainInterface.cpp \
    Interface/simuwin.cpp \
    GlobalExperiments/expThs.cpp \
    GlobalExperiments/namesTh.cpp \
    R1/modeleThTot5custom.cpp \
    R1/modeleThTot6.cpp \
    R1/modeleMinNoIL10.cpp \
    R1/modeleSimpleNoIL10.cpp \ 
    Extreminator/common/myFiles.cc \
    Extreminator/common/myRandom.cc \
    Extreminator/common/myTimes.cc \
    Extreminator/optimizers/SRES/ESES.cc \
    Extreminator/optimizers/SRES/ESSRSort.cc \
    Extreminator/optimizers/SRES/sharefunc.cc \
    Extreminator/optimizers/baseOptimizer.cc \
    Extreminator/optimizers/CMAES.cc \
    Extreminator/optimizers/Config.cc \
    Extreminator/optimizers/Genetic.cc \
    Extreminator/optimizers/GeneticGeneral.cc \
    Extreminator/optimizers/GradientDescent.cc \
    Extreminator/optimizers/individual.cc \
    Extreminator/optimizers/Orthogonalise.cc \
    Extreminator/optimizers/population.cc \
    Extreminator/optimizers/scalingFunctions.cc \
    Extreminator/optimizers/simulatedAnnealing.cc \
    Extreminator/optimizers/SRES.cc \
    R1/ScriptMai2015.cpp \
    Interface/optselect.cpp \
    Framework/parameterSets.cpp \
	R1/modeleLatentTbet.cpp \
    R1/modeleLatentTbet2.cpp


