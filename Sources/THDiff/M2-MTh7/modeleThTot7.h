// ------- Automatically generated model -------- //
#ifndef MODELEThTot7_H
#define MODELEThTot7_H
#include "../Moonfit/moonfit.h"

#ifdef COMPILE_AUTOGEN
#include "../GlobalExperiments/namesTh.h"

/* Automatically generated modele */ 
struct ModeleThTot7 : public Model {
	ModeleThTot7();
	enum {IL2, IL4, IL6, IL12, IL17, IL21, IFNG, TGFB, TBET, GATA3, RORGT, FOXP3, IL2mRNA, IL4mRNA, IL17mRNA, IL21mRNA, IFNGmRNA, TGFBmRNA, TBETmRNA, GATA3mRNA, RORGTmRNA, FOXP3mRNA, TCR, NBVAR};
	enum {KDIL2, KDIL4, KDIL6, KDIL12, KDIL17, KDIL21, KDIFNG, KDTGFB, KDTBET, KDGATA3, KDRORGT, KDFOXP3, KDIL2mRNA, KDIL4mRNA, KDIL17mRNA, KDIL21mRNA, KDIFNGmRNA, KDTGFBmRNA, KDTBETmRNA, KDGATA3mRNA, KDRORGTmRNA, KDFOXP3mRNA, IL2EQ, IL4EQ, IL17EQ, IL21EQ, IFNGEQ, TGFBEQ, TBETEQ, GATA3EQ, RORGTEQ, FOXP3EQ, IL2mRNAEQ, IL4mRNAEQ, IL17mRNAEQ, IL21mRNAEQ, IFNGmRNAEQ, TGFBmRNAEQ, TBETmRNAEQ, GATA3mRNAEQ, RORGTmRNAEQ, FOXP3mRNAEQ, IL2EQ, IL4EQ, IL17EQ, IL21EQ, IFNGEQ, TGFBEQ, TBETEQ, GATA3EQ, RORGTEQ, FOXP3EQ, IL2mRNAEQ, IL4mRNAEQ, IL17mRNAEQ, IL21mRNAEQ, IFNGmRNAEQ, TGFBmRNAEQ, TBETmRNAEQ, GATA3mRNAEQ, RORGTmRNAEQ, FOXP3mRNAEQ, PIL2, PIL4, PIL17, PIL21, PIFNG, PTGFB, PTBET, PGATA3, PRORGT, PFOXP3, CIL2mRNA, KIL2_TO_IL2mRNA, NIL2_TO_IL2mRNA, CIL4mRNA, KGATA3_TO_IL4mRNA, NGATA3_TO_IL4mRNA, CIL17mRNA, KRORGT_TO_IL17mRNA, NRORGT_TO_IL17mRNA, CIL21mRNA, KRORGT_TO_IL21mRNA, NRORGT_TO_IL21mRNA, CIFNGmRNA, KTBET_TO_IFNGmRNA, NTBET_TO_IFNGmRNA, PTGFBmRNA, CTBETmRNA, KIL6_TO_TBETmRNA, NIL6_TO_TBETmRNA, KIL12_TO_TBETmRNA, NIL12_TO_TBETmRNA, KIFNG_TO_TBETmRNA, NIFNG_TO_TBETmRNA, KGATA3_TO_TBETmRNA, NGATA3_TO_TBETmRNA, KRORGT_TO_TBETmRNA, NRORGT_TO_TBETmRNA, KFOXP3_TO_TBETmRNA, NFOXP3_TO_TBETmRNA, CGATA3mRNA, KIL2_TO_GATA3mRNA, NIL2_TO_GATA3mRNA, KIL4_TO_GATA3mRNA, NIL4_TO_GATA3mRNA, KTBET_TO_GATA3mRNA, NTBET_TO_GATA3mRNA, KGATA3_TO_GATA3mRNA, NGATA3_TO_GATA3mRNA, KRORGT_TO_GATA3mRNA, NRORGT_TO_GATA3mRNA, KFOXP3_TO_GATA3mRNA, NFOXP3_TO_GATA3mRNA, CRORGTmRNA, KIL6_TO_RORGTmRNA, NIL6_TO_RORGTmRNA, KIL21_TO_RORGTmRNA, NIL21_TO_RORGTmRNA, KTGFB_TO_RORGTmRNA, NTGFB_TO_RORGTmRNA, KTBET_TO_RORGTmRNA, NTBET_TO_RORGTmRNA, KGATA3_TO_RORGTmRNA, NGATA3_TO_RORGTmRNA, KFOXP3_TO_RORGTmRNA, NFOXP3_TO_RORGTmRNA, CFOXP3mRNA, KIL2_TO_FOXP3mRNA, NIL2_TO_FOXP3mRNA, KTGFB_TO_FOXP3mRNA, NTGFB_TO_FOXP3mRNA, KTBET_TO_FOXP3mRNA, NTBET_TO_FOXP3mRNA, KGATA3_TO_FOXP3mRNA, NGATA3_TO_FOXP3mRNA, KRORGT_TO_FOXP3mRNA, NRORGT_TO_FOXP3mRNA, BIL2, BIL4, BIL17, BIL21, BIFNG, BTGFB, BTBET, BGATA3, BRORGT, BFOXP3, BIL2mRNA, BIL4mRNA, BIL17mRNA, BIL21mRNA, BIFNGmRNA, BTGFBmRNA, BTBETmRNA, BGATA3mRNA, BRORGTmRNA, BFOXP3mRNA, NBPARAM};

    long long background;
	void derivatives(const vector<double> &x, vector<double> &dxdt, const double t);
    void initialise(long long _background = Back::WT);
	void setBaseParameters();
	double Activ0Inhib1(double Val1, double K1, double N1){
		if((Val1 <= 0.0)) return 0.0; 
		else return (pow(K1, N1) / ( pow(K1, N1) + pow(Val1, N1) )); }
	double Activ1Inhib0(double Val1, double K1, double N1){
		if((Val1 <= 0.0)) return 0.0; 
		else return (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) *1); }
	double Activ2Inhib3(double Val1, double K1, double N1,double Val2, double K2, double N2,double Val3, double K3, double N3,double Val4, double K4, double N4,double Val5, double K5, double N5){
		if((Val1 <= 0.0) || (Val2 <= 0.0) || (Val3 <= 0.0) || (Val4 <= 0.0) || (Val5 <= 0.0)) return 0.0; 
		else return (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) *pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) *pow(K3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) * pow(K4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) * pow(K5, N5) / ( pow(K5, N5) + pow(Val5, N5) )); }
	double Activ3Inhib3(double Val1, double K1, double N1,double Val2, double K2, double N2,double Val3, double K3, double N3,double Val4, double K4, double N4,double Val5, double K5, double N5,double Val6, double K6, double N6){
		if((Val1 <= 0.0) || (Val2 <= 0.0) || (Val3 <= 0.0) || (Val4 <= 0.0) || (Val5 <= 0.0) || (Val6 <= 0.0)) return 0.0; 
		else return (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) *pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) *pow(Val3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) *pow(K4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) * pow(K5, N5) / ( pow(K5, N5) + pow(Val5, N5) ) * pow(K6, N6) / ( pow(K6, N6) + pow(Val6, N6) )); }
};
#endif
#endif
