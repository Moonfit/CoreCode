#ifndef TESTS_H
#define TESTS_H

#include "../../Framework/modele.h"
#include "../../Framework/generate.h"
#include "../namesTh.h"
#include "../expThs.h"

void    testExperimentTH1();
void    testModeleAndEvaluator();
int     testModeleAlone();
void    testGenerate();
void    testeEvaluator();
void    testTableCourse();

#endif // TESTS_H
