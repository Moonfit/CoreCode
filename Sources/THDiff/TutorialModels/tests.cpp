#include "tests.h"

struct ModeleTest : public Modele {
    ModeleTest();
    enum  {IL12, STAT4P, TBET, NBVAR};
    enum {KD12, KDS4P, KDTBET, S4PEQ, CS4P, TBETEQ, CTBET, CTCR, BS4P, BTBET, NBPARAM};
    long long background;
    void one_step(double _t, double _delta_t);
    void initialise(long long _background = Back::WT);
    void setBaseParameters();
};

ModeleTest::ModeleTest() : Modele(NBVAR, NBPARAM), background(Back::WT) {
    dt = 0.2;
    print_all_secs = 1200;

    names[IL12] = string("IL12");
    names[STAT4P] = string("STAT4P");
    names[TBET] = string("TBET");

    extNames[IL12] = N::IL12;
    extNames[STAT4P] = N::STAT4P;
    extNames[TBET] = N::TBET;

    backSimulated.clear();
    backSimulated.push_back(Back::WT);
    backSimulated.push_back(Back::STAT4KO);
    backSimulated.push_back(Back::TBETKO);

    #ifdef PRINT_KIN
    cerr << "Modele for Th1 differentiation (M110L) Nb of parameters : " << NBPARAM << ", variables :" << NBVAR << endl;
    #endif
}

void ModeleTest::setBaseParameters(){
    params.clear();
    params.resize(NBPARAM);
    params[KD12] = 1.1e-4;
    params[KDS4P] = 2e-5;
    params[KDTBET] = 1e-5;
    params[S4PEQ] = 0.05;
    params[CS4P] = 1.0;
    params[TBETEQ] = 0.05;
    params[CTBET] = 1.0;
    params[CTCR] = 1.0;
    // params[BS4P] and params[BTBET] will be defined in initialize
    setBaseParametersDone();
}

void ModeleTest::initialise(long long _background){

    background = _background;
    val.clear();        val.resize(NBVAR, 0.0);
    //valTemp.clear();    valTemp.resize(NBVAR, 0.0);
    init.clear();       init.resize(NBVAR, 0.0);

    // parameters that depend on other ones are better here in case one wants to use loadParameters
    params[BS4P]  = params[KDS4P] * params[S4PEQ];
    params[BTBET] = params[KDTBET] * params[TBETEQ] - params[CTBET] * params[S4PEQ] * params[TBETEQ] * (1 + params[CTCR]);
    if(params[BS4P]  < 0) params[BS4P] = 0;
    if(params[BTBET] < 0) params[BTBET] = 0;

    init[IL12]   = 0.0;
    init[TBET]   = params[TBETEQ];
    init[STAT4P] = params[S4PEQ];

    if(background == Back::STAT4KO) init[STAT4P] = 0.0;
    if(background == Back::TBETKO) init[TBET] = 0.0;

    for(int i = 0; i < NBVAR; ++i){val[i] = init[i];}
    t = 0;
}

void ModeleTest::one_step(double _t, double delta_t){
    /*valTemp[IL12]   = delta_t * (- params[KD12] * val[IL12]);
    valTemp[STAT4P] = delta_t * (- params[KDS4P] * val[STAT4P] + params[BS4P] + params[CS4P] * val[IL12] * val[STAT4P]);
    valTemp[TBET]   = delta_t * (- params[KDTBET] * val[TBET] + params[BTBET] + params[CTBET] * val[STAT4P] * val[TBET] * (1 + params[CTCR]));
    for(int i = 0; i < NBVAR; ++i){val[i] += valTemp[i];}*/
}






void testExperimentTH1(){
/*    Modele* currentModel = new ModeleTest();
    Experiment* currentExperiment = new expThs(currentModel);
    currentExperiment->init();
    currentExperiment->costPart();              // to give 'wish data' to evaluators
    currentExperiment->simulationsPart();
    cerr << currentExperiment->costPart();      // retrieves data from evaluator

    currentModel->loadParameters(string("C:/Users/parobert/Desktop/Optimisation/ThModeles/Modeles/ParamSets/BestSetM110.txt"));
    currentModel->setPrintMode(true, 120);
    currentModel->initialise();
    currentModel->setValue(N::IL12, 10.0);
    currentModel->simule(10000);
    {
        TableCourse tc = currentModel->getCinetique();
        tc.print();
    }
*/
    //currentExperiment->init();

    /* for(int i = 0; i < currentExperiment->nbCond(); ++i){
        cout << "=============== asking for experiment nb " << i << "================\n";
        currentExperiment->simulationsPart(i);
        TableCourse tc = currentExperiment->m->getCinetique();
        tc.print();
    }*/
/*
    cerr << "Testing generation of data"<< endl;
    vector<int> timePoints;
    timePoints.push_back(3600);
    timePoints.push_back(2*3600);
    timePoints.push_back(3*3600);
    timePoints.push_back(4*3600);
    vector<int> listGlobVars;
    listGlobVars.push_back(N::IL12);
    listGlobVars.push_back(N::STAT4P);
    listGlobVars.push_back(N::TBET);

    string store;
    store = currentExperiment->extractData(timePoints, listGlobVars, getCodingNames());
    cout << "Extract Data gives the following table :\n";
    cout << store;
    cout << "Now, converted into 'cost' function :\n";
    cout << generateCostCodeFromData(store);
    */
}

int testModeleAlone(){
    /*
    Modele* M = new ModeleTest();
    M->setBaseParameters();
    M->initialise(Back::WT);
    M->print();
    M->simule(100);
    M->initialise(Back::WT);
    M->setValue(N::IL12, 0.01);
    M->setPrintMode(true, 120);
    M->stabilize(100);  // note that in stabilize there is no divergence test/stop
    M->simule(10000);
    TableCourse T = M->getCinetique();
    T.print();
    if(M->penalities > 0) cout << "The simulation diverged and was stopped ; penalty = " << M->penalities << endl;

    cerr << "Variables : ";
    for(int i = 0; i < M->getNbVars(); ++i){
        cerr << M->getName(i) << "\t";
    }

    cerr << "the value of IL12 at the end is " << M->getValue(N::IL12) << endl;
    cerr << "Testing if backgrounds or variables are possible to simulate : ";
    if(M->isSimuBack(Back::WT)) cerr << "Back::WT is simulated" << endl; else cerr << "Back::WT is NOT simulated" << endl;
    if(M->isSimuBack(Back::IFNGRKO)) cerr << "Back::IFNGRKO is simulated" << endl; else  cerr << "Back::IFNGRKO is NOT simulated" << endl;
    if(M->isSimuBack(N::SOCS1)) cerr << "N::SOCS1 is simulated" << endl; else  cerr << "N::SOCS1 is NOT simulated" << endl;

*/
    return 0;
}

void testModeleAndEvaluator(){
    /*
    Modele* currentModel = new Modele110();
    currentModel->setBaseParameters();
    currentModel->initialise(Back::WT);
    currentModel->loadParameters(string("C:/Users/parobert/Desktop/Optimisation/ThModeles/Modeles/ParamSets/BestSetM110.txt"));
    currentModel->print();
    Evaluator* E = new Evaluator();
    cerr << E->getVal(currentModel->internValName(N::STAT4P), 1*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::STAT4P), 2*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::STAT4P), 3*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::STAT4P), 4*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::TBET), 1*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::TBET), 2*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::TBET), 3*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::TBET), 4*3600) << "\t";
    E->recordingCompleted();
    currentModel->setPrintMode(true, 120);
    currentModel->simule(5*3600, E);
    TableCourse T = currentModel->getCinetique();
    T.print();
    cerr << E->getVal(currentModel->internValName(N::STAT4P), 1*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::STAT4P), 2*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::STAT4P), 3*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::STAT4P), 4*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::TBET), 1*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::TBET), 2*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::TBET), 3*3600) << "\t";
    cerr << E->getVal(currentModel->internValName(N::TBET), 4*3600) << "\t";
    cerr << endl;
    */
}

void testTableCourse(){

    TableCourse A(3);
    A.setHeader(0, string("TableEssai"));
    A.setHeader(1, string("VarA"));
    A.setHeader(2, string("VarB"));
    A.setHeader(3, string("VarC"));
    vector<double> t1 = vector<double>(3, 0.0); // example of data for t1 at t=10sec
    t1[0] = 1.0; t1[1] = 0.5; t1[2] = 0.3;
    vector<double> t2 = vector<double>(3, 0.0); // example of data for t2 at t=20sec
    t2[0] = 0.8; t2[1] = 0.5; t2[2] = 0.4;
    A.addSet(10, t1);
    A.addSet(20, t2);

    A.print();
    A.save("EssaiTableCourse.txt", "Let's see if iut works");
    cerr << "Time course of variable B : ";
    vector<double> courseOfVarB = A.getTimeCourse(1);
    vector<double> timePoints = A.getTimePoints();
    if(courseOfVarB.size() != timePoints.size()) cerr << "ERR : the number of time points and the length of a time course should be the same !!\n";
    for(int i = 0; i < (int) courseOfVarB.size(); ++i){
        cerr << "t=" << timePoints[i] << ", B=" << courseOfVarB[i] << "\t" << endl;
    }
}


void testeEvaluator() {
    Evaluator E = Evaluator();
    E.getVal(1,15);
    E.getVal(3,20);
    E.getVal(1,20);
    E.getVal(5,50);

    E.recordingCompleted();

    //during simulation
    //E.printState();
    for(int i = 0; i < 100; ++i){
        if(E.takeDataAtThisTime(i)){
            int z = E.speciesToUpdate();
            while(z >= 0){
                E.setValNow(z, (double) (i + z*100));
                z = E.speciesToUpdate();
            }
            //E.printState();
        }
    }
    cerr << "\n";
    cerr << E.getVal(1,15) << "\t";
    cerr << E.getVal(3,20) << "\t";
    cerr << E.getVal(1,20) << "\t";
    cerr << E.getVal(5,50) << "\n";
    cerr << E.printState();
}


void testGenerate(){
     Generate a = Generate("Essai");
     a.addVar("IL12", PROP_DEG, CST_FROM_EQ, "N::IL12");
     a.addVar("ST4P", PROP_DEG, CST_FROM_EQ, "N::STAT4P");
     a.addVar("TBET", PROP_DEG, CST_FROM_EQ, "N::TBET");
     a.addVar("IFNG", PROP_DEG, CST_FROM_EQ, "N::IFNG");

     a.addReaction("IL12", "ST4P", +1);
     a.addReaction("ST4P", "TBET", +1);
     a.addReaction("ST4P", "ST4P", +1);
     a.addReaction("TBET", "IFNG", +1);
     a.addReaction("IFNG", "TBET", +1);
     a.addReaction("IFNG", "IFNG", -1);
     a.addReaction("ST4P", "IFNG", +1);

     a.addInclude(string("../Modeles/namesTh.h"));


     // enum D {NO_D, PROP_DEG, NB_DEG_POL};
     // enum B {NO_B, CST, CST_FROM_EQ, NB_BAS_POL};
     // Generate(string _MName);
     // void addVar(string nameVar, D degPolicy = PROP_DEG, B basalPolicy = CST_FROM_EQ, string globalName = string(""));
     // void addReaction(string Var1, string Var2, int IDfunction);


     //    vector<string> generateParameters();
     //    string generateActiv(int nbAct, int nbInh);
     //    string generateCodeHeader();
     //    string generateCodeSource();
     //    string generateEquations();
     //    string generateLatex();

     //  cerr << a.generateCodeHeader();
     //  cerr << a.generateCodeSource();
     //  ofstream fichier("C:\Users\parobert\Desktop\Optimisation\ThModeles\Modeles", ios::out);

     ofstream fichier("C:/Users/parobert/Desktop/Optimisation/ThModeles/ModelesAuto/modeleEssai.h", ios::out);
     fichier << a.generateCodeHeader();
     fichier.close();

     ofstream fichier2("C:/Users/parobert/Desktop/Optimisation/ThModeles/ModelesAuto/modeleEssai.cpp", ios::out);
     fichier2 << a.generateCodeSource();
     fichier2.close();
}


void testGenerateWithGraph(){

    Generate a = Generate("Essai");
    a.addVar("IL12", PROP_DEG, CST_FROM_EQ, "N::IL12");
    a.addVar("ST4P", PROP_DEG, CST_FROM_EQ, "N::STAT4P");
    a.addVar("TBET", PROP_DEG, CST_FROM_EQ, "N::TBET");
    a.addVar("IFNG", PROP_DEG, CST_FROM_EQ, "N::IFNG");

    a.addReaction("IL12", "ST4P", +1);
    a.addReaction("ST4P", "TBET", +1);
    a.addReaction("ST4P", "ST4P", +1);
    a.addReaction("TBET", "IFNG", +1);
    a.addReaction("IFNG", "TBET", +1);
    a.addReaction("IFNG", "IFNG", -1);
    a.addReaction("ST4P", "IFNG", +1);

    // this would lead to the same thing
    Graphe G = a.g;
    a.clearReactions();
    a.useReactionsFromGraph(G);

    // warning, it should be the same name than generate()
    ofstream fichier("C:/Users/parobert/Desktop/Optimisation/ThModeles/ModelesAuto/modeleEssai2.h", ios::out);
    fichier << a.generateCodeHeader();
    fichier.close();

    ofstream fichier2("C:/Users/parobert/Desktop/Optimisation/ThModeles/ModelesAuto/modeleEssai2.cpp", ios::out);
    fichier2 << a.generateCodeSource();
    fichier2.close();

}
















