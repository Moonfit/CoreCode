#include "../Moonfit/moonfit.h"

#include "namesTh.h"
#include "expThs.h"
#include "expPred.h"
#include "M6a-LatentTbet2/modeleLatentTbet2Hours.h"

static string folder = "C:/Users/pprobert/Desktop/Softwares/NewArchaeropteryx/Sources/THDiff/";
static string folderBaseResults = "C:/Users/pprobert/Desktop/Softwares/NewArchaeropteryx/Results/";

vector<double> rescaleHrs2(vector<double> vinput){
    for(size_t i = 0; i < vinput.size(); ++i){
        vinput[i] = vinput[i] / 3600.;
    }
    return vinput;
}

#define nRep 10

int main(int argc, char *argv[]){
    if(argc < 2) {cerr << "MiniMain takes only 1 argument (which combination)" << endl; return 0;}
    int IDcomb = -1;
    if(argc == 2){IDcomb = atoi(argv[1]);}
    cout << "Will process combination " << IDcomb << endl;

    createFolder(folderBaseResults);

    TableCourse* TTh1   = new TableCourse(folder + string("DATA/2022_Kin_Th1_All.txt"));
    TableCourse* TTh2   = new TableCourse(folder + string("DATA/2022_Kin_Th2_All.txt"));
    TableCourse* TiTreg = new TableCourse(folder + string("DATA/2022_Kin_iTreg_All.txt"));
    TableCourse* TTh17  = new TableCourse(folder + string("DATA/2022_Kin_Th17_All.txt"));
    TableCourse* TTh0   = new TableCourse(folder + string("DATA/2022_Kin_Th0_All.txt"));

    TableCourse* TTh1Std   = new TableCourse(folder + string("DATA/2022_Kin_Th1_All_STD.txt"));
    TableCourse* TTh2Std   = new TableCourse(folder + string("DATA/2022_Kin_Th2_All_STD.txt"));
    TableCourse* TiTregStd = new TableCourse(folder + string("DATA/2022_Kin_iTreg_All_STD.txt"));
    TableCourse* TTh17Std  = new TableCourse(folder + string("DATA/2022_Kin_Th17_All_STD.txt"));
    TableCourse* TTh0Std   = new TableCourse(folder + string("DATA/2022_Kin_Th0_All_STD.txt"));


    // in Hours
    TTh1->attribut = rescaleHrs2(TTh1->attribut);
    TTh2->attribut = rescaleHrs2(TTh2->attribut);
    TiTreg->attribut = rescaleHrs2(TiTreg->attribut);
    TTh17->attribut = rescaleHrs2(TTh17->attribut);
    TTh0->attribut = rescaleHrs2(TTh0->attribut);

    TTh1Std->attribut = rescaleHrs2(TTh1Std->attribut);
    TTh2Std->attribut = rescaleHrs2(TTh2Std->attribut);
    TiTregStd->attribut = rescaleHrs2(TiTregStd->attribut);
    TTh17Std->attribut = rescaleHrs2(TTh17Std->attribut);
    TTh0Std->attribut = rescaleHrs2(TTh0Std->attribut);

    overrider* OverTh1 = new overrider(TTh1);                                               // Note : never create overrider as a non pointer, to be used by the graphical interface because they will be erased when function closes and gives control to the interface --> use a pointer and new ...
    overrider* OverTh2 = new overrider(TTh2);
    overrider* OveriTreg = new overrider(TiTreg);
    overrider* OverTh17 = new overrider(TTh17);
    overrider* OverTh0 = new overrider(TTh0);

    //    case SQUARE_COST: res << "RSS"; break;
    //    case SQUARE_COST_STD: res << "RSS + StdDev"; break;
    //    case LOG_COST: res << "Log"; break;
    //    case PROPORTION_COST: res << "Ratios"; break;

    //    case NO_NORM: res << " (No Norm)"; break;
    //    case NORM_AVERAGE: res << " Norm/Avg vars"; break;
    //    case NORM_NB_PTS: res << " Norm/Nb Points"; break;
    //    case NORM_AVG_AND_NB_PTS: res << " Norm/Avg and Nb Pts "; break;
    //    case NORM_MAX: res << " Norm/Max vars"; break;
    //    case NORM_MAX_AND_NB_PTS: res << " Norm/Max and Nb Pts"; break;
    setTypeCost(SQUARE_COST);
    setTypeNorm(NORM_AVERAGE);

    string configFile = string("ConfigForGlobal.txt");
    Model* currentModel = new modeleLatentTbet2Hours();
    expThs* currentExperiment = (expThs*) new expThsHours(currentModel);

    currentExperiment->giveData(TTh1, TH1, TTh1Std);
    currentExperiment->giveData(TTh2, TH2, TTh2Std);
    currentExperiment->giveData(TiTreg, TREG, TiTregStd);
    currentExperiment->giveData(TTh17, TH17, TTh17Std);
    currentExperiment->giveData(TTh0, TH0, TTh0Std);
    currentExperiment->loadEvaluators();

    cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";

    currentExperiment->setOverrider(TH1,    OverTh1);
    currentExperiment->setOverrider(TH2,    OverTh2);
    currentExperiment->setOverrider(TREG,   OveriTreg);
    currentExperiment->setOverrider(TH17,   OverTh17);
    currentExperiment->setOverrider(TH0,    OverTh0);



    manageSims* msi = new manageSims(currentExperiment);
    msi->loadConfig(folder + configFile);
    vector<string> listGeneratedFilesSets;

    for(int i = 0; i < msi->nbCombs; ++i){
        if(IDcomb < 0 || IDcomb == i){

            stringstream headerOptimizer;                                                   // each further script might use different optimizer options, will be stored in the following stringstream
            if(IDcomb == 0) headerOptimizer << optFileHeader(Genetic1M);
            else headerOptimizer << optFileHeader(Genetic50k);

            for(int j = 0; j < nRep; ++j){
                stringstream codeSimu;      codeSimu << "CombNr" << i << "-" << codeTime();               // generates a text code for this particular optimization, in case parallel optimizations are running
                stringstream folderComb;    folderComb << folderBaseResults << codeSimu.str() << "/";        // creates a folder for this particular optimization, to create figures etc ...
                createFolder(folderComb.str());

                cout << "   -> Optimizing combination (" << i << ") with ID: " << codeSimu.str() << "\n";
                msi->resetParamSetFromConfig(folder + configFile);
                currentExperiment->m->setPrintMode(false, 5000./3600.);
                currentExperiment->m->dt = 10./3600.;

                string optOptions = msi->motherCreateOptimizerFile(i, headerOptimizer.str());       // for each combination, will need to re-create an optimizer file
                ofstream f1((folderComb.str() + string("Optimizer.txt")).c_str(), ios::out); if(f1) {f1 << optOptions << "\n"; f1.close();}

                msi->motherOverrideUsingComb(i);                                                    // chose the variables to simulate and to replace by data according to this combination
                msi->motherOptimize(folderComb.str() + string("Optimizer.txt"), 1000);              // DOES THE OPTIMIZATION !!!, and records the 1000 best sets

                msi->saveHistory(folderComb.str() + string("History.txt"));                         // SAVES all the best parameter sets. by default, 10 000, can be modified by             msi->history.resize(max_nb_sets_to_record);
                listGeneratedFilesSets.push_back(folderComb.str() + string("History.txt"));         // list[i] = historyFile for comb i

                msi->useParamSetFromHistory(0);                                                     // takes the first set of parameters (the best), also possible to use msi->useParamSetFromHistory(0, i); for overriding only parameters from this combination,
                msi->simulate();
                ofstream f2((folderComb.str() + string("FitnessBestSetOf") + codeSimu.str() + string(".txt")).c_str(), ios::out); if(f2) {f2 << currentExperiment->costReport() << "\n"; f2.close();}
                ofstream f3((folderComb.str() + string("CostEvolutionDuringOptimization.txt") + codeSimu.str()).c_str(), ios::out); if(f3) {f3 << msi->costRecords.print() << "\n"; f3.close();}

            }
        }
    }
    return 0;
}

