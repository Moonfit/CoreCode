// ------- Automatically generated model -------- //

#include "modeleLatentTbet2Hours.h"

// time factor of 1 = in seconds, 3600 = in hours
#define timeFactor 3600

/* additional needed mechanisms :
 * - Latency for transcription
 *      rorgt
 *      foxp3   (can be due to Foxp3 autoactivation)
 *      IL2
 *      but is clearly not true for all the genes (cf TGFb, socs ...)
 * OK
 * - latency for translation
 *      common to all factors, before 16 hrs
 *      will have an impact on IL2 and rorgt, but then also gata3 and tbet and IL21 (but then it's cool because gata3 is away)
 *      also on TGFb but we have no idea
 * - latency for secretion,
 *      can be only because of the number of cells or just because of latency of secretion
 *      SECRETION
 * - Additional interactions :
 *      to explain the rise of Tbet before,
 *          Tbet --> IL12 signaling (makes it a feedback to Tbet itself, inside the hill function of IL12)
 *          or Tbet --> Tbet (but would not work in the Th17 case, or because of limited translation)
 *          or IL2 --> Tbet
 *              IL12 n'explique pas pourquoi Tbet apparait chez Th0
 *          deux parties du promoteur : une partie TCR et STAT1 dépendante
 *          et une partie methylation et STAT5 dépendante
 *
 *          Wei 2011 , Gata3 binds in the middle of the gene, but induces histone methylation at the TSS and end of gene
 *          The CpGs that are demethylated in Th1 are located around the TSS Zhong 2014 (J Autoimmunity)
 *          And Stat5 binding sites as well Liao 2011 Nat Immunol
 *          DONE
 *      NOOOO IL2 activates Tbet is not true
 *
 *
 *      to explain the kinetics of IL2 mRNA,
 *          IL4 --| IL2 ref villarino NI 2007 and Schwarz JI 1993
 *          but does not explain the two waves of IL2. Might be due to TNFa or other ones,
 *      to explain IL21, which raises before rorgt,
 *          IL6 --> IL21 directly
 *          needs opening force as well
 *      to explain the second wave of IL4,
 *          could be IL13
 *      to explain IL17 goes down
 *          could be IL10
 *      to explain IL21 two phases
 *          IL6/IL1b ---> IL21
 *          (then the second phase comes with rorgr)
 *
 *  FAIRE marcher les blöocking antibodies
 *
 *
 *  Additional interactions :
 *
 *      TCR --> IFNg in the beginning OK
 *          --> low doses IFNg --> launches Tbet even if we don't see it
 *              then Tbet --> booste IFNg
 *      IL12 and Tbet synergistic for IFNg production. Tbet alone is sufficient but Gata3 inhibits
 *          Tbet required because
 *
 *
 *      IL12 activates IFNg required
 *
 *      Question : IL12 inhibits Gata3 or anti IL4 is responsible for it ?
 *
 *      Gata3 inhibits IFNg required for late IFNg differences not sure required
 *
 *      IFNg inhibits GATA3 (Ariga 2007 Immunology)
*/

// for a variable, put -1 not to be rescaled, or 1.0 will also work, just calculates for nothing.
// 0 values will be ignored (any negative or null value)
void modeleLatentTbet2Hours::postRescaleParameters(vector<double> coeffPerVariable, bool preserveOtherCurves, bool rescaleInit){

    if(coeffPerVariable.size() != static_cast<size_t>(getNbVars())){
        cerr << "ERR: modeleLatentTbet2Hours::postRescaleParameters, you gave a vector of " << coeffPerVariable.size() << " coefficients, instead of " << getNbVars() << " variables" << endl;
        return;
    }
    //bool rescaleInit = true;
    #define TellRescale true
//    template: for all variables that have experimental data:
//    if(coeffPerVariable[XXX] > 0){
//        double lambda = coeffPerVariable[XXX];
//        // 1 - multiplies all production terms by the coefficient
//        params[XXX] = lambda * params[XXX];
//        // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
//        params[PXXX] = params[PXXX] / lambda;
//        //or
//        params[KXXX] = lambda * params[KIL4_TO_IL2mRNA];
//        if(rescaleInit){ params[XXXEQ] = lambda * params[XXXEQ];}
//    }
//    Note: it does not change the initial values added by the experiment::simulate, but all the rest is rescaled perfectly

    // and for non-rescable varibles in this model:
    if(coeffPerVariable[TCR] > 0){
        // that would be only rescaling the init value.
        if(fabs(coeffPerVariable[TCR] - 1.0) > 1e-5) cerr << "ERR: impossible to rescale TCR in this model" << endl;
    }
    if(coeffPerVariable[IL2] > 0){
           double lambda = coeffPerVariable[IL2];
           if(TellRescale) cout << " Rescaling IL2 with coeff " << lambda << endl;
           // 1 - multiplies all production terms by the coefficient
           params[PIL2] = lambda * params[PIL2];
           //params[KDIL2] = lambda * params[KDIL2];

           if(preserveOtherCurves){
               // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
               params[KIL2_TO_IL2mRNA] = lambda * params[KIL2_TO_IL2mRNA];

               params[KIL2_TO_GATA3mRNA] = lambda * params[KIL2_TO_GATA3mRNA];
               params[KIL2_TO_FOXP3mRNA] = lambda * params[KIL2_TO_FOXP3mRNA];
               // not sure params[KANTI_IL2] =  params[KANTI_IL2] / lambda;
           }
           if(rescaleInit){
               params[IL2EQ] = lambda * params[IL2EQ];
               params[IL2INITTREG] = lambda * params[IL2INITTREG];
           }

    }
    if(coeffPerVariable[IL4] > 0){
           double lambda = coeffPerVariable[IL4];
           if(TellRescale) cout << " Rescaling IL4 with coeff " << lambda << endl;
           // 1 - multiplies all production terms by the coefficient
           params[PIL4] = lambda * params[PIL4];
           //params[KDIL4] = lambda * params[KDIL4];
           if(preserveOtherCurves){
               // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
               params[KIL4_TO_IL2mRNA] = lambda * params[KIL4_TO_IL2mRNA];
               params[KIL4_TO_GATA3mRNA] = lambda * params[KIL4_TO_GATA3mRNA];
               // not sure params[KANTI_IL4] = params[KANTI_IL4] / lambda;
           }
           if(rescaleInit){
               params[IL4EQ] = lambda * params[IL4EQ];
               params[IL4INITTH2] = lambda * params[IL4INITTH2];
           }
    }
    if(coeffPerVariable[IL17] > 0){
        double lambda = coeffPerVariable[IL17];
        if(TellRescale) cout << " Rescaling IL17 with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[PIL17] = lambda *  params[PIL17];
        //params[KDIL17] = lambda *  params[KDIL17];
        // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
        // No dependencies for IL17
        if(rescaleInit){ params[IL17EQ] = lambda * params[IL17EQ];}
    }
    if(coeffPerVariable[IL21] > 0){
        double lambda = coeffPerVariable[IL21];
        if(TellRescale) cout << " Rescaling IL21 with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[PIL21] = lambda * params[PIL21];
        //params[KDIL21] = lambda * params[KDIL21];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[KIL21_TO_RORGTmRNA] = lambda * params[KIL21_TO_RORGTmRNA];
        }
        if(rescaleInit){params[IL21EQ] = lambda * params[IL21EQ];}
    }
    if(coeffPerVariable[IFNG] > 0){
        double lambda = coeffPerVariable[IFNG];
        if(TellRescale) cout << " Rescaling IFNG with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[PIFNG] = lambda * params[PIFNG];
        //params[KDIFNG] = lambda * params[KDIFNG];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            // not sure, - params[KANTI_IFNG]*x[IFNG]
            // normally, to get the exact same curve, we should also double the anti IFNg, but it should be exceeding amoubts anyways,
            params[KIFNG_TO_TBETmRNA] = lambda * params[KIFNG_TO_TBETmRNA];
        }
        if(rescaleInit){ params[IFNGEQ] = lambda * params[IFNGEQ];}
    }
    if(coeffPerVariable[TGFB] > 0){
        double lambda = coeffPerVariable[TGFB];
        if(TellRescale) cout << " Rescaling TGFB with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[PTGFB] = lambda * params[PTGFB];
        //params[KDTGFB] = lambda * params[KDTGFB];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[KTGFB_TO_RORGTmRNA] = lambda * params[KTGFB_TO_RORGTmRNA];
            params[KTGFB_TO_FOXP3mRNA] = lambda * params[KTGFB_TO_FOXP3mRNA];
        }
        if(rescaleInit){ params[TGFBEQ] = lambda * params[TGFBEQ];}
    }
    if(coeffPerVariable[TBET] > 0){
        double lambda = coeffPerVariable[TBET];
        if(TellRescale) cout << " Rescaling TBET with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[PTBET] = lambda * params[PTBET];
        //params[KDTBET] = lambda * params[KDTBET];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[KTBET_TO_IFNGmRNA]  = lambda * params[KTBET_TO_IFNGmRNA];
            params[KTBET_TO_GATA3mRNA] = lambda * params[KTBET_TO_GATA3mRNA];
            params[KTBET_TO_RORGTmRNA] = lambda * params[KTBET_TO_RORGTmRNA];
            params[KTBET_TO_FOXP3mRNA] = lambda * params[KTBET_TO_FOXP3mRNA];
        }
        if(rescaleInit){ params[TBETEQ] = lambda * params[TBETEQ];}
    }
    if(coeffPerVariable[GATA3] > 0){
        double lambda = coeffPerVariable[GATA3];
        if(TellRescale) cout << " Rescaling GATA3 with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[PGATA3] = lambda * params[PGATA3];
        //params[KDGATA3] = lambda * params[KDGATA3];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[KGATA3_TO_IL4mRNA]   = lambda * params[KGATA3_TO_IL4mRNA];
            params[KGATA3_TO_GATA3mRNA] = lambda * params[KGATA3_TO_GATA3mRNA];
            params[KGATA3_TO_TBETmRNA]  = lambda * params[KGATA3_TO_TBETmRNA];
            params[KGATA3_TO_RORGTmRNA] = lambda * params[KGATA3_TO_RORGTmRNA];
            params[KGATA3_TO_FOXP3mRNA] = lambda * params[KGATA3_TO_FOXP3mRNA];
        }
        if(rescaleInit){ params[GATA3EQ] = lambda * params[GATA3EQ];}
    }
    if(coeffPerVariable[RORGT] > 0){
        double lambda = coeffPerVariable[RORGT];
        if(TellRescale) cout << " Rescaling RORGT with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[PRORGT] = lambda * params[PRORGT];
        //params[KDRORGT] = lambda * params[KDRORGT];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[KRORGT_TO_IL17mRNA]  = lambda * params[KRORGT_TO_IL17mRNA];
            params[KRORGT_TO_IL21mRNA]  = lambda * params[KRORGT_TO_IL21mRNA];
            params[KRORGT_TO_GATA3mRNA] = lambda * params[KRORGT_TO_GATA3mRNA];
            params[KRORGT_TO_TBETmRNA]  = lambda * params[KRORGT_TO_TBETmRNA];
            params[KRORGT_TO_FOXP3mRNA] = lambda * params[KRORGT_TO_FOXP3mRNA];
        }
        if(rescaleInit){ params[RORGTEQ] = lambda * params[RORGTEQ];}
    }
    if(coeffPerVariable[FOXP3] > 0){
        double lambda = coeffPerVariable[FOXP3];
        if(TellRescale) cout << " Rescaling FOXP3 with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[PFOXP3] = lambda * params[PFOXP3];
        //params[KDFOXP3] = lambda * params[KDFOXP3];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[KFOXP3_TO_GATA3mRNA] = lambda * params[KFOXP3_TO_GATA3mRNA];
            params[KFOXP3_TO_TBETmRNA] = lambda * params[KFOXP3_TO_TBETmRNA];
            params[KFOXP3_TO_RORGTmRNA] = lambda * params[KFOXP3_TO_RORGTmRNA];
        }
        if(rescaleInit){ params[FOXP3EQ] = lambda * params[FOXP3EQ];}
    }
    if(coeffPerVariable[IL2mRNA] > 0){
        double lambda = coeffPerVariable[IL2mRNA];
        if(TellRescale) cout << " Rescaling IL2mRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CIL2mRNA] = lambda * params[CIL2mRNA];
        //params[KDIL2mRNA] = lambda * params[KDIL2mRNA];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PIL2] = params[PIL2] / lambda;
        }
        if(rescaleInit){ params[IL2mRNAEQ] = lambda * params[IL2mRNAEQ];}
    }
    if(coeffPerVariable[IL4mRNA] > 0){
        double lambda = coeffPerVariable[IL4mRNA];
        if(TellRescale) cout << " Rescaling IL4mRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CIL4mRNA]  = lambda * params[CIL4mRNA] ;
        //params[KDIL4mRNA]  = lambda * params[KDIL4mRNA] ;
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PIL4] = params[PIL4] / lambda;
        }
        if(rescaleInit){ params[IL4mRNAEQ] = lambda * params[IL4mRNAEQ];}
    }

    if(coeffPerVariable[IL17mRNA] > 0){
        double lambda = coeffPerVariable[IL17mRNA];
        if(TellRescale) cout << " Rescaling IL17mRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CIL17mRNA] = lambda * params[CIL17mRNA];
        //params[KDIL17mRNA] = lambda * params[KDIL17mRNA];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PIL17] = params[PIL17] / lambda;
        }
        if(rescaleInit){ params[IL17mRNAEQ] = lambda * params[IL17mRNAEQ];}
    }

    if(coeffPerVariable[IL21mRNA] > 0){
        double lambda = coeffPerVariable[IL21mRNA];
        if(TellRescale) cout << " Rescaling IL21mRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CIL21mRNA] = lambda * params[CIL21mRNA];
        //params[KDIL21mRNA] = lambda * params[KDIL21mRNA];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PIL21] = params[PIL21] / lambda;
        }
        if(rescaleInit){ params[IL21mRNAEQ] = lambda * params[IL21mRNAEQ];}
    }

    if(coeffPerVariable[IFNGmRNA] > 0){
        double lambda = coeffPerVariable[IFNGmRNA];
        if(TellRescale) cout << " Rescaling IFNGmRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CIFNGmRNA]  = lambda * params[CIFNGmRNA];
        //params[KDIFNGmRNA]  = lambda * params[KDIFNGmRNA];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PIFNG] = params[PIFNG] / lambda;
        }
        if(rescaleInit){ params[IFNGmRNAEQ] = lambda * params[IFNGmRNAEQ];}
    }
    if(coeffPerVariable[TGFBmRNA] > 0){
        double lambda = coeffPerVariable[TGFBmRNA];
        if(TellRescale) cout << " Rescaling TGFBmRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CTGFBmRNA] = lambda * params[CTGFBmRNA] ;
        //params[KDTGFBmRNA] = lambda * params[KDTGFBmRNA] ;
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PTGFB] = params[PTGFB] / lambda;
        }
        if(rescaleInit){ params[TGFBmRNAEQ] = lambda * params[TGFBmRNAEQ];}
    }
    if(coeffPerVariable[TBETmRNA] > 0){
        double lambda = coeffPerVariable[TBETmRNA];
        if(TellRescale) cout << " Rescaling TBETmRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CTBETmRNA] = lambda * params[CTBETmRNA];
        //params[KDTBETmRNA] = lambda * params[KDTBETmRNA];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PTBET] = params[PTBET] / lambda;
        }
        if(rescaleInit){ params[TBETmRNAEQ] = lambda * params[TBETmRNAEQ];}
    }
    if(coeffPerVariable[GATA3mRNA] > 0){
        double lambda = coeffPerVariable[GATA3mRNA];
        if(TellRescale) cout << " Rescaling GATA3mRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CGATA3mRNA]  = lambda * params[CGATA3mRNA] ;
        //params[KDGATA3mRNA]  = lambda * params[KDGATA3mRNA] ;
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PGATA3] = params[PGATA3] / lambda;
        }
        if(rescaleInit){ params[GATA3mRNAEQ] = lambda * params[GATA3mRNAEQ];}
    }
    if(coeffPerVariable[RORGTmRNA] > 0){
        double lambda = coeffPerVariable[RORGTmRNA];
        if(TellRescale) cout << " Rescaling RORGTmRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CRORGTmRNA] = lambda * params[CRORGTmRNA];
        //params[KDRORGTmRNA] = lambda * params[KDRORGTmRNA];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PRORGT] = params[PRORGT] / lambda;
        }
        if(rescaleInit){ params[RORGTmRNAEQ] = lambda * params[RORGTmRNAEQ];}
    }
    if(coeffPerVariable[FOXP3mRNA] > 0){
        double lambda = coeffPerVariable[FOXP3mRNA];
        if(TellRescale) cout << " Rescaling FOXP3mRNA with coeff " << lambda << endl;
        // 1 - multiplies all production terms by the coefficient
        params[CFOXP3mRNA] = lambda * params[CFOXP3mRNA];
        //params[KDFOXP3mRNA] = lambda * params[KDFOXP3mRNA];
        if(preserveOtherCurves){
            // 2 - adapts all the dependencies to behave the same even if the variable is now rescaled
            params[PFOXP3] = params[PFOXP3] / lambda;
        }
        if(rescaleInit){ params[FOXP3mRNAEQ] = lambda * params[FOXP3mRNAEQ];}
    }

    vector<size_t> listNonRescalable = {IL6, IL12, antiIL4, antiIFNg, antiIL2, openIL2, openIL21, openFOXP3, openRORGT, openTBET, transl, secret};
    for(size_t i = 0; i < listNonRescalable.size(); ++i){
        if(coeffPerVariable[listNonRescalable[i]] > 0){
            if(fabs(coeffPerVariable[i] - 1.0) > 1e-5) cerr << "ERR: ModelLatentTbet2, rescale() not allowed to rescale " << i << getName(static_cast<int>(listNonRescalable[i])) << " in this model" << endl;
        }
    }
}



modeleLatentTbet2Hours::modeleLatentTbet2Hours() : Model(NBVAR, NBPARAM), background(Back::WT) {
    name = string("modeleLatentTbet2Hours");
    dt =10.0/timeFactor;
    print_every_dt = 2500/timeFactor;
	// Name of variables
	names[IL2] = string("IL2");
	names[IL4] = string("IL4");
	names[IL6] = string("IL6");			// measured : is constant
	names[IL12] = string("IL12");		
	names[IL17] = string("IL17");
	names[IL21] = string("IL21");
	names[IFNG] = string("IFNG");
	names[TGFB] = string("TGFB");
	names[TBET] = string("TBET");
	names[GATA3] = string("GATA3");
	names[RORGT] = string("RORGT");
	names[FOXP3] = string("FOXP3");
	names[IL2mRNA] = string("IL2mRNA");
	names[IL4mRNA] = string("IL4mRNA");
	names[IL17mRNA] = string("IL17mRNA");
	names[IL21mRNA] = string("IL21mRNA");
	names[IFNGmRNA] = string("IFNGmRNA");
	names[TGFBmRNA] = string("TGFBmRNA");
	names[TBETmRNA] = string("TBETmRNA");
	names[GATA3mRNA] = string("GATA3mRNA");
	names[RORGTmRNA] = string("RORGTmRNA");
	names[FOXP3mRNA] = string("FOXP3mRNA");
	names[TCR] = string("TCR");
    names[antiIL4] = string("antiIL4");
    names[antiIFNg] = string("antiIFNg");
    names[antiIL2] = string("antiIL2");
    names[openIL2] = string("openIL2");
    names[openIL21] = string("openIL21");
    names[openFOXP3] = string("openFOXP3");
    names[openRORGT] = string("openRORGT");
    names[openTBET] = string("openTBET");
    names[transl]    = string("transl");
    names[secret]    = string("secret");


	// IL1b is not explicitly simulated as redundant with IL6 (and present in the same condition)
	
	// the names of variables that can be accessed by outside (global name-space)
    extNames[IL2] = "IL2"; // GlobalName(N::IL2";
    extNames[IL4] = "IL4";
    extNames[IL6] = "IL6";
    extNames[IL12] = "IL12";
    extNames[IL17] = "IL17";
    extNames[IL21] = "IL21";
    extNames[IFNG] = "IFNG";
    extNames[TGFB] = "TGFB";
    extNames[TBET] = "TBET";
    extNames[GATA3] = "GATA3";
    extNames[RORGT] = "RORGT";
    extNames[FOXP3] = "FOXP3";
    extNames[IL2mRNA] = "IL2mRNA";
    extNames[IL4mRNA] = "IL4mRNA";
    extNames[IL17mRNA] = "IL17mRNA";
    extNames[IL21mRNA] = "IL21mRNA";
    extNames[IFNGmRNA] = "IFNGmRNA";
    extNames[TGFBmRNA] = "TGFBmRNA";
    extNames[TBETmRNA] = "TBETmRNA";
    extNames[GATA3mRNA] = "GATA3mRNA";
    extNames[RORGTmRNA] = "RORGTmRNA";
    extNames[FOXP3mRNA] = "FOXP3mRNA";
    extNames[TCR] = "TCR";
    extNames[antiIL4] = "antiIL4";
    extNames[antiIFNg] = "antiIFNg";
    extNames[antiIL2] = "antiIL2";
    extNames[openIL2] = "openIL2";
    extNames[openIL21] = "openIL21";
    extNames[openFOXP3] = "openFOXP3";
    extNames[openRORGT] = "openRORGT";
    extNames[openTBET] = "openTBET";
    extNames[transl]    = "transl";
    extNames[secret]    = "secret";

	// Name of parameters
    paramNames[TCRPEAK] 	= "TCRPEAK";
    paramNames[TCRCOEFF] 	= "TCRCOEFF";
    paramNames[KTCRGATA3] 	= "KTCRGATA3";
    paramNames[KTCRIL2] 	= "KTCRIL2";
    paramNames[KTCRGATA3POS]= "KTCRGATA3POS";
    paramNames[KTCRTGFB] 	= "KTCRTGFB";
    paramNames[KTCRTBET] 	= "KTCRTBET";
    paramNames[KTCRIFNG] 	= "KTCRIFNG";

	paramNames[KDIL2] = "KDIL2";
	paramNames[KDIL4] = "KDIL4";
	paramNames[KDIL12] = "KDIL12";		// can be set constant (not measured)
	paramNames[KDIL17] = "KDIL17";
	paramNames[KDIL21] = "KDIL21";
	paramNames[KDIFNG] = "KDIFNG";
	paramNames[KDTGFB] = "KDTGFB";
	paramNames[KDTBET] = "KDTBET";
	paramNames[KDGATA3] = "KDGATA3";
	paramNames[KDRORGT] = "KDRORGT";
	paramNames[KDFOXP3] = "KDFOXP3";
	paramNames[KDIL2mRNA] = "KDIL2mRNA";
	paramNames[KDIL4mRNA] = "KDIL4mRNA";
	paramNames[KDIL17mRNA] = "KDIL17mRNA";
	paramNames[KDIL21mRNA] = "KDIL21mRNA";
	paramNames[KDIFNGmRNA] = "KDIFNGmRNA";
	paramNames[KDTGFBmRNA] = "KDTGFBmRNA";
	paramNames[KDTBETmRNA] = "KDTBETmRNA";
	paramNames[KDGATA3mRNA] = "KDGATA3mRNA";
	paramNames[KDRORGTmRNA] = "KDRORGTmRNA";
	paramNames[KDFOXP3mRNA] = "KDFOXP3mRNA";
	paramNames[PIL2] = "PIL2";
	paramNames[PIL4] = "PIL4";
	paramNames[PIL17] = "PIL17";
	paramNames[PIL21] = "PIL21";
	paramNames[PIFNG] = "PIFNG";
	paramNames[PTGFB] = "PTGFB";
	paramNames[PTBET] = "PTBET";
	paramNames[PGATA3] = "PGATA3";
	paramNames[PRORGT] = "PRORGT";
	paramNames[PFOXP3] = "PFOXP3";
    paramNames[CTGFBmRNA] = "CTGFBmRNA";
	paramNames[CIL2mRNA] = "CIL2mRNA";
	paramNames[SIL2_TO_IL2mRNA] = "SIL2_TO_IL2mRNA";
	paramNames[KIL2_TO_IL2mRNA] = "KIL2_TO_IL2mRNA";
	paramNames[NIL2_TO_IL2mRNA] = "NIL2_TO_IL2mRNA";			// Inhib, can be put to 0
    paramNames[SIL4_TO_IL2mRNA] = "SIL4_TO_IL2mRNA";
    paramNames[KIL4_TO_IL2mRNA] = "KIL4_TO_IL2mRNA";
    paramNames[NIL4_TO_IL2mRNA] = "NIL4_TO_IL2mRNA";
	paramNames[CIL4mRNA] = "CIL4mRNA";
	paramNames[SGATA3_TO_IL4mRNA] = "SGATA3_TO_IL4mRNA";
	paramNames[KGATA3_TO_IL4mRNA] = "KGATA3_TO_IL4mRNA";
	paramNames[NGATA3_TO_IL4mRNA] = "NGATA3_TO_IL4mRNA";
	paramNames[CIL17mRNA] = "CIL17mRNA";
	paramNames[SRORGT_TO_IL17mRNA] = "SRORGT_TO_IL17mRNA";
	paramNames[KRORGT_TO_IL17mRNA] = "KRORGT_TO_IL17mRNA";
	paramNames[NRORGT_TO_IL17mRNA] = "NRORGT_TO_IL17mRNA";
	paramNames[CIL21mRNA] = "CIL21mRNA";
    paramNames[FIL6_TO_IL21mRNA] = "FIL6_TO_IL21mRNA";
	paramNames[SRORGT_TO_IL21mRNA] = "SRORGT_TO_IL21mRNA";
	paramNames[KRORGT_TO_IL21mRNA] = "KRORGT_TO_IL21mRNA";
	paramNames[NRORGT_TO_IL21mRNA] = "NRORGT_TO_IL21mRNA";
	paramNames[CIFNGmRNA] = "CIFNGmRNA";
    paramNames[FIL12_TO_IFNGmRNA] = "FIL12_TO_IFNGmRNA";

	paramNames[STBET_TO_IFNGmRNA] = "STBET_TO_IFNGmRNA";
	paramNames[KTBET_TO_IFNGmRNA] = "KTBET_TO_IFNGmRNA";
	paramNames[NTBET_TO_IFNGmRNA] = "NTBET_TO_IFNGmRNA";
	paramNames[CTBETmRNA] = "CTBETmRNA";
    paramNames[FIL6_TO_TBETmRNA] = "FIL6_TO_TBETmRNA";
    //paramNames[CIL2_TBETmRNA] = "CIL2_TBETmRNA";
    //paramNames[SIL2_TO_TBETmRNA] = "SIL2_TO_TBETmRNA";
    //paramNames[KIL2_TO_TBETmRNA] = "KIL2_TO_TBETmRNA";
    //paramNames[NIL2_TO_TBETmRNA] = "NIL2_TO_TBETmRNA";
	paramNames[SIL12_TO_TBETmRNA] = "SIL12_TO_TBETmRNA";		// (i.e IL12 = 0 -> 1; IL12 = 10 ng/mL -> SIL12_TO_TBETmRNA)
	paramNames[KIL12_TO_TBETmRNA] = "KIL12_TO_TBETmRNA";		// Useless now -> K = 5 
	paramNames[NIL12_TO_TBETmRNA] = "NIL12_TO_TBETmRNA";		// Useless now -> N = 5
	paramNames[SIFNG_TO_TBETmRNA] = "SIFNG_TO_TBETmRNA";
	paramNames[KIFNG_TO_TBETmRNA] = "KIFNG_TO_TBETmRNA";
	paramNames[NIFNG_TO_TBETmRNA] = "NIFNG_TO_TBETmRNA";
	paramNames[SGATA3_TO_TBETmRNA] = "SGATA3_TO_TBETmRNA";		// Inhib, can be put to 0
	paramNames[KGATA3_TO_TBETmRNA] = "KGATA3_TO_TBETmRNA";
	paramNames[NGATA3_TO_TBETmRNA] = "NGATA3_TO_TBETmRNA";		
	paramNames[SRORGT_TO_TBETmRNA] = "SRORGT_TO_TBETmRNA";		// Inhib, can be put to 0
	paramNames[KRORGT_TO_TBETmRNA] = "KRORGT_TO_TBETmRNA";
	paramNames[NRORGT_TO_TBETmRNA] = "NRORGT_TO_TBETmRNA";
	paramNames[SFOXP3_TO_TBETmRNA] = "SFOXP3_TO_TBETmRNA";		// Inhib, can be put to 0
	paramNames[KFOXP3_TO_TBETmRNA] = "KFOXP3_TO_TBETmRNA";
	paramNames[NFOXP3_TO_TBETmRNA] = "NFOXP3_TO_TBETmRNA";
	paramNames[CGATA3mRNA] = "CGATA3mRNA";
    paramNames[FIL12_TO_GATA3mRNA] = "FIL12_TO_GATA3mRNA";

    paramNames[SIL2_TO_GATA3mRNA] = "SIL2_TO_GATA3mRNA";
	paramNames[KIL2_TO_GATA3mRNA] = "KIL2_TO_GATA3mRNA";
	paramNames[NIL2_TO_GATA3mRNA] = "NIL2_TO_GATA3mRNA";
	paramNames[SIL4_TO_GATA3mRNA] = "SIL4_TO_GATA3mRNA";
	paramNames[KIL4_TO_GATA3mRNA] = "KIL4_TO_GATA3mRNA";
    paramNames[NIL4_TO_GATA3mRNA] = "NIL4_TO_GATA3mRNA";
    paramNames[SIFNG_TO_GATA3mRNA] = "SIFNG_TO_GATA3mRNA";
    paramNames[KIFNG_TO_GATA3mRNA] = "KIFNG_TO_GATA3mRNA";
    paramNames[NIFNG_TO_GATA3mRNA] = "NIFNG_TO_GATA3mRNA";


	paramNames[STBET_TO_GATA3mRNA] = "STBET_TO_GATA3mRNA";		// Inhib, can be put to 0
	paramNames[KTBET_TO_GATA3mRNA] = "KTBET_TO_GATA3mRNA";
	paramNames[NTBET_TO_GATA3mRNA] = "NTBET_TO_GATA3mRNA";
	paramNames[SGATA3_TO_GATA3mRNA] = "SGATA3_TO_GATA3mRNA";		
	paramNames[KGATA3_TO_GATA3mRNA] = "KGATA3_TO_GATA3mRNA";
	paramNames[NGATA3_TO_GATA3mRNA] = "NGATA3_TO_GATA3mRNA";
	paramNames[SRORGT_TO_GATA3mRNA] = "SRORGT_TO_GATA3mRNA";	// Inhib, can be put to 0
	paramNames[KRORGT_TO_GATA3mRNA] = "KRORGT_TO_GATA3mRNA";
	paramNames[NRORGT_TO_GATA3mRNA] = "NRORGT_TO_GATA3mRNA";
	paramNames[SFOXP3_TO_GATA3mRNA] = "SFOXP3_TO_GATA3mRNA";	// Inhib, can be put to 0
	paramNames[KFOXP3_TO_GATA3mRNA] = "KFOXP3_TO_GATA3mRNA";
	paramNames[NFOXP3_TO_GATA3mRNA] = "NFOXP3_TO_GATA3mRNA";
	paramNames[CRORGTmRNA] = "CRORGTmRNA";
    paramNames[FIL6_TO_RORGTmRNA]="FIL6_TO_RORGTmRNA";
	paramNames[SIL21_TO_RORGTmRNA] = "SIL21_TO_RORGTmRNA";
	paramNames[KIL21_TO_RORGTmRNA] = "KIL21_TO_RORGTmRNA";
	paramNames[NIL21_TO_RORGTmRNA] = "NIL21_TO_RORGTmRNA";
	paramNames[STGFB_TO_RORGTmRNA] = "STGFB_TO_RORGTmRNA";
	paramNames[KTGFB_TO_RORGTmRNA] = "KTGFB_TO_RORGTmRNA";
	paramNames[NTGFB_TO_RORGTmRNA] = "NTGFB_TO_RORGTmRNA";
	paramNames[STBET_TO_RORGTmRNA] = "STBET_TO_RORGTmRNA";		// Inhib, can be put to 0
	paramNames[KTBET_TO_RORGTmRNA] = "KTBET_TO_RORGTmRNA";
	paramNames[NTBET_TO_RORGTmRNA] = "NTBET_TO_RORGTmRNA";
	paramNames[SGATA3_TO_RORGTmRNA] = "SGATA3_TO_RORGTmRNA";	// Inhib, can be put to 0
	paramNames[KGATA3_TO_RORGTmRNA] = "KGATA3_TO_RORGTmRNA";
	paramNames[NGATA3_TO_RORGTmRNA] = "NGATA3_TO_RORGTmRNA";
	paramNames[SFOXP3_TO_RORGTmRNA] = "SFOXP3_TO_RORGTmRNA";	// Inhib, can be put to 0
	paramNames[KFOXP3_TO_RORGTmRNA] = "KFOXP3_TO_RORGTmRNA";
	paramNames[NFOXP3_TO_RORGTmRNA] = "NFOXP3_TO_RORGTmRNA";
	paramNames[CFOXP3mRNA] = "CFOXP3mRNA";
	paramNames[SIL2_TO_FOXP3mRNA] = "SIL2_TO_FOXP3mRNA";
	paramNames[KIL2_TO_FOXP3mRNA] = "KIL2_TO_FOXP3mRNA";
	paramNames[NIL2_TO_FOXP3mRNA] = "NIL2_TO_FOXP3mRNA";
	paramNames[STGFB_TO_FOXP3mRNA] = "STGFB_TO_FOXP3mRNA";
	paramNames[KTGFB_TO_FOXP3mRNA] = "KTGFB_TO_FOXP3mRNA";
	paramNames[NTGFB_TO_FOXP3mRNA] = "NTGFB_TO_FOXP3mRNA";
	paramNames[STBET_TO_FOXP3mRNA] = "STBET_TO_FOXP3mRNA";		// Inhib, can be put to 0
	paramNames[KTBET_TO_FOXP3mRNA] = "KTBET_TO_FOXP3mRNA";
	paramNames[NTBET_TO_FOXP3mRNA] = "NTBET_TO_FOXP3mRNA";
	paramNames[SGATA3_TO_FOXP3mRNA] = "SGATA3_TO_FOXP3mRNA";	// Inhib, can be put to 0
	paramNames[KGATA3_TO_FOXP3mRNA] = "KGATA3_TO_FOXP3mRNA";
	paramNames[NGATA3_TO_FOXP3mRNA] = "NGATA3_TO_FOXP3mRNA";
	paramNames[SRORGT_TO_FOXP3mRNA] = "SRORGT_TO_FOXP3mRNA";	// Inhib, can be put to 0
	paramNames[KRORGT_TO_FOXP3mRNA] = "KRORGT_TO_FOXP3mRNA";
	paramNames[NRORGT_TO_FOXP3mRNA] = "NRORGT_TO_FOXP3mRNA";

	// desired steady state at t=0, fixed from data
	paramNames[IL2EQ] = "IL2EQ";
	paramNames[IL4EQ] = "IL4EQ";
    paramNames[IL12EQ] = "IL12EQ";
	paramNames[IL17EQ] = "IL17EQ";
	paramNames[IL21EQ] = "IL21EQ";
	paramNames[IFNGEQ] = "IFNGEQ";
	paramNames[TGFBEQ] = "TGFBEQ";
	paramNames[TBETEQ] = "TBETEQ";
	paramNames[GATA3EQ] = "GATA3EQ";
	paramNames[RORGTEQ] = "RORGTEQ";
	paramNames[FOXP3EQ] = "FOXP3EQ";
	paramNames[IL2mRNAEQ] = "IL2mRNAEQ";
	paramNames[IL4mRNAEQ] = "IL4mRNAEQ";
	paramNames[IL17mRNAEQ] = "IL17mRNAEQ";
	paramNames[IL21mRNAEQ] = "IL21mRNAEQ";
	paramNames[IFNGmRNAEQ] = "IFNGmRNAEQ";
	paramNames[TGFBmRNAEQ] = "TGFBmRNAEQ";
	paramNames[TBETmRNAEQ] = "TBETmRNAEQ";
	paramNames[GATA3mRNAEQ] = "GATA3mRNAEQ";
	paramNames[RORGTmRNAEQ] = "RORGTmRNAEQ";
	paramNames[FOXP3mRNAEQ] = "FOXP3mRNAEQ";
    paramNames[IL4INITTH2] = "IL4INITTH2";
    paramNames[IL2INITTREG] = "IL2INITTREG";
    paramNames[FORCEIL2]    = "FORCEIL2";
    paramNames[FORCEIL21]    = "FORCEIL21";
    paramNames[FORCEFOXP3]  = "FORCEFOXP3";
    paramNames[FORCERORGT]  = "FORCERORGT";
    paramNames[RESCALE_FACTOR]  = "PlayRescale";
    paramNames[FORCETRANSL]  = "FORCETRANSL";
    paramNames[FORCESECRET]  = "FORCESECRET";
    paramNames[KANTI_IL2]           = "KANTI_IL2";
    paramNames[KANTI_IFNG]          = "KANTI_IFNG";
    paramNames[KANTI_IL4]           = "KANTI_IL4";



    paramLowBounds[TCRPEAK]     = 1;            paramUpBounds[TCRPEAK] 	= 10;
    paramLowBounds[TCRCOEFF]    = 1e-4;         paramUpBounds[TCRCOEFF] = 0.2;
    paramLowBounds[KTCRGATA3]   = 100;        	paramUpBounds[KTCRGATA3] = 10000;
    paramLowBounds[KTCRIL2] 	= 1.1;          paramUpBounds[KTCRIL2]		= 10000;
    paramLowBounds[KTCRGATA3POS]= 100;          paramUpBounds[KTCRGATA3POS]= 10000;
    paramLowBounds[KTCRTGFB] 	= 1.1;          paramUpBounds[KTCRTGFB] 	= 10000;
    paramLowBounds[KTCRTBET] 	= 1.1;          paramUpBounds[KTCRTBET]	= 20000;
    paramLowBounds[KTCRIFNG] 	= 1.1;          paramUpBounds[KTCRIFNG]	= 20000;


    paramLowBounds[KDIL2]       = 1e-006*timeFactor;		paramUpBounds[KDIL2] 	= 1e-4*timeFactor;
    paramLowBounds[KDIL4]       = 1e-006*timeFactor;		paramUpBounds[KDIL4] 	= 1e-4*timeFactor;
    paramLowBounds[KDIL12]      = 1e-6*timeFactor;         paramUpBounds[KDIL12] 	= 1e-4*timeFactor;
    paramLowBounds[KDIL17]      = 1e-6*timeFactor;         paramUpBounds[KDIL17] 	= 1e-4*timeFactor;
    paramLowBounds[KDIL21]      = 1e-007*timeFactor;		paramUpBounds[KDIL21] 	= 1e-4*timeFactor;
    paramLowBounds[KDIFNG]      = 1e-007*timeFactor;		paramUpBounds[KDIFNG] 	= 1e-4*timeFactor;
    paramLowBounds[KDTGFB]      = 1e-6*timeFactor;         paramUpBounds[KDTGFB] 	= 1e-4*timeFactor;
    paramLowBounds[KDTBET]      = 1e-5*timeFactor;         paramUpBounds[KDTBET] 	= 1e-4*timeFactor;
    paramLowBounds[KDGATA3] 	= 1e-5*timeFactor;         paramUpBounds[KDGATA3] 	= 1e-4*timeFactor;
    paramLowBounds[KDRORGT] 	= 1e-5*timeFactor;         paramUpBounds[KDRORGT] 	= 1e-4*timeFactor;
    paramLowBounds[KDFOXP3] 	= 1e-5*timeFactor;         paramUpBounds[KDFOXP3] 	= 1e-4*timeFactor;
    paramLowBounds[KDIL2mRNA] 	= 2e-6*timeFactor;         paramUpBounds[KDIL2mRNA] 	= 5e-5*timeFactor;
    paramLowBounds[KDIL4mRNA] 	= 2e-6*timeFactor;         paramUpBounds[KDIL4mRNA] 	= 5e-5*timeFactor;
    paramLowBounds[KDIL17mRNA] 	= 2e-6*timeFactor;         paramUpBounds[KDIL17mRNA] 	= 5e-5*timeFactor;
    paramLowBounds[KDIL21mRNA] 	= 2e-6*timeFactor;         paramUpBounds[KDIL21mRNA] 	= 5e-5*timeFactor;
    paramLowBounds[KDIFNGmRNA] 	= 2e-6*timeFactor;         paramUpBounds[KDIFNGmRNA] 	= 5e-5*timeFactor;
    paramLowBounds[KDTGFBmRNA] 	= 2e-6*timeFactor;         paramUpBounds[KDTGFBmRNA] 	= 5e-5*timeFactor;
    paramLowBounds[KDTBETmRNA] 	= 2e-6*timeFactor;         paramUpBounds[KDTBETmRNA] 	= 5e-5*timeFactor;
    paramLowBounds[KDGATA3mRNA] = 2e-6*timeFactor;         paramUpBounds[KDGATA3mRNA] 	= 5e-5*timeFactor;
    paramLowBounds[KDRORGTmRNA] = 2e-6*timeFactor;         paramUpBounds[KDRORGTmRNA] 	= 5e-5*timeFactor;
    paramLowBounds[KDFOXP3mRNA] = 2e-6*timeFactor;         paramUpBounds[KDFOXP3mRNA] 	= 5e-5*timeFactor;
    paramLowBounds[IL2EQ] 	= 0.00001;          paramUpBounds[IL2EQ] 	= 0.25;
    paramLowBounds[IL4EQ] 	= 0.00001;          paramUpBounds[IL4EQ] 	= 0.25;
    paramLowBounds[IL12EQ] 	= 0.00001;          paramUpBounds[IL12EQ] 	= 0.25;
    paramLowBounds[IL17EQ] 	= 0.00001;          paramUpBounds[IL17EQ] 	= 0.25;
    paramLowBounds[IL21EQ] 	= 0.00001;          paramUpBounds[IL21EQ] 	= 0.25;
    paramLowBounds[IFNGEQ] 	= 0.00001;          paramUpBounds[IFNGEQ] 	= 0.25;
    paramLowBounds[TGFBEQ] 	= 0.00001;          paramUpBounds[TGFBEQ] 	= 0.25;
    paramLowBounds[TBETEQ] 	= 0.00001;          paramUpBounds[TBETEQ] 	= 0.25;
    paramLowBounds[GATA3EQ] 	= 0.416;		paramUpBounds[GATA3EQ] 	= 0.25;
    paramLowBounds[RORGTEQ] 	= 1e-6;         paramUpBounds[RORGTEQ] 	= 0.25;
    paramLowBounds[FOXP3EQ] 	= 0.04;         paramUpBounds[FOXP3EQ] 	= 0.25;
    paramLowBounds[IL2mRNAEQ] 	= 0.0094;		paramUpBounds[IL2mRNAEQ] 	= 0.25;
    paramLowBounds[IL4mRNAEQ] 	= 0.0165;		paramUpBounds[IL4mRNAEQ] 	= 0.25;
    paramLowBounds[IL17mRNAEQ] 	= 0.00016;		paramUpBounds[IL17mRNAEQ] 	= 0.25;
    paramLowBounds[IL21mRNAEQ] 	= 0.034;		paramUpBounds[IL21mRNAEQ] 	= 0.25;
    paramLowBounds[IFNGmRNAEQ] 	= 0.041;		paramUpBounds[IFNGmRNAEQ] 	= 0.25;
    paramLowBounds[TGFBmRNAEQ] 	= 0.97;         paramUpBounds[TGFBmRNAEQ] 	= 0.25;
    paramLowBounds[TBETmRNAEQ] 	= 0.015;		paramUpBounds[TBETmRNAEQ] 	= 0.25;
    paramLowBounds[GATA3mRNAEQ] = 1.36;         paramUpBounds[GATA3mRNAEQ] 	= 0.25;
    paramLowBounds[RORGTmRNAEQ] = 0.032;		paramUpBounds[RORGTmRNAEQ] 	= 0.25;
    paramLowBounds[FOXP3mRNAEQ]	= 0.213;        paramUpBounds[FOXP3mRNAEQ] 	= 0.25;
    paramLowBounds[CTGFBmRNA]           = 1e-005*timeFactor;		paramUpBounds[CTGFBmRNA] 	= 1*timeFactor;
    paramLowBounds[CIL2mRNA]            = 1e-005*timeFactor;		paramUpBounds[CIL2mRNA] 	= 1*timeFactor;
    paramLowBounds[SIL2_TO_IL2mRNA] 	= 1e-6;         paramUpBounds[SIL2_TO_IL2mRNA] 	= 0.9;
    paramLowBounds[KIL2_TO_IL2mRNA] 	= 0.01;         paramUpBounds[KIL2_TO_IL2mRNA] 	= 50;
    paramLowBounds[NIL2_TO_IL2mRNA] 	= 0.25;         paramUpBounds[NIL2_TO_IL2mRNA] 	= 5;
    paramLowBounds[SIL4_TO_IL2mRNA]     = 1e-6;         paramUpBounds[SIL4_TO_IL2mRNA] = 0.9;
    paramLowBounds[KIL4_TO_IL2mRNA]     = 1;            paramUpBounds[KIL4_TO_IL2mRNA] = 20;
    paramLowBounds[NIL4_TO_IL2mRNA]     = 0.25;         paramUpBounds[NIL4_TO_IL2mRNA] = 5;
    paramLowBounds[CIL4mRNA]            = 1e-005*timeFactor;		paramUpBounds[CIL4mRNA]     = 1*timeFactor;
    paramLowBounds[SGATA3_TO_IL4mRNA] 	= 1.1;          paramUpBounds[SGATA3_TO_IL4mRNA] 	= 50;
    paramLowBounds[KGATA3_TO_IL4mRNA] 	= 0.01;         paramUpBounds[KGATA3_TO_IL4mRNA] 	= 10;
    paramLowBounds[NGATA3_TO_IL4mRNA] 	= 0.25;         paramUpBounds[NGATA3_TO_IL4mRNA] 	= 5;
    paramLowBounds[CIL17mRNA]           = 1e-005*timeFactor;		paramUpBounds[CIL17mRNA] 	= 1*timeFactor;
    paramLowBounds[SRORGT_TO_IL17mRNA] 	= 1.1;          paramUpBounds[SRORGT_TO_IL17mRNA] 	= 50;
    paramLowBounds[KRORGT_TO_IL17mRNA] 	= 0.01;         paramUpBounds[KRORGT_TO_IL17mRNA] 	= 10;
    paramLowBounds[NRORGT_TO_IL17mRNA] 	= 0.25;         paramUpBounds[NRORGT_TO_IL17mRNA] 	= 5;
    paramLowBounds[CIL21mRNA]           = 1e-005*timeFactor;		paramUpBounds[CIL21mRNA] 	= 1*timeFactor;
    paramLowBounds[FIL6_TO_IL21mRNA]    = 1.1;          paramUpBounds[FIL6_TO_IL21mRNA]     = 50;
    paramLowBounds[SRORGT_TO_IL21mRNA] 	= 1.1;          paramUpBounds[SRORGT_TO_IL21mRNA] 	= 50;
    paramLowBounds[KRORGT_TO_IL21mRNA] 	= 0.01;         paramUpBounds[KRORGT_TO_IL21mRNA] 	= 10;
    paramLowBounds[NRORGT_TO_IL21mRNA] 	= 0.25;         paramUpBounds[NRORGT_TO_IL21mRNA] 	= 5;
    paramLowBounds[CIFNGmRNA]           = 1e-005*timeFactor;		paramUpBounds[CIFNGmRNA] 	= 1*timeFactor;
    paramLowBounds[FIL12_TO_IFNGmRNA]   = 0.1;          paramUpBounds[FIL12_TO_IFNGmRNA] 	= 100;

    paramLowBounds[STBET_TO_IFNGmRNA] 	= 1.1;          paramUpBounds[STBET_TO_IFNGmRNA] 	= 50;
    paramLowBounds[KTBET_TO_IFNGmRNA] 	= 0.01;         paramUpBounds[KTBET_TO_IFNGmRNA] 	= 10;
    paramLowBounds[NTBET_TO_IFNGmRNA] 	= 0.25;         paramUpBounds[NTBET_TO_IFNGmRNA] 	= 5;
    paramLowBounds[CTBETmRNA]           = 1e-005*timeFactor;		paramUpBounds[CTBETmRNA] 	= 1*timeFactor;
    paramLowBounds[FIL6_TO_TBETmRNA] 	= 1.1;          paramUpBounds[FIL6_TO_TBETmRNA] 	= 50;
    //paramLowBounds[CIL2_TBETmRNA]       = 1e-5;         paramUpBounds[CIL2_TBETmRNA]= 1;
    //paramLowBounds[SIL2_TO_TBETmRNA] 	= 1.1;          paramUpBounds[SIL2_TO_TBETmRNA] 	= 50;
    //paramLowBounds[KIL2_TO_TBETmRNA] 	= 0.01;         paramUpBounds[KIL2_TO_TBETmRNA] 	= 50;
    //paramLowBounds[NIL2_TO_TBETmRNA] 	= 0.25;         paramUpBounds[NIL2_TO_TBETmRNA] 	= 5;
    paramLowBounds[SIL12_TO_TBETmRNA] 	= 1.1;          paramUpBounds[SIL12_TO_TBETmRNA] 	= 50;
    paramLowBounds[KIL12_TO_TBETmRNA] 	= 0.01;         paramUpBounds[KIL12_TO_TBETmRNA] 	= 10;
    paramLowBounds[NIL12_TO_TBETmRNA] 	= 0.25;         paramUpBounds[NIL12_TO_TBETmRNA] 	= 5;
    paramLowBounds[SIFNG_TO_TBETmRNA] 	= 1.1;          paramUpBounds[SIFNG_TO_TBETmRNA] 	= 50;
    paramLowBounds[KIFNG_TO_TBETmRNA] 	= 0.01;         paramUpBounds[KIFNG_TO_TBETmRNA] 	= 100;
    paramLowBounds[NIFNG_TO_TBETmRNA] 	= 0.25;         paramUpBounds[NIFNG_TO_TBETmRNA] 	= 5;
    paramLowBounds[SGATA3_TO_TBETmRNA] 	= 1e-006;		paramUpBounds[SGATA3_TO_TBETmRNA] 	= 0.9;
    paramLowBounds[KGATA3_TO_TBETmRNA] 	= 0.01;         paramUpBounds[KGATA3_TO_TBETmRNA] 	= 10;
    paramLowBounds[NGATA3_TO_TBETmRNA] 	= 0.25;         paramUpBounds[NGATA3_TO_TBETmRNA] 	= 5;
    paramLowBounds[SRORGT_TO_TBETmRNA] 	= 1e-006;		paramUpBounds[SRORGT_TO_TBETmRNA] 	= 0.9;
    paramLowBounds[KRORGT_TO_TBETmRNA] 	= 0.01;         paramUpBounds[KRORGT_TO_TBETmRNA] 	= 10;
    paramLowBounds[NRORGT_TO_TBETmRNA] 	= 0.25;         paramUpBounds[NRORGT_TO_TBETmRNA] 	= 5;
    paramLowBounds[SFOXP3_TO_TBETmRNA] 	= 1e-006;		paramUpBounds[SFOXP3_TO_TBETmRNA] 	= 0.9;
    paramLowBounds[KFOXP3_TO_TBETmRNA] 	= 0.01;         paramUpBounds[KFOXP3_TO_TBETmRNA] 	= 10;
    paramLowBounds[NFOXP3_TO_TBETmRNA] 	= 0.25;         paramUpBounds[NFOXP3_TO_TBETmRNA] 	= 5;
    paramLowBounds[CGATA3mRNA]          = 1e-005*timeFactor;		paramUpBounds[CGATA3mRNA] 	= 1*timeFactor;
    paramLowBounds[FIL12_TO_GATA3mRNA]  = 0.0001;		paramUpBounds[FIL12_TO_GATA3mRNA] 	= 1;

    paramLowBounds[SIL2_TO_GATA3mRNA] 	= 1.1;          paramUpBounds[SIL2_TO_GATA3mRNA] 	= 50;
    paramLowBounds[KIL2_TO_GATA3mRNA] 	= 0.01;         paramUpBounds[KIL2_TO_GATA3mRNA] 	= 100;
    paramLowBounds[NIL2_TO_GATA3mRNA] 	= 0.25;         paramUpBounds[NIL2_TO_GATA3mRNA] 	= 5;
    paramLowBounds[SIL4_TO_GATA3mRNA] 	= 1.1;          paramUpBounds[SIL4_TO_GATA3mRNA] 	= 50;
    paramLowBounds[KIL4_TO_GATA3mRNA] 	= 0.01;         paramUpBounds[KIL4_TO_GATA3mRNA] 	= 20;
    paramLowBounds[NIL4_TO_GATA3mRNA] 	= 0.25;         paramUpBounds[NIL4_TO_GATA3mRNA] 	= 5;
    paramLowBounds[SIFNG_TO_GATA3mRNA] 	= 1.1;          paramUpBounds[SIFNG_TO_GATA3mRNA] 	= 50;
    paramLowBounds[KIFNG_TO_GATA3mRNA] 	= 0.01;         paramUpBounds[KIFNG_TO_GATA3mRNA] 	= 20;
    paramLowBounds[NIFNG_TO_GATA3mRNA] 	= 0.25;         paramUpBounds[NIFNG_TO_GATA3mRNA] 	= 5;

	paramLowBounds[STBET_TO_GATA3mRNA] 	= 1e-006;		paramUpBounds[STBET_TO_GATA3mRNA] 	= 0.9;
    paramLowBounds[KTBET_TO_GATA3mRNA] 	= 0.01;         paramUpBounds[KTBET_TO_GATA3mRNA] 	= 10;
    paramLowBounds[NTBET_TO_GATA3mRNA] 	= 0.25;         paramUpBounds[NTBET_TO_GATA3mRNA] 	= 5;
    paramLowBounds[SGATA3_TO_GATA3mRNA] = 1.1;          paramUpBounds[SGATA3_TO_GATA3mRNA] 	= 50;
    paramLowBounds[KGATA3_TO_GATA3mRNA] = 0.01;         paramUpBounds[KGATA3_TO_GATA3mRNA] 	= 10;
    paramLowBounds[NGATA3_TO_GATA3mRNA] = 0.25;         paramUpBounds[NGATA3_TO_GATA3mRNA] 	= 5;
    paramLowBounds[SRORGT_TO_GATA3mRNA] = 1e-006;		paramUpBounds[SRORGT_TO_GATA3mRNA] 	= 0.9;
    paramLowBounds[KRORGT_TO_GATA3mRNA] = 0.01;         paramUpBounds[KRORGT_TO_GATA3mRNA] 	= 10;
    paramLowBounds[NRORGT_TO_GATA3mRNA] = 0.25;         paramUpBounds[NRORGT_TO_GATA3mRNA] 	= 5;
    paramLowBounds[SFOXP3_TO_GATA3mRNA] = 1e-006;		paramUpBounds[SFOXP3_TO_GATA3mRNA] 	= 0.9;
    paramLowBounds[KFOXP3_TO_GATA3mRNA] = 0.01;         paramUpBounds[KFOXP3_TO_GATA3mRNA] 	= 10;
    paramLowBounds[NFOXP3_TO_GATA3mRNA] = 0.25;         paramUpBounds[NFOXP3_TO_GATA3mRNA] 	= 5;
    paramLowBounds[CRORGTmRNA] 	= 1e-005*timeFactor;               paramUpBounds[CRORGTmRNA] 	= 1*timeFactor;
    paramLowBounds[FIL6_TO_RORGTmRNA] 	= 1.1;          paramUpBounds[FIL6_TO_RORGTmRNA] 	= 50;
    paramLowBounds[SIL21_TO_RORGTmRNA] 	= 1.1;          paramUpBounds[SIL21_TO_RORGTmRNA] 	= 50;
    paramLowBounds[KIL21_TO_RORGTmRNA] 	= 0.01;         paramUpBounds[KIL21_TO_RORGTmRNA] 	= 5;
    paramLowBounds[NIL21_TO_RORGTmRNA] 	= 0.25;         paramUpBounds[NIL21_TO_RORGTmRNA] 	= 5;
    paramLowBounds[STGFB_TO_RORGTmRNA] 	= 1.1;          paramUpBounds[STGFB_TO_RORGTmRNA] 	= 50;
    paramLowBounds[KTGFB_TO_RORGTmRNA] 	= 0.01;         paramUpBounds[KTGFB_TO_RORGTmRNA] 	= 1;
    paramLowBounds[NTGFB_TO_RORGTmRNA] 	= 0.25;         paramUpBounds[NTGFB_TO_RORGTmRNA] 	= 5;
	paramLowBounds[STBET_TO_RORGTmRNA] 	= 1e-006;		paramUpBounds[STBET_TO_RORGTmRNA] 	= 0.9;
    paramLowBounds[KTBET_TO_RORGTmRNA] 	= 0.01;         paramUpBounds[KTBET_TO_RORGTmRNA] 	= 10;
    paramLowBounds[NTBET_TO_RORGTmRNA] 	= 0.25;         paramUpBounds[NTBET_TO_RORGTmRNA] 	= 5;
    paramLowBounds[SGATA3_TO_RORGTmRNA] = 1e-006;		paramUpBounds[SGATA3_TO_RORGTmRNA] 	= 0.9;
    paramLowBounds[KGATA3_TO_RORGTmRNA] = 0.01;         paramUpBounds[KGATA3_TO_RORGTmRNA] 	= 10;
    paramLowBounds[NGATA3_TO_RORGTmRNA] = 0.25;         paramUpBounds[NGATA3_TO_RORGTmRNA] 	= 5;
    paramLowBounds[SFOXP3_TO_RORGTmRNA] = 1e-006;		paramUpBounds[SFOXP3_TO_RORGTmRNA] 	= 0.9;
    paramLowBounds[KFOXP3_TO_RORGTmRNA] = 0.01;         paramUpBounds[KFOXP3_TO_RORGTmRNA] 	= 10;
    paramLowBounds[NFOXP3_TO_RORGTmRNA] = 0.25;         paramUpBounds[NFOXP3_TO_RORGTmRNA] 	= 5;
    paramLowBounds[CFOXP3mRNA] 	= 1e-005*timeFactor;               paramUpBounds[CFOXP3mRNA] 	= 1*timeFactor;
    paramLowBounds[SIL2_TO_FOXP3mRNA] 	= 1.1;          paramUpBounds[SIL2_TO_FOXP3mRNA] 	= 50;
    paramLowBounds[KIL2_TO_FOXP3mRNA] 	= 0.01;         paramUpBounds[KIL2_TO_FOXP3mRNA] 	= 100;
    paramLowBounds[NIL2_TO_FOXP3mRNA] 	= 0.25;         paramUpBounds[NIL2_TO_FOXP3mRNA] 	= 5;
    paramLowBounds[STGFB_TO_FOXP3mRNA] 	= 1.1;          paramUpBounds[STGFB_TO_FOXP3mRNA] 	= 50;
    paramLowBounds[KTGFB_TO_FOXP3mRNA] 	= 0.01;         paramUpBounds[KTGFB_TO_FOXP3mRNA] 	= 1;
    paramLowBounds[NTGFB_TO_FOXP3mRNA] 	= 0.25;         paramUpBounds[NTGFB_TO_FOXP3mRNA] 	= 5;
	paramLowBounds[STBET_TO_FOXP3mRNA] 	= 1e-006;		paramUpBounds[STBET_TO_FOXP3mRNA] 	= 0.9;
    paramLowBounds[KTBET_TO_FOXP3mRNA] 	= 0.01;         paramUpBounds[KTBET_TO_FOXP3mRNA] 	= 10;
    paramLowBounds[NTBET_TO_FOXP3mRNA] 	= 0.25;         paramUpBounds[NTBET_TO_FOXP3mRNA] 	= 5;
    paramLowBounds[SGATA3_TO_FOXP3mRNA] = 1e-006;		paramUpBounds[SGATA3_TO_FOXP3mRNA] 	= 0.9;
    paramLowBounds[KGATA3_TO_FOXP3mRNA] = 0.01;         paramUpBounds[KGATA3_TO_FOXP3mRNA] 	= 10;
    paramLowBounds[NGATA3_TO_FOXP3mRNA] = 0.25;         paramUpBounds[NGATA3_TO_FOXP3mRNA] 	= 5;
    paramLowBounds[SRORGT_TO_FOXP3mRNA] = 1e-006;		paramUpBounds[SRORGT_TO_FOXP3mRNA] 	= 0.9;
    paramLowBounds[KRORGT_TO_FOXP3mRNA] = 0.01;         paramUpBounds[KRORGT_TO_FOXP3mRNA] 	= 10;
    paramLowBounds[NRORGT_TO_FOXP3mRNA] = 0.25;         paramUpBounds[NRORGT_TO_FOXP3mRNA] 	= 5;
    paramLowBounds[IL4INITTH2] = 12;        paramUpBounds[IL4INITTH2] = 16;
    paramLowBounds[IL2INITTREG] = 18;       paramUpBounds[IL2INITTREG] = 20;
    paramLowBounds[PIL2] = 1e-6*timeFactor;            paramUpBounds[PIL2] = 1e-3*timeFactor;
    paramLowBounds[PIL4] = 1e-6*timeFactor;            paramUpBounds[PIL4] = 1e-3*timeFactor;
    paramLowBounds[PIL17] = 1e-6*timeFactor;           paramUpBounds[PIL17] = 1e-3*timeFactor;
    paramLowBounds[PIL21] = 1e-6*timeFactor;           paramUpBounds[PIL21] = 1e-3*timeFactor;
    paramLowBounds[PIFNG] = 1e-6*timeFactor;           paramUpBounds[PIFNG] = 1e-3*timeFactor;
    paramLowBounds[PTGFB] = 1e-7*timeFactor;           paramUpBounds[PTGFB] = 1e-3*timeFactor;
    paramLowBounds[PTBET] = 1e-6*timeFactor;           paramUpBounds[PTBET] = 1e-3*timeFactor;
    paramLowBounds[PGATA3] = 1e-6*timeFactor;          paramUpBounds[PGATA3] = 1e-3*timeFactor;
    paramLowBounds[PRORGT] = 1e-6*timeFactor;          paramUpBounds[PRORGT] = 1e-3*timeFactor;
    paramLowBounds[PFOXP3] = 1e-6*timeFactor;          paramUpBounds[PFOXP3] = 1e-3*timeFactor;
    paramLowBounds[FORCEIL2]        = 1e-6*timeFactor;     paramUpBounds[FORCEIL2] = 1e-3*timeFactor;   // ??? no idea of range
    paramLowBounds[FORCEIL21]       = 1e-6*timeFactor;     paramUpBounds[FORCEIL21] = 1e-3*timeFactor;    // idem
    paramLowBounds[FORCEFOXP3]      = 1e-6*timeFactor;     paramUpBounds[FORCEFOXP3] = 1e-3*timeFactor;
    paramLowBounds[FORCERORGT]      = 1e-6*timeFactor;     paramUpBounds[FORCERORGT] = 1e-3*timeFactor;
    paramLowBounds[RESCALE_FACTOR]       = 1e-6;     paramUpBounds[RESCALE_FACTOR] = 1e-3;
    paramLowBounds[FORCETRANSL]     = 1e-6*timeFactor;     paramUpBounds[FORCETRANSL] = 1e-3*timeFactor;
    paramLowBounds[FORCESECRET]     = 1e-6*timeFactor;     paramUpBounds[FORCESECRET] = 1e-3*timeFactor;
    paramLowBounds[KANTI_IL2]       = 1e-6*timeFactor;     paramUpBounds[KANTI_IL2] = 1e-3*timeFactor;
    paramLowBounds[KANTI_IFNG]      = 1e-6*timeFactor;     paramUpBounds[KANTI_IFNG] = 1e-3*timeFactor;
    paramLowBounds[KANTI_IL4]       = 1e-6*timeFactor;     paramUpBounds[KANTI_IL4] = 1e-3*timeFactor;


}

void modeleLatentTbet2Hours::setBaseParameters(){
    background = Back::WT;
	params.clear();     // to make sure they are all put to zero
	params.resize(NBPARAM, 0.0);
    // TCR peak serait mieux à 2
    params[TCRPEAK] =       2.5;
    params[TCRCOEFF]=       0.005*timeFactor;
    params[KTCRGATA3]=      2500./timeFactor;
    params[KTCRIL2] =       8500./timeFactor;
    params[KTCRGATA3POS]=   2000./timeFactor;
    params[KTCRTGFB] =      1000./timeFactor;
    params[KTCRTBET] =      15000./timeFactor;
    params[KTCRIFNG] =      2500./timeFactor;
    params[KDIL2] =         1e-005*timeFactor;
    params[KDIL4] =         1e-005*timeFactor;
    params[KDIL12] =        5e-6*timeFactor;
    params[KDIL17] =        5e-6*timeFactor;
    params[KDIL21] =        1e-007*timeFactor;
    params[KDIFNG] =        1e-007*timeFactor;
    params[KDTGFB] =        1e-5*timeFactor;
    params[KDTBET] =        1.5e-5*timeFactor;
    params[KDGATA3] =       2e-5*timeFactor;
    params[KDRORGT] =       7e-5*timeFactor;
    params[KDFOXP3] =       5e-5*timeFactor;
    params[KDIL2mRNA] =     1.3e-005*timeFactor;
    params[KDIL4mRNA] =     1e-005*timeFactor;
    params[KDIL17mRNA] =    1e-005*timeFactor;
    params[KDIL21mRNA] =    5e-005*timeFactor;
    params[KDIFNGmRNA] =    1e-4*timeFactor;
    params[KDTGFBmRNA] =    2e-4*timeFactor;
    params[KDTBETmRNA] =    5e-4*timeFactor;
    params[KDGATA3mRNA] =   1e-4*timeFactor;
    params[KDRORGTmRNA] =   3e-005*timeFactor;
    params[KDFOXP3mRNA] =   1e-005*timeFactor;
    // should be none, take a value below detection
    params[IL2EQ] = 0.00001;
    params[IL4EQ] = 0.00001;
    params[IL12EQ] = 0.00001;
    params[IL17EQ] = 0.00001;
    params[IL21EQ] = 0.00001;
    params[IFNGEQ] = 0.00001;
    params[TGFBEQ] = 0.00001;
    params[TBETEQ] = 0.00001;
    // given by data : the starting point
    params[GATA3EQ] = 0.416;
    params[RORGTEQ] = 1e-6;
    params[FOXP3EQ] = 0.04;
    params[IL2mRNAEQ] = 0.0094;
    params[IL4mRNAEQ] = 0.0165;
    params[IL17mRNAEQ] = 0.00016;
    params[IL21mRNAEQ] = 0.034;
    params[IFNGmRNAEQ] = 0.041;
    params[TGFBmRNAEQ] = 0.97;
    params[TBETmRNAEQ] = 0.015;
    params[GATA3mRNAEQ] = 1.36;
    params[RORGTmRNAEQ] = 0.032;
    params[FOXP3mRNAEQ] = 0.213;

    params[CTGFBmRNA] = 2.5e-4*timeFactor;
    params[CIL2mRNA] = 1e-005*timeFactor;
    params[SIL2_TO_IL2mRNA] = 0.01;
    params[KIL2_TO_IL2mRNA] = 10;
    params[NIL2_TO_IL2mRNA] = 2.5;
    params[SIL4_TO_IL2mRNA] = 0.2;
    params[KIL4_TO_IL2mRNA] = 15;
    params[NIL4_TO_IL2mRNA] = 2;
    params[CIL4mRNA] 	= 1e-006*timeFactor;
    params[SGATA3_TO_IL4mRNA] 	= 100;
    params[KGATA3_TO_IL4mRNA] 	= 2.1;
    params[NGATA3_TO_IL4mRNA] 	= 2;
    params[CIL17mRNA] 	= 1e-006*timeFactor;
    params[SRORGT_TO_IL17mRNA] 	= 80;
    params[KRORGT_TO_IL17mRNA] 	= 6;
    params[NRORGT_TO_IL17mRNA] 	= 4;
    params[CIL21mRNA] 	= 1e-005*timeFactor;
    params[FIL6_TO_IL21mRNA] = 2;
    params[SRORGT_TO_IL21mRNA] 	= 10;
    params[KRORGT_TO_IL21mRNA] 	= 2;
    params[NRORGT_TO_IL21mRNA] 	= 3;
    params[CIFNGmRNA] 	= 1e-005*timeFactor;
    params[FIL12_TO_IFNGmRNA] 	= 6;

    params[STBET_TO_IFNGmRNA] 	= 24;
    params[KTBET_TO_IFNGmRNA] 	= 4;
    params[NTBET_TO_IFNGmRNA] 	= 2;
    params[CTBETmRNA] 	= 1e-005*timeFactor;
    params[FIL6_TO_TBETmRNA] 	= 3.5;
    //params[CIL2_TBETmRNA] 	= 1e-005;
    //params[SIL2_TO_TBETmRNA]    = 5;
    //params[KIL2_TO_TBETmRNA] 	= 2;
    //params[NIL2_TO_TBETmRNA] 	= 2;
    params[SIL12_TO_TBETmRNA] 	= 1.6;
    params[KIL12_TO_TBETmRNA] 	= 2;
    params[NIL12_TO_TBETmRNA] 	= 1;
    params[SIFNG_TO_TBETmRNA] 	= 250;
    params[KIFNG_TO_TBETmRNA] 	= 0.2;
    params[NIFNG_TO_TBETmRNA] 	= 2;
    params[SGATA3_TO_TBETmRNA] 	= 0.005;
    params[KGATA3_TO_TBETmRNA] 	= 0.48;
    params[NGATA3_TO_TBETmRNA] 	= 4;
    params[SRORGT_TO_TBETmRNA] 	= 1;
    params[KRORGT_TO_TBETmRNA] 	= 5;
    params[NRORGT_TO_TBETmRNA] 	= 2;
    params[SFOXP3_TO_TBETmRNA] 	= 0.1;
    params[KFOXP3_TO_TBETmRNA] 	= 5;
    params[NFOXP3_TO_TBETmRNA] 	= 2;
    params[CGATA3mRNA] 	= 1e-006*timeFactor;
    params[FIL12_TO_GATA3mRNA] = 0.1;
    params[SIL2_TO_GATA3mRNA] 	= 5;
    params[KIL2_TO_GATA3mRNA] 	= 40;
    params[NIL2_TO_GATA3mRNA] 	= 1.2;
    params[SIL4_TO_GATA3mRNA] 	= 30;
    params[KIL4_TO_GATA3mRNA] 	= 8;
    params[NIL4_TO_GATA3mRNA] 	= 1.2;
    params[SIFNG_TO_GATA3mRNA] 	= 30;
    params[KIFNG_TO_GATA3mRNA] 	= 8;
    params[NIFNG_TO_GATA3mRNA] 	= 1.2;

    params[STBET_TO_GATA3mRNA] 	= 0.1;
    params[KTBET_TO_GATA3mRNA] 	= 5;
    params[NTBET_TO_GATA3mRNA] 	= 1;
    params[SGATA3_TO_GATA3mRNA] 	= 40;
    params[KGATA3_TO_GATA3mRNA] 	= 1.2;
    params[NGATA3_TO_GATA3mRNA] 	= 4;
    params[SRORGT_TO_GATA3mRNA] 	= 0.1;
    params[KRORGT_TO_GATA3mRNA] 	= 5;
    params[NRORGT_TO_GATA3mRNA] 	= 2;
    params[SFOXP3_TO_GATA3mRNA] 	= 0.5;
    params[KFOXP3_TO_GATA3mRNA] 	= 2;
    params[NFOXP3_TO_GATA3mRNA] 	= 1.2;
    params[CRORGTmRNA] 	= 5e-6*timeFactor;
    params[FIL6_TO_RORGTmRNA] 	= 6;
    params[SIL21_TO_RORGTmRNA] 	= 2;
    params[KIL21_TO_RORGTmRNA] 	= 1.2;
    params[NIL21_TO_RORGTmRNA] 	= 0.9;
    params[STGFB_TO_RORGTmRNA] 	= 15;
    params[KTGFB_TO_RORGTmRNA] 	= 0.169;
    params[NTGFB_TO_RORGTmRNA] 	= 3;
    params[STBET_TO_RORGTmRNA] 	= 0.5;
    params[KTBET_TO_RORGTmRNA] 	= 5;
    params[NTBET_TO_RORGTmRNA] 	= 2;
    params[SGATA3_TO_RORGTmRNA] 	= 0.2;
    params[KGATA3_TO_RORGTmRNA] 	= 5;
    params[NGATA3_TO_RORGTmRNA] 	= 2;
    params[SFOXP3_TO_RORGTmRNA] 	= 0.01;
    params[KFOXP3_TO_RORGTmRNA] 	= 1;
    params[NFOXP3_TO_RORGTmRNA] 	= 2;
    params[CFOXP3mRNA] 	= 1e-005*timeFactor;
    params[SIL2_TO_FOXP3mRNA] 	= 3;
    params[KIL2_TO_FOXP3mRNA] 	= 45;
    params[NIL2_TO_FOXP3mRNA] 	= 1;
    params[STGFB_TO_FOXP3mRNA] 	= 25;
    params[KTGFB_TO_FOXP3mRNA] 	= 0.4;
    params[NTGFB_TO_FOXP3mRNA] 	= 3;
    params[STBET_TO_FOXP3mRNA] 	= 1e-006;
    params[KTBET_TO_FOXP3mRNA] 	= 0.01;
    params[NTBET_TO_FOXP3mRNA] 	= 0.25;
    params[SGATA3_TO_FOXP3mRNA] 	= 1e-006;
    params[KGATA3_TO_FOXP3mRNA] 	= 2;
    params[NGATA3_TO_FOXP3mRNA] 	= 3;
    params[SRORGT_TO_FOXP3mRNA] 	= 1e-006;
    params[KRORGT_TO_FOXP3mRNA] 	= 1;
    params[NRORGT_TO_FOXP3mRNA] 	= 3;
    params[IL4INITTH2] = 14.6;
    params[IL2INITTREG] = 19.05;
    params[PIL2] = 3e-4*timeFactor;
    params[PIL4] = 2e-6*timeFactor;
    params[PIL17] = 4e-4*timeFactor;
    params[PIL21] = 1.5e-6*timeFactor;
    params[PIFNG] = 6e-6*timeFactor;
    params[PTGFB] = 5e-7*timeFactor;
    params[PTBET] = 1.8e-5*timeFactor;
    params[PGATA3] = 3e-5*timeFactor;
    params[PRORGT] = 8e-5*timeFactor;
    params[PFOXP3] = 7e-5*timeFactor;
    params[FORCEIL2] = 2e-5*timeFactor;
    params[FORCEIL21] = 2e-5*timeFactor;
    params[FORCEFOXP3] = 2e-5*timeFactor;
    params[FORCERORGT] = 2e-5*timeFactor;
    params[RESCALE_FACTOR] = 1.0;
    params[FORCETRANSL] = 2e-5*timeFactor;
    params[FORCESECRET] = 2e-5*timeFactor;
    params[KANTI_IL2] = 2e-5*timeFactor;
    params[KANTI_IFNG] = 2e-5*timeFactor;
    params[KANTI_IL4] = 2e-5*timeFactor;

    // this doesnt help
    /*vector<double> coeffPerVariable(this->nbVars, 0);
    coeffPerVariable[TBETmRNA] = 10.6;
    coeffPerVariable[GATA3mRNA] = 100.4;
    coeffPerVariable[FOXP3mRNA] = 9.93;
    coeffPerVariable[RORGTmRNA] = 31.6;
    coeffPerVariable[IFNGmRNA] = 115;
    coeffPerVariable[IL4mRNA] = 7.8;
    coeffPerVariable[IL21mRNA] = 28.3;
    coeffPerVariable[IL2mRNA] = 641;
    coeffPerVariable[TGFBmRNA] = 358;
    coeffPerVariable[IL17mRNA] = 1021;
    coeffPerVariable[TBET] = 730;
    coeffPerVariable[GATA3] = 1470;
    coeffPerVariable[FOXP3] = 7.0;
    coeffPerVariable[FOXP3] = 786;

    coeffPerVariable[IL21] = 0.3;
    coeffPerVariable[IL17] = 21.8;
    coeffPerVariable[IL4] = 1.45;
    coeffPerVariable[IL2] = 4.7;
    coeffPerVariable[IFNG] = 8.5;

    postRescaleParameters(coeffPerVariable, false, true);*/

    // Parameters as defined in the Matlab model, and that are not fitted
    params[TCRPEAK] = 2.5;
    params[TCRCOEFF] = 18;
    //params[TCRBASAL] = 0.2; // check
    params[GATA3EQ] = 450;
    params[RORGTEQ] = 26;
    params[FOXP3EQ] = 0.07;
    params[IL2mRNAEQ] = 1;
    params[IL4mRNAEQ] = 0.04;
    params[IL17mRNAEQ] = 0.06;
    params[IL21mRNAEQ] = 0.44;
    params[IFNGmRNAEQ] = 2.3;
    params[TGFBmRNAEQ] = 430;
    params[TBETmRNAEQ] = 0.19;
    params[GATA3mRNAEQ] = 88;
    params[RORGTmRNAEQ] = 0.5;
    params[FOXP3mRNAEQ] = 2.2;
    params[IL4INITTH2] = 14.3;
    params[IL2INITTREG] = 13.4;
    params[FORCEIL2] = 2;
    params[FORCEIL21] = 2;
    params[FORCEFOXP3] = 0.58;
    params[FORCERORGT] = 2.5;
    params[FORCETRANSL] = 0.58;
    params[FORCESECRET] = 0.58;
    params[KANTI_IL2] = 7.2e-2;
    params[KANTI_IFNG] = 7.2e-2;
    params[KANTI_IL4] = 7.2e-2;

    // Result of a parameter estimation in Matlab
    params[KTCRGATA3] = 0.010156942;
    params[KTCRIL2] = 2.548643541;
    params[KTCRGATA3POS] = 0.012798255;
    params[KTCRTGFB] = 0.162255132;
    params[KTCRTBET] = 0.776102528;
    params[KTCRIFNG] = 0.0100005;
    params[KDTBET] = 0.051311926;
    params[KDGATA3] = 0.136417794;
    params[KDRORGT] = 0.07175868;
    params[KDFOXP3] = 0.065472772;
    params[KDIL2mRNA] = 0.030649214;
    params[KDIL4mRNA] = 4.592433672;
    params[KDIL17mRNA] = 0.085223009;
    params[KDIL21mRNA] = 2.361474024;
    params[KDIFNGmRNA] = 0.237375672;
    params[KDTBETmRNA] = 5.013576454;
    params[KDGATA3mRNA] = 2.541564651;
    params[KDRORGTmRNA] = 0.572008155;
    params[KDFOXP3mRNA] = 0.04159668;
    params[CTGFBmRNA] = 734.9991461;
    params[CIL2mRNA] = 27.24564854;
    params[SIL2_TO_IL2mRNA] = 0.19543737;
    params[KIL2_TO_IL2mRNA] = 0.101183782;
    params[SIL4_TO_IL2mRNA] = 0.121739119;
    params[KIL4_TO_IL2mRNA] = 17.29367617;
    params[CIL4mRNA] = 8.348521784;
    params[SGATA3_TO_IL4mRNA] = 32.37870986;
    params[KGATA3_TO_IL4mRNA] = 6900.934822;
    params[CIL17mRNA] = 13.21416602;
    params[SRORGT_TO_IL17mRNA] = 119.4301649;
    params[KRORGT_TO_IL17mRNA] = 7719.085853;
    params[CIL21mRNA] = 7.797575592;
    params[FIL6_TO_IL21mRNA] = 43.29011539;
    params[SRORGT_TO_IL21mRNA] = 1.330527453;
    params[KRORGT_TO_IL21mRNA] = 216.6908311;
    params[CIFNGmRNA] = 0.48690526;
    params[FIL12_TO_IFNGmRNA] = 2.260290754;
    params[STBET_TO_IFNGmRNA] = 479.3560674;
    params[KTBET_TO_IFNGmRNA] = 7934.855585;
    params[CTBETmRNA] = 4.694751649;
    params[FIL6_TO_TBETmRNA] = 5.498521701;
    params[SIFNG_TO_TBETmRNA] = 56.51212082;
    params[KIFNG_TO_TBETmRNA] = 0.065584317;
    params[SGATA3_TO_TBETmRNA] = 0.083548381;
    params[KGATA3_TO_TBETmRNA] = 209.4329258;
    params[CGATA3mRNA] = 2.594618787;
    params[FIL12_TO_GATA3mRNA] = 0.05126307;
    params[SIL2_TO_GATA3mRNA] = 21.77186153;
    params[KIL2_TO_GATA3mRNA] = 2.217892811;
    params[SIL4_TO_GATA3mRNA] = 44.91947399;
    params[KIL4_TO_GATA3mRNA] = 0.106893807;
    params[STBET_TO_GATA3mRNA] = 0.054196885;
    params[KTBET_TO_GATA3mRNA] = 200.5696647;
    params[SGATA3_TO_GATA3mRNA] = 2.031807018;
    params[KGATA3_TO_GATA3mRNA] = 597.1144372;
    params[SRORGT_TO_GATA3mRNA] = 0.0502868;
    params[KRORGT_TO_GATA3mRNA] = 408.3406961;
    params[CRORGTmRNA] = 10.63671851;
    params[FIL6_TO_RORGTmRNA] = 3.859726673;
    params[SIL21_TO_RORGTmRNA] = 1.343529152;
    params[KIL21_TO_RORGTmRNA] = 0.103898383;
    params[STGFB_TO_RORGTmRNA] = 7.650610393;
    params[KTGFB_TO_RORGTmRNA] = 0.137863587;
    params[STBET_TO_RORGTmRNA] = 0.051509547;
    params[KTBET_TO_RORGTmRNA] = 7864.206734;
    params[SGATA3_TO_RORGTmRNA] = 0.222179425;
    params[KGATA3_TO_RORGTmRNA] = 160.0399908;
    params[SFOXP3_TO_RORGTmRNA] = 0.01011245;
    params[KFOXP3_TO_RORGTmRNA] = 11.35057696;
    params[CFOXP3mRNA] = 0.149076322;
    params[SIL2_TO_FOXP3mRNA] = 9.190383783;
    params[KIL2_TO_FOXP3mRNA] = 22.17956442;
    params[STGFB_TO_FOXP3mRNA] = 35.6107379;
    params[KTGFB_TO_FOXP3mRNA] = 0.055768852;
    params[STBET_TO_FOXP3mRNA] = 0.129931671;
    params[KTBET_TO_FOXP3mRNA] = 1288.713763;
    params[SGATA3_TO_FOXP3mRNA] = 0.05590835;
    params[KGATA3_TO_FOXP3mRNA] = 462.4150469;
    params[SRORGT_TO_FOXP3mRNA] = 0.050242935;
    params[KRORGT_TO_FOXP3mRNA] = 206.152177;
    params[PIL2] = 0.001057529;
    params[PIL4] = 0.000303223;
    params[PIL17] = 0.001115182;
    params[PIL21] = 0.000420621;
    params[PIFNG] = 0.003686101;
    params[PTBET] = 6.618975764;
    params[PGATA3] = 2.672963478;
    params[PRORGT] = 2.458490377;
    params[PFOXP3] = 0.06223192;

    params[KDIL2] =         0.022;
    params[KDIL4] =         0.022;
    params[KDIL12] =        0.022;
    params[KDIL17] =        0.022;
    params[KDIL21] =        0.022;
    params[KDIFNG] =        0.022;
    params[KDTGFB] =        0.036;
    params[KDTBET] =        0.051312;
    params[KDGATA3] =       0.136418;
    params[KDRORGT] =       0.071759;
    params[KDFOXP3] =       0.065473;

	setBaseParametersDone();
}

void modeleLatentTbet2Hours::initialise(long long _background){ // don't touch to parameters !
	val.clear();
	val.resize(NBVAR, 0.0);
	init.clear();
	init.resize(NBVAR, 0.0);

    // test if each possible background is in the current combination of backgrounds (stored in the field background)
    if((background != Back::WT) && (background != _background)){
        cerr << "WRN : modeleLatentTbet2Hours::initialize, when the background has been changed from WT, you should not change it again, because parameters can not be retrieved. Please load parameters again" << endl;
    }
    background = _background;
    if(background & Back::WT){}
    if(background & Back::TCRGATA3NEGKO){   params[KTCRGATA3]   = 0;}
    if(background & Back::TCRIL2KO){        params[KTCRIL2]     = 0;}
    if(background & Back::TCRGARA3POSKO){   params[KTCRGATA3POS]= 0;}
    if(background & Back::TCRTGFBKO){       params[KTCRTGFB]    = 0;}
    if(background & Back::TCRTBETKO){       params[KTCRTBET]    = 0;}
    if(background & Back::TCRIFNGKO){       params[KTCRIFNG]    = 0;}
    if(background & Back::IL2KO){           params[IL2EQ] = 0;      params[CIL2mRNA] = 0;}
    if(background & Back::IL4KO){           params[IL4EQ] = 0;      params[CIL4mRNA] = 0;}
    if(background & Back::IL17KO){          params[IL17EQ]= 0;      params[CIL17mRNA]= 0;}
    if(background & Back::IL21KO){          params[IL21EQ]= 0;      params[CIL21mRNA]= 0;}
    if(background & Back::IFNGKO){          params[IFNGEQ]= 0;      params[CIFNGmRNA]= 0;}
    if(background & Back::TGFBKO){          params[TGFBEQ]= 0;      params[CTGFBmRNA]= 0;}
    if(background & Back::TBETKO){          params[TBETEQ]= 0;      params[CTBETmRNA]= 0;}
    if(background & Back::GATA3KO){         params[GATA3EQ]=0;      params[CGATA3mRNA]=0;}
    if(background & Back::RORGTKO){         params[RORGTEQ]=0;      params[CRORGTmRNA]=0;}
    if(background & Back::FOXP3KO){         params[FOXP3EQ]=0;      params[CFOXP3mRNA]=0;}
    if(background & Back::STAT1KO){         params[SIFNG_TO_TBETmRNA] = 1;   params[SIFNG_TO_GATA3mRNA] = 1;}
    if(background & Back::STAT3KO){         params[FIL6_TO_IL21mRNA] = 1;    params[FIL6_TO_TBETmRNA] = 1;   params[FIL6_TO_RORGTmRNA] = 1;  params[SIL21_TO_RORGTmRNA] = 1;}
    if(background & Back::STAT4KO){         params[FIL12_TO_IFNGmRNA] = 1;   params[SIL12_TO_TBETmRNA] = 1;  params[FIL12_TO_GATA3mRNA] = 1;}
    if(background & Back::STAT5KO){         params[SIL2_TO_IL2mRNA] = 1;     params[SIL2_TO_GATA3mRNA] = 1;  params[SIL2_TO_FOXP3mRNA] = 1;}
    if(background & Back::STAT6KO){         params[SIL4_TO_IL2mRNA] = 1;     params[SIL4_TO_GATA3mRNA] = 1;}
    if(background & Back::IL6RKO){          params[FIL6_TO_IL21mRNA] = 1;    params[FIL6_TO_TBETmRNA] = 1;   params[FIL6_TO_RORGTmRNA] = 1;}
    if(background & Back::IL21RKO){         params[SIL21_TO_RORGTmRNA] = 1;}
    if(background & Back::TGFBRKO){         params[STGFB_TO_RORGTmRNA] = 1;  params[STGFB_TO_FOXP3mRNA] = 1;}
    if(background & Back::NOIL2INHIBIL2){   params[SIL2_TO_IL2mRNA] = 1;}
    if(background & Back::NOIL4INHIBIL){    params[SIL4_TO_IL2mRNA] = 1;}
    if(background & Back::NOGATA3ACTIVIL4){ params[SGATA3_TO_IL4mRNA] = 1;}
    if(background & Back::NOIL6ACTIVIL21){  params[FIL6_TO_IL21mRNA] = 1;}
    if(background & Back::NORORGTACTIVIL21){params[SRORGT_TO_IL21mRNA] = 1;}
    if(background & Back::NOIL12ACTIVIFN){  params[FIL12_TO_IFNGmRNA] = 1;}
    if(background & Back::NOTBETACTIVIFNG){ params[STBET_TO_IFNGmRNA] = 1;}
    if(background & Back::NOIL6ACTIVTBET){  params[FIL6_TO_TBETmRNA] = 1;}
    if(background & Back::NOIL12ACTIVTBET){ params[SIL12_TO_TBETmRNA] = 1;}
    if(background & Back::NOIFNGACTIVTBET){ params[SIFNG_TO_TBETmRNA] = 1;}
    if(background & Back::NOGATA3INHIBTBET){params[SGATA3_TO_TBETmRNA] = 1;}
    if(background & Back::NORORGTINHIBTBET){params[SRORGT_TO_TBETmRNA] = 1;}
    if(background & Back::NOFOXP3INHIBTBET){params[SFOXP3_TO_TBETmRNA] = 1;}
    if(background & Back::NOIL12INHIBGATA3){params[FIL12_TO_GATA3mRNA] = 1;}
    if(background & Back::NOIL2ACTIVGATA3){ params[SIL2_TO_GATA3mRNA] = 1;}
    if(background & Back::NOIL4ACTIVGATA3){ params[SIL4_TO_GATA3mRNA] = 1;}
    if(background & Back::NOIFNGINHIBGATA3){params[SIFNG_TO_GATA3mRNA] = 1;}
    if(background & Back::NOTBETINHIBGATA3){params[STBET_TO_GATA3mRNA] = 1;}
    if(background & Back::GATA3FEEDBACKKO){ params[SGATA3_TO_GATA3mRNA] = 1;}
    if(background & Back::NORORGTINHIBGATA3){params[SRORGT_TO_GATA3mRNA] = 1;}
    if(background & Back::NOFOXP3INHIBGATA3){params[SFOXP3_TO_GATA3mRNA] = 1;}
    if(background & Back::NOIL6ACTIVRORGT){  params[FIL6_TO_RORGTmRNA] = 1;}
    if(background & Back::NOIL21ACTIVRORGT){ params[SIL21_TO_RORGTmRNA] = 1;}
    if(background & Back::NOTGFBACTIVRORGT){ params[STGFB_TO_RORGTmRNA] = 1;}
    if(background & Back::NOTBETINHIBRORGT){ params[STBET_TO_RORGTmRNA] = 1;}
    if(background & Back::NOGATA3INHIBRORGT){params[SGATA3_TO_RORGTmRNA] = 1;}
    if(background & Back::NOFOXP3INHIBRORGT){params[SFOXP3_TO_RORGTmRNA] = 1;}
    if(background & Back::NOIL2ACTIVFOXP3){  params[SIL2_TO_FOXP3mRNA] = 1;}
    if(background & Back::NOTGFBACTIVFOXP3){ params[STGFB_TO_FOXP3mRNA] = 1;}
    if(background & Back::NOTBETINHIBFOXP3){ params[STBET_TO_FOXP3mRNA] = 1;}
    if(background & Back::NOGATA3INHIBFOXP3){params[SGATA3_TO_FOXP3mRNA] = 1;}
    if(background & Back::NORORGTINHIBFOXP3){params[SRORGT_TO_FOXP3mRNA] = 1;}
    if(background & Back::NODELAYIL2){       params[FORCEIL2] = 1;}
    if(background & Back::NODELAYIL21){      params[FORCEIL21] = 1;}
    if(background & Back::NODELAYFOXP3){     params[FORCEFOXP3] = 1;}
    if(background & Back::NODELAYRORGT){     params[FORCERORGT] = 1;}
    if(background & Back::NODELAYTRANSL){    params[FORCETRANSL] = 1;}
    if(background & Back::NODELAYSECRET){    params[FORCESECRET] = 1;}




	/*params[PIL2] = params[KDIL2] * params[IL2EQ] / (1);
	params[PIL4] = params[KDIL4] * params[IL4EQ] / (1);
	params[PIL17] = params[KDIL17] * params[IL17EQ] / (1);
	params[PIL21] = params[KDIL21] * params[IL21EQ] / (1);
	params[PIFNG] = params[KDIFNG] * params[IFNGEQ] / (1);
	params[PTGFB] = params[KDTGFB] * params[TGFBEQ] / (1);
	params[PTBET] = params[KDTBET] * params[TBETEQ] / (1);
	params[PGATA3] = params[KDGATA3] * params[GATA3EQ] / (1);
	params[PRORGT] = params[KDRORGT] * params[RORGTEQ] / (1);
	params[PFOXP3] = params[KDFOXP3] * params[FOXP3EQ] / (1);*/

    /*params[CIL2mRNA]  = params[KDIL2mRNA]   * params[IL2mRNAEQ]  / ( Activ0Inhib1(params[IL2EQ],params[KIL2_TO_IL2mRNA],params[NIL2_TO_IL2mRNA], params[SIL2_TO_IL2mRNA]));
    params[CIL4mRNA]  = params[KDIL4mRNA]   * params[IL4mRNAEQ]  / ( Activ1Inhib0(params[GATA3EQ],params[KGATA3_TO_IL4mRNA],params[NGATA3_TO_IL4mRNA], params[SGATA3_TO_IL4mRNA]));
    params[CIL17mRNA] = params[KDIL17mRNA]  * params[IL17mRNAEQ] / ( Activ1Inhib0(params[RORGTEQ],params[KRORGT_TO_IL17mRNA],params[NRORGT_TO_IL17mRNA], params[SRORGT_TO_IL17mRNA]));
    params[CIL21mRNA] = params[KDIL21mRNA] * params[IL21mRNAEQ]  / ( Activ1Inhib0(params[RORGTEQ],params[KRORGT_TO_IL21mRNA],params[NRORGT_TO_IL21mRNA], params[SRORGT_TO_IL21mRNA]));
    params[CIFNGmRNA] = params[KDIFNGmRNA] * params[IFNGmRNAEQ]  / ( Activ1Inhib0(params[TBETEQ],params[KTBET_TO_IFNGmRNA],params[NTBET_TO_IFNGmRNA], params[STBET_TO_IFNGmRNA]));
    params[CTGFBmRNA] = params[KDTGFBmRNA] * params[TGFBmRNAEQ] ;
    params[CTBETmRNA] = params[KDTBETmRNA]   * params[TBETmRNAEQ]  / ( Activ2Inhib3(params[IL12EQ],params[KIL12_TO_TBETmRNA],params[NIL12_TO_TBETmRNA], params[SIL12_TO_TBETmRNA],params[IFNGEQ],params[KIFNG_TO_TBETmRNA],params[NIFNG_TO_TBETmRNA], params[SIFNG_TO_TBETmRNA],params[GATA3EQ],params[KGATA3_TO_TBETmRNA],params[NGATA3_TO_TBETmRNA], params[SGATA3_TO_TBETmRNA],params[RORGTEQ],params[KRORGT_TO_TBETmRNA],params[NRORGT_TO_TBETmRNA], params[SRORGT_TO_TBETmRNA],params[FOXP3EQ],params[KFOXP3_TO_TBETmRNA],params[NFOXP3_TO_TBETmRNA], params[SFOXP3_TO_TBETmRNA]));
    params[CGATA3mRNA] = params[KDGATA3mRNA] * params[GATA3mRNAEQ] / ( Activ3Inhib3(params[IL2EQ],params[KIL2_TO_GATA3mRNA],params[NIL2_TO_GATA3mRNA], params[SIL2_TO_GATA3mRNA],params[IL4EQ],params[KIL4_TO_GATA3mRNA],params[NIL4_TO_GATA3mRNA], params[SIL4_TO_GATA3mRNA],params[GATA3EQ],params[KGATA3_TO_GATA3mRNA],params[NGATA3_TO_GATA3mRNA], params[SGATA3_TO_GATA3mRNA],params[TBETEQ],params[KTBET_TO_GATA3mRNA],params[NTBET_TO_GATA3mRNA], params[STBET_TO_GATA3mRNA],params[RORGTEQ],params[KRORGT_TO_GATA3mRNA],params[NRORGT_TO_GATA3mRNA], params[SRORGT_TO_GATA3mRNA],params[FOXP3EQ],params[KFOXP3_TO_GATA3mRNA],params[NFOXP3_TO_GATA3mRNA], params[SFOXP3_TO_GATA3mRNA]));
    params[CRORGTmRNA] = params[KDRORGTmRNA] * params[RORGTmRNAEQ] / ( Activ2Inhib3(params[IL21EQ],params[KIL21_TO_RORGTmRNA],params[NIL21_TO_RORGTmRNA], params[SIL21_TO_RORGTmRNA],params[TGFBEQ],params[KTGFB_TO_RORGTmRNA],params[NTGFB_TO_RORGTmRNA], params[STGFB_TO_RORGTmRNA],params[TBETEQ],params[KTBET_TO_RORGTmRNA],params[NTBET_TO_RORGTmRNA], params[STBET_TO_RORGTmRNA],params[GATA3EQ],params[KGATA3_TO_RORGTmRNA],params[NGATA3_TO_RORGTmRNA], params[SGATA3_TO_RORGTmRNA],params[FOXP3EQ],params[KFOXP3_TO_RORGTmRNA],params[NFOXP3_TO_RORGTmRNA], params[SFOXP3_TO_RORGTmRNA]));
    params[CFOXP3mRNA] = params[KDFOXP3mRNA] * params[FOXP3mRNAEQ] / ( Activ2Inhib3(params[IL2EQ],params[KIL2_TO_FOXP3mRNA],params[NIL2_TO_FOXP3mRNA], params[SIL2_TO_FOXP3mRNA],params[TGFBEQ],params[KTGFB_TO_FOXP3mRNA],params[NTGFB_TO_FOXP3mRNA], params[STGFB_TO_FOXP3mRNA],params[TBETEQ],params[KTBET_TO_FOXP3mRNA],params[NTBET_TO_FOXP3mRNA], params[STBET_TO_FOXP3mRNA],params[GATA3EQ],params[KGATA3_TO_FOXP3mRNA],params[NGATA3_TO_FOXP3mRNA], params[SGATA3_TO_FOXP3mRNA],params[RORGTEQ],params[KRORGT_TO_FOXP3mRNA],params[NRORGT_TO_FOXP3mRNA], params[SRORGT_TO_FOXP3mRNA]));
    */

//        // to remove, for playing with normalization
//    if(params[RESCALE_FACTOR] != 1.0){
//        vector<double> coeffPerVariable = vector<double>(getNbVars(), -1.);
//        coeffPerVariable[FOXP3mRNA] = params[RESCALE_FACTOR];
//        postRescaleParameters(coeffPerVariable);
//        params[RESCALE_FACTOR] = 1.0;
//    }

    init[IL2] = params[IL2EQ];
	init[IL4] = params[IL4EQ];
    init[IL12] = params[IL12EQ];
	init[IL17] = params[IL17EQ];
	init[IL21] = params[IL21EQ];
	init[IFNG] = params[IFNGEQ];
	init[TGFB] = params[TGFBEQ];
	init[TBET] = params[TBETEQ];
	init[GATA3] = params[GATA3EQ];
	init[RORGT] = params[RORGTEQ];
	init[FOXP3] = params[FOXP3EQ];
	init[IL2mRNA] = params[IL2mRNAEQ];
	init[IL4mRNA] = params[IL4mRNAEQ];
	init[IL17mRNA] = params[IL17mRNAEQ];
	init[IL21mRNA] = params[IL21mRNAEQ];
	init[IFNGmRNA] = params[IFNGmRNAEQ];
	init[TGFBmRNA] = params[TGFBmRNAEQ];
	init[TBETmRNA] = params[TBETmRNAEQ];
	init[GATA3mRNA] = params[GATA3mRNAEQ];
	init[RORGTmRNA] = params[RORGTmRNAEQ];
	init[FOXP3mRNA] = params[FOXP3mRNAEQ];
    //open states start at 0
    init[openIL2]   = 1e-4;
    init[openIL21]  = 1e-4;
    init[openFOXP3] = 1e-4;
    init[openRORGT] = 1e-4;
    init[openTBET]  = 1e-4;
    init[transl]    = 1e-2;
    init[secret]    = 1e-4;
    for(size_t i = 0; i < NBVAR; ++i){
		val[i] = init[i];}
	t = 0;
	initialiseDone();
}

#define TCRBASAL 0.0
void modeleLatentTbet2Hours::derivatives(const vector<double> &x, vector<double> &dxdt, const double t){
	double LAMBDA = params[TCRCOEFF] / (params[TCRPEAK] * params[TCRCOEFF] - TCRBASAL);
    double tcrval = x[TCR]; //= (-TCRBASAL + params[TCRCOEFF] * (t/(3600.0/timeFactor))) * exp(-(t/(3600.0/timeFactor)) * LAMBDA) + TCRBASAL;
    if(!over(TCR))	dxdt[TCR] = -LAMBDA * (-TCRBASAL + params[TCRCOEFF] * (t /(3600.0/timeFactor))) * exp(-(t /(3600.0/timeFactor)) * LAMBDA) + params[TCRCOEFF] * exp(-(t /(3600.0/timeFactor)) * LAMBDA);

    if(!over(IL2))		dxdt[IL2] 	= - params[KANTI_IL2] * x[IL2] * x[antiIL2] + (- params[KDIL2]  * x[IL2]  + params[PIL2]  * x[secret] * x[IL2mRNA]);
    if(!over(IL4))		dxdt[IL4]  	= - params[KANTI_IL4] * x[IL4] * x[antiIL4] + (- params[KDIL4]  * x[IL4]  + params[PIL4]  * x[secret] * x[IL4mRNA]);
	if(!over(IL6))		dxdt[IL6]  	= 0.0;
	if(!over(IL12))		dxdt[IL12] 	= (- params[KDIL12] * x[IL12] );
    if(!over(IL17))		dxdt[IL17] 	= (- params[KDIL17] * x[IL17] + params[PIL17] * x[secret] * x[IL17mRNA]);
    if(!over(IL21))		dxdt[IL21] 	= (- params[KDIL21] * x[IL21] + params[PIL21] * x[secret] * x[IL21mRNA]);
    if(!over(IFNG))		dxdt[IFNG] 	= - params[KANTI_IFNG]* x[IFNG]* x[antiIFNg] + (- params[KDIFNG] * x[IFNG] + params[PIFNG] * x[secret] * x[IFNGmRNA]);
    if(!over(TGFB))		dxdt[TGFB] 	= (- params[KDTGFB] * x[TGFB] + params[PTGFB] * x[secret] * x[TGFBmRNA]);
    if(!over(TBET))		dxdt[TBET] 	= (- params[KDTBET] * x[TBET] + params[PTBET] * x[transl] * x[TBETmRNA]);
    if(!over(GATA3))	dxdt[GATA3] = (- params[KDGATA3] * x[GATA3] * (1 + params[KTCRGATA3] * tcrval) + params[PGATA3] * x[GATA3mRNA]); // Gata3 is already translated at steady-state
    if(!over(RORGT))	dxdt[RORGT] = (- params[KDRORGT] * x[RORGT] + params[PRORGT] * x[transl] * x[RORGTmRNA]);
    if(!over(FOXP3))	dxdt[FOXP3] = (- params[KDFOXP3] * x[FOXP3] + params[PFOXP3] * x[transl] * x[FOXP3mRNA]);

    double activIL2 = (1 + params[KTCRIL2] * tcrval) 		* Activ0Inhib2(x[IL2],params[KIL2_TO_IL2mRNA],params[NIL2_TO_IL2mRNA], params[SIL2_TO_IL2mRNA], x[IL4], params[KIL4_TO_IL2mRNA], params[NIL4_TO_IL2mRNA], params[SIL4_TO_IL2mRNA]);
    if(!over(openIL2))      dxdt[openIL2]   = params[FORCEIL2] * /* activIL2 * */ x[openIL2] * (1 - x[openIL2]);
    if(!over(IL2mRNA))		dxdt[IL2mRNA] 	= (- params[KDIL2mRNA]  * x[IL2mRNA]   + params[CIL2mRNA]   * x[openIL2] * activIL2);
    if(!over(IL4mRNA))		dxdt[IL4mRNA] 	= (- params[KDIL4mRNA]  * x[IL4mRNA]   + params[CIL4mRNA]   * Activ1Inhib0(x[GATA3],params[KGATA3_TO_IL4mRNA],params[NGATA3_TO_IL4mRNA], params[SGATA3_TO_IL4mRNA]));
	if(!over(IL17mRNA))		dxdt[IL17mRNA] 	= (- params[KDIL17mRNA] * x[IL17mRNA]  + params[CIL17mRNA]  * Activ1Inhib0(x[RORGT],params[KRORGT_TO_IL17mRNA],params[NRORGT_TO_IL17mRNA], params[SRORGT_TO_IL17mRNA]));
    double activIL21 =      ((x[IL6] > 0.5) ? params[FIL6_TO_IL21mRNA] : 1.0) * Activ1Inhib0(x[RORGT],params[KRORGT_TO_IL21mRNA],params[NRORGT_TO_IL21mRNA], params[SRORGT_TO_IL21mRNA]);
    if(!over(openIL21))     dxdt[openIL21]  = params[FORCEIL21] * /* activIL21 * */ x[openIL21] * (1 - x[openIL21]);
    if(!over(IL21mRNA))		dxdt[IL21mRNA] 	= (- params[KDIL21mRNA] * x[IL21mRNA]  + params[CIL21mRNA]  * x[openIL21] * activIL21);
    if(!over(IFNGmRNA))		dxdt[IFNGmRNA] 	= (- params[KDIFNGmRNA] * x[IFNGmRNA]  + params[CIFNGmRNA]  * ((x[IL12] > 0.5) ? params[FIL12_TO_IFNGmRNA] : 1.0) * ((1 + params[KTCRIFNG] * tcrval) * Activ1Inhib0(x[TBET],params[KTBET_TO_IFNGmRNA],params[NTBET_TO_IFNGmRNA], params[STBET_TO_IFNGmRNA])));
    if(!over(TGFBmRNA))		dxdt[TGFBmRNA] 	= (- params[KDTGFBmRNA] * x[TGFBmRNA]  + params[CTGFBmRNA] 	* (1 + params[KTCRTGFB] * tcrval));

    //if(!over(GATA3mRNA))	dxdt[GATA3mRNA] = (- params[KDGATA3mRNA]* x[GATA3mRNA] + params[CGATA3mRNA] * (1 + params[KTCRGATA3POS] * tcrval) * ((x[IL12] > 0.5) ? params[FIL12_TO_GATA3mRNA] : 1.0) * Activ3Inhib4(x[IL2],params[KIL2_TO_GATA3mRNA],params[NIL2_TO_GATA3mRNA], params[SIL2_TO_GATA3mRNA],x[IL4],params[KIL4_TO_GATA3mRNA],params[NIL4_TO_GATA3mRNA], params[SIL4_TO_GATA3mRNA],x[GATA3],params[KGATA3_TO_GATA3mRNA],params[NGATA3_TO_GATA3mRNA], params[SGATA3_TO_GATA3mRNA],x[TBET],params[KTBET_TO_GATA3mRNA],params[NTBET_TO_GATA3mRNA], params[STBET_TO_GATA3mRNA],x[RORGT],params[KRORGT_TO_GATA3mRNA],params[NRORGT_TO_GATA3mRNA], params[SRORGT_TO_GATA3mRNA],x[FOXP3],params[KFOXP3_TO_GATA3mRNA],params[NFOXP3_TO_GATA3mRNA], params[SFOXP3_TO_GATA3mRNA], x[IFNG], params[KIFNG_TO_GATA3mRNA], params[NIFNG_TO_GATA3mRNA], params[SIFNG_TO_GATA3mRNA]));

    // here I have a better fit by separating IL2 --> Gata3 and Gata3 --| Gata3 through a + instead of a * (as written in the equations).
    // I will find a way to fix this ...
    if(!over(GATA3mRNA))	dxdt[GATA3mRNA] = (- params[KDGATA3mRNA]* x[GATA3mRNA] + params[CGATA3mRNA] * (1 + params[KTCRGATA3POS] * tcrval) * ((x[IL12] > 0.5) ? params[FIL12_TO_GATA3mRNA] : 1.0) * ( Activ1Inhib0(x[IL2],params[KIL2_TO_GATA3mRNA],params[NIL2_TO_GATA3mRNA], params[SIL2_TO_GATA3mRNA] ) * Activ0Inhib1(x[GATA3],params[KGATA3_TO_GATA3mRNA],params[NGATA3_TO_GATA3mRNA], params[SGATA3_TO_GATA3mRNA])  ) * Activ1Inhib3(x[IL4],params[KIL4_TO_GATA3mRNA],params[NIL4_TO_GATA3mRNA], params[SIL4_TO_GATA3mRNA],x[TBET],params[KTBET_TO_GATA3mRNA],params[NTBET_TO_GATA3mRNA], params[STBET_TO_GATA3mRNA],x[RORGT],params[KRORGT_TO_GATA3mRNA],params[NRORGT_TO_GATA3mRNA], params[SRORGT_TO_GATA3mRNA],x[FOXP3],params[KFOXP3_TO_GATA3mRNA],params[NFOXP3_TO_GATA3mRNA], params[SFOXP3_TO_GATA3mRNA]));

    //double activTBET =      params[CIL2_TBETmRNA] * x[openTBET] * Activ1Inhib0(x[IL2], params[KIL2_TO_TBETmRNA], params[NIL2_TO_TBETmRNA], params[SIL2_TO_TBETmRNA]) * Activ2Inhib3(x[IL12],params[KIL12_TO_TBETmRNA],params[NIL12_TO_TBETmRNA], params[SIL12_TO_TBETmRNA],x[IFNG],params[KIFNG_TO_TBETmRNA],params[NIFNG_TO_TBETmRNA], params[SIFNG_TO_TBETmRNA],x[GATA3],params[KGATA3_TO_TBETmRNA],params[NGATA3_TO_TBETmRNA], params[SGATA3_TO_TBETmRNA],x[RORGT],params[KRORGT_TO_TBETmRNA],params[NRORGT_TO_TBETmRNA], params[SRORGT_TO_TBETmRNA],x[FOXP3],params[KFOXP3_TO_TBETmRNA],params[NFOXP3_TO_TBETmRNA], params[SFOXP3_TO_TBETmRNA]);
    //if(!over(openTBET))     dxdt[openTBET]  = params[RESCALE_FACTOR] * /* activTBET * */ x[openTBET] * (1 - x[openTBET]);
    if(!over(TBETmRNA))		dxdt[TBETmRNA] 	= (- params[KDTBETmRNA] * x[TBETmRNA]  +  params[CTBETmRNA]  * (1 + params[KTCRTBET] * tcrval) * (((x[IL6] > 0.5) ? params[FIL6_TO_TBETmRNA] : 1.0) + Activ2Inhib3(x[IL12],params[KIL12_TO_TBETmRNA],params[NIL12_TO_TBETmRNA], params[SIL12_TO_TBETmRNA],x[IFNG],params[KIFNG_TO_TBETmRNA],params[NIFNG_TO_TBETmRNA], params[SIFNG_TO_TBETmRNA],x[GATA3],params[KGATA3_TO_TBETmRNA],params[NGATA3_TO_TBETmRNA], params[SGATA3_TO_TBETmRNA],x[RORGT],params[KRORGT_TO_TBETmRNA],params[NRORGT_TO_TBETmRNA], params[SRORGT_TO_TBETmRNA],x[FOXP3],params[KFOXP3_TO_TBETmRNA],params[NFOXP3_TO_TBETmRNA], params[SFOXP3_TO_TBETmRNA])));
    double activRORGT =     ((x[IL6] > 0.5) ? params[FIL6_TO_RORGTmRNA] : 1.0) * Activ2Inhib3(x[IL21],params[KIL21_TO_RORGTmRNA],params[NIL21_TO_RORGTmRNA], params[SIL21_TO_RORGTmRNA],x[TGFB],params[KTGFB_TO_RORGTmRNA],params[NTGFB_TO_RORGTmRNA], params[STGFB_TO_RORGTmRNA],x[TBET],params[KTBET_TO_RORGTmRNA],params[NTBET_TO_RORGTmRNA], params[STBET_TO_RORGTmRNA],x[GATA3],params[KGATA3_TO_RORGTmRNA],params[NGATA3_TO_RORGTmRNA], params[SGATA3_TO_RORGTmRNA],x[FOXP3],params[KFOXP3_TO_RORGTmRNA],params[NFOXP3_TO_RORGTmRNA], params[SFOXP3_TO_RORGTmRNA]);
    if(!over(openRORGT))    dxdt[openRORGT] = params[FORCERORGT] * /* activRORGT * */ x[openRORGT] * (1 - x[openRORGT]);
    if(!over(RORGTmRNA))	dxdt[RORGTmRNA] = (- params[KDRORGTmRNA]* x[RORGTmRNA] + params[CRORGTmRNA] * x[openRORGT] * activRORGT);
    double activFOXP3 =     Activ2Inhib3(x[IL2],params[KIL2_TO_FOXP3mRNA],params[NIL2_TO_FOXP3mRNA], params[SIL2_TO_FOXP3mRNA],x[TGFB],params[KTGFB_TO_FOXP3mRNA],params[NTGFB_TO_FOXP3mRNA], params[STGFB_TO_FOXP3mRNA],x[TBET],params[KTBET_TO_FOXP3mRNA],params[NTBET_TO_FOXP3mRNA], params[STBET_TO_FOXP3mRNA],x[GATA3],params[KGATA3_TO_FOXP3mRNA],params[NGATA3_TO_FOXP3mRNA], params[SGATA3_TO_FOXP3mRNA],x[RORGT],params[KRORGT_TO_FOXP3mRNA],params[NRORGT_TO_FOXP3mRNA], params[SRORGT_TO_FOXP3mRNA]);
    if(!over(openFOXP3))    dxdt[openFOXP3] = params[FORCEFOXP3] * /* activFOXP3 * */ x[openFOXP3] * (1 - x[openFOXP3]);
    if(!over(FOXP3mRNA))	dxdt[FOXP3mRNA] = (- params[KDFOXP3mRNA]* x[FOXP3mRNA] + params[CFOXP3mRNA] * x[openFOXP3] * activFOXP3);

    if(!over(transl))       dxdt[transl]    = params[FORCETRANSL] * x[transl] * (1 - x[transl]);
    if(!over(secret))       dxdt[secret]    = params[FORCESECRET] * x[secret] * (1 - x[secret]);

    if(!over(antiIFNg))     dxdt[antiIFNg]  = - params[KANTI_IFNG]* x[IFNG]* x[antiIFNg] ; // + 1e-6 * (init[antiIFNg] - params[antiIFNg]);  //- params[KANTI_IFNG] * dxdt[IFNG] * init[antiIFNg] / ((params[KANTI_IFNG] * x[IFNG] + 1) * (params[KANTI_IFNG] * x[IFNG] + 1));
    if(!over(antiIL2))      dxdt[antiIL2]   = - params[KANTI_IL2] * x[IL2] * x[antiIL2] ;  //- params[KANTI_IFNG] * dxdt[IFNG] * init[antiIFNg] / ((params[KANTI_IFNG] * x[IFNG] + 1) * (params[KANTI_IFNG] * x[IFNG] + 1));
    if(!over(antiIL4))      dxdt[antiIL4]   = - params[KANTI_IL4] * x[IL4] * x[antiIL4] ;  //- params[KANTI_IFNG] * dxdt[IFNG] * init[antiIFNg] / ((params[KANTI_IFNG] * x[IFNG] + 1) * (params[KANTI_IFNG] * x[IFNG] + 1));


}







