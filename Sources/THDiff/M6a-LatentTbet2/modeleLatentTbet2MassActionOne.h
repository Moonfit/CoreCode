// ------- Automatically generated model -------- //
#ifndef modeleLatentTbet2MAO_H
#define modeleLatentTbet2MAO_H
#include "../Moonfit/moonfit.h"
#include "../namesTh.h"

/* This model has  unknown parameters, +  */
struct modeleLatentTbet2MassActionOne : public Model {
    modeleLatentTbet2MassActionOne();
    enum {IL2, IL4, IL6, IL12, IL17, IL21, IFNG, TGFB, TBET, GATA3, RORGT, FOXP3, IL2mRNA, IL4mRNA, IL17mRNA, IL21mRNA, IFNGmRNA, TGFBmRNA, TBETmRNA, GATA3mRNA, RORGTmRNA, FOXP3mRNA, TCR, antiIL4, antiIFNg, antiIL2, openIL2, openIL21, openFOXP3, openRORGT, openTBET, transl, secret, NBVAR};
    enum {TCRPEAK, TCRCOEFF, KTCRGATA3, KTCRIL2, KTCRGATA3POS, KTCRTGFB, KTCRTBET, KTCRIFNG, KDIL2, KDIL4, KDIL12, KDIL17, KDIL21, KDIFNG, KDTGFB, KDTBET, KDGATA3, KDRORGT, KDFOXP3, KDIL2mRNA, KDIL4mRNA, KDIL17mRNA, KDIL21mRNA, KDIFNGmRNA, KDTGFBmRNA, KDTBETmRNA, KDGATA3mRNA, KDRORGTmRNA, KDFOXP3mRNA, PIL2, PIL4, PIL17, PIL21, PIFNG, PTGFB, PTBET, PGATA3, PRORGT, PFOXP3, CTGFBmRNA, CIL2mRNA, SIL2_TO_IL2mRNA, KIL2_TO_IL2mRNA, NIL2_TO_IL2mRNA, SIL4_TO_IL2mRNA, KIL4_TO_IL2mRNA, NIL4_TO_IL2mRNA,  CIL4mRNA, SGATA3_TO_IL4mRNA, KGATA3_TO_IL4mRNA, NGATA3_TO_IL4mRNA, CIL17mRNA, SRORGT_TO_IL17mRNA, KRORGT_TO_IL17mRNA, NRORGT_TO_IL17mRNA, CIL21mRNA, FIL6_TO_IL21mRNA, SRORGT_TO_IL21mRNA, KRORGT_TO_IL21mRNA, NRORGT_TO_IL21mRNA, CIFNGmRNA, FIL12_TO_IFNGmRNA, STBET_TO_IFNGmRNA, KTBET_TO_IFNGmRNA, NTBET_TO_IFNGmRNA, CTBETmRNA, FIL6_TO_TBETmRNA, /*CIL2_TBETmRNA, KIL2_TO_TBETmRNA, NIL2_TO_TBETmRNA, SIL2_TO_TBETmRNA, */ SIL12_TO_TBETmRNA, KIL12_TO_TBETmRNA, NIL12_TO_TBETmRNA, SIFNG_TO_TBETmRNA, KIFNG_TO_TBETmRNA, NIFNG_TO_TBETmRNA, SGATA3_TO_TBETmRNA, KGATA3_TO_TBETmRNA, NGATA3_TO_TBETmRNA, SRORGT_TO_TBETmRNA, KRORGT_TO_TBETmRNA, NRORGT_TO_TBETmRNA, SFOXP3_TO_TBETmRNA, KFOXP3_TO_TBETmRNA, NFOXP3_TO_TBETmRNA, CGATA3mRNA, FIL12_TO_GATA3mRNA, SIL2_TO_GATA3mRNA, KIL2_TO_GATA3mRNA, NIL2_TO_GATA3mRNA, SIL4_TO_GATA3mRNA, KIL4_TO_GATA3mRNA, NIL4_TO_GATA3mRNA, SIFNG_TO_GATA3mRNA, KIFNG_TO_GATA3mRNA, NIFNG_TO_GATA3mRNA, STBET_TO_GATA3mRNA, KTBET_TO_GATA3mRNA, NTBET_TO_GATA3mRNA, SGATA3_TO_GATA3mRNA, KGATA3_TO_GATA3mRNA, NGATA3_TO_GATA3mRNA, SRORGT_TO_GATA3mRNA, KRORGT_TO_GATA3mRNA, NRORGT_TO_GATA3mRNA, SFOXP3_TO_GATA3mRNA, KFOXP3_TO_GATA3mRNA, NFOXP3_TO_GATA3mRNA, CRORGTmRNA, FIL6_TO_RORGTmRNA, SIL21_TO_RORGTmRNA, KIL21_TO_RORGTmRNA, NIL21_TO_RORGTmRNA, STGFB_TO_RORGTmRNA, KTGFB_TO_RORGTmRNA, NTGFB_TO_RORGTmRNA, STBET_TO_RORGTmRNA, KTBET_TO_RORGTmRNA, NTBET_TO_RORGTmRNA, SGATA3_TO_RORGTmRNA, KGATA3_TO_RORGTmRNA, NGATA3_TO_RORGTmRNA, SFOXP3_TO_RORGTmRNA, KFOXP3_TO_RORGTmRNA, NFOXP3_TO_RORGTmRNA, CFOXP3mRNA, SIL2_TO_FOXP3mRNA, KIL2_TO_FOXP3mRNA, NIL2_TO_FOXP3mRNA, STGFB_TO_FOXP3mRNA, KTGFB_TO_FOXP3mRNA, NTGFB_TO_FOXP3mRNA, STBET_TO_FOXP3mRNA, KTBET_TO_FOXP3mRNA, NTBET_TO_FOXP3mRNA, SGATA3_TO_FOXP3mRNA, KGATA3_TO_FOXP3mRNA, NGATA3_TO_FOXP3mRNA, SRORGT_TO_FOXP3mRNA, KRORGT_TO_FOXP3mRNA, NRORGT_TO_FOXP3mRNA, IL2EQ, IL4EQ, IL12EQ, IL17EQ, IL21EQ, IFNGEQ, TGFBEQ, TBETEQ, GATA3EQ, RORGTEQ, FOXP3EQ, IL2mRNAEQ, IL4mRNAEQ, IL17mRNAEQ, IL21mRNAEQ, IFNGmRNAEQ, TGFBmRNAEQ, TBETmRNAEQ, GATA3mRNAEQ, RORGTmRNAEQ, FOXP3mRNAEQ, IL4INITTH2, IL2INITTREG,  FORCEIL2, FORCEIL21, FORCEFOXP3, FORCERORGT, FORCETBET, FORCETRANSL, FORCESECRET, KANTI_IL2, KANTI_IFNG, KANTI_IL4, NBPARAM};

    long long background;
	void derivatives(const vector<double> &x, vector<double> &dxdt, const double t);
    void initialise(long long _background = Back::WT);
	void setBaseParameters();

	double Activ0Inhib1(double Val1, double K1, double N1, double S1){
        Val1 = max(Val1, 0.0);
        //if((Val1 < 0.0)) return 1.0;
        //else
        return ((S1 + (1.0 - S1) * ( pow(K1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) ) )); }
	double Activ1Inhib0(double Val1, double K1, double N1, double S1){
        Val1 = max(Val1, 0.0);
        //if((Val1 < 0.0)) return 1.0;
        //else
        return ((1.0 + (S1 - 1.0) * (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) ) )*1); }
    double Activ0Inhib2(double Val1, double K1, double N1, double S1,double Val2, double K2, double N2, double S2){
        Val1 = max(Val1, 0.0);
        Val2 = max(Val2, 0.0);
        return ((S1 + (1.0 - S1) * ( pow(K1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) ) ) * (S2 + (1.0 - S2) * ( pow(K2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) ) )); }
    double Activ1Inhib3(double Val1, double K1, double N1, double S1,double Val2, double K2, double N2, double S2,double Val3, double K3, double N3, double S3,double Val4, double K4, double N4, double S4){
            Val1 = max(Val1, 0.0);
            Val2 = max(Val2, 0.0);
            Val3 = max(Val3, 0.0);
            Val4 = max(Val4, 0.0);
            return ((1.0 + (S1 - 1.0) * (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) ) )*(S2 + (1.0 - S2) * ( pow(K2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) ) ) * (S3 + (1.0 - S3) * ( pow(K3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) ) ) * (S4 + (1.0 - S4) * ( pow(K4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) ) )); }
    double Activ2Inhib3(double Val1, double K1, double N1, double S1,double Val2, double K2, double N2, double S2,double Val3, double K3, double N3, double S3,double Val4, double K4, double N4, double S4,double Val5, double K5, double N5, double S5){
        Val1 = max(Val1, 0.0);
        Val2 = max(Val2, 0.0);
        Val3 = max(Val3, 0.0);
        Val4 = max(Val4, 0.0);
        Val5 = max(Val5, 0.0);
        //if((Val1 < 0.0) || (Val2 < 0.0) || (Val3 < 0.0) || (Val4 < 0.0) || (Val5 < 0.0)) return 1.0;
        //else
        return ((1.0 + (S1 - 1.0) * (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) ) )*(1.0 + (S2 - 1.0) * (pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) ) )*(S3 + (1.0 - S3) * ( pow(K3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) ) ) * (S4 + (1.0 - S4) * ( pow(K4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) ) ) * (S5 + (1.0 - S5) * ( pow(K5, N5) / ( pow(K5, N5) + pow(Val5, N5) ) ) )); }
	double Activ3Inhib3(double Val1, double K1, double N1, double S1,double Val2, double K2, double N2, double S2,double Val3, double K3, double N3, double S3,double Val4, double K4, double N4, double S4,double Val5, double K5, double N5, double S5,double Val6, double K6, double N6, double S6){
        Val1 = max(Val1, 0.0);
        Val2 = max(Val2, 0.0);
        Val3 = max(Val3, 0.0);
        Val4 = max(Val4, 0.0);
        Val5 = max(Val5, 0.0);
        Val6 = max(Val6, 0.0);
        //if((Val1 < 0.0) || (Val2 < 0.0) || (Val3 < 0.0) || (Val4 < 0.0) || (Val5 < 0.0) || (Val6 < 0.0)) return 1.0;
        //else
        return ((1.0 + (S1 - 1.0) * (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) ) )*(1.0 + (S2 - 1.0) * (pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) ) )*(1.0 + (S3 - 1.0) * (pow(Val3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) ) )*(S4 + (1.0 - S4) * ( pow(K4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) ) ) * (S5 + (1.0 - S5) * ( pow(K5, N5) / ( pow(K5, N5) + pow(Val5, N5) ) ) ) * (S6 + (1.0 - S6) * ( pow(K6, N6) / ( pow(K6, N6) + pow(Val6, N6) ) ) )); }
    double Activ3Inhib4(double Val1, double K1, double N1, double S1,double Val2, double K2, double N2, double S2,double Val3, double K3, double N3, double S3,double Val4, double K4, double N4, double S4,double Val5, double K5, double N5, double S5,double Val6, double K6, double N6, double S6,double Val7, double K7, double N7, double S7){
        Val1 = max(Val1, 0.0);
        Val2 = max(Val2, 0.0);
        Val3 = max(Val3, 0.0);
        Val4 = max(Val4, 0.0);
        Val5 = max(Val5, 0.0);
        Val6 = max(Val6, 0.0);
        Val7 = max(Val7, 0.0);
        return ((1.0 + (S1 - 1.0) * (pow(Val1, N1) / ( pow(K1, N1) + pow(Val1, N1) ) ) )*(1.0 + (S2 - 1.0) * (pow(Val2, N2) / ( pow(K2, N2) + pow(Val2, N2) ) ) )*(1.0 + (S3 - 1.0) * (pow(Val3, N3) / ( pow(K3, N3) + pow(Val3, N3) ) ) )*(S4 + (1.0 - S4) * ( pow(K4, N4) / ( pow(K4, N4) + pow(Val4, N4) ) ) ) * (S5 + (1.0 - S5) * ( pow(K5, N5) / ( pow(K5, N5) + pow(Val5, N5) ) ) ) * (S6 + (1.0 - S6) * ( pow(K6, N6) / ( pow(K6, N6) + pow(Val6, N6) ) ) ) * (S7 + (1.0 - S7) * ( pow(K7, N7) / ( pow(K7, N7) + pow(Val7, N7) ) ) )); }
    void action(string name, double parameter){
        if(!name.compare("wash")){
            if((parameter > 1.0) || (parameter < 0)) {cerr << "ERR: ModeleMinLatent::action(" << name << ", " << parameter << "), wrong parameter value\n"; return;}
            val[IL2] =    (1 - parameter) * val[IL2];
            val[IL4] =    (1 - parameter) * val[IL4];
            val[IL6] =    (1 - parameter) * val[IL6];
            val[IL12] =   (1 - parameter) * val[IL12];
            val[IL17] =   (1 - parameter) * val[IL17];
            val[IFNG] =   (1 - parameter) * val[IFNG];
            val[TGFB] =   (1 - parameter) * val[TGFB];
            val[antiIFNg]=(1 - parameter) * val[antiIFNg];
            val[antiIL2] =(1 - parameter) * val[antiIL2];
            val[antiIL4] =(1 - parameter) * val[antiIL4];
            return;
        }
        if(!name.compare("correctProductionByFactor")){
            if((parameter > 1000.0) || (parameter < 0)) {cerr << "ERR: ModeleMinLatent::action(" << name << ", " << parameter << "), wrong parameter value (0-->1000)\n"; return;}
            params[PIL2] *= parameter;
            params[PIL4] *= parameter;
            params[PIL17]*= parameter;
            params[PIL21]*= parameter;
            params[PIFNG]*= parameter;
            params[PTGFB]*= parameter;
            return;
        }
        if(!name.compare("cycloheximidine")){
            if((parameter > 10.0) || (parameter < 0)) {cerr << "ERR: ModeleMinLatent::action(" << name << ", " << parameter << "), wrong parameter value\n"; return;}
            params[PIL2] *= parameter;
            params[PIL4] *= parameter;
            params[PIL17] *= parameter;
            params[PIL21] *= parameter;
            params[PIFNG] *= parameter;
            params[PTGFB] *= parameter;
            params[PTBET] *= parameter;
            params[PGATA3] *= parameter;
            params[PRORGT] *= parameter;
            params[PFOXP3] *= parameter;
            return;
        }
        if(!name.compare("transcriptionInhibition")){
            if((parameter > 10.0) || (parameter < 0)) {cerr << "ERR: ModeleMinLatent::action(" << name << ", " << parameter << "), wrong parameter value\n"; return;}
            params[CTGFBmRNA] *= parameter;
            params[CIL2mRNA] *= parameter;
            params[CIL4mRNA] *= parameter;
            params[CIL17mRNA] *= parameter;
            params[CIL21mRNA] *= parameter;
            params[CIFNGmRNA] *= parameter;
            params[CTBETmRNA] *= parameter;
            params[CGATA3mRNA] *= parameter;
            params[CRORGTmRNA] *= parameter;
            params[CFOXP3mRNA] *= parameter;
            return;
        }
        if(!name.compare("InhibTCR")){
//            params[] = 0;
        }
        if(!name.compare("induceBackground")){
            cerr << "ERR: ModeleLatentTbet2, the action 'induceBackground' requires more than one argument. Please use the function action(string, vector<double>) " << endl;
            return;
        }
        cerr << "ERR: ModeleLatentTbet2::action(string, double), no defined action for '" << name << "'\n";
    }
/*    void action(string name, vector<double> compactParameter){
        if(!name.compare("induceBackground")){
            if(compactParameter.size() != 2) {cerr << "ERR: action(induceBackground, vector<double>), the vector shouls have 2 arguments (efficiency and backgroundID)" << endl; return;}
            double efficiency = compactParameter[0];
            double backgroundID = compactParameter[1];







            return;
        }
        cerr << "ERR: ModeleLatentTbet2::action(string, vector<double>), no defined action for '" << name << "'\n";
    }*/
};
#endif

