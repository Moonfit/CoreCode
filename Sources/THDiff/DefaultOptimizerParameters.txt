######################################### OPTIMIZER PART ##################################

geneticAlgorithm 14		# New syntax : IdOptimizer TypeScaling NbArgsForScaling ArgsScaling NbArgstoOptimizer
21	# 0 = CEP 	1 = SSGA 	2 = GGA		+10 = test Mut/Cross	+20 = test Pop / %mut
7	# Selection parents	1Rank	2Random	3456Tournament5710725750	7-12Prop	
7	-1	# Cross-Over
7	# Mutation
0	# Replacement policy
0	# selection policy
7	0.005	# Strategy
1	# Num tries
80000	# Num Calls
80	# Population size
0.1	# x=Proportion of cross-over. for CEP,  x cross-over and 1-x mutations. For SSGA or GGA, popSize*x cross-overs
1.0	# fork coefficient (lambda/mu)

42    		   # Number of parameters to be optimized (listed below with initial boundaries)
0 1e-1 1
1 1e-1 1
2 1e-1 1
3 1e-1 1
4 1e-1 1
5 1e-1 1
6 1e-1 1
7 1e-1 1
8 1e-1 1
9 1e-1 1
10 1e-1 1
11 1e-1 1
12 1e-1 1
13 1e-1 1
14 1e-1 1
15 1e-1 1
16 1e-1 1
17 1e-1 1
18 1e-1 1
19 1e-1 1
20 1e-1 1
21 1e-1 1
22 1e-1 1
23 1e-1 1
24 1e-1 1
25 1e-1 1
26 1e-1 1
27 1e-1 1
28 1e-1 1
29 1e-1 1
30 1e-1 1
31 1e-1 1
32 1e-1 1
33 1e-1 1
34 1e-1 1
35 1e-1 1
36 1e-1 1
37 1e-1 1
38 1e-1 1
39 1e-1 1
40 1e-1 1
41 1e-1 1

0     	       	   # Number of initial parameter sets to be read (typically none, listed below)

ExponentialBounded	5
-12 12 0.000001 1000000 0.8