#include "mainboard.h"
#include "ui_mainboard.h"
#include "wholethymus.h"

MainBoard::MainBoard(multiPopSimulation *_sim, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainBoard)
{
    ui->setupUi(this);
    sim = _sim;

    ui->lcdNumber->hide();
    ui->lcdNumber_2->hide();
    ui->lcdNumber_3->hide();
    ui->lcdNumber_4->hide();
    ui->lcdNumber_5->hide();
    ui->lcdNumber_6->hide();
    ui->lcdNumber_7->hide();
    ui->lcdNumber_8->hide();
    ui->lcdNumber_9->hide();
    ui->lcdNumber_10->hide();
    ui->lcdNumber_11->hide();
    ui->lcdNumber_12->hide();
    ui->lcdNumber_13->hide();

    plotDynDNs = new grapheCustom(ui->widgetDynDNs);
    plotDynAllPops = new grapheCustom(ui->widgetDynPops);
    plotDynBrdu = new grapheCustom(ui->widgetDynBRDU);
    plotDoseBrdu = new grapheCustom(ui->widgetDoses);

    plotDynDNs->setNbCurves(4);
    plotDynAllPops->setNbCurves(6);
    plotDynBrdu->setNbCurves(6);
    plotDoseBrdu->setNbCurves(2);

    plotDynDNs->setTitle(QString("DN populations"));
    plotDynAllPops->setTitle(QString("Major populations"));
    plotDynBrdu->setTitle(QString("% BRDU in each population"));
    plotDoseBrdu->setTitle(QString("Dose BRDU and EDU"));

    ui->doubleSpinBoxStabilizationTime->setValue(sim->preSimTime); // 25 days
    ui->doubleSpinBoxTimeBRDU->setValue(sim->timeBRDU);
    ui->doubleSpinBoxDurationBRDU->setValue(sim->durationBRDU);
    ui->doubleSpinBoxTimeEDU->setValue(sim->timeEDU);
    ui->doubleSpinBoxDurationEDU->setValue(sim->durationEDU);
    ui->doubleSpinBoxTimeAnalysis->setValue(sim->timeSimulation); // hours
    ui->doubleSpinBoxApoptoticTime->setValue(sim->apoptoticTime); // 6 mins, not very useful now, but for later on dead cells

    ui->checkBoxSimDN1->setChecked(sim->simulatedStages[DN1]);
    ui->checkBoxSimDN2->setChecked(sim->simulatedStages[DN2]);
    ui->checkBoxSimDN3->setChecked(sim->simulatedStages[DN3]);
    ui->checkBoxSimDN4->setChecked(sim->simulatedStages[DN4]);
    ui->checkBoxSimeDP->setChecked(sim->simulatedStages[eDP]);
    ui->checkBoxSimlDP->setChecked(sim->simulatedStages[lDP]);
    ui->checkBoxSimSP4->setChecked(sim->simulatedStages[SP4]);
    ui->checkBoxSimSP8->setChecked(sim->simulatedStages[SP8]);
    ui->checkBoxSimTreg->setChecked(sim->simulatedStages[Tregs]);

    ui->doubleSpinBoxInitDN1->setValue(sim->initialNumbersPerPop[DN1]);
    ui->doubleSpinBoxInitDN2->setValue(sim->initialNumbersPerPop[DN2]);
    ui->doubleSpinBoxInitDN3->setValue(sim->initialNumbersPerPop[DN3]);
    ui->doubleSpinBoxInitDN4->setValue(sim->initialNumbersPerPop[DN4]);
    ui->doubleSpinBoxIniteDP->setValue(sim->initialNumbersPerPop[eDP]);
    ui->doubleSpinBoxInitlDP->setValue(sim->initialNumbersPerPop[lDP]);
    ui->doubleSpinBoxInitSP4->setValue(sim->initialNumbersPerPop[SP4]);
    ui->doubleSpinBoxInitSP8->setValue(sim->initialNumbersPerPop[SP8]);
    ui->doubleSpinBoxInitTreg->setValue(sim->initialNumbersPerPop[Tregs]);

    ui->doubleSpinBoxInflowDN1->setValue(sim->additionalInflow[DN1]);
    ui->doubleSpinBoxInflowDN2->setValue(sim->additionalInflow[DN2]);
    ui->doubleSpinBoxInflowDN3->setValue(sim->additionalInflow[DN3]);
    ui->doubleSpinBoxInflowDN4->setValue(sim->additionalInflow[DN4]);
    ui->doubleSpinBoxInfloweDP->setValue(sim->additionalInflow[eDP]);
    ui->doubleSpinBoxInflowlDP->setValue(sim->additionalInflow[lDP]);
    ui->doubleSpinBoxInflowSP4->setValue(sim->additionalInflow[SP4]);
    ui->doubleSpinBoxInflowSP8->setValue(sim->additionalInflow[SP8]);
    ui->doubleSpinBoxInflowTreg->setValue(sim->additionalInflow[Tregs]);

    ui->doubleSpinBoxReducDN1->setValue(sim->rescalingFactors[DN1]);
    ui->doubleSpinBoxReducDN2->setValue(sim->rescalingFactors[DN2]);
    ui->doubleSpinBoxReducDN3->setValue(sim->rescalingFactors[DN3]);
    ui->doubleSpinBoxReducDN4->setValue(sim->rescalingFactors[DN4]);
    ui->doubleSpinBoxReduceeDP->setValue(sim->rescalingFactors[eDP]);
    ui->doubleSpinBoxReducelDP->setValue(sim->rescalingFactors[lDP]);
    ui->doubleSpinBoxReducSPs->setValue(sim->rescalingFactors[SP4]);

    ui->checkBoxSynchronizedInflow->setChecked(sim->synchronizeInflow);

    ui->doubleSpinBoxDt->setValue(sim->dt);
}

void MainBoard::applyParamsToSimu(){


    sim->preSimTime         = ui->doubleSpinBoxStabilizationTime->value(); // 25 days
    sim->timeBRDU           = ui->doubleSpinBoxTimeBRDU->value();
    sim->durationBRDU       = ui->doubleSpinBoxDurationBRDU->value();
    sim->timeEDU            = ui->doubleSpinBoxTimeEDU->value();
    sim->durationEDU        = ui->doubleSpinBoxDurationEDU->value();
    sim->timeSimulation     = ui->doubleSpinBoxTimeAnalysis->value(); // hours
    sim->apoptoticTime      = ui->doubleSpinBoxApoptoticTime->value(); // 6 mins, not very useful now, but for later on dead cells

    sim->initialNumbersPerPop[DN1]    = ui->doubleSpinBoxInitDN1->value();
    sim->initialNumbersPerPop[DN2]    = ui->doubleSpinBoxInitDN2->value();
    sim->initialNumbersPerPop[DN3]    = ui->doubleSpinBoxInitDN3->value();
    sim->initialNumbersPerPop[DN4]    = ui->doubleSpinBoxInitDN4->value();
    sim->initialNumbersPerPop[eDP]    = ui->doubleSpinBoxIniteDP->value();
    sim->initialNumbersPerPop[lDP]    = ui->doubleSpinBoxInitlDP->value();
    sim->initialNumbersPerPop[SP4]    = ui->doubleSpinBoxInitSP4->value();
    sim->initialNumbersPerPop[SP8]    = ui->doubleSpinBoxInitSP8->value();
    sim->initialNumbersPerPop[Tregs]  = ui->doubleSpinBoxInitTreg->value();

    sim->additionalInflow[DN1]  = ui->doubleSpinBoxInflowDN1->value();
    sim->additionalInflow[DN2]  = ui->doubleSpinBoxInflowDN2->value();
    sim->additionalInflow[DN3]  = ui->doubleSpinBoxInflowDN3->value();
    sim->additionalInflow[DN4]  = ui->doubleSpinBoxInflowDN4->value();
    sim->additionalInflow[eDP]  = ui->doubleSpinBoxInfloweDP->value();
    sim->additionalInflow[lDP]  = ui->doubleSpinBoxInflowlDP->value();
    sim->additionalInflow[SP4]  = ui->doubleSpinBoxInflowSP4->value();
    sim->additionalInflow[SP8]  = ui->doubleSpinBoxInflowSP8->value();
    sim->additionalInflow[Tregs] =ui->doubleSpinBoxInflowTreg->value();

    sim->rescalingFactors[DN1]  = ui->doubleSpinBoxReducDN1->value();
    sim->rescalingFactors[DN2]  = ui->doubleSpinBoxReducDN2->value();
    sim->rescalingFactors[DN3]  = ui->doubleSpinBoxReducDN3->value();
    sim->rescalingFactors[DN4]  = ui->doubleSpinBoxReducDN4->value();
    sim->rescalingFactors[eDP]  = ui->doubleSpinBoxReduceeDP->value();
    sim->rescalingFactors[lDP]  = ui->doubleSpinBoxReducelDP->value();
    sim->rescalingFactors[SP4]  = ui->doubleSpinBoxReducSPs->value();

    sim->simulatedStages[DN1]   = ui->checkBoxSimDN1->isChecked();
    sim->simulatedStages[DN2]   = ui->checkBoxSimDN2->isChecked();
    sim->simulatedStages[DN3]   = ui->checkBoxSimDN3->isChecked();
    sim->simulatedStages[DN4]   = ui->checkBoxSimDN4->isChecked();
    sim->simulatedStages[eDP]   = ui->checkBoxSimeDP->isChecked();
    sim->simulatedStages[lDP]   = ui->checkBoxSimlDP->isChecked();
    sim->simulatedStages[SP4]   = ui->checkBoxSimSP4->isChecked();
    sim->simulatedStages[SP8]   = ui->checkBoxSimSP8->isChecked();
    sim->simulatedStages[Tregs]   = ui->checkBoxSimTreg->isChecked();

    sim->synchronizeInflow = ui->checkBoxSynchronizedInflow->isChecked();

    sim->dt = ui->doubleSpinBoxDt->value();

    //AnalysisPoints.resize(10, 0);
    //AnalysisPoints = {2,3,4,5,6,8,10,12,16,20};

    // ratiolDPtoSP4 = 0.3;
    // ratiolDPtoSP8 = 1 - ratiolDPtoSP4;
    // ratioOfSP4InflowToTregs = 0.05; // 5% of eDP to SP4 will become Tregs
}

MainBoard::~MainBoard()
{
    delete ui;
}

void MainBoard::display(masterObserver* newMO){

    plotDynDNs->Plot(0, newMO->o_popDN1.variable, newMO->o_popDN1.time, QString("DN1"), Qt::darkGray);
    plotDynDNs->Plot(1, newMO->o_popDN2.variable, newMO->o_popDN2.time, QString("DN2"), Qt::blue);
    plotDynDNs->Plot(2, newMO->o_popDN3.variable, newMO->o_popDN3.time, QString("DN3"), Qt::green);
    plotDynDNs->Plot(3, newMO->o_popDN4.variable, newMO->o_popDN4.time, QString("DN4"), Qt::red);

    plotDynAllPops->Plot(0, newMO->o_popDNTot.variable, newMO->o_popDNTot.time, QString("DNs"), Qt::red);
    plotDynAllPops->Plot(1, newMO->o_popeDP.variable, newMO->o_popeDP.time, QString("eDP"), Qt::blue);
    plotDynAllPops->Plot(2, newMO->o_poplDP.variable, newMO->o_poplDP.time, QString("lDP"), Qt::green);
    plotDynAllPops->Plot(3, newMO->o_popSP4.variable, newMO->o_popSP4.time, QString("SP4"), Qt::darkRed);
    plotDynAllPops->Plot(4, newMO->o_popSP8.variable, newMO->o_popSP8.time, QString("SP8"), Qt::darkBlue);
    plotDynAllPops->Plot(5, newMO->o_popTreg.variable, newMO->o_popTreg.time, QString("Treg"), Qt::darkGreen);

    plotDynBrdu->Plot(0, newMO->o_prcBRDUDNTot.variable, newMO->o_prcBRDUDNTot.time, QString("DNs"), Qt::red);
    plotDynBrdu->Plot(1, newMO->o_prcBRDUeDP.variable, newMO->o_prcBRDUeDP.time, QString("eDP"), Qt::blue);
    plotDynBrdu->Plot(2, newMO->o_prcBRDUlDP.variable, newMO->o_prcBRDUlDP.time, QString("lDP"), Qt::green);
    plotDynBrdu->Plot(3, newMO->o_prcBRDUSP4.variable, newMO->o_prcBRDUSP4.time, QString("SP4"), Qt::darkRed);
    plotDynBrdu->Plot(4, newMO->o_prcBRDUSP8.variable, newMO->o_prcBRDUSP8.time, QString("SP8"), Qt::darkBlue);
    plotDynBrdu->Plot(5, newMO->o_prcBRDUTreg.variable, newMO->o_prcBRDUTreg.time, QString("Treg"), Qt::darkGreen);

    plotDoseBrdu->Plot(0, newMO->o_doseBRDU.variable, newMO->o_doseBRDU.time, QString("DoseBRDU"), Qt::red);
    plotDoseBrdu->Plot(1, newMO->o_doseEDU.variable, newMO->o_doseEDU.time, QString("DoseBRDU"), Qt::blue);

    //plotDynBrdu->Plot(0, newMO->o_popRED.variable, newMO->o_popRED.time, QString("[RED]"), Qt::darkRed);
    //grapheCyclePost->Plot(1, newMO->o_popAbsGFP.variable, newMO->o_popAbsGFP.time, QString("Abs GFP"), Qt::green);
    //grapheCyclePre->Plot(1, newMO->o_popAbsRED.variable, newMO->o_popAbsRED.time, QString("Abs Red"), Qt::red);
    //grapheCycleMixed->Plot(0, newMO->o_popRatio.variable, newMO->o_popRatio.time, QString("Ratio"), Qt::darkGray);

    //grapheGFP->rescaleY(0., 5.0);
    //grapheRFP->rescaleY(0, 5.0);
    //grapheRatio->rescaleY(0, 1.0);

    //>Plot(IDrepeat, listObservers[i]->variable, listObservers[i]->time, QString("Curve ") + QString::number(i+1), plots[i]->baseList(IDrepeat));
}

void MainBoard::enable(bool enable){
    ui->doubleSpinBoxStabilizationTime->setEnabled(enable);
    ui->doubleSpinBoxTimeBRDU->setEnabled(enable);
    ui->doubleSpinBoxDurationBRDU->setEnabled(enable);
    ui->doubleSpinBoxTimeEDU->setEnabled(enable);
    ui->doubleSpinBoxDurationEDU->setEnabled(enable);
    ui->doubleSpinBoxTimeAnalysis->setEnabled(enable);
    ui->doubleSpinBoxApoptoticTime->setEnabled(enable);

    ui->doubleSpinBoxInitDN1->setEnabled(enable);
    ui->doubleSpinBoxInitDN2->setEnabled(enable);
    ui->doubleSpinBoxInitDN3->setEnabled(enable);
    ui->doubleSpinBoxInitDN4->setEnabled(enable);
    ui->doubleSpinBoxIniteDP->setEnabled(enable);
    ui->doubleSpinBoxInitlDP->setEnabled(enable);
    ui->doubleSpinBoxInitSP4->setEnabled(enable);
    ui->doubleSpinBoxInitSP8->setEnabled(enable);
    ui->doubleSpinBoxInitTreg->setEnabled(enable);

    ui->doubleSpinBoxInflowDN1->setEnabled(enable);
    ui->doubleSpinBoxInflowDN2->setEnabled(enable);
    ui->doubleSpinBoxInflowDN3->setEnabled(enable);
    ui->doubleSpinBoxInflowDN4->setEnabled(enable);
    ui->doubleSpinBoxInfloweDP->setEnabled(enable);
    ui->doubleSpinBoxInflowlDP->setEnabled(enable);
    ui->doubleSpinBoxInflowSP4->setEnabled(enable);
    ui->doubleSpinBoxInflowSP8->setEnabled(enable);
    ui->doubleSpinBoxInflowTreg->setEnabled(enable);

    ui->doubleSpinBoxReducDN1->setEnabled(enable);
    ui->doubleSpinBoxReducDN2->setEnabled(enable);
    ui->doubleSpinBoxReducDN3->setEnabled(enable);
    ui->doubleSpinBoxReducDN4->setEnabled(enable);
    ui->doubleSpinBoxReduceeDP->setEnabled(enable);
    ui->doubleSpinBoxReducelDP->setEnabled(enable);
    ui->doubleSpinBoxReducSPs->setEnabled(enable);

    ui->checkBoxSynchronizedInflow->setEnabled(enable);

    ui->doubleSpinBoxDt->setEnabled(enable);

}


