#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "cytonwidget.h"
#include "mainboard.h"
#include "cytonSimu.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool simRunning;

    void simulationLoop();

public slots:
    void runSim();



private:
    multiPopSimulation* mainSimu;
    MainBoard* Board;
    CytonWidget* CWDN1;
    CytonWidget* CWDN2;
    CytonWidget* CWDN3;
    CytonWidget* CWDN4;
    CytonWidget* CWeDP;
    CytonWidget* CWlDP;
    CytonWidget* CWSP4;
    CytonWidget* CWSP8;
    CytonWidget* CWTreg;
    vector<CytonWidget*> widgets;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
