#ifndef BOOTSTRAP_H
#define BOOTSTRAP_H

#include <vector>
#include <string>
#include <iostream>
#include "../Moonfit/moonfit.h"
#include "random.h"
using namespace std;

struct bootstrap {
    static TableCourse randomize(TableCourse& avg, TableCourse& std, size_t N){
        TableCourse res = TableCourse(avg);
        if((avg.nbVar != std.nbVar) || (avg.nbLignes != std.nbLignes)){
            cerr << "ERR: bootstrap::randomize, the avd and stddev have different sizes" << endl;
            return res;
        }
        for(size_t vari = 0; vari < static_cast<size_t>(avg.nbVar); ++vari){
            for(size_t timej = 0; timej < static_cast<size_t>(avg.nbLignes); ++timej){
                double average = (*(avg.storage)[timej])[vari];
                double stddev = (*(std.storage)[timej])[vari];
                double newVal = static_cast<double>(NAN);
                if((!std::isnan(average)) && (!std::isinf(average))){
                    newVal = 0;
                    for(size_t repi = 0; repi < N; ++repi){
                        newVal += max(1e-5, random::normal(average, stddev));
                    }
                    newVal = newVal / max(1.0, static_cast<double>(N));
                }
                (*(res.storage)[timej])[vari] = newVal;
            }
        }
        cout << "===== DATA HAS BEEN BOOTSTRAPPED: =====" << endl;
        cout << res.print() << endl;
        return res;
    }
};


#endif // BOOTSTRAP_H
