geneticAlgorithm	14
0	#CEP  - Classical Evolutionary Programming
8          #Proportional / From Worst / Basic Sampling
7	1      #SBX Cross-Over
1          #Mutation normal all points
0          #NO_NEED_REPLACEMENT
0          #Select Best
7	0.005	#MUTATIVE_SEPARATED
1      #Nb Repeats
25000	#Max nb of simulations-costs
250	#Population Size
0.2	#Proportion of CrossOver (vs offspring) in new individuals
0.5	#Fork coeff (%renewed each generation)

4
#	0	2000	20000	#aimedPopSize
	1	2	25	#avgG1
	2	2	20	#avgS
	3	0.2	5	#avgG2M
	4	0.1	10	#widthG1
#	5	0.1	10	#widthS
#	6	0.1	5	#widthG2M
#	7	1	60	#avgG0
#	8	0.01	1	#widthOrPercentLongG0
#	9	0.01	0.98	#percentQuiescentG0
#	10	1.25	6	#paramDiff
#	11	0.001	10	#inflowRate
#	12	0.01	0.01	#thresholdPos
#	13	0.1	200	#preSimTime
#	14	1	1	#timeBRDU
#	15	0.5	1	#durationBRDU
#	16	0	0	#timeEDU
#	17	0.5	1	#durationEDU
#	18	200	200	#timeSimulation
#	19	0.01	24	#apoptoticTime
#	20	0	3	#pop:lam0,nDiv1,rate2,+3LogNorm
#	21	0.0001	0.1	#deathRate
#	22	0.05	20	#modulatedG1KO
#	23	0.05	20	#modulatedSKO
#	24	0.05	20	#modulatedG2MKO
#	25	0.05	20	#modulatedWidthG1KO
#	26	0.05	20	#modulatedWidthSKO
#	27	0.05	20	#exitDuringS


0
Exponential
0
