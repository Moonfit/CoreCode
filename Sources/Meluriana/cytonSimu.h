#ifndef PROLIF_CELL_H
#define PROLIF_CELL_H

// This file defines the basic classes to do a simulation: cells and populations

#include <cstdio>
#include <vector>
#include <iostream>
#include <iomanip>          // for setprecision
#include <cmath>            // for exp
#include "distribution.h"
#include "statistiques.h"
using namespace std;


// put to true and the program will talk to you (debug mode)
#define verbose 0
#define useLowMemory true

/// @brief List of states
enum {NORMAL, DIVIDING, DEAD, REMOVED, DIVREMOVED, NBSTATUS};
string nameStatus(int i);
/// @brief List of cell cycle phases
enum {G0, G1, S, G2M, NBCycleStates};
string nameCycle(int i);
/// @brief list of events that can happen at a time-point to a cell
enum {NOTHING=0, DIVSTARTG1=1, STARTS=2, STARTG2M=3, DOMITOSIS=4, DIE=5, DISAPPEAR=6, NBEVENTS=7};


/// @brief 1/ a Cell is a class that has an ID and keeps track of the parents (and the genealogy of all ancestors as well)
struct cell {
    cell(cell* c = nullptr);
    int IDcell;
    int ID();
    int ID_parent;
    int par();
    vector<int> genealogy;
    vector<int>* getGenealogy();
};

void showCurveDNA();

/// @brief 2/ A proliferating cell contains the information of when each event will happen
/// this is just a storage class for the info on one cell. No important function.
struct thymocyte : public cell {

    static double testDelay;

    double tbirth;      // note: the start is considered to be G0 (or G1 if G0 is skipped)
    double tstartG1, tendG1, tendS, tendG2M, tdie;
    double tdisapp;
    int gen;            // number of its generation inside this stage !! Founder cells will have generation 0
    int status;
    int cyclestatus;
    double texit;       // only if waitEndM = false and finite nr of divisions

    /// @brief constructor that creates a new cell with these values. By default, start at G0
    thymocyte( double _tbirth,double _tstartG1,double durG1,double durS,double durG2M,
               double _tdie,double _durdisapp,int _gen,int status = NORMAL,int cyclestatus = G0,
               cell* parent = nullptr);

    /// @brief Notifies what is the next event to happen between time and time + dt
    int event(double time, double dt);

    /// @brief change status (normal, dividing, dead ...)
    void setStatus(int z);

    /// @brief retrieves the pointer of this
    cell* me();

    /// @brief Amount of DNA in this cell (content = 1 at G1, grows linearly to 2 during S and stays 2 during G2)
    double DNA;
    double currentDNA();
    /// @brief Volume of the cell
    double VOL;
    double currentVOL();
    /// @brief amount of labelled BDRU in this cell
    double BRDU;
    double currentBRDU();
    /// @brief amoung of labelled EDU in this cell
    double EDU;
    double currentEDU();

    /// @brief Updates the levels of EDU and BRDU for the period between t and t+dt knowing the current dose of EDU and BRDU
    void updateVariables(double t, double currentDoseEDU, double currentDoseBRDU, double dt);

    /// @brief Time the cell has spent in this population (before exiting or dying)
    double timeInThisPopulation;
    /// @brief This field will be true if the cell just entered the population and did not divide so far
    bool incomer;

    /// @brief If a cell is supposed to leave (depending on the population structure), possibility to let cells stay
    /// till it divides. Also, for fixed number of divisions, when it is a floating number, allow some cells to divide once more.
    /// then they will be marked 'mark_stay'
    enum {mark_exit = -1, unmarked = 0, mark_stay_till_end_div = 1};
    int markedForExit;

    /// @brief Gives the content of a cell as text
    string print();

    ~thymocyte();
};

/// @brief 3/ A stage is a population of cells, with its dynamical properties: population structure,
/// distributions of time in each compartment, etc... and that can evolve over time.
struct stage {
    string name;

    /// @brief Constructor: just fills the name and reserve memory for the vectors.
    stage(string _name);

    /// @brief Fills the parameters for this population. See class Law for distributions.
    void setParams(int _typeDifferentiation, double _paramDiff, Law* _distribDeath, Law* _distribG0, Law* _distribG1,Law* _distribS,Law* _distribG2M,Law* _distribTot,bool _waitEndMtoDifferentiate, bool _doNotRescaleS, double _apoptoticTime, double _percentBystanderG0 = 0);

    /// @brief List of cells inside the population (as pointers)
    vector<thymocyte*> insiders;

    /// @brief Current number of alive cells in the population
    size_t nbInsiders();

    /// @brief Generate a random cell according to the parameters of this population. To add a cell to the population,
    /// first create one with this function and then use addCell() to add it to the population
    thymocyte* generateCell(double currentT, bool synchroStartG0 = false, int generation = 0, thymocyte *parent = nullptr);

    /// @brief Creates a cell that is in G0 and will stay in G0
    thymocyte* generateQuiescent(double currentT);

    /// @brief Add n cells randomly according to expected steady state. Their cell cycle lengths are estimated
    /// from the distribution, and their birth date is randomly spread before 'time' such that they are distributed
    /// in different cell cycle stages. The amoung of cells in each generation will depend on the population structure.
    /// In particular, with finite number of divisions, there are less cells at generation 1 compared to generation 2, 3, etc...
    /// and this will also depend on the death rate.
    void initializeRandom(double n, double time);

    /// @brief Add n newcomers (at generation 0 and time in this stage = 0)
    void addInflow(double nCells, bool CellsComeInG0, double time);

    /// @brief Main Function: Make the population evolve from time to time+dt. Will create new cells, make them progress the cell cycle,
    /// label their EDU/BRDU according to the current dose in the system, and make cells die/disappear
    vector<thymocyte*> timeStep(double time, double dt, double currentEDUdose, double currentBRDUdose);




    /// @brief The possible types of population structures/differentiation,
    enum diffTypes {laminarFiniteTime, finiteNrDiv, constantDiffRate, nrTypesDiff};
    /// @brief type of population structure
    int typeDifferentiation;
    /// @brief A value for this population structure: either limited time, number of divisiona, or rate of differentiation (exit)
    double paramDiff;

    /// @brief Time distributions for cells in this population
    Law* distribDeath;
    Law* distribG0;
    Law* distribG1;
    Law* distribS;
    Law* distribG2M;

    /// @brief Instead of a time distribution for each phase, it is also possible to decide a time-distribution of the total cycle
    /// and have a fixed fraction of each cell cycle phase inside.
    Law* distribTot;

    /// @brief Will tell how long a dead cell stays detectable in the population. When a cell is created,
    /// the time to disappear will be time of death + apoptotic time.
    double apoptoticTime;

    /// @brief Percent of bystander G0 cells that stay at G0. This is another way to have G0 cells than waiting for mitosis
    /// and assigning some cells a G0 duration.
    double percentBystanderG0;

    /// @brief Option: when a cell should exit the population, requests to wait that the cell cycle is completed before exiting
    bool waitEndMtoDifferentiate;
    /// @brief I think this option is not implementet yet. Don't remember what it was for.
    bool doNotRescaleS;


    /// @brief Clears the list of cells (as pointers) and DELETES their content
    void clear();

    void addCell(thymocyte* t, bool resetStates = false);
    /// @brief Adds a cell to the population. If resetStates, makes gen=0, time in this pop=0, and markedforexit = unmarked
    /// @brief Adds a list of cells to the population
    void addMultipleCells(vector<thymocyte*> cellsToAdd);
    /// @brief Removes the pointer of the cell at position IDpos inside the cell list insiders. The content is not deleted.
    void removeCell(int IDpos);

    /// @brief Returns the average length of a full cycle, from mitosis to next mitosis. Includes G0 duration.
    double getInterCycleTime();

    /// @brief In case the population has fixed number of divisions (Ntot, can be floating number), returns the
    /// fraction of cells at each generation at steady state. This will depend on the death rate.
    vector<double> getSteadyStateGenerations(double Ntot);

    /// @brief Shows the content of the population as text
    string print();

    /// @brief This will delete the laws but not the content of the cells stored into the insider list.
    ~stage();
};


/// @brief A 'one population' is the setting of a labelling experiment on a stage.
struct onePopulation {

    /// @brief a/ constructor: initializes memory and the parameters to default values
    onePopulation();

    /// @brief The population of cells that will be used for the labelling
    stage* pop;

    /// @brief Class that will store all the statistics and dynamics of a simulation
    observerOneSim* currentObserver;

    /// @brief b/ all the parameters will need to be given one by one
    /// the parameters of the population pop should also be given manually.


    // Parameters
    double dt;
    int initialNumbers;
    double inflowRate;
    bool synchronizeInflow;     // true means they enter at beginning of G0
    double scalingRatio;        // means all numbers are divided by scalingRatio for the simulation but plotted
                                // as real numbers.
    double preSimTime;          // stabilization time
    double timeSimulation;      // hours
    double timeEDU;
    double durationEDU;
    double timeBRDU;
    double durationBRDU;
    vector<double> AnalysisPoints;
    double thresholdEDU;
    double thresholdBRDU;
    //double deathRate;


    /// @brief Initializes a new simulation and gives an observer class to store the results.
    /// sets time to zero.
    virtual void initialize(observerOneSim* MO);

    /// @brief Main function: This performs one time-step between time and time+dt.
    /// It updates the dose of EDU and BRDU at that time,
    /// It makes the population evolve over time.
    /// It will return false when time > timeSimulation, ie. when the simulation is finished.
    /// Can be called via 'while(timeStep()){}' for instance.
    virtual bool timeStep(onePopulation* populationToTakeCellsFrom = nullptr); // note, the upstream population will be updated (time step will be called)
    virtual void analyzeTimePoint(stage* popOfInterest, observerOneSim* observerThisPop, bool onlyIncomers = false);


    /// @brief So far, does nothing. Could be used later to output files as results
    virtual void finalize();

    /// @brief empties the population and delete the content of the cells
    virtual void reset();

    /// @brief dynamic variables for the simulation
    double currentBRDUdose;
    double currentEDUdose;
    double time;

    /// @brief Shows the content of the simulation as text
    virtual string print();

    /// @brief the destructor will delete the population and the content of the cells
    virtual ~onePopulation(){pop->clear();}

    /// @brief In case another population is used as source of inflow
    size_t inflowDeficit;

};

/// @brief a structure that store cells in memory, even after cell die, so that statistics including dead cells can be done after a simulation */
struct mem {
    mem();
    void clear();
    size_t size;
    vector<cell*> all;              /// @brief Note that a vector<cell> instead of vector<cell*> is prohibited here because resizing a vector
                                    /// relocalises it into the memory, therefore a vector<cell> would have variable pointers for its cells ... quite bad !
    bool check_exists(int ID);
    void add(int ID, cell*);
    void print();
};

/// @brief  a simple generator of unique IDs
struct counter {
    counter();
    int cnt;
    int gen();
    void clear();
};





#endif


