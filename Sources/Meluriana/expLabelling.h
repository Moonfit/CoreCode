#ifndef EXPERIMENTSLabelling_H
#define EXPERIMENTSLabelling_H

#include "../Moonfit/moonfit.h"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;


struct simplestExpDoubleLabel : public Experiment {
    simplestExpDoubleLabel(Model* _m) : Experiment(_m, 1) { // just one condition
        Identification = string("One thymic population labelling");
        names_exp[0] = "Sim";
    }

    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                default:{
                    m->initialise(0);
                    m->simulate(24, E); // bug: includes stabilization
                    break;
                }
            }
            m->setOverrider(nullptr);
        }
    }
};


struct twoPopsExpDoubleLabel : public Experiment {
    enum EXPS{MainPop, Ancestors, Incomers, UnlabelledInflow, NB_EXP};
    twoPopsExpDoubleLabel(Model* _m) : Experiment(_m, NB_EXP) { // just one condition
        Identification = string("One thymic population labelling");
        names_exp[MainPop] = "Main";
        names_exp[Ancestors] = "Ancestors";
        names_exp[Incomers] = "Incomers";
        names_exp[UnlabelledInflow] = "UnlabelledInflow";
    }

    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(0);
            if(IdExp == MainPop) m->action("showMainPop", 0);
            if(IdExp == Ancestors) m->action("showAncestors", 0);
            if(IdExp == Incomers) m->action("showIncomers", 0);
            if(IdExp == UnlabelledInflow) m->action("showUnlabelled", 0);

            if(IdExp == UnlabelledInflow) m->action("removeInflow", 0);
            m->simulate(24, E); // bug: includes stabilization
            if(IdExp == UnlabelledInflow) m->action("restoreInflow", 0);
            m->setOverrider(nullptr);
        }
    }
};

struct twoPopsExpDoubleLabelSlim : public Experiment {
    enum EXPS{MainPop, NB_EXP};
    twoPopsExpDoubleLabelSlim(Model* _m) : Experiment(_m, NB_EXP) { // just one condition
        Identification = string("One thymic population labelling");
        names_exp[MainPop] = "Main";
    }

    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            m->initialise(0);
            if(IdExp == MainPop) m->action("showMainPop", 0);
            m->simulate(24, E); // bug: includes stabilization
            m->setOverrider(nullptr);
        }
    }
};




struct expDoubleLabel : public Experiment {
    enum POPS{expDN1, expDN2, expDN3, expDN4, expeDP, explDP, expSP4, expSP8, expTreg, expPeriph, NB_POP}; // experiments    - use expLMajor::Small_Dose
	enum EXPS{NormalLabelling, NB_EXP};
	POPS currentPop;
	expDoubleLabel(Model* _m, POPS population) : Experiment(_m, NB_EXP) {
		currentPop = population;
        Identification = string("One thymic population labelling");
        names_exp[NormalLabelling] = "Labelling";
    }

    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
				default:{
                    m->initialise(0);
                    m->simulate(24, E); // bug: includes stabilization
					break;
				} 
            }
            m->setOverrider(nullptr);
        }
    }
};

struct expDoubleLabelWT_KO : public Experiment {
    enum POPS{expDN1, expDN2, expDN3, expDN4, expeDP, explDP, expSP4, expSP8, expTreg, expPeriph, NB_POP}; // experiments    - use expLMajor::Small_Dose
    enum EXPS{WT, DKO, NB_EXP};
    POPS currentPop;
    expDoubleLabelWT_KO(Model* _m) : Experiment(_m, NB_EXP) {
        Identification = string("One thymic population labelling");
        names_exp[WT] = "WT";
        names_exp[DKO] = "DKO";
    }

    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                case WT:{
                    m->initialise(WT);
                    m->simulate(24, E); // bug: includes stabilization
                    break;
                }
                case DKO:{
                    m->initialise(DKO);
                    m->simulate(24, E); // bug: includes stabilization
                    break;
                }
            }
            m->setOverrider(nullptr);
        }
    }
};


/*
struct expSynchronizedCycle : public Experiment {
    enum EXPS{startG1, startS, startG2M, NB_EXP};
    expSynchronizedCycle(Model* _m) : Experiment(_m, NB_EXP) {
        Identification = string("Cycling from 1 phase");
        names_exp[startG1] = "startG1";
        names_exp[startS] = "startS";
        names_exp[startG2M] = "startG2M";
    }

    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false) {// if no E is given, VTG[i] is used
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                case startG1:{
                    m->initialise( WT);
                    m->simulate(12, E); // bug: includes stabilization
                    break;
                }
                case startS:{
                    m->initialise(WT);
                    m->simulate(12, E); // bug: includes stabilization
                    break;
                }
                case startG2M:{
                    m->initialise(WT);
                    m->simulate(12, E); // bug: includes stabilization
                    break;
                }
            }
            m->setOverrider(nullptr);
        }
    }
};*/







// Always put param 100 at the end to restore the initial parameter set at the end ...
enum AROUNDPARAMETERS {PARAM0, PARAM0P1, PARAM1, PARAM5, PARAM20, PARAM50, PARAM80, PARAM120, PARAM150, PARAM200, PARAM500, PARAM1000, PARAM100, NB_AROUNDPARAMETERS};
struct expParameters : public Experiment {
    int parameterID;  // this time the parameter is the index of the input variable.
    bool aroundInitialValue; // if yes, samples % variations around the initial value. If not, then samples the whole range of the configuration.
    vector<double> listPoints;
    bool initialized;

    expParameters(Model* _m, int _parameterID, bool _aroundInitialValue) : Experiment(_m, NB_AROUNDPARAMETERS), parameterID(_parameterID), aroundInitialValue(_aroundInitialValue), listPoints(NB_AROUNDPARAMETERS, 0.0), initialized(false) {
        stringstream res; res << "Th1, change parameter " << m->getParamName(parameterID) << " around " << aroundInitialValue;
        Identification = res.str();
        if((parameterID < 0) || (parameterID >= m->getNbParams())){cerr << "ERR: expParameters, parameter ID(" << parameterID << ") incorrect, only " << m->getNbParams() << " parameters in the model" << endl;}
        names_exp.clear();
        names_exp.resize(NB_AROUNDPARAMETERS, string(""));
    }
    void simulate(int IdExp, Evaluator* E, bool force){
        // Initializing the names of experiments and parameter values to check - everything that depends on parameter values should be done inside simulate and not in the constructor.
        if(!initialized){
            listPoints = cutSpace(13, true, 2, m->getLowerBound(parameterID), m->getUpperBound(parameterID));
            if(aroundInitialValue){
                double initVal = m->getParam(parameterID);
                listPoints[PARAM0] = 0.0;
                listPoints[PARAM0P1] = 0.001*initVal;
                listPoints[PARAM1] = 0.01*initVal;
                listPoints[PARAM5] = 0.05*initVal;
                listPoints[PARAM20] = 0.2*initVal;
                listPoints[PARAM50] = 0.5*initVal;
                listPoints[PARAM80] = 0.8*initVal;
                listPoints[PARAM100] = 1.0*initVal;
                listPoints[PARAM120] = 1.2*initVal;
                listPoints[PARAM150] = 1.5*initVal;
                listPoints[PARAM200] = 2.0*initVal;
                listPoints[PARAM500] = 5.0*initVal;
                listPoints[PARAM1000] = 10.0*initVal;
                names_exp[PARAM0]   = string("(0%)  ");
                names_exp[PARAM0P1] = string("(0.1%)");
                names_exp[PARAM1]   = string("(1%)  ");
                names_exp[PARAM5]   = string("(5%)  ");
                names_exp[PARAM20]  = string("(20%) ");
                names_exp[PARAM50]  = string("(50%) ");
                names_exp[PARAM80]  = string("(80%) ");
                names_exp[PARAM100] = string("(100%)");
                names_exp[PARAM120] = string("(120%)");
                names_exp[PARAM150] = string("(150%)");
                names_exp[PARAM200] = string("(2X)  ");
                names_exp[PARAM500] = string("(5X)  ");
                names_exp[PARAM1000] = string("(10X) ");
            }
            for(size_t i = 0; i < names_exp.size(); ++i){
                stringstream buildName;
                buildName << listPoints[i];
                names_exp[i] += buildName.str();
            }
            initialized = true;
        }
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                default:{
                    m->setParam(parameterID, listPoints[IdExp]);
                    m->initialise(expDoubleLabel::expeDP);
                    m->simulate(24, E); // bug: includes stabilization
                    break;
                }
            }
        }
        m->setOverrider(nullptr);
    }
};



#endif // EXPERIMENTSTHALL_H
