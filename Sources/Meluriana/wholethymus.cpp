#include "wholethymus.h"

void initializeStageFromThymus(stage* currentStage, int IDthymicPopulation){

    double ratioG1 = 0.25, ratioS = 0.65, ratioG2M = 0.1;
    int typeDifferentiation = stage::finiteNrDiv;
    double paramDiff = 1;           // either limited time, nr divs, or rate diff
    bool waitEndMtoDifferentiate = true;
    bool doNotRescaleS = false;
    double apoptoticTime = 0.1; // can improve

    Law* distribDeath = new Law();
    Law* distribG0 = new Law();
    Law* distribG1 = new Law();
    Law* distribS = new Law();
    Law* distribG2M = new Law();
    Law* distribTot = new Law();
    double percentBystanderG0 = 0.5;

    distribTot->set(Fixed, 1.0);

    double totalCycle = 10; // hours
    double rateDeath = 0;
    distribG0->set(Fixed, 0.0);     // By default, no cells in G0.

    switch(IDthymicPopulation){
        case DN1:{
            typeDifferentiation = stage::finiteNrDiv;
            paramDiff = 11; // 11 divisions so 12 generations
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = 1.07 * 24; // Manesso, in hours
            rateDeath = 0.13 / (1.07*24);
            break;
        }
        case DN2:{
            typeDifferentiation = stage::constantDiffRate;
            paramDiff = 0.58 / (0.631 * 24.);
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = 0.631 * 24.; // Manesso, in hours
            rateDeath = 0.028 / (0.631*24.);
            break;
        }
        case DN3:{
            typeDifferentiation = stage::finiteNrDiv;
            paramDiff = 3; // 11 divisions so 12 generations
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = 16;
            rateDeath = 0.1 / 24.; // per day, right ??
            distribG0->set(BiModal,0, 0.001, 0.2, 2.0, 0.2); // example: 80% cells do not have G0, 20% take around 0.7 days, std = 0.2
            break;
        }
        case DN4:{
            typeDifferentiation = stage::finiteNrDiv;
            paramDiff = 2; // 11 divisions so 12 generations
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = 16;
            rateDeath = 0.1 / 24.; // per day, right ??
            distribG0->set(BiModal,0, 0.001, 0.8, 0.7, 0.2); // example: 80% cells do not have G0, 20% take around 0.7 days, std = 0.2
            break;
        }
        case eDP:{
            typeDifferentiation = stage::finiteNrDiv;
            paramDiff = 5; // 11 divisions so 12 generations
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = (1/4.5) * 24.; // Manesso, in hours
            rateDeath = 0.0000001;
            break;
        }
        case lDP:{ // ALL in G0 !!!
            typeDifferentiation = stage::constantDiffRate;
            paramDiff = 0.136; //11; // 11 divisions so 12 generations
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = 1000; // Irrelevant, all in G0
            rateDeath = 0.246 / 24.; // per day, right ??
            distribG0->set(Fixed, 1e6); // they are all in G0, no cycling.
            break;
        }
        case SP4:{
            typeDifferentiation = stage::constantDiffRate;
            paramDiff = 0.0466; // exit proba
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = 1.0 * 24.; // No idea,
            rateDeath = 0.655 / 24.; // per day, right ??
            distribG0->set(BiModal,0, 0.001, 0.035, 100, 0.001); // Not SURE !! 3,5% prolif in total. the rest  will stay always in G0
            break;
        }
        case SP8:{
            typeDifferentiation = stage::constantDiffRate;
            paramDiff = 0.06; // exit proba
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = 1.0 * 24.; // No idea,
            rateDeath = 0.4235 / 24.; // per day, right ??
            distribG0->set(BiModal,0, 0.001, 0.2, 100, 0.001); // 10% will do a cycle. 90% will stay always in G0
            break;
        }
        case Tregs:{ // no idea for these params
            typeDifferentiation = stage::constantDiffRate;
            paramDiff = 0.06; // exit proba
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = 1.0 * 24.; // No idea,
            rateDeath = 0.4235 / 24.; // per day, right ??
            distribG0->set(BiModal,0, 0.001, 0.2, 100, 0.001); // 10% will do a cycle. 90% will stay always in G0
            break;
        }
        case Periph:{ // no idea for these params
            typeDifferentiation = stage::constantDiffRate;
            paramDiff = 0.00; // exit proba
            waitEndMtoDifferentiate =true; // for a fixed divisions, better
            doNotRescaleS = false;
            totalCycle = 1.0 * 24.; // No idea,
            rateDeath = 0;
            distribG0->set(Fixed,1e9); // 10% will do a cycle. 90% will stay always in G0
            break;
        }
        default:{}
    }
    if(rateDeath == 0) {
        distribDeath->set(Fixed, 1e9);
    } else {
        distribDeath->set(Exponential, rateDeath);
    }
    distribG1->set(Normal, ratioG1 * totalCycle, ratioG1 * totalCycle*0.2);
    distribS->set(Normal, ratioS * totalCycle, ratioS * totalCycle*0.2);
    distribG2M->set(Normal, ratioG2M * totalCycle, ratioG2M * totalCycle*0.2);
    distribTot->set(Fixed, 1.0); //Normal, 1.0, 0.2);

    //cout << "Setting up for stage " << endl;
    //cout << percentBystanderG0 << " Quiescent " << endl;
    currentStage->setParams(typeDifferentiation, paramDiff, distribDeath, distribG0, distribG1,distribS,distribG2M,distribTot,waitEndMtoDifferentiate,doNotRescaleS,apoptoticTime, percentBystanderG0);

}


string nameStage(int i){
    vector<string> out = {"DN1", "DN2", "DN3", "DN4", "eDP", "lDP", "SP4", "SP8", "Tregs", "Periph", "NbStages"};
    if((i < 0) || (i >= NbStages)) cerr << "Unknown stage " << i << endl;
    return out.at(i);
}

stage* multiPopSimulation::getStage(int IDstage){return WholePop[IDstage];}

//will probabilistically decide SP4, SP8 or Tregs

int multiPopSimulation::getIDNextStage(int IDstage){
    switch(IDstage){
        case DN1: return DN2;
        case DN2: return DN3;
        case DN3: return DN4;
        case DN4: return eDP;
        case eDP: return lDP;
        case lDP: {
            if(random::uniformDouble(0,1) < ratiolDPtoSP8) return SP8;
            if(random::uniformDouble(0,1) < ratioOfSP4InflowToTregs) return Tregs;
            return SP4;
        }
        case SP4: case SP8: case Tregs: return Periph;
    case Periph: return Periph;
    }
    return Periph;
}

void multiPopSimulation::reset(){
    for(int i = 0; i < NbStages; ++i){
        if(WholePop[i]) {
            WholePop[i]->clear();
        }
    }
}

multiPopSimulation::multiPopSimulation() {

    dt = 0.05;
    apoptoticTime = 0.1;

    thresholdEDU = 0.1;
    thresholdBRDU = 0.1;

    // Note: the pointers of the laws are given to the interface. NEVER DELETE THE LAW, use them, do not replace them.
    WholePop.resize(NbStages, nullptr); // empty constructors
    for(int i = 0; i < NbStages; ++i){
        WholePop[i] = new stage(nameStage(i));
        cout << "Setting up for stage " << nameStage(i) << endl;
        initializeStageFromThymus(WholePop.at(i), i);
    }

    preSimTime = 10; //2*24; // 1 day *25; // 25 days
    timeEDU = 0;
    durationEDU = 1;
    timeBRDU = 1;
    durationBRDU = 1;
    timeSimulation = 15; // hours
    apoptoticTime = 0.1; // 6 mins, not very useful now, but for later on dead cells
    AnalysisPoints.resize(10, 0);
    AnalysisPoints = {2,3,4,5,6,8,10,12,16,20};

    ratiolDPtoSP4 = 0.3;
    ratiolDPtoSP8 = 1 - ratiolDPtoSP4;
    ratioOfSP4InflowToTregs = 0.05; // 5% of eDP to SP4 will become Tregs

    time = 0;

    double scalingRatio = 20;
    initialNumbersPerPop.resize(NbStages, 0);
    initialNumbersPerPop[DN1] =   107000. / scalingRatio;
    initialNumbersPerPop[DN2] =   102800. / scalingRatio;
    initialNumbersPerPop[DN3] =  1640000. / scalingRatio;
    initialNumbersPerPop[DN4] =  2550000. / scalingRatio; // 1550000
    initialNumbersPerPop[eDP] =  5017000. / scalingRatio;
    initialNumbersPerPop[lDP] = 57600000. / scalingRatio;
    initialNumbersPerPop[SP4] =  9600000. / scalingRatio;
    initialNumbersPerPop[SP8] =  3400000. / scalingRatio;
    initialNumbersPerPop[Tregs] = 257000. / scalingRatio;
    initialNumbersPerPop[Periph] = 0;

    additionalInflow.resize(NbStages, 0);
    simulatedStages.resize(NbStages, true);
    additionalInflow[DN1] = 100. / scalingRatio;

    rescalingFactors.resize(NbStages, 0);
    rescalingFactors[DN1] = 1.;
    rescalingFactors[DN2] = 1.;
    rescalingFactors[DN3] = 1.;
    rescalingFactors[DN4] = 1.;
    rescalingFactors[eDP] = 40.;
    rescalingFactors[lDP] = 40.;
    rescalingFactors[SP4] = 40.;
    rescalingFactors[SP8] = 40.;
    rescalingFactors[Tregs] = 40.;
    rescalingFactors[Periph] = 40.; // not allowed to reduce coefficient

    synchronizeInflow = true; // all additional are in beginning og G0

    currentObserverAllPops = nullptr;
    currentObserver = nullptr;
}

void multiPopSimulation::initialize(observerOneSim *OOS, masterObserver* MO){

    // the interface will try to modify one population parameter, we can chose which one
    pop = getStage(DN3);

    currentObserver = OOS;
    if(currentObserver) currentObserver->clear();

    currentObserverAllPops = MO;
    if(currentObserverAllPops) currentObserverAllPops->clear();

    time = -preSimTime;
    currentEDUdose = 0;
    currentBRDUdose = 0;


    stringstream InfosInit;
    // Put init numbers of unsynchronized cells in each compartment. Not: no dead cells yet, will need to wait.
    for(int st = 0; st < NbStages-1; ++st){ // no need to do periph

        double initN = initialNumbersPerPop[st] / rescalingFactors[st];
        int toCreate = (int) initN;
        if(random::uniformDouble(0,1) < (initN - (double)(int(initN)))) toCreate++;
        if(!simulatedStages.at(st)) toCreate = 0;


        cout << "Generating " << toCreate << " cells at stage " << nameStage(st) << endl;
        stage* cs = getStage(st);
        if(!cs) cerr << "ERR: empty stage " << nameStage(st) << endl;
        else {
            cs->clear(); // will also delete the cells
            cs->initializeRandom(toCreate, time);
            InfosInit << "------------------ Initial stage " << nameStage(st) << " --------------------" << endl;
            InfosInit << cs->print() << endl;
        }
    }
    ofstream f("InitPops.txt");
    f << InfosInit.str() << endl;
    f.close();

    cout << "Initialized multipop with the following parameters: " << endl;
    cout << printParameters();
    cout << "   -> Details of initial population written into InitPops.txt" << endl;

}

string multiPopSimulation::printParameters(){

    stringstream res;
    res << "dt\t" << dt << "\n";
    res << "preSimTime\t" << preSimTime << "\n";
    res << "timeBRDU\t" << timeBRDU << "\n";
    res << "durationBRDU\t" << durationBRDU << "\n";
    res << "timeEDU\t" << timeEDU << "\n";
    res << "durationEDUt" << durationEDU << "\n";
    res << "timeSimulation\t" << timeSimulation << "\n";
    res << "analysisPoints\t";
    for(size_t i = 0; i < AnalysisPoints.size(); ++i){
        res << AnalysisPoints[i] << " ";
    } res << "\n";
    res << "time\t" << time << "\n";
    res << "synchronizeInflow\t" << ((synchronizeInflow) ? "Yes" : "No") << "\n";
    res << "apoptoticTime\t" << apoptoticTime << "\n";

//    vector<double> initialNumbersPerPop; // size NbStages;
//    vector<double> additionalInflow; // size NbStages
//    vector<bool> simulatedStages;
//    vector<double> rescalingFactors;

    res << "ratiolDPtoSP4\t" << ratiolDPtoSP4 << "\n";
    res << "ratiolDPtoSP8\t" << ratiolDPtoSP8 << "\n";
    res << "ratioOfSP4InflowToTregs\t" << ratioOfSP4InflowToTregs << "\n";
    return res.str();
}

string multiPopSimulation::printPops(){
    stringstream res;
    res << "t=";
    for(int i = 0; i < NbStages; ++i){
        res << "\t" << nameStage(i);
    }
    res << endl;
    res << time;
    for(int i = 0; i < NbStages; ++i){
        res << "\t" << WholePop.at(i)->nbInsiders();
    }
    return res.str();
}


bool multiPopSimulation::timeStep(){


    static vector<int> flowExport = vector<int>(NbStages, 0);
    static vector<int> flowProlif = vector<int>(NbStages, 0);
    static vector<int> flowDead = vector<int>(NbStages, 0);

    cout << "t=" << time << endl;

    static int cpt = 0;
    if(cpt == 20) {
        cout << "Exp:";
        for(size_t i = 0; i < NbStages; ++i){
            cout << "\t" << flowExport[i];
        }
        cout << endl << "Prolif";
        for(size_t i = 0; i < NbStages; ++i){
            cout << "\t" << flowProlif[i];
        }
        cout << endl << "Dead";
        for(size_t i = 0; i < NbStages; ++i){
            cout << "\t" << flowDead[i];
        }
        cout << endl;
        cout << endl << printPops() << endl;
        flowExport.clear();
        flowProlif.clear();
        flowDead.clear();
        flowExport.resize(NbStages, 0);
        flowProlif.resize(NbStages, 0);
        flowDead.resize(NbStages, 0);
        cpt = 0;
    }
    cpt++;



    /*preSimTime = 24*25; // 25 days

        apoptoticTime = 0.1; // 6 mins, not very useful now, but for later on dead cells
        AnalysisPoints.resize(9, 0);
        AnalysisPoints = {1,2,3,4,6,8,10,12,15};

        rescalingFactors.resize(NbStages, 0);
        rescalingFactors[DN1] = 1.;
        rescalingFactors[DN2] = 1.;
        rescalingFactors[DN3] = 5.;
        rescalingFactors[DN4] = 5.;
        rescalingFactors[eDP] = 20.;
        rescalingFactors[lDP] = 200.;
        rescalingFactors[SP4] = 20.;
        rescalingFactors[SP8] = 20.;
        rescalingFactors[Tregs] = 10.;

        synchronizeInflow = true; // all additional are in beginning og G0*/



    if(time >= timeSimulation) {
        time += dt;
        return false;
    }

    // For each cell,

    // 1 - progress : does it die or what appens to it
    // 2 - make cell change compartment if required
    // 3 - add new inflow cells if required.

    if((time >= timeBRDU) && (time < timeBRDU + durationBRDU + dt)){
        currentBRDUdose = 1.0;
    } else {
        currentBRDUdose = 0;
    }
    if((time >= timeEDU) && (time < timeEDU + durationEDU + dt)){
        currentEDUdose = 1.0;
    } else {
        currentEDUdose = 0.0;
    }

    //cout << "t= " << time << " EDU = " << currentEDUdose << " BRDU = " << currentBRDUdose << endl;

    // For statistics
    vector<int> nrBRDUPerStage = vector<int>(NbStages, 0);
    vector<int> nrEDUPerStage = vector<int>(NbStages, 0);

    // Updates all existing insider cell
    for(size_t iSt = 0; iSt < NbStages-1; ++ iSt){
        stage* currentStage = WholePop.at(iSt);
        vector<thymocyte*> listLeavingCells = currentStage->timeStep(time, dt, currentEDUdose, currentBRDUdose);
        flowExport[iSt] += listLeavingCells.size();

        for(size_t iT = 0; iT < listLeavingCells.size(); ++iT){
            thymocyte* currentInsider = listLeavingCells[iT];
            currentInsider->gen = 0;
            int nSt = getIDNextStage(iSt);
            double factor  = rescalingFactors[iSt] / (max(1e-6, rescalingFactors[nSt])); // note: might be converted into more than one cell :-)
            if(factor > 1.0) cerr << "ERR: it is not allowed to have a higher rescaling factor in a next stage, because more than one cell would be created" << endl;
            if(random::uniformDouble(0,1) < rescalingFactors[iSt] / (max(1e-6, rescalingFactors[nSt]))) {
                getStage(nSt)->addCell(currentInsider);
                currentInsider->incomer = true;
            }
        }
    }

    // Now add cells according to additional influx (useful when simulating only one population)
    for(size_t iSt = 0; iSt < NbStages-1; ++ iSt){
        stage* currentStage = WholePop.at(iSt);
        currentStage->addInflow(static_cast<double>(additionalInflow[iSt]) * dt, false, time);
    }

    //
    analyzeTimePoint(getStage(DN3), currentObserver);


    // Now stores kinetic informations into the observers
    static int cpt2 = 0;
    cpt2++;
    if(true) {//cpt2 == 5){
        //cpt = 0;
        currentObserverAllPops->o_doseBRDU.pushData(currentBRDUdose, time);
        currentObserverAllPops->o_doseEDU.pushData(currentEDUdose, time);

        currentObserverAllPops->o_popDN1.pushData(rescalingFactors[DN1] * getStage(DN1)->nbInsiders(), time);
        currentObserverAllPops->o_popDN2.pushData(rescalingFactors[DN2] *getStage(DN2)->nbInsiders(), time);
        currentObserverAllPops->o_popDN3.pushData(rescalingFactors[DN3] *getStage(DN3)->nbInsiders(), time);
        currentObserverAllPops->o_popDN4.pushData(rescalingFactors[DN4] *getStage(DN4)->nbInsiders(), time);
        currentObserverAllPops->o_popDNTot.pushData(rescalingFactors[DN1] *getStage(DN1)->nbInsiders()+rescalingFactors[DN2] *getStage(DN2)->nbInsiders()+rescalingFactors[DN3] *getStage(DN3)->nbInsiders()+rescalingFactors[DN4] *getStage(DN4)->nbInsiders(), time);
        currentObserverAllPops->o_popeDP.pushData(rescalingFactors[eDP] *getStage(eDP)->nbInsiders(), time);
        currentObserverAllPops->o_poplDP.pushData(rescalingFactors[lDP] *getStage(lDP)->nbInsiders(), time);
        currentObserverAllPops->o_popSP4.pushData(rescalingFactors[SP4] *getStage(SP4)->nbInsiders(), time);
        currentObserverAllPops->o_popSP8.pushData(rescalingFactors[SP8] *getStage(SP8)->nbInsiders(), time);
        currentObserverAllPops->o_popTreg.pushData(rescalingFactors[Tregs] *getStage(Tregs)->nbInsiders(), time);

        currentObserverAllPops->o_prcBRDUDN1.pushData(100.* (double) nrBRDUPerStage[DN1] / (max(1.,(double) getStage(DN1)->nbInsiders())), time);
        currentObserverAllPops->o_prcBRDUDN2.pushData(100.* (double) nrBRDUPerStage[DN2] / (max(1.,(double)getStage(DN2)->nbInsiders())), time);
        currentObserverAllPops->o_prcBRDUDN3.pushData(100.* (double) nrBRDUPerStage[DN3] / (max(1.,(double)getStage(DN3)->nbInsiders())), time);
        currentObserverAllPops->o_prcBRDUDN4.pushData(100.* (double) nrBRDUPerStage[DN4] / (max(1.,(double)getStage(DN4)->nbInsiders())), time);
        currentObserverAllPops->o_prcBRDUDNTot.pushData(100.* (double) (nrBRDUPerStage[DN1] + nrBRDUPerStage[DN2] + nrBRDUPerStage[DN3] + nrBRDUPerStage[DN4])  / (max(1.,(double) (getStage(DN1)->nbInsiders()+getStage(DN2)->nbInsiders()+getStage(DN3)->nbInsiders()+getStage(DN4)->nbInsiders()))), time);
        currentObserverAllPops->o_prcBRDUeDP.pushData(100.* (double) nrBRDUPerStage[eDP] / (max(1.,(double)getStage(eDP)->nbInsiders())), time);
        currentObserverAllPops->o_prcBRDUlDP.pushData(100.* (double) nrBRDUPerStage[lDP] / (max(1.,(double)getStage(lDP)->nbInsiders())), time);
        currentObserverAllPops->o_prcBRDUSP4.pushData(100.* (double) nrBRDUPerStage[SP4] / (max(1.,(double)getStage(SP4)->nbInsiders())), time);
        currentObserverAllPops->o_prcBRDUSP8.pushData(100.* (double) nrBRDUPerStage[SP8] / (max(1.,(double)getStage(SP8)->nbInsiders())), time);
        currentObserverAllPops->o_prcBRDUTreg.pushData(100.* (double) nrBRDUPerStage[Tregs] / (max(1.,(double)getStage(Tregs)->nbInsiders())), time);

        currentObserverAllPops->o_prcEDUDN1.pushData(100.* (double) nrEDUPerStage[DN1] / (max(1.,(double)getStage(DN1)->nbInsiders())), time);
        currentObserverAllPops->o_prcEDUDN2.pushData(100.* (double) nrEDUPerStage[DN2] / (max(1.,(double)getStage(DN2)->nbInsiders())), time);
        currentObserverAllPops->o_prcEDUDN3.pushData(100.* (double) nrEDUPerStage[DN3] / (max(1.,(double)getStage(DN3)->nbInsiders())), time);
        currentObserverAllPops->o_prcEDUDN4.pushData(100.* (double) nrEDUPerStage[DN4] / (max(1.,(double)getStage(DN4)->nbInsiders())), time);
        currentObserverAllPops->o_prcEDUDNTot.pushData(100.* (double) (nrEDUPerStage[DN1] + nrEDUPerStage[DN2] + nrEDUPerStage[DN3] + nrEDUPerStage[DN4])  /(max(1.,(double) ( getStage(DN1)->nbInsiders()+getStage(DN2)->nbInsiders()+getStage(DN3)->nbInsiders()+getStage(DN4)->nbInsiders()))), time);
        currentObserverAllPops->o_prcEDUeDP.pushData(100.* (double) nrEDUPerStage[eDP] / (max(1.,(double)getStage(eDP)->nbInsiders())), time);
        currentObserverAllPops->o_prcEDUlDP.pushData(100.* (double) nrEDUPerStage[lDP] / (max(1.,(double)getStage(lDP)->nbInsiders())), time);
        currentObserverAllPops->o_prcEDUSP4.pushData(100.* (double) nrEDUPerStage[SP4] / (max(1.,(double)getStage(SP4)->nbInsiders())), time);
        currentObserverAllPops->o_prcEDUSP8.pushData(100.* (double) nrEDUPerStage[SP8] / (max(1.,(double)getStage(SP8)->nbInsiders())), time);
        currentObserverAllPops->o_prcEDUTreg.pushData(100.* (double) nrEDUPerStage[Tregs] / (max(1.,(double)getStage(Tregs)->nbInsiders())), time);
    }

    // Finally, updates the time.
    time += dt;
    return true; // means to continue
}

void multiPopSimulation::finalize(){
    if(currentObserverAllPops) currentObserverAllPops->writeFiles();
}










