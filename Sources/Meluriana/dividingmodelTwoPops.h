#ifndef DIVIDINGMODEL_H
#define DIVIDINGMODEL_H

// This file defines a model for moonfit, i.e. it will take care of initialization
// and it has now a list of parameters

#include "../Moonfit/moonfit.h"
#include "cytonSimu.h"
#include "wholethymus.h"

// enum Backgrounds {WT, DKO};

/// @brife A dividing model is the wrapper around an agent-based simulation (see class cytonSimu class).
/// It contains the parameters and information to run the simulations, and will retrieve the observed variables fron
/// the agent-based simulation at the requested time-steps.
/// It implements all functions necessary to set-up a simulation, then the mother class modelAgentBased is pvoviding
/// a 'simulate()' function that call all these functions, and stops at each time-point where analysis is requested
struct dividingModelWithLabelledInflow : public modelAgentBased {

    /// @brief The agent-based model class that will be used for simulation
    onePopulation* currentSim;

    /// @brief A second population of cells that will be simulated as ancestors
    /// (founder cells can be picked from it)
    onePopulation* ancestors;

    /// @brief defines names of parameters/variables and parameter boundaries
    dividingModelWithLabelledInflow();
    /// @brief fills parameters values with a default (bad) set that simulations can run (for tests)
    void setBaseParameters();
    /// @brief Start a new instance of the agent-based simulation class (currentSim), gives to it the parameter values,
    /// and creates the initial population of cells.
    virtual void initialise(long long _background = 0); // will depend on the submodels

    void timeStep( const double tstart, const double tend);
    void analyzeState(const double t);
    void finalize();

    observerOneSim* currentObs;
    observerOneSim* currentObsAncestors;
    observerOneSim* currentObsIncomers;

        enum{aimedPopSize,
            avgG1,
            avgS,
            avgG2M,
            widthG1,
            widthS,
            widthG2M,
            paramDiff,
            deathRate,
            initialG1,
            initialS,
            initialG2M,
            initialWidthG1,
            initialWidthS,
            initialWidthG2M,
            initialParamDiff,
            initialDeathRate,
            percentQuiescentG0,
            typePopulation,
            waitDivideToDiff,
            initialWaitDivideToDiff,
            thresholdPos,
            timeBRDU,
            durationBRDU,
            timeEDU,
            durationEDU,
            preSimTime,
            timeSimulation,
            apoptoticTime,
            inflowRate,
            showAncestors,
            wacthing_window_pop,
            NbParameters};

    // Data we have over time
    enum{
        // put fitted variables first for the plots
        prcEDUpBRDUp,
        prcEDUpBRDUn,
        prcEDUnBRDUp,
        prcEDUnBRDUn,
        PreInG1,
        MiddleInG1,
        PostInG1,
        PreInS,
        MiddleInS,
        PostInS,
        PreInG2M,
        MiddleInG2M,
        PostInG2M,
        UnstInG1,
        UnstInS,
        UnstInG2M,
        TotInG1,
        TotInS,
        TotInG2,
        AvgDNAPre,
        AvgDNAMiddle,
        AvgDNAPost,
        AvgDNABRDU,
        AvgDNAUNeg,
        AvgDNAEDU,
        prcBRDU,
        prcEDU,
        doseEDU,
        doseBRDU,
        nCells,
        popSize,
        avgGen,
        nbG1,
        nbS,
        nbG2M,
        nbG0,
        nbEarly,
        nbMiddle,
        nbPost,
        nbNewcomers,
        flowOut,
        avgGenOut,
        gen0,
        gen1,
        gen2,
        gen3,
        gen4,
        gen5,
        gen6,
        gen7,
        gen8,
        gen9,
        gen10,
        apopt,
        NbVariables};

    long long background; // to simulate multiple options, like WT and KO
    //virtual void derivatives(const vector<double> &x, vector<double> &dxdt, const double t);

    void action(string nameAction, double parameter){
        if(!nameAction.compare("showMainPop")){
            params[showAncestors] = 0;
            return;
        }
        if(!nameAction.compare("showAncestors")){
            params[showAncestors] = 1;
            return;
        }
        if(!nameAction.compare("showIncomers")){
            params[showAncestors] = 2;
            return;
        }
        if(!nameAction.compare("showUnlabelled")){
            params[showAncestors] = 3;
            return;
        }
        // negative initial diff means no ancestors
        if(!nameAction.compare("removeInflow")){
            params[initialParamDiff] = -params[initialParamDiff];
            return;
        }
        if(!nameAction.compare("restoreInflow")){
            params[initialParamDiff] = -params[initialParamDiff];
            return;
        }
    }
};



#endif // DIVIDINGMODEL_H
