/********************************************************************************
** Form generated from reading UI file 'optselect.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPTSELECT_H
#define UI_OPTSELECT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_optSelect
{
public:
    QComboBox *comboBoxOptimizer;
    QLabel *label;
    QPlainTextEdit *plainTextEdit;
    QGroupBox *groupBox;
    QDoubleSpinBox *doubleSpinBoxParamCrossOver;
    QComboBox *comboBox6Selection;
    QComboBox *comboBox1Algo;
    QComboBox *comboBox5Replace;
    QComboBox *comboBox4Mutation;
    QComboBox *comboBox3CrossOver;
    QComboBox *comboBox2Parents;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QComboBox *comboBox7Strategy;
    QLabel *label_9;
    QDoubleSpinBox *doubleSpinBoxParamStrategy;
    QSpinBox *spinBoxNbRepeats;
    QSpinBox *spinBoxMaxCosts;
    QSpinBox *spinBoxPopSize;
    QDoubleSpinBox *doubleSpinBoxPropCrossOver;
    QDoubleSpinBox *doubleSpinBoxForkCoeff;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QPushButton *pushButtonGenerate;

    void setupUi(QWidget *optSelect)
    {
        if (optSelect->objectName().isEmpty())
            optSelect->setObjectName(QString::fromUtf8("optSelect"));
        optSelect->resize(331, 509);
        comboBoxOptimizer = new QComboBox(optSelect);
        comboBoxOptimizer->setObjectName(QString::fromUtf8("comboBoxOptimizer"));
        comboBoxOptimizer->setGeometry(QRect(120, 10, 161, 21));
        label = new QLabel(optSelect);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(40, 10, 61, 16));
        plainTextEdit = new QPlainTextEdit(optSelect);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(10, 410, 311, 91));
        groupBox = new QGroupBox(optSelect);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 40, 311, 371));
        doubleSpinBoxParamCrossOver = new QDoubleSpinBox(groupBox);
        doubleSpinBoxParamCrossOver->setObjectName(QString::fromUtf8("doubleSpinBoxParamCrossOver"));
        doubleSpinBoxParamCrossOver->setGeometry(QRect(220, 70, 81, 22));
        comboBox6Selection = new QComboBox(groupBox);
        comboBox6Selection->setObjectName(QString::fromUtf8("comboBox6Selection"));
        comboBox6Selection->setGeometry(QRect(80, 160, 191, 22));
        comboBox1Algo = new QComboBox(groupBox);
        comboBox1Algo->setObjectName(QString::fromUtf8("comboBox1Algo"));
        comboBox1Algo->setGeometry(QRect(80, 10, 191, 22));
        comboBox5Replace = new QComboBox(groupBox);
        comboBox5Replace->setObjectName(QString::fromUtf8("comboBox5Replace"));
        comboBox5Replace->setGeometry(QRect(80, 130, 191, 22));
        comboBox4Mutation = new QComboBox(groupBox);
        comboBox4Mutation->setObjectName(QString::fromUtf8("comboBox4Mutation"));
        comboBox4Mutation->setGeometry(QRect(80, 100, 191, 22));
        comboBox3CrossOver = new QComboBox(groupBox);
        comboBox3CrossOver->setObjectName(QString::fromUtf8("comboBox3CrossOver"));
        comboBox3CrossOver->setGeometry(QRect(80, 70, 131, 22));
        comboBox2Parents = new QComboBox(groupBox);
        comboBox2Parents->setObjectName(QString::fromUtf8("comboBox2Parents"));
        comboBox2Parents->setGeometry(QRect(80, 40, 191, 22));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 10, 46, 13));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 40, 46, 13));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 70, 51, 16));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(10, 100, 46, 13));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 130, 46, 13));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(10, 160, 51, 16));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(10, 190, 46, 13));
        comboBox7Strategy = new QComboBox(groupBox);
        comboBox7Strategy->setObjectName(QString::fromUtf8("comboBox7Strategy"));
        comboBox7Strategy->setGeometry(QRect(80, 190, 131, 22));
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(10, 220, 101, 16));
        doubleSpinBoxParamStrategy = new QDoubleSpinBox(groupBox);
        doubleSpinBoxParamStrategy->setObjectName(QString::fromUtf8("doubleSpinBoxParamStrategy"));
        doubleSpinBoxParamStrategy->setGeometry(QRect(220, 190, 81, 22));
        spinBoxNbRepeats = new QSpinBox(groupBox);
        spinBoxNbRepeats->setObjectName(QString::fromUtf8("spinBoxNbRepeats"));
        spinBoxNbRepeats->setGeometry(QRect(130, 220, 71, 22));
        spinBoxMaxCosts = new QSpinBox(groupBox);
        spinBoxMaxCosts->setObjectName(QString::fromUtf8("spinBoxMaxCosts"));
        spinBoxMaxCosts->setGeometry(QRect(130, 250, 71, 22));
        spinBoxPopSize = new QSpinBox(groupBox);
        spinBoxPopSize->setObjectName(QString::fromUtf8("spinBoxPopSize"));
        spinBoxPopSize->setGeometry(QRect(130, 280, 71, 22));
        doubleSpinBoxPropCrossOver = new QDoubleSpinBox(groupBox);
        doubleSpinBoxPropCrossOver->setObjectName(QString::fromUtf8("doubleSpinBoxPropCrossOver"));
        doubleSpinBoxPropCrossOver->setGeometry(QRect(130, 310, 91, 22));
        doubleSpinBoxForkCoeff = new QDoubleSpinBox(groupBox);
        doubleSpinBoxForkCoeff->setObjectName(QString::fromUtf8("doubleSpinBoxForkCoeff"));
        doubleSpinBoxForkCoeff->setGeometry(QRect(130, 340, 91, 22));
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(10, 250, 101, 16));
        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(10, 280, 101, 16));
        label_12 = new QLabel(groupBox);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(10, 310, 111, 16));
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(10, 340, 111, 16));
        pushButtonGenerate = new QPushButton(groupBox);
        pushButtonGenerate->setObjectName(QString::fromUtf8("pushButtonGenerate"));
        pushButtonGenerate->setGeometry(QRect(230, 340, 75, 23));

        retranslateUi(optSelect);

        QMetaObject::connectSlotsByName(optSelect);
    } // setupUi

    void retranslateUi(QWidget *optSelect)
    {
        optSelect->setWindowTitle(QApplication::translate("optSelect", "Form", nullptr));
        label->setText(QApplication::translate("optSelect", "Method", nullptr));
        groupBox->setTitle(QString());
        label_2->setText(QApplication::translate("optSelect", "Algo", nullptr));
        label_3->setText(QApplication::translate("optSelect", "Parents", nullptr));
        label_4->setText(QApplication::translate("optSelect", "CrossOver", nullptr));
        label_5->setText(QApplication::translate("optSelect", "Mutation", nullptr));
        label_6->setText(QApplication::translate("optSelect", "Replace", nullptr));
        label_7->setText(QApplication::translate("optSelect", "Selection", nullptr));
        label_8->setText(QApplication::translate("optSelect", "Strategy", nullptr));
        label_9->setText(QApplication::translate("optSelect", "nb Indep Repeats", nullptr));
        label_10->setText(QApplication::translate("optSelect", "Max Sims per opt", nullptr));
        label_11->setText(QApplication::translate("optSelect", "Population size", nullptr));
        label_12->setText(QApplication::translate("optSelect", "Proportion by CrossOv", nullptr));
        label_13->setText(QApplication::translate("optSelect", "ForkCoefficient", nullptr));
        pushButtonGenerate->setText(QApplication::translate("optSelect", "Generate", nullptr));
    } // retranslateUi

};

namespace Ui {
    class optSelect: public Ui_optSelect {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPTSELECT_H
