FORMS += \
    GUI/cytonwidget.ui \
    GUI/distribwidget.ui \
    GUI/mainboard.ui \
    GUI/mainwindow.ui \
    GUI/viewdistrib.ui

HEADERS += \
    GUI/common.h \
    cytonSimu.h \
    GUI/cytonwidget.h \
    distribution.h \
    GUI/distribwidget.h \
    GUI/grapheCustom.h \
    GUI/mainboard.h \
    GUI/mainwindow.h \
    random.h \
    statistiques.h \
    GUI/viewdistrib.h \
    GUI/QCustomPlot/qcustomplot.h \
    wholethymus.h

SOURCES += \
    GUI/common.cpp \
    cytonSimu.cpp \
    GUI/cytonwidget.cpp \
    distribution.cpp \
    GUI/distribwidget.cpp \
    GUI/grapheCustom.cpp \
    GUI/main.cpp \
    GUI/mainboard.cpp \
    GUI/mainwindow.cpp \
    random.cpp \
    statistiques.cpp \
    GUI/viewdistrib.cpp \
    GUI/QCustomPlot/qcustomplot.cpp \
    wholethymus.cpp

QT += core widgets printsupport
