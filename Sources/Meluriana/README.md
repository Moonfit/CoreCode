# Meluriana
Simulation software for the manuscript:
**High-resolution mapping of cell cycle dynamics during steady-state T-cell development and regeneration in vivo**

**Meluriana software** 

- simulates a population of cell going through the phases of the cell cycle and following a dual-pulse labelling with EdU, BrdU and DNA measurement at different time-points.
- finds the most likely duration of cell cycle phases to explain experimental dual-pulse datasets and allow to compare cycle heterogeneity hypotheses.
- simulates two conditions (WT and perturbed) and compare scenarios of differences in cell cycle phases to find the most likely explanation for the perturbated labelling dynamics.

Contact for issues / questions: philippe dot robert at ens minus lyon dot org
Please inform us with the above e-mail when creating an issue request in this github, we do not get automatic notification.

![Meluriana overview](doc/Scheme0.png?raw=true)

## Download / clone

```bash
git clone https://github.com/Moonfit/Meluriana/
```

## Usage

```bash
			 action             population                                         condition            heterogeneous_phases
./Meluriana  gui/fit/identifXX  DN1/DN2/DN3A/DN3B/DN4/preSel/postSel/CD4SP/CD8SP   WT/CTR/IRR/CTR_IRR   [VarG1/VarS/VarG2M/VarAll/NoVar]

allowingQuiescent      dataset                                                      addingBootstrap     differences_CTR/IRR          
[FixedG0/VariableG0]   [6P_D1D2D3/4P_D1/4P_D1D2/4P_D1D2D3/4P_D1D2D3D4/6P_D1D2D3D4]  [none/bootstrap]    [none/diffG1/diffS/diffG2M/diffAll]

Notes: 
- Possible identifiability commands (identifXX=identifG1/identifS/identifG2M/identifQuiescent/identifWG1/identifWS/identifWG2M/identifDeath/identifnDiv
- [] arguments are optional if using Meluriana with the graphical interface (first argument = gui)
- dataset refers to subselection of datapoints. 4P/=6P  4/6 time-points. D1: steady state EdU/BrdU. D2: Return of Post and Middle in G1, Unstained in S and Post in S. 
  D3: steady percents of cells in G1/S/G2M. D4: Avg DNA levels in Pre and Middle
```

# Simulation of one population at a time (WT, CTR or IRR)
Examples: minimal executions using the graphical interface
```bash
./Meluriana gui DN1 WT
./Meluriana gui DN3A IRR
./Meluriana gui DN3A IRR
```

Execution giving all parameters for a parameter estimation
```bash
./Meluriana gui DN3B WT VarG1 FixedG0 6P_D1D2D3D4 none 
./Meluriana gui DN3A IRR VarS VariableG0 4P_D1D2 bootstrap
```

Which is identical to the following commands, that will run by themselves in command line without gui (graphical interface)
```bash
./Meluriana fit DN3B WT VarG1 FixedG0 6P_D1D2D3D4 none 
./Meluriana fit DN3A IRR VarS VariableG0 4P_D1D2 bootstrap
```

To explore the likelihood of each phase duration (profile likelyhood). Do not use bootstrap.
```bash
./Meluriana identifS DN3B WT VarG1 FixedG0 6P_D1D2D3D4 none 
```

# Simulation of one population at a time (CTR and IRR together)

```bash
./Meluriana gui DN3B CTR_IRR
```

```bash
./Meluriana fit DN3B CTR_IRR VarG1 VariableG0 6P_D1D2D3D4 none diffS
```

## Installation

Meluriana was written in C++ using the Moonfit framework for parameter estimation.
- requires **a C++ compiler**
- requires the **Qt framework**

Please define the working folders of interest manually inside Meluriana/main.cpp, lines 63-64
```bash
folder = "/home/... to fill .../Meluriana/";
folderBaseResults = "/home/... tofill ... /Meluriana/Results/";
```


#Installing on linux (simplest):
```bash
#sudo apt-get install build-essential  #if you don't have g++ and make 

#to find the available qt packages in your distribution
apt-cache search qtbase
apt-cache search libqt5

#These packages should do the job
sudo apt-get install qtbase5-dev
sudo apt-get install qtcreator
sudo apt-get install libqt5svg5
sudo apt-get install libqt5printsupport5

cd Meluriana/
qmake Meluriana.pro
make
```

#Installing on Windows:

We recommend installing the Qt framework including a C++ compiler if you don't have. Please visit qt.io/download

Compiling 

```bash
cd Meluriana/bin
qmake ../Meluriana.pro	#qmake creates a new Makefile embedding Qt libraries locations
make		#This creates the executable of Meluriana
```

Alternately, if you have installed the Qt framework with qtcreator, 
```bash
qtcreator Meluriana/Meluriana.pro
```
When qtcreator opens, select the C++ compiler, then once the project is opened, select Release instead of debug (bottom left) and use the green arrow to compile and run.
Use projects tab > Desktop > Run > command line arguments to put the desired arguments (without ./Meluriana)

#Installing on MAC:

```bash
#if no recent g++ compiler, use this command (will also install g++)
brew install gcc 
brew install qt
```

Then, from the Meluriana/bin/ folder, 
```bash
qmake ../Meluriana.pro
make
```

Some OS specific points:
Inside Meluriana.pro, your compiler might recognize only -o1 or -O1. 
Linking the libraries might require that you provide their folder. Can either add it in the Absolut.pro linker options, or inside the Makefile manually (but then do not run qmake anymore). 
Depending on your g++ compiler, there might be conflicts between the C++ language of the libraries and the C++ standard libraries provided with the compiler. 
This might be solved by adding "QMAKE_CXXFLAGS += -std=c++14 -std=c++17" inside Meluriana.pro. We didn't add these lines by default in case your C++ compiler doesn't support C++17.

*Meluriana has been run on a HPC cluster with CentOS Linux version 7, and gcc version 4.8.5, Qt version 5.12.5*  

## Detailed explanation 

# File structure

Moonfit/ contains the Moonfit GUI/simulation framework 

Meluriana/ contains the code to simulate an agent-based model of proliferating cells and their labelling, following a program design that Moonfit graphical interface can call. 

CytonSimu/ defines agents (cells), populations (stages) and functions to create new cells or evolve the cells through the cell cycle. This file is named after the Cyton model introduced by Wellard 2011, since we simulate analoguous populations with log-normal residence times. 

ExpLabelling/ defines the setup of a simulation

DividingModel/ defines simulations using CytonSimu as a Moonfit subclass, with inputs = parameters (cycle durations) and output = results of labelling experiment after simulating t=~20 hours.

# Steps of a simulation

The parameters of a simulation define how the cells will behave (cell cycle phase, among others). Labelling is performed in a yes-or-no manner to cells that are in the S phase. The population of cells is monitored like in the experimental settings. Further, a populations has entry from previous populations and exit after a fixed number of divisions. 

![Meluriana overview](doc/Scheme1.png?raw=true)

This allows to simulate a flow cytometry experiment in silico:

![Meluriana overview](doc/Scheme2.png?raw=true)

By chosing standard deviations cycle phase durations, one can simulate different scenario of cycle heterogeneity

![Meluriana overview](doc/Scheme3.png?raw=true)

It is possible to compare such different scenario compared to experimental data

![Meluriana overview](doc/Scheme4.png?raw=true)
 
Finally, by doing parameter optimization, a huge number of possible phase durations is tested and the scenario best fitting to the data can be compared depending on the heterogeneity scenario

![Meluriana overview](doc/Scheme5.png?raw=true)

By doing parameter Identifiability for one parameter (for instance for the duration of S phase), each possible value of S phase is fixed and other parameter are estimated. If the cost does not depend on the fixed S value, it means S duration is not identifiable. However, if S duration shows a minimum cost, it means only this minimum can explain best the data.

![Meluriana overview](doc/Scheme6.png?raw=true)

## Documentation / Reference / Reproducing

Robert, Philippe A., Henrik Jönsson, and Michael Meyer-Hermann. 2018. “MoonFit, a Minimal Interface for Fitting ODE Dynamical Models, Bridging Simulation by Experimentalists and Customization by C++ Programmers.” bioRxiv. https://doi.org/10.1101/281188.

Wellard, C., Markham, J. F., Hawkins, E. D., & Hodgkin, P. D. (2011). The cyton model for lymphocyte proliferation and differentiation. In Mathematical Models and Immune Cell Biology (pp. 107-120). New York, NY: Springer New York.