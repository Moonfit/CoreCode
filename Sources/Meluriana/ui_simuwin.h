/********************************************************************************
** Form generated from reading UI file 'simuwin.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIMUWIN_H
#define UI_SIMUWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_simuWin
{
public:
    QGroupBox *groupBoxPlot;
    QComboBox *comboBoxVariable;
    QProgressBar *progressBar;
    QDoubleSpinBox *doubleSpinBoxCostVar;
    QLabel *labelCostVar;
    QLabel *labelCostExp;
    QDoubleSpinBox *doubleSpinBoxCostExp;
    QWidget *widgetGraphe;
    QPushButton *pushButtonExpandPlot;
    QDoubleSpinBox *doubleSpinBoxCostMultiExp;
    QCheckBox *checkBoxCostMultiExp;
    QCheckBox *checkBoxLog;
    QTextBrowser *textBrowserStatus;
    QPushButton *pushButtonGo;
    QGroupBox *groupBox_3;
    QPushButton *pushButtonResetParams;
    QPushButton *pushButtonSaveSet;
    QPushButton *pushButtonLoadSet;
    QPushButton *pushButtonSaveConfig;
    QPushButton *pushButtonLoadConfig;
    QPushButton *pushButtonReport;
    QPushButton *pushButtonSensitivity;
    QPushButton *pushButtonIdentifiability;
    QTabWidget *tabWidget;
    QWidget *tabParameters;
    QWidget *widgetForTable;
    QCheckBox *checkBoxOnlyComb;
    QSpinBox *spinBoxOnlyComb;
    QLabel *labelLastParam;
    QDoubleSpinBox *doubleSpinBoxLastParam;
    QWidget *tabOptimization;
    QWidget *widgetCostEvolution;
    QTableView *tableViewMultiExp;
    QLabel *label_8;
    QWidget *tabSettings;
    QLabel *labelSolver;
    QLabel *labelDivergence;
    QDoubleSpinBox *doubleSpinBoxGraphDT;
    QLabel *label;
    QLabel *label_2;
    QDoubleSpinBox *doubleSpinBoxSimDT;
    QWidget *widgetOpt;
    QPlainTextEdit *plainTextEditOptFile;
    QLabel *labelComboCost;
    QComboBox *comboBoxCostType;
    QLabel *labelComboCost_2;
    QComboBox *comboBoxCostNorm;
    QPushButton *pushButtonPerturb;
    QPushButton *pushButtonCompare;
    QComboBox *comboBoxSubExperiment;
    QLabel *label_3;
    QCheckBox *checkBoxAll;
    QGroupBox *groupBoxHistory;
    QTableView *tableViewHistory;
    QPushButton *pushButtonRefresh;
    QSpinBox *spinBoxDisplay;
    QSpinBox *spinBoxStore;
    QLabel *label_5;
    QLabel *label_7;
    QPushButton *pushButtonSaveHistory;
    QPushButton *pushButtonLoadHistory;
    QCheckBox *checkBoxLinkToCombs;
    QTabWidget *tabWidgetRight;
    QWidget *widgetOpt1;
    QWidget *widgetForCost;
    QWidget *widgetMacros;
    QTextBrowser *textBrowserMacro;
    QPushButton *pushButtonResetMacro;
    QCheckBox *checkBoxRecord;
    QPushButton *pushButtonRefreshMacro;
    QGroupBox *groupBoxFuture;
    QPushButton *pushButtonLoadOpt;
    QLabel *label_6;
    QLabel *labelModelName;
    QLabel *labelMultiExp;
    QComboBox *comboBoxMultiExperiment;
    QGroupBox *groupBoxOptim;
    QLCDNumber *lcdNumberCostCall;
    QPushButton *pushButtonStop;
    QPushButton *pushButtonOptimize;
    QCheckBox *checkBoxDisplayCurves;
    QLineEdit *lineEditWorkingFolder;
    QLabel *label_4;
    QLabel *label_12;
    QLabel *labelCostFn;

    void setupUi(QWidget *simuWin)
    {
        if (simuWin->objectName().isEmpty())
            simuWin->setObjectName(QString::fromUtf8("simuWin"));
        simuWin->resize(1341, 915);
        groupBoxPlot = new QGroupBox(simuWin);
        groupBoxPlot->setObjectName(QString::fromUtf8("groupBoxPlot"));
        groupBoxPlot->setGeometry(QRect(720, 10, 611, 491));
        groupBoxPlot->setFlat(true);
        comboBoxVariable = new QComboBox(groupBoxPlot);
        comboBoxVariable->setObjectName(QString::fromUtf8("comboBoxVariable"));
        comboBoxVariable->setGeometry(QRect(80, 0, 281, 28));
        progressBar = new QProgressBar(groupBoxPlot);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(370, 0, 201, 23));
        progressBar->setValue(24);
        doubleSpinBoxCostVar = new QDoubleSpinBox(groupBoxPlot);
        doubleSpinBoxCostVar->setObjectName(QString::fromUtf8("doubleSpinBoxCostVar"));
        doubleSpinBoxCostVar->setGeometry(QRect(90, 410, 91, 21));
        labelCostVar = new QLabel(groupBoxPlot);
        labelCostVar->setObjectName(QString::fromUtf8("labelCostVar"));
        labelCostVar->setGeometry(QRect(10, 410, 91, 16));
        labelCostExp = new QLabel(groupBoxPlot);
        labelCostExp->setObjectName(QString::fromUtf8("labelCostExp"));
        labelCostExp->setGeometry(QRect(190, 410, 91, 16));
        doubleSpinBoxCostExp = new QDoubleSpinBox(groupBoxPlot);
        doubleSpinBoxCostExp->setObjectName(QString::fromUtf8("doubleSpinBoxCostExp"));
        doubleSpinBoxCostExp->setEnabled(true);
        doubleSpinBoxCostExp->setGeometry(QRect(290, 411, 91, 21));
        widgetGraphe = new QWidget(groupBoxPlot);
        widgetGraphe->setObjectName(QString::fromUtf8("widgetGraphe"));
        widgetGraphe->setGeometry(QRect(10, 40, 591, 361));
        pushButtonExpandPlot = new QPushButton(groupBoxPlot);
        pushButtonExpandPlot->setObjectName(QString::fromUtf8("pushButtonExpandPlot"));
        pushButtonExpandPlot->setGeometry(QRect(570, 0, 41, 28));
        doubleSpinBoxCostMultiExp = new QDoubleSpinBox(groupBoxPlot);
        doubleSpinBoxCostMultiExp->setObjectName(QString::fromUtf8("doubleSpinBoxCostMultiExp"));
        doubleSpinBoxCostMultiExp->setEnabled(true);
        doubleSpinBoxCostMultiExp->setGeometry(QRect(510, 410, 91, 21));
        checkBoxCostMultiExp = new QCheckBox(groupBoxPlot);
        checkBoxCostMultiExp->setObjectName(QString::fromUtf8("checkBoxCostMultiExp"));
        checkBoxCostMultiExp->setGeometry(QRect(400, 410, 101, 20));
        checkBoxLog = new QCheckBox(groupBoxPlot);
        checkBoxLog->setObjectName(QString::fromUtf8("checkBoxLog"));
        checkBoxLog->setGeometry(QRect(0, 20, 51, 20));
        textBrowserStatus = new QTextBrowser(groupBoxPlot);
        textBrowserStatus->setObjectName(QString::fromUtf8("textBrowserStatus"));
        textBrowserStatus->setGeometry(QRect(10, 440, 591, 41));
        pushButtonGo = new QPushButton(groupBoxPlot);
        pushButtonGo->setObjectName(QString::fromUtf8("pushButtonGo"));
        pushButtonGo->setGeometry(QRect(400, 0, 141, 28));
        groupBox_3 = new QGroupBox(simuWin);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 90, 711, 911));
        groupBox_3->setFlat(true);
        pushButtonResetParams = new QPushButton(groupBox_3);
        pushButtonResetParams->setObjectName(QString::fromUtf8("pushButtonResetParams"));
        pushButtonResetParams->setGeometry(QRect(310, 45, 91, 21));
        pushButtonSaveSet = new QPushButton(groupBox_3);
        pushButtonSaveSet->setObjectName(QString::fromUtf8("pushButtonSaveSet"));
        pushButtonSaveSet->setGeometry(QRect(200, 45, 101, 21));
        pushButtonLoadSet = new QPushButton(groupBox_3);
        pushButtonLoadSet->setObjectName(QString::fromUtf8("pushButtonLoadSet"));
        pushButtonLoadSet->setGeometry(QRect(90, 45, 101, 21));
        pushButtonSaveConfig = new QPushButton(groupBox_3);
        pushButtonSaveConfig->setObjectName(QString::fromUtf8("pushButtonSaveConfig"));
        pushButtonSaveConfig->setGeometry(QRect(200, 10, 101, 31));
        pushButtonLoadConfig = new QPushButton(groupBox_3);
        pushButtonLoadConfig->setObjectName(QString::fromUtf8("pushButtonLoadConfig"));
        pushButtonLoadConfig->setGeometry(QRect(90, 10, 101, 31));
        pushButtonReport = new QPushButton(groupBox_3);
        pushButtonReport->setObjectName(QString::fromUtf8("pushButtonReport"));
        pushButtonReport->setGeometry(QRect(310, 10, 91, 31));
        pushButtonSensitivity = new QPushButton(groupBox_3);
        pushButtonSensitivity->setObjectName(QString::fromUtf8("pushButtonSensitivity"));
        pushButtonSensitivity->setGeometry(QRect(410, 10, 101, 31));
        pushButtonIdentifiability = new QPushButton(groupBox_3);
        pushButtonIdentifiability->setObjectName(QString::fromUtf8("pushButtonIdentifiability"));
        pushButtonIdentifiability->setGeometry(QRect(520, 10, 101, 31));
        tabWidget = new QTabWidget(groupBox_3);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(10, 70, 701, 851));
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabParameters = new QWidget();
        tabParameters->setObjectName(QString::fromUtf8("tabParameters"));
        widgetForTable = new QWidget(tabParameters);
        widgetForTable->setObjectName(QString::fromUtf8("widgetForTable"));
        widgetForTable->setGeometry(QRect(0, 20, 691, 881));
        checkBoxOnlyComb = new QCheckBox(tabParameters);
        checkBoxOnlyComb->setObjectName(QString::fromUtf8("checkBoxOnlyComb"));
        checkBoxOnlyComb->setGeometry(QRect(430, 0, 181, 20));
        spinBoxOnlyComb = new QSpinBox(tabParameters);
        spinBoxOnlyComb->setObjectName(QString::fromUtf8("spinBoxOnlyComb"));
        spinBoxOnlyComb->setGeometry(QRect(620, 0, 71, 22));
        labelLastParam = new QLabel(tabParameters);
        labelLastParam->setObjectName(QString::fromUtf8("labelLastParam"));
        labelLastParam->setGeometry(QRect(30, 0, 191, 21));
        doubleSpinBoxLastParam = new QDoubleSpinBox(tabParameters);
        doubleSpinBoxLastParam->setObjectName(QString::fromUtf8("doubleSpinBoxLastParam"));
        doubleSpinBoxLastParam->setGeometry(QRect(220, 0, 101, 21));
        doubleSpinBoxLastParam->setDecimals(6);
        tabWidget->addTab(tabParameters, QString());
        tabOptimization = new QWidget();
        tabOptimization->setObjectName(QString::fromUtf8("tabOptimization"));
        widgetCostEvolution = new QWidget(tabOptimization);
        widgetCostEvolution->setObjectName(QString::fromUtf8("widgetCostEvolution"));
        widgetCostEvolution->setGeometry(QRect(20, 10, 661, 411));
        tableViewMultiExp = new QTableView(tabOptimization);
        tableViewMultiExp->setObjectName(QString::fromUtf8("tableViewMultiExp"));
        tableViewMultiExp->setGeometry(QRect(20, 470, 661, 191));
        label_8 = new QLabel(tabOptimization);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(10, 450, 401, 16));
        tabWidget->addTab(tabOptimization, QString());
        tabSettings = new QWidget();
        tabSettings->setObjectName(QString::fromUtf8("tabSettings"));
        labelSolver = new QLabel(tabSettings);
        labelSolver->setObjectName(QString::fromUtf8("labelSolver"));
        labelSolver->setGeometry(QRect(420, 30, 111, 16));
        labelDivergence = new QLabel(tabSettings);
        labelDivergence->setObjectName(QString::fromUtf8("labelDivergence"));
        labelDivergence->setGeometry(QRect(420, 120, 111, 16));
        doubleSpinBoxGraphDT = new QDoubleSpinBox(tabSettings);
        doubleSpinBoxGraphDT->setObjectName(QString::fromUtf8("doubleSpinBoxGraphDT"));
        doubleSpinBoxGraphDT->setGeometry(QRect(500, 180, 81, 22));
        label = new QLabel(tabSettings);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(420, 150, 46, 21));
        label_2 = new QLabel(tabSettings);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(420, 180, 61, 21));
        doubleSpinBoxSimDT = new QDoubleSpinBox(tabSettings);
        doubleSpinBoxSimDT->setObjectName(QString::fromUtf8("doubleSpinBoxSimDT"));
        doubleSpinBoxSimDT->setGeometry(QRect(500, 150, 81, 22));
        doubleSpinBoxSimDT->setDecimals(5);
        widgetOpt = new QWidget(tabSettings);
        widgetOpt->setObjectName(QString::fromUtf8("widgetOpt"));
        widgetOpt->setGeometry(QRect(10, 10, 391, 451));
        plainTextEditOptFile = new QPlainTextEdit(tabSettings);
        plainTextEditOptFile->setObjectName(QString::fromUtf8("plainTextEditOptFile"));
        plainTextEditOptFile->setGeometry(QRect(10, 470, 391, 191));
        labelComboCost = new QLabel(tabSettings);
        labelComboCost->setObjectName(QString::fromUtf8("labelComboCost"));
        labelComboCost->setGeometry(QRect(420, 60, 101, 16));
        comboBoxCostType = new QComboBox(tabSettings);
        comboBoxCostType->setObjectName(QString::fromUtf8("comboBoxCostType"));
        comboBoxCostType->setGeometry(QRect(540, 60, 131, 22));
        labelComboCost_2 = new QLabel(tabSettings);
        labelComboCost_2->setObjectName(QString::fromUtf8("labelComboCost_2"));
        labelComboCost_2->setGeometry(QRect(420, 90, 101, 16));
        comboBoxCostNorm = new QComboBox(tabSettings);
        comboBoxCostNorm->setObjectName(QString::fromUtf8("comboBoxCostNorm"));
        comboBoxCostNorm->setGeometry(QRect(540, 90, 131, 22));
        tabWidget->addTab(tabSettings, QString());
        pushButtonPerturb = new QPushButton(groupBox_3);
        pushButtonPerturb->setObjectName(QString::fromUtf8("pushButtonPerturb"));
        pushButtonPerturb->setGeometry(QRect(410, 45, 101, 21));
        pushButtonCompare = new QPushButton(groupBox_3);
        pushButtonCompare->setObjectName(QString::fromUtf8("pushButtonCompare"));
        pushButtonCompare->setGeometry(QRect(520, 45, 101, 21));
        comboBoxSubExperiment = new QComboBox(simuWin);
        comboBoxSubExperiment->setObjectName(QString::fromUtf8("comboBoxSubExperiment"));
        comboBoxSubExperiment->setGeometry(QRect(100, 60, 411, 21));
        label_3 = new QLabel(simuWin);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 60, 71, 16));
        checkBoxAll = new QCheckBox(simuWin);
        checkBoxAll->setObjectName(QString::fromUtf8("checkBoxAll"));
        checkBoxAll->setGeometry(QRect(540, 60, 161, 21));
        groupBoxHistory = new QGroupBox(simuWin);
        groupBoxHistory->setObjectName(QString::fromUtf8("groupBoxHistory"));
        groupBoxHistory->setGeometry(QRect(720, 570, 611, 281));
        groupBoxHistory->setFlat(true);
        tableViewHistory = new QTableView(groupBoxHistory);
        tableViewHistory->setObjectName(QString::fromUtf8("tableViewHistory"));
        tableViewHistory->setGeometry(QRect(10, 51, 591, 221));
        pushButtonRefresh = new QPushButton(groupBoxHistory);
        pushButtonRefresh->setObjectName(QString::fromUtf8("pushButtonRefresh"));
        pushButtonRefresh->setGeometry(QRect(20, 20, 71, 28));
        spinBoxDisplay = new QSpinBox(groupBoxHistory);
        spinBoxDisplay->setObjectName(QString::fromUtf8("spinBoxDisplay"));
        spinBoxDisplay->setGeometry(QRect(400, 20, 71, 22));
        spinBoxDisplay->setMaximum(10000);
        spinBoxStore = new QSpinBox(groupBoxHistory);
        spinBoxStore->setObjectName(QString::fromUtf8("spinBoxStore"));
        spinBoxStore->setGeometry(QRect(520, 20, 81, 22));
        spinBoxStore->setMaximum(100000);
        label_5 = new QLabel(groupBoxHistory);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(350, 20, 41, 21));
        label_7 = new QLabel(groupBoxHistory);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(480, 20, 31, 21));
        pushButtonSaveHistory = new QPushButton(groupBoxHistory);
        pushButtonSaveHistory->setObjectName(QString::fromUtf8("pushButtonSaveHistory"));
        pushButtonSaveHistory->setGeometry(QRect(100, 20, 51, 28));
        pushButtonLoadHistory = new QPushButton(groupBoxHistory);
        pushButtonLoadHistory->setObjectName(QString::fromUtf8("pushButtonLoadHistory"));
        pushButtonLoadHistory->setGeometry(QRect(160, 20, 51, 28));
        checkBoxLinkToCombs = new QCheckBox(groupBoxHistory);
        checkBoxLinkToCombs->setObjectName(QString::fromUtf8("checkBoxLinkToCombs"));
        checkBoxLinkToCombs->setGeometry(QRect(230, 20, 111, 21));
        tabWidgetRight = new QTabWidget(simuWin);
        tabWidgetRight->setObjectName(QString::fromUtf8("tabWidgetRight"));
        tabWidgetRight->setGeometry(QRect(1440, 970, 421, 831));
        widgetOpt1 = new QWidget();
        widgetOpt1->setObjectName(QString::fromUtf8("widgetOpt1"));
        tabWidgetRight->addTab(widgetOpt1, QString());
        widgetForCost = new QWidget();
        widgetForCost->setObjectName(QString::fromUtf8("widgetForCost"));
        tabWidgetRight->addTab(widgetForCost, QString());
        widgetMacros = new QWidget();
        widgetMacros->setObjectName(QString::fromUtf8("widgetMacros"));
        textBrowserMacro = new QTextBrowser(widgetMacros);
        textBrowserMacro->setObjectName(QString::fromUtf8("textBrowserMacro"));
        textBrowserMacro->setGeometry(QRect(10, 40, 391, 461));
        pushButtonResetMacro = new QPushButton(widgetMacros);
        pushButtonResetMacro->setObjectName(QString::fromUtf8("pushButtonResetMacro"));
        pushButtonResetMacro->setGeometry(QRect(320, 10, 75, 23));
        checkBoxRecord = new QCheckBox(widgetMacros);
        checkBoxRecord->setObjectName(QString::fromUtf8("checkBoxRecord"));
        checkBoxRecord->setGeometry(QRect(10, 10, 70, 17));
        pushButtonRefreshMacro = new QPushButton(widgetMacros);
        pushButtonRefreshMacro->setObjectName(QString::fromUtf8("pushButtonRefreshMacro"));
        pushButtonRefreshMacro->setGeometry(QRect(70, 10, 75, 23));
        tabWidgetRight->addTab(widgetMacros, QString());
        groupBoxFuture = new QGroupBox(simuWin);
        groupBoxFuture->setObjectName(QString::fromUtf8("groupBoxFuture"));
        groupBoxFuture->setGeometry(QRect(1400, 930, 421, 80));
        pushButtonLoadOpt = new QPushButton(groupBoxFuture);
        pushButtonLoadOpt->setObjectName(QString::fromUtf8("pushButtonLoadOpt"));
        pushButtonLoadOpt->setGeometry(QRect(180, 20, 131, 28));
        label_6 = new QLabel(simuWin);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 0, 71, 31));
        labelModelName = new QLabel(simuWin);
        labelModelName->setObjectName(QString::fromUtf8("labelModelName"));
        labelModelName->setGeometry(QRect(80, 0, 431, 31));
        labelMultiExp = new QLabel(simuWin);
        labelMultiExp->setObjectName(QString::fromUtf8("labelMultiExp"));
        labelMultiExp->setGeometry(QRect(10, 30, 441, 21));
        comboBoxMultiExperiment = new QComboBox(simuWin);
        comboBoxMultiExperiment->setObjectName(QString::fromUtf8("comboBoxMultiExperiment"));
        comboBoxMultiExperiment->setGeometry(QRect(100, 30, 411, 21));
        groupBoxOptim = new QGroupBox(simuWin);
        groupBoxOptim->setObjectName(QString::fromUtf8("groupBoxOptim"));
        groupBoxOptim->setGeometry(QRect(720, 500, 611, 80));
        groupBoxOptim->setFlat(true);
        groupBoxOptim->setCheckable(false);
        lcdNumberCostCall = new QLCDNumber(groupBoxOptim);
        lcdNumberCostCall->setObjectName(QString::fromUtf8("lcdNumberCostCall"));
        lcdNumberCostCall->setGeometry(QRect(450, 20, 151, 23));
        pushButtonStop = new QPushButton(groupBoxOptim);
        pushButtonStop->setObjectName(QString::fromUtf8("pushButtonStop"));
        pushButtonStop->setGeometry(QRect(120, 20, 41, 28));
        pushButtonOptimize = new QPushButton(groupBoxOptim);
        pushButtonOptimize->setObjectName(QString::fromUtf8("pushButtonOptimize"));
        pushButtonOptimize->setGeometry(QRect(20, 20, 91, 28));
        QPalette palette;
        QBrush brush(QColor(170, 170, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        pushButtonOptimize->setPalette(palette);
        checkBoxDisplayCurves = new QCheckBox(groupBoxOptim);
        checkBoxDisplayCurves->setObjectName(QString::fromUtf8("checkBoxDisplayCurves"));
        checkBoxDisplayCurves->setGeometry(QRect(170, 20, 111, 21));
        lineEditWorkingFolder = new QLineEdit(groupBoxOptim);
        lineEditWorkingFolder->setObjectName(QString::fromUtf8("lineEditWorkingFolder"));
        lineEditWorkingFolder->setGeometry(QRect(170, 50, 431, 21));
        label_4 = new QLabel(groupBoxOptim);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(30, 50, 141, 16));
        label_12 = new QLabel(groupBoxOptim);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(330, 20, 111, 21));
        labelCostFn = new QLabel(simuWin);
        labelCostFn->setObjectName(QString::fromUtf8("labelCostFn"));
        labelCostFn->setGeometry(QRect(540, 30, 171, 16));
        comboBoxMultiExperiment->raise();
        groupBoxPlot->raise();
        groupBox_3->raise();
        comboBoxSubExperiment->raise();
        label_3->raise();
        checkBoxAll->raise();
        groupBoxHistory->raise();
        tabWidgetRight->raise();
        groupBoxFuture->raise();
        label_6->raise();
        labelModelName->raise();
        groupBoxOptim->raise();
        labelMultiExp->raise();
        labelCostFn->raise();

        retranslateUi(simuWin);

        tabWidget->setCurrentIndex(0);
        tabWidgetRight->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(simuWin);
    } // setupUi

    void retranslateUi(QWidget *simuWin)
    {
        simuWin->setWindowTitle(QApplication::translate("simuWin", "Form", nullptr));
        groupBoxPlot->setTitle(QApplication::translate("simuWin", "Variable", nullptr));
        labelCostVar->setText(QApplication::translate("simuWin", "Cost Variable", nullptr));
        labelCostExp->setText(QApplication::translate("simuWin", "Cost Experiments", nullptr));
        pushButtonExpandPlot->setText(QApplication::translate("simuWin", "Big", nullptr));
        checkBoxCostMultiExp->setText(QApplication::translate("simuWin", "Cost MultiExp", nullptr));
        checkBoxLog->setText(QApplication::translate("simuWin", "Log", nullptr));
        pushButtonGo->setText(QApplication::translate("simuWin", "(re-) Simulate", nullptr));
        groupBox_3->setTitle(QString());
        pushButtonResetParams->setText(QApplication::translate("simuWin", "Default Set", nullptr));
        pushButtonSaveSet->setText(QApplication::translate("simuWin", "Save Set", nullptr));
        pushButtonLoadSet->setText(QApplication::translate("simuWin", "Load Set", nullptr));
        pushButtonSaveConfig->setText(QApplication::translate("simuWin", "Save Config", nullptr));
        pushButtonLoadConfig->setText(QApplication::translate("simuWin", "Load Config", nullptr));
        pushButtonReport->setText(QApplication::translate("simuWin", "Report", nullptr));
        pushButtonSensitivity->setText(QApplication::translate("simuWin", "Sensitivity", nullptr));
        pushButtonIdentifiability->setText(QApplication::translate("simuWin", "Identifiability", nullptr));
        checkBoxOnlyComb->setText(QApplication::translate("simuWin", "Show Only Combination nr", nullptr));
        labelLastParam->setText(QApplication::translate("simuWin", "Value last changed parameter", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabParameters), QApplication::translate("simuWin", "Parameters", nullptr));
        label_8->setText(QApplication::translate("simuWin", "Description of cost per variable", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabOptimization), QApplication::translate("simuWin", "Optimization", nullptr));
        labelSolver->setText(QApplication::translate("simuWin", "Solver", nullptr));
        labelDivergence->setText(QApplication::translate("simuWin", "Divg. Threshold:", nullptr));
        label->setText(QApplication::translate("simuWin", "sim dt", nullptr));
        label_2->setText(QApplication::translate("simuWin", "graph dt", nullptr));
        labelComboCost->setText(QApplication::translate("simuWin", "Cost Function", nullptr));
        labelComboCost_2->setText(QApplication::translate("simuWin", "Cost Normalization", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabSettings), QApplication::translate("simuWin", "Settings", nullptr));
        pushButtonPerturb->setText(QApplication::translate("simuWin", "Modulate", nullptr));
        pushButtonCompare->setText(QApplication::translate("simuWin", "Compare Sets", nullptr));
        label_3->setText(QApplication::translate("simuWin", "Conditions", nullptr));
        checkBoxAll->setText(QApplication::translate("simuWin", "Simulate All Conditions", nullptr));
        groupBoxHistory->setTitle(QApplication::translate("simuWin", "History - Best parameter sets", nullptr));
        pushButtonRefresh->setText(QApplication::translate("simuWin", "Refresh", nullptr));
        label_5->setText(QApplication::translate("simuWin", "Display", nullptr));
        label_7->setText(QApplication::translate("simuWin", "Store", nullptr));
        pushButtonSaveHistory->setText(QApplication::translate("simuWin", "Save", nullptr));
        pushButtonLoadHistory->setText(QApplication::translate("simuWin", "Load", nullptr));
        checkBoxLinkToCombs->setText(QApplication::translate("simuWin", "Link to Combs", nullptr));
        tabWidgetRight->setTabText(tabWidgetRight->indexOf(widgetOpt1), QApplication::translate("simuWin", "Optimizers", nullptr));
        tabWidgetRight->setTabText(tabWidgetRight->indexOf(widgetForCost), QApplication::translate("simuWin", "Cost evolution", nullptr));
        pushButtonResetMacro->setText(QApplication::translate("simuWin", "Reset", nullptr));
        checkBoxRecord->setText(QApplication::translate("simuWin", "Record", nullptr));
        pushButtonRefreshMacro->setText(QApplication::translate("simuWin", "Refresh", nullptr));
        tabWidgetRight->setTabText(tabWidgetRight->indexOf(widgetMacros), QApplication::translate("simuWin", "Macro", nullptr));
        groupBoxFuture->setTitle(QApplication::translate("simuWin", "GroupBox", nullptr));
        pushButtonLoadOpt->setText(QApplication::translate("simuWin", "Load Optimizer File", nullptr));
        label_6->setText(QApplication::translate("simuWin", "Model :", nullptr));
        labelModelName->setText(QApplication::translate("simuWin", "-", nullptr));
        labelMultiExp->setText(QApplication::translate("simuWin", "Group", nullptr));
        groupBoxOptim->setTitle(QApplication::translate("simuWin", "Optimization", nullptr));
        pushButtonStop->setText(QApplication::translate("simuWin", "Stop", nullptr));
        pushButtonOptimize->setText(QApplication::translate("simuWin", "Optimize", nullptr));
        checkBoxDisplayCurves->setText(QApplication::translate("simuWin", "Display Curves", nullptr));
        label_4->setText(QApplication::translate("simuWin", "WorkingFolder", nullptr));
        label_12->setText(QApplication::translate("simuWin", "Nr. Sim. Performed:", nullptr));
        labelCostFn->setText(QApplication::translate("simuWin", "CostFn", nullptr));
    } // retranslateUi

};

namespace Ui {
    class simuWin: public Ui_simuWin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIMUWIN_H
