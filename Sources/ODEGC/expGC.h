#ifndef EXPERIMENTSTHALL_H
#define EXPERIMENTSTHALL_H


#include "../Moonfit/moonfit.h"
#include "OwnV4/OwnV4.h"

#include <vector>
#include <string>
#include <iostream>

using namespace std;
enum  {HEALTHY, N_GCEXP}; //INJECT_FIVE, INJECT_NO_AG_COMPET, INJECT_NO_TFHCOMPET, INJECT_NO_COMPET, INJECT_NO_SHM,
namespace Back {
    enum {UNTREATED, NO_AG_COMPET, NO_TFH_COMPET, NO_COMPET, NO_SHM};
}

struct expGC : public Experiment {

    expGC(Model* _m) : Experiment(_m, N_GCEXP){
        Identification = string("ODE GC");
        names_exp[HEALTHY] = string("Healthy");
        /*names_exp[INJECT_FIVE] = string("StartMedium");
        names_exp[INJECT_NO_AG_COMPET] = string("Medium+NoAgComp");
        names_exp[INJECT_NO_TFHCOMPET] = string("Medium+NoTfhComp");
        names_exp[INJECT_NO_COMPET] = string("Medium+NoAgNoTfhCompet");
        names_exp[INJECT_NO_SHM] = string("Medium+NoSHM");*/
        m->setBaseParameters();
    }

    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                case HEALTHY: {m->initialise(Back::UNTREATED); break;}
                /*case INJECT_FIVE: {m->initialise(Back::UNTREATED); break;}
                case INJECT_NO_AG_COMPET: {m->initialise(Back::NO_AG_COMPET); break;}
                case INJECT_NO_TFHCOMPET: {m->initialise(Back::NO_TFH_COMPET); break;}
                case INJECT_NO_COMPET: {m->initialise(Back::NO_COMPET); break;}
                case INJECT_NO_SHM: {m->initialise(Back::NO_SHM); break;}*/
                default: {m->initialise(Back::UNTREATED); break;}
            }
            cout << "Init exp " << IdExp << ":" << names_exp[IdExp] << endl;

            // m->setPrintMode(true, 10800);

            switch(IdExp){
                case HEALTHY:{
                m->simulate(2, E);
                m->setValue("Ag", 1000.);
                m->simulate(22, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(100, E);
                break;}
            default: {
                m->simulate(2, E);
                m->setValue("CC5", 1000.);
                m->simulate(28, E);
                m->setValue("CC5", 1000.);
                m->simulate(100, E);
                break;}

            }
            m->setOverrider(nullptr);
        }
    }
};


enum  {NODRUG, RITUXIMAB, JAK, ANTITNF, TNF_Combi, N_DRUGEXP};


struct expDrugs : public Experiment {

    expDrugs(Model* _m) : Experiment(_m, N_DRUGEXP){
        Identification = string("ODE GC");
        names_exp[NODRUG] = string("Untreated");
        names_exp[RITUXIMAB] = string("Rituximab");
        names_exp[JAK] = string("Abatacept");
        names_exp[ANTITNF] = string("Anti-TNF");
        names_exp[TNF_Combi] = string("aTNF-Combi");
        m->setBaseParameters();
    }

    void simulate(int IdExp, Evaluator* E = nullptr, bool force = false){
        if(motherPrepareSimulate(IdExp, E, force)){
            switch(IdExp){
                case NODRUG: {m->initialise(Back::UNTREATED); break;}
                case RITUXIMAB: {m->initialise(Back::UNTREATED); break;}
                case JAK: {m->initialise(Back::UNTREATED); break;}
                case ANTITNF: {m->initialise(Back::UNTREATED); break;}
                case TNF_Combi: {m->initialise(Back::UNTREATED); break;}
                case N_DRUGEXP: {m->initialise(Back::UNTREATED); break;}
                    default: {m->initialise(Back::UNTREATED); break;}
            }
            cout << "Init exp " << IdExp << ":" << names_exp[IdExp] << endl;

            // m->setPrintMode(true, 10800);

            switch(IdExp){
                case HEALTHY:{
                m->simulate(2, E);
                m->setValue("Ag", 1000.);
                m->simulate(22, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(100, E);
                break;}
            // remember: fast reconstitution is 4 months, some people longer than 6 months
                case RITUXIMAB:{
                m->simulate(2, E);
                m->setValue("BN", 650000.);
                m->setValue("Ag", 1000.);
                m->simulate(22, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(100, E);
                break;}
            case JAK:{
                double saveOriginalParam = m->getParam(modelOwnV4::fTBtoT);
                m->setParam(modelOwnV4::fTBtoT, saveOriginalParam / 5.);
                m->simulate(2, E);
                m->setValue("Ag", 1000.);
                m->simulate(22, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(100, E);
                m->setParam(modelOwnV4::fTBtoT, saveOriginalParam);
                break;}
            case ANTITNF:{
                double saveOriginalParam = m->getParam(modelOwnV4::Sadj);
                m->setParam(modelOwnV4::Sadj, saveOriginalParam / 1.8);
                m->simulate(2, E);
                m->setValue("Ag", 1000.);
                m->simulate(22, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(100, E);
                m->setParam(modelOwnV4::Sadj, saveOriginalParam);
                break;}
            case TNF_Combi:{
                double saveOriginalParam = m->getParam(modelOwnV4::fTBtoT);
                m->setParam(modelOwnV4::fTBtoT, saveOriginalParam / 1.6);
                double saveOriginalParam2 = m->getParam(modelOwnV4::Sadj);
                m->setParam(modelOwnV4::Sadj, saveOriginalParam2 / 1.8);

                m->simulate(2, E);
                m->setValue("Ag", 1000.);
                m->simulate(22, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(100, E);

                m->setParam(modelOwnV4::fTBtoT, saveOriginalParam);
                m->setParam(modelOwnV4::Sadj, saveOriginalParam2);
                break;}
            default: {
                m->simulate(2, E);
                m->setValue("CC5", 1000.);
                m->simulate(28, E);
                m->setValue("CC5", 1000.);
                m->simulate(200, E);
                m->setValue("CC5", 1000.);
                m->simulate(200, E);
                m->setValue("Ag", 1000.);
                m->simulate(100, E);
                break;}

            }
            m->setOverrider(nullptr);
        }
    }
};

#endif // EXPERIMENTSTHALL_H
