// ------- Automatically generated model -------- //
#ifndef modeleOwn4_H
#define modeleOwn4_H
#include "../Moonfit/moonfit.h"
//#include "../expGC.h"


// Note, the ETA parameter is nonphysio (decrease of Tfh if more B cells)
// and it assumes plasma are only produced at the end, don't like
struct modelOwnV4 : public Model {
    modelOwnV4();
    enum {Ag, SAg, Inf, TN, TE, Tfh, BN, BE, earlyEF, EFpc, GCpc, Ab, CBtot, CCtot, GCsize, PCtot, MEMtot, AffPC, AffMBC, AbFromEF, AbFromGCPC, availInter, avgCaptureProba, absoluteCapturePerDay, AvgTfhHelp, CB0, CB1, CB2, CB3, CB4, CB5, CB6, CB7, CB8, CB9, CB10, CB11, CB12, CB13, CB14, CB15, CB16, CB17, CB18, CB19, CB20, CB21, CB22, CB23, CB24, CB25, CC0, CC1, CC2, CC3, CC4, CC5, CC6, CC7, CC8, CC9, CC10, CC11, CC12, CC13, CC14, CC15, CC16, CC17, CC18, CC19, CC20, CC21, CC22, CC23, CC24, CC25, MEM0, MEM1, MEM2, MEM3, MEM4, MEM5, MEM6, MEM7, MEM8, MEM9, MEM10, MEM11, MEM12, MEM13, MEM14, MEM15, MEM16, MEM17, MEM18, MEM19, MEM20, MEM21, MEM22, MEM23, MEM24, MEM25, PC0, PC1, PC2, PC3, PC4, PC5, PC6, PC7, PC8, PC9, PC10, PC11, PC12, PC13, PC14, PC15, PC16, PC17, PC18, PC19, PC20, PC21, PC22, PC23, PC24, PC25, memCB0, memCB1, memCB2, memCB3, memCB4, memCB5, memCB6, memCB7, memCB8, memCB9, memCB10, memCB11, memCB12, memCB13, memCB14, memCB15, memCB16, memCB17, memCB18, memCB19, memCB20, memCB21, memCB22, memCB23, memCB24, memCB25, memCC0, memCC1, memCC2, memCC3, memCC4, memCC5, memCC6, memCC7, memCC8, memCC9, memCC10, memCC11, memCC12, memCC13, memCC14, memCC15, memCC16, memCC17, memCC18, memCC19, memCC20, memCC21, memCC22, memCC23, memCC24, memCC25, NBVAR};
    enum {Dag, Ssag, Dsag, Sbn, Dbn, actB, actBMem, recallMemGC, Stn, Dtn, actT, Di, actI, Sadj, flowBinter, flowTinter, fTBtoT, fTBtoB, fracEF, Dtfh, Ptfh, Dpc, matEF, Defpc, Pcb, Tcb, Tcc, Fdel, Fdec, Fimp, Fjump, Fneut, earlyExit, CmaxAgCapture, KAgCapture, C1capture, C2capture, tau, KtfhSupport, alphaTfhSel, K1TfhSel, K2TfhSel, nDivMax, Vmax, memoryRate, plasmaRate, Dmem, Pab, Dab, FactorAbFromEF, N_levelsMut, NBPARAM};
    //enum {Ag, SAg, Inf, TN, TE, Tfh, BN, BE, EFpc, GCpc, GCmbc, Ab, CB0, CB1, CB2, CB3, CB4, CB5, CB6, CB7, CB8, CB9, CB10, CB11, CB12, CB13, CB14, CB15, CB16, CB17, CB18, CB19, CC0, CC1, CC2, CC3, CC4, CC5, CC6, CC7, CC8, CC9, CC10, CC11, CC12, CC13, CC14, CC15, CC16, CC17, CC18, CC19, CBtot, CCtot, GCsize, PCtot, AffPC, AffMBC, NBVAR};
    //enum {N_levelsMut, SN_prodNaiveT, DN_deathNaiveT, AN_activNaiveT, DH_deathPreT, GAMMA_genTfh, DG_deathTfh, ETA_TBinter_Compet,  D_deathB, ALPHA_offspring, SIGMA_MutPos, KAPPA_prodPlasma, MU_degradAg, SN_prodNaiveB, DN_deathNaiveB, AN_activNaiveB, DH_deathEF, NBPARAM};

    // these coefficients allow to modulate mechanisms without touching the parameter values
    double coeffTfhSel;     // 1: full Tfh selection, 0: all cells survive
    double coeffAntigenCapture; // 1: can take, 0: nobody takes
    double coeffAntigenSel; // 1: full antigen selection, 0: all cells survive
    double coeffSpreadMutation; // 1: full spreading up and down, 0: the cells do not change affinity
    double coeffRecycleStayIn;  // 1: normal recycling, 0: all cells leave

    long long background;
    void derivatives(const vector<double> &x, vector<double> &dxdt, const double _t);

    void updateDerivedVariables(double _t = 0);
    void initialise(long long _background = 0);
	void setBaseParameters();

    void action(string _name, double parameter){
        if(!_name.compare("nameAction")){
            //params[] = 0;
        }
        cerr << "ERR: modelErwinA, no define action for '" << name << "'\n";
    }
};
#endif

