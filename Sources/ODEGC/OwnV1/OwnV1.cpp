// ------- Automatically generated model -------- //
#include "OwnV1.h"


void modelOwnV1::derivatives(const vector<double> &x, vector<double> &dxdt, const double _t){
    if(!over(NnaiveT))		dxdt[NnaiveT] = params[SN_prodNaiveT] - params[DN_deathNaiveT] * x[NnaiveT] - params[AN_activNaiveT] * x[Vantigen] * x[NnaiveT];
    if(!over(HpreTfh))		dxdt[HpreTfh] = params[AN_activNaiveT] * x[Vantigen] * x[NnaiveT] - params[DH_deathPreT] * x[HpreTfh] - params[GAMMA_genTfh] * x[HpreTfh] * x[B0];
    if(!over(Gtfh))         dxdt[Gtfh] = params[GAMMA_genTfh] * x[HpreTfh] * x[B0] - params[DG_deathTfh] * x[Gtfh] - params[ETA_TBinter_Compet] * x[Gtfh] * x[Btot];

    size_t n = static_cast<size_t>(min(10, static_cast<int>(params[N_levelsMut])));
    if(!over(B0))           dxdt[B0] = - params[SIGMA_MutPos] * x[B0] * x[HpreTfh] - params[D_deathB] * x[B0];
    if(!over(B1))           dxdt[B1] = + params[ALPHA_offspring] * params[SIGMA_MutPos] * x[B0] * x[HpreTfh] - params[SIGMA_MutPos] * x[B1] * x[Gtfh] - params[D_deathB] * x[B1];
    for(size_t i = 2; i < n; ++i){
        if(!over(B0+i))           dxdt[B0+i] = + params[ALPHA_offspring] * params[SIGMA_MutPos] * x[B0+i-1] * x[Gtfh] - params[SIGMA_MutPos] * x[B0+i] * x[Gtfh] - params[D_deathB] * x[B0+i];
    }
    if(!over(B0+n))         dxdt[B0+n] = + params[ALPHA_offspring] * params[SIGMA_MutPos] * x[B0+n-1] - params[SIGMA_MutPos] * x[B0+n] * x[Gtfh] - params[KAPPA_prodPlasma] * x[B0+n];
    if(!over(Memory))		dxdt[Memory] =  params[KAPPA_prodPlasma] * (0.1 * x[B1] + 0.2 * x[B1] + 0.3* x[B2] + 0.2 * x[B3] + 0.1 * x[B4] + 0.05 * x[B5] + 0.05 * x[B6]);
    if(!over(Pplasma))		dxdt[Pplasma] = params[KAPPA_prodPlasma] * (0.1 * x[B4] + 0.2 * x[B5] + 0.3* x[B6] + 0.2 * x[B7] + 0.1 * x[B8] + 0.05 * x[B9] + 0.05 * x[B10]);

    if(!over(NnaiveB))		dxdt[NnaiveB] = params[SN_prodNaiveB] - params[DN_deathNaiveB] * x[NnaiveB] - params[AN_activNaiveB] * x[Vantigen] * x[NnaiveB];
    if(!over(ExtraFol))		dxdt[ExtraFol] = params[AN_activNaiveB] * x[Vantigen] * x[NnaiveB];
    if(!over(Infla))		dxdt[Pplasma] = params[KAPPA_prodPlasma] * x[B0+n];

    if(!over(Vantigen))		dxdt[Vantigen] = - params[MU_degradAg] * x[Vantigen];
}

modelOwnV1::modelOwnV1() : Model(NBVAR, NBPARAM), background(0) {
    name = string("Erwin 2017");    // name of this particular model
    dt = 0.001;   print_every_dt = 0.1;              // initial values for dt of simulation and for following the kinetics. Can be modified by user later.
    names[NnaiveT] = "NnaiveT";
    names[HpreTfh] = "HpreTfh";
    names[Gtfh] = "Gtfh";
    names[B0] = "B0";
    names[B1] = "B1";
    names[B2] = "B2";
    names[B3] = "B3";
    names[B4] = "B4";
    names[B5] = "B5";
    names[B6] = "B6";
    names[B7] = "B7";
    names[B8] = "B8";
    names[B9] = "B9";
    names[B10] = "B10";
    names[Pplasma] = "Pplasma";
    names[Vantigen] = "Vantigen";
    names[ExtraFol] = "ExtraFol";
    names[Memory] = "Memory";
    names[Infla] = "Infla";
    names[Btot] = " Btot";


    // associates the indices of the variables inside the model with the 'official' names/index of variables to be accessed by outside.
    // so, when the model have a different number of variables, they are always accessed with the same 'official' name (ex : 'N::IL2'), from outside, even if they actually have a different indice/order inside the model ('IL2')

    paramNames[N_levelsMut] = "N_levelsMut";
    paramNames[SN_prodNaiveT] = "SN_prodNaiveT";
    paramNames[DN_deathNaiveT] = "DN_deathNaiveT";
    paramNames[AN_activNaiveT] = "AN_activNaiveT";
    paramNames[DH_deathPreT] = "DH_deathPreT";
    paramNames[GAMMA_genTfh] = "GAMMA_genTfh";
    paramNames[DG_deathTfh] = "DG_deathTfh";
    paramNames[ETA_TBinter_Compet] = "ETA_TBinter";
    paramNames[ D_deathB] = "D_deathB";
    paramNames[ALPHA_offspring] = "ALPHA_slowMut";
    paramNames[SIGMA_MutPos] = "SIGMA_MutPos";
    paramNames[KAPPA_prodPlasma] = "KAPPA_prodPlasma";
    paramNames[MU_degradAg] = "MU_degradAg";
    paramNames[SN_prodNaiveB] = "SN_prodNaiveB";
    paramNames[DN_deathNaiveB] = "DN_deathNaiveB";
    paramNames[AN_activNaiveB] = "AN_activNaiveB";
    paramNames[DH_deathEF] = "DH_deathEF";

    paramLowBounds[N_levelsMut] = 2;
    paramLowBounds[SN_prodNaiveT] = 10;
    paramLowBounds[DN_deathNaiveT] = 1.00E-04;
    paramLowBounds[AN_activNaiveT] = 1.00E-13;
    paramLowBounds[DH_deathPreT] = 1.00E-04;
    paramLowBounds[GAMMA_genTfh] = 0.05;
    paramLowBounds[DG_deathTfh] = 1.00E-04;
    paramLowBounds[ETA_TBinter_Compet] = 1.00E-08;
    paramLowBounds[ D_deathB] = 1.00E-04;
    paramLowBounds[ALPHA_offspring] = 0.01;
    paramLowBounds[SIGMA_MutPos] = 0.01;
    paramLowBounds[KAPPA_prodPlasma] = 0.05;
    paramLowBounds[MU_degradAg] = 0.05;

    paramUpBounds[N_levelsMut] = 10;
    paramUpBounds[SN_prodNaiveT] = 1.00E+07;
    paramUpBounds[DN_deathNaiveT] = 1;
    paramUpBounds[AN_activNaiveT] = 1.00E-05;
    paramUpBounds[DH_deathPreT] = 1;
    paramUpBounds[GAMMA_genTfh] = 100;
    paramUpBounds[DG_deathTfh] = 1;
    paramUpBounds[ETA_TBinter_Compet] = 1.00E-04;
    paramUpBounds[ D_deathB] = 1;
    paramUpBounds[ALPHA_offspring] = 1;
    paramUpBounds[SIGMA_MutPos] = 1;
    paramUpBounds[KAPPA_prodPlasma] = 100;
    paramUpBounds[MU_degradAg] = 100;


}

void modelOwnV1::setBaseParameters(){
    params.clear();                 // to make sure they are all put to zero
	params.resize(NBPARAM, 0.0);
    params[N_levelsMut] = 8;
    params[SN_prodNaiveT] = 10000;
    params[DN_deathNaiveT] = 0.01;
    params[AN_activNaiveT] = 1.80E-11;
    params[DH_deathPreT] = 0.01;
    params[GAMMA_genTfh] = 2;
    params[DG_deathTfh] = 0.01;
    params[ETA_TBinter_Compet] = 1.00E-05;
    params[D_deathB] = 0.8;
    params[ALPHA_offspring] = 27.5;
    params[SIGMA_MutPos] = 0.2;
    params[KAPPA_prodPlasma] = 1.2;
    params[MU_degradAg] = 2;
    params[SN_prodNaiveB] = 10000;
    params[DN_deathNaiveB] = 0.01;
    params[AN_activNaiveB] = 1.8e-11;
    params[DH_deathEF] = 0.01;

	setBaseParametersDone();
}

void modelOwnV1::initialise(long long _background){ // don't touch to parameters !
	background = _background;
	val.clear();
	val.resize(NBVAR, 0.0);
	init.clear();
	init.resize(NBVAR, 0.0);


    init[NnaiveT] = 1.00E+06;
    init[HpreTfh] = 0;
    init[Gtfh] = 0;
    init[B0] = 3;
    init[B1] = 0;
    init[B2] = 0;
    init[B3] = 0;
    init[B4] = 0;
    init[B5] = 0;
    init[B6] = 0;
    init[B7] = 0;
    init[B8] = 0;
    init[B9] = 0;
    init[B10] = 0;
    init[Pplasma] = 0;
    init[Vantigen] = 2.00E+08;
    init[Btot] = 0;

    for(size_t i = 0; i < NBVAR; ++i){
        val[i] = init[i];}
	t = 0;
	initialiseDone();
}








