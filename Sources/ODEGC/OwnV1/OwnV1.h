// ------- Automatically generated model -------- //
#ifndef modeleOwn1_H
#define modeleOwn1_H
#include "../Moonfit/moonfit.h"



// Note, the ETA parameter is nonphysio (decrease of Tfh if more B cells)
// and it assumes plasma are only produced at the end, don't like
struct modelOwnV1 : public Model {
    modelOwnV1();
    enum {NnaiveT, HpreTfh, Gtfh, B0, B1, B2, B3, B4, B5, B6, B7, B8, B9, B10, Pplasma, Vantigen, NnaiveB, ExtraFol, Memory, Infla, Btot, NBVAR};
    enum {N_levelsMut, SN_prodNaiveT, DN_deathNaiveT, AN_activNaiveT, DH_deathPreT, GAMMA_genTfh, DG_deathTfh, ETA_TBinter_Compet,  D_deathB, ALPHA_offspring, SIGMA_MutPos, KAPPA_prodPlasma, MU_degradAg, SN_prodNaiveB, DN_deathNaiveB, AN_activNaiveB, DH_deathEF, NBPARAM};

    long long background;
    void derivatives(const vector<double> &x, vector<double> &dxdt, const double _t);

    void updateDerivedVariables(double _t = 0) {
        if(!over(Btot)) {
            val[Btot] = 0;
            for(size_t i = 0; i < 11; ++i){
                val[Btot] += val[B0+i];
            }
        }
    }
    void initialise(long long _background = 0);
	void setBaseParameters();

    void action(string _name, double parameter){
        if(!_name.compare("nameAction")){
            //params[] = 0;
        }
        cerr << "ERR: modelErwinA, no define action for '" << name << "'\n";
    }
};
#endif

