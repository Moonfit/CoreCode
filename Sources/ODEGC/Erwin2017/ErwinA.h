// ------- Automatically generated model -------- //
#ifndef modeleErwinA_H
#define modeleErwinA_H
#include "../Moonfit/moonfit.h"



// Note, the ETA parameter is nonphysio (decrease of Tfh if more B cells)
// and it assumes plasma are only produced at the end, don't like
struct modelErwinA : public Model {
    modelErwinA();
    enum {NnaiveT, HpreTfh, Gtfh, B0, B1, B2, B3, B4, B5, B6, B7, B8, B9, B10, Pplasma, Vantigen, Btot, NBVAR};
    enum {N_levelsMut, SN_prodNaiveT, DN_deathNaiveT, AN_activNaiveT, DH_deathPreT, GAMMA_genTfh, DG_deathTfh, ETA_TBinter_Compet,  D_deathB, ALPHA_offspring, SIGMA_MutPos, KAPPA_prodPlasma, MU_degradAg, NBPARAM};

    long long background;
    void derivatives(const vector<double> &x, vector<double> &dxdt, const double _t){
        if(!over(NnaiveT))		dxdt[NnaiveT] = params[SN_prodNaiveT] - params[DN_deathNaiveT] * x[NnaiveT] - params[AN_activNaiveT] * x[Vantigen] * x[NnaiveT];
        if(!over(HpreTfh))		dxdt[HpreTfh] = params[AN_activNaiveT] * x[Vantigen] * x[NnaiveT] - params[DH_deathPreT] * x[HpreTfh] - params[GAMMA_genTfh] * x[HpreTfh] * x[B0];
        if(!over(Gtfh))         dxdt[Gtfh] = params[GAMMA_genTfh] * x[HpreTfh] * x[B0] - params[DG_deathTfh] * x[Gtfh] - params[ETA_TBinter_Compet] * x[Gtfh] * x[Btot];

        size_t n = static_cast<size_t>(min(10, static_cast<int>(params[N_levelsMut])));
        if(!over(B0))           dxdt[B0] = - params[SIGMA_MutPos] * x[B0] * x[HpreTfh] - params[D_deathB] * x[B0];
        if(!over(B1))           dxdt[B1] = + params[ALPHA_offspring] * params[SIGMA_MutPos] * x[B0] * x[HpreTfh] - params[SIGMA_MutPos] * x[B1] * x[Gtfh] - params[D_deathB] * x[B1];
        for(size_t i = 2; i < n; ++i){
            if(!over(B0+i))           dxdt[B0+i] = + params[ALPHA_offspring] * params[SIGMA_MutPos] * x[B0+i-1] * x[Gtfh] - params[SIGMA_MutPos] * x[B0+i] * x[Gtfh] - params[D_deathB] * x[B0+i];
        }
        if(!over(B0+n))         dxdt[B0+n] = + params[ALPHA_offspring] * params[SIGMA_MutPos] * x[B0+n-1] - params[SIGMA_MutPos] * x[B0+n] * x[Gtfh] - params[KAPPA_prodPlasma] * x[B0+n];
        if(!over(Pplasma))		dxdt[Pplasma] = params[KAPPA_prodPlasma] * x[B0+n];
        if(!over(Vantigen))		dxdt[Vantigen] = - params[MU_degradAg] * x[Vantigen];
    }

    void updateDerivedVariables(double _t = 0) {
        if(!over(Btot)) {
            val[Btot] = 0;
            for(size_t i = 0; i < 11; ++i){
                val[Btot] += val[B0+i];
            }
        }
    }
    void initialise(long long _background = 0);
	void setBaseParameters();

    void action(string _name, double parameter){
        if(!_name.compare("nameAction")){
            //params[] = 0;
        }
        cerr << "ERR: modelErwinA, no define action for '" << name << "'\n";
    }
};
#endif

