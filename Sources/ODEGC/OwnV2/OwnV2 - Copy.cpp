// ------- Automatically generated model -------- //
#include "OwnV2.h"


void modelOwnV2::derivatives(const vector<double> &x, vector<double> &dxdt, const double _t){
    //enum {Ag, SAg, Inf, TN, TE, Tfh, BN, BE, EFpc, GCpc, GCmbc, Ab, CB0, CB1, CB2, CB3, CB4, CB5, CB6, CB7, CB8, CB9, CB10, CB11, CB12, CB13, CB14, CB15, CB16, CB17, CB18, CB19, CC0, CC1, CC2, CC3, CC4, CC5, CC6, CC7, CC8, CC9, CC10, CC11, CC12, CC13, CC14, CC15, CC16, CC17, CC18, CC19, MEM0, MEM1, MEM2, MEM3, MEM4, MEM5, MEM6, MEM7, MEM8, MEM9, MEM10, MEM11, MEM12, MEM13, MEM14, MEM15, MEM16, MEM17, MEM18, MEM19, PC0, PC1, PC2, PC3, PC4, PC5, PC6, PC7, PC8, PC9, PC10, PC11, PC12, PC13, PC14, PC15, PC16, PC17, PC18, PC19, CBtot, CCtot, GCsize, PCtot, AffPC, AffMBC, NBVAR};
    //enum {Dag, Ssag, Dsag, Sbn, Dbn, actB, actBMem, Stn, Dtn, actT, Di, actI, flowBinter, flowTinter, fTBtoT, fTBtoB, fracEF, Dtfh, Ptfh, Dpc, Pcb, Tcb, Dcc, Tcc, Fdel, Fdec, Fimp, Fjump, earlyExit, CmaxAgCapture, KAgCapture, C1capture, C2capture, KtfhSupport, alphaTfhSel, K1TfhSel, K2TfhSel, recycleSpeed, memoryRate, plasmaRate, Dmem, Pab, Dab, N_levelsMut, NBPARAM};

    if(params[N_levelsMut] < 2) params[N_levelsMut] = 2;
    size_t n = static_cast<size_t>(min(20, static_cast<int>(params[N_levelsMut])));
    vector<double> AgCaptureStrength = vector<double>(n, 0);
    double summedCapture = 0;
    for(size_t i = 0; i < n; ++i){
        // This is the survival probability (but unit???)
        AgCaptureStrength[i] = (x[SAg] / (max(1e-12, x[SAg] + params[KAgCapture]))) * (1 / (1 + params[C1capture] * std::exp(i - params[C2capture])));
        summedCapture += params[CmaxAgCapture] * x[CC0+i] * AgCaptureStrength[i];
    }

    // Part 1: we have two antigens: native (inflammatory) and stored (long-term)
    if(!over(Ag))           dxdt[Ag] = - params[Dag] * x[Ag];
    if(!over(SAg))          dxdt[SAg] = params[Ssag] * x[Ag] - params[Dsag] * x[SAg] - summedCapture;

    // Part 2: transient activation of B and T cells into early effectors (before B-T interaction)
    // do we take stored antigen or fresh antigen for activation? would say fresh
    if(!over(BN))           dxdt[BN] = params[Sbn] - params[Dbn] * x[BN] - params[actB] * x[BN] * x[Ag];
    if(!over(TN))           dxdt[TN] = params[Stn] - params[Dtn] * x[TN] - params[actT] * x[TN] * x[Ag];

    if(!over(BE))           dxdt[BE] = + params[actB] * x[BN] * x[Ag] - params[flowBinter] * x[BE];
    if(!over(TE))           dxdt[TE] = + params[actT] * x[TN] * x[Ag] - params[flowTinter] * x[TE];

    // Part 3: We simulate an inflammatory level (from the innate system) that can be tuned by adjuvants or anti-inflammatory drugs
    if(!over(Inf))          dxdt[Inf] = params[actI] * x[Ag] - params[Di] * x[Inf];

    // Part 4: The T-B border will generate CB0s and Tfhs. Tfhs are also amplified by the amount of CCtot
    double interactions = params[flowBinter] * x[BE] * params[flowTinter] * x[TE] * x[Inf];

    if(!over(Tfh))          dxdt[Tfh] = params[fTBtoT] * interactions - params[Dtfh] * x[Tfh] + params[Ptfh] * x[Tfh] * ( x[CCtot] / ( max(1e-12,x[CCtot] + params[KtfhSupport])));
    if(!over(EFpc))         dxdt[EFpc] = params[fTBtoB] * interactions * params[fracEF] * x[Inf] - params[Dpc] * x[EFpc];

    // There is a competition term missing for the CC, if they are too much they get less per CC as well, damned
    vector<double> TfhHelp = vector<double>(n, 0);
    for(size_t i = 0; i < n; ++i){
        double disadvantageHigherAffinityCells = 0;
        for(size_t j = i+1; j < n; ++j){
            disadvantageHigherAffinityCells += std::pow(params[alphaTfhSel],j) * x[CC0+j];
        }
        double coeff = x[Tfh] / (x[Tfh] + params[K1TfhSel] * (1 + disadvantageHigherAffinityCells / (max(1e-12, params[K2TfhSel]))));
        TfhHelp[i] = std::pow(2.0, 4 * (coeff - 1));
    }


    // calculate recycling back from CC0/CC1 ; the early exit makes EF PCs?
    double recycling0 = TfhHelp[0] * params[recycleSpeed];
    // shall we say that proliferation in CB0 includes death? => death is included before going back to CC
    if(!over(CB0))          dxdt[CB0] = params[fTBtoB] * interactions * (1 - min(1.0,params[fracEF] * x[Inf])) +
                                        (params[Pcb] - params[Tcb] - params[earlyExit]) * x[CB0] +
                                        recycling0 * x[CC0];

    for(size_t i = 1; i < n; ++i){
        double recyclingi = TfhHelp[i] * params[recycleSpeed];
        if(!over(CB0+i))    dxdt[CB0+i] = (params[Pcb] - params[Tcb]) * x[CB0 + i] + recyclingi * x[CC0+i];
    }



    double Fneut = 1 - params[Fdec] - params[Fimp] - params[Fjump];
    if(Fneut < 0) cerr << "ERR: negative parameter for neutral mutations." << Fneut << endl;
    if(!over(CC0))          dxdt[CC0] = params[Tcb] * ((params[Fdec] + Fneut) * x[CB0] + params[Fdec] * x[CB1])
                                        - params[Dcc] * max(0., (1 - min(1.0, AgCaptureStrength[0]))) * x[CC0] - params[Tcc] * x[CC0]; // this combines death and recycling

    for(size_t i = 1; i < n-1; ++i){
        if(!over(CC0 + i))          dxdt[CC0 + i] = params[Tcb] * (params[Fdec] * x[CB0+i+1] + Fneut * x[CB0+i] + params[Fimp] * x[CB0 + i - 1] + ((i > 1) ? params[Fjump] * x[CB0 + i - 2] : 0))
                                            - params[Dcc] * max(0., (1 - min(1.0, AgCaptureStrength[i]))) * x[CC0+i] - params[Tcc] * x[CC0+i]; // we could have in time here
    }
    if(!over(CC0 + n))          dxdt[CC0 + n] = params[Tcb] * ((params[Fimp] + params[Fjump]) * x[CB0+n] + Fneut * x[CB0+n] + params[Fimp] * x[CB0 + n - 1] + ((n > 1) ? params[Fjump] * x[CB0 + n - 2] : 0))
                                        - params[Dcc] * max(0., (1 - min(1.0, AgCaptureStrength[n]))) * x[CC0+n] - params[Tcc] * x[CC0+n]; // this combines death and recycling


    // We will define memory versus plasma as the absolute efficiency of antigen uptake (but then does secondary GCs make memory?)
    // let's do simple: constant rate of exit of memory for all classes (LZ), while Tfh help driven exit of plasma cells (via the DZ)
    // Check if recyclespeed and Tcc are redundant or not
    for(size_t i = 0; i < n; ++i){
        if(!over(MEM0 + i))   dxdt[MEM0 + i] = params[memoryRate] * x[CC0+i] - params[Dmem] * x[MEM0+i];
        if(!over(PC0 + i))   dxdt[PC0 + i] = params[plasmaRate] * TfhHelp[i] * params[recycleSpeed] * x[CC0+i] - params[Dpc] * x[PC0+i];
    }

    double Abprod = x[EFpc] * params[Pab];
    for(size_t i = 0; i < n; ++i){

        Abprod += x[PC0 + i] * params[Pab];
    }
    if(!over(Ab))    dxdt[Ab] = Abprod - params[Dab] * x[Ab];
}

void modelOwnV2::updateDerivedVariables(double _t){
    val[CBtot] = 0;
    val[CCtot] = 0;
    val[PCtot] = 0;
    val[MEMtot] = 0;

    size_t n = static_cast<size_t>(min(10, static_cast<int>(params[N_levelsMut])));
    for(size_t i = 0; i < n; ++i){
        val[CBtot] += val[CB0+i];
        val[CCtot] += val[CC0+i];
        val[PCtot] += val[PC0+i];
        val[MEMtot] += val[MEM0+i];
    }

    val[GCsize] = val[CBtot] + val[CCtot];
    val[availInter] = params[flowBinter] * val[BE] * params[flowTinter] * val[TE] * val[Inf];


    double summedCapture = 0;
    vector<double> AgCaptureStrength = vector<double>(n, 0);
    val[avgCaptureProba] = 0;
    for(size_t i = 0; i < n; ++i){
        // This is the survival probability (but unit???)
        AgCaptureStrength[i] = (val[SAg] / (max(1e-12, val[SAg] + params[KAgCapture]))) * (1 / (1 + params[C1capture] * std::exp(i - params[C2capture])));
        summedCapture += params[CmaxAgCapture] * val[CC0+i] * AgCaptureStrength[i];
        val[avgCaptureProba] += val[CC0+i] * AgCaptureStrength[i];
    }
    val[avgCaptureProba] =  val[avgCaptureProba] / (max(1e-12, val[CCtot]));
    val[absoluteCapturePerDay] = summedCapture;

    vector<double> TfhHelp = vector<double>(n, 0);
    double avgTfh = 0;
    for(size_t i = 0; i < n; ++i){
        double disadvantageHigherAffinityCells = 0;
        for(size_t j = i+1; j < n; ++j){
            disadvantageHigherAffinityCells += std::pow(params[alphaTfhSel],j) * val[CC0+j];
        }
        double coeff = val[Tfh] / (val[Tfh] + params[K1TfhSel] * (1 + disadvantageHigherAffinityCells / (max(1e-12, params[K2TfhSel]))));
        TfhHelp[i] = std::pow(2.0, 4 * (coeff - 1));
        cout << i << "\t" << TfhHelp[i] << " CCi=" << val[CC0 + i] << endl;
        //avgTfh += TfhHelp[i] * val[CC0 + i];
    }
    //cout << "=>" << avgTfh << endl;
    val[AvgTfhHelp] = avgTfh / max(1e-12, val[CCtot]);


}

modelOwnV2::modelOwnV2() : Model(NBVAR, NBPARAM), background(0) {
    name = string("Erwin 2017");    // name of this particular model
    dt = 0.001;   print_every_dt = 0.1;              // initial values for dt of simulation and for following the kinetics. Can be modified by user later.


    names[Ag] = "Ag (antigen)";
    names[SAg] = "SAg (stored antigen)";
    names[Inf] = "Inf (Inflammation)";
    names[TN] = "TN (T naive)";
    names[TE] = "TE (T early activated)";
    names[Tfh] = "Tfh";
    names[BN] = "BN (B naive)";
    names[BE] = "BE (B early activated)";
    names[EFpc] = "EFpc (Extra Follicular Plasma Cells)";
    names[GCpc] = "GCpc (GC-derived Plasma Cells)";
    names[Ab] = "Ab (Antibodies)";
    names[CB0] = "CB0 (Centroblast Aff bin 0)";
    names[CB1] = "CB1 (Centroblast Aff bin 1)";
    names[CB2] = "CB2 (Centroblast Aff bin 2)";
    names[CB3] = "CB3 (Centroblast Aff bin 3)";
    names[CB4] = "CB4 (Centroblast Aff bin 4)";
    names[CB5] = "CB5 (Centroblast Aff bin 5)";
    names[CB6] = "CB6 (Centroblast Aff bin 6)";
    names[CB7] = "CB7 (Centroblast Aff bin 7)";
    names[CB8] = "CB8 (Centroblast Aff bin 8)";
    names[CB9] = "CB9 (Centroblast Aff bin 9)";
    names[CB10] = "CB10 (Centroblast Aff bin 10)";
    names[CB11] = "CB11 (Centroblast Aff bin 11)";
    names[CB12] = "CB12 (Centroblast Aff bin 12)";
    names[CB13] = "CB13 (Centroblast Aff bin 13)";
    names[CB14] = "CB14 (Centroblast Aff bin 14)";
    names[CB15] = "CB15 (Centroblast Aff bin 15)";
    names[CB16] = "CB16 (Centroblast Aff bin 16)";
    names[CB17] = "CB17 (Centroblast Aff bin 17)";
    names[CB18] = "CB18 (Centroblast Aff bin 18)";
    names[CB19] = "CB19 (Centroblast Aff bin 19)";
    names[CC0] = "CC0 (Centrocyte Aff bin 0)";
    names[CC1] = "CC1 (Centrocyte Aff bin 1)";
    names[CC2] = "CC2 (Centrocyte Aff bin 2)";
    names[CC3] = "CC3 (Centrocyte Aff bin 3)";
    names[CC4] = "CC4 (Centrocyte Aff bin 4)";
    names[CC5] = "CC5 (Centrocyte Aff bin 5)";
    names[CC6] = "CC6 (Centrocyte Aff bin 6)";
    names[CC7] = "CC7 (Centrocyte Aff bin 7)";
    names[CC8] = "CC8 (Centrocyte Aff bin 8)";
    names[CC9] = "CC9 (Centrocyte Aff bin 9)";
    names[CC10] = "CC10 (Centrocyte Aff bin 10)";
    names[CC11] = "CC11 (Centrocyte Aff bin 11)";
    names[CC12] = "CC12 (Centrocyte Aff bin 12)";
    names[CC13] = "CC13 (Centrocyte Aff bin 13)";
    names[CC14] = "CC14 (Centrocyte Aff bin 14)";
    names[CC15] = "CC15 (Centrocyte Aff bin 15)";
    names[CC16] = "CC16 (Centrocyte Aff bin 16)";
    names[CC17] = "CC17 (Centrocyte Aff bin 17)";
    names[CC18] = "CC18 (Centrocyte Aff bin 18)";
    names[CC19] = "CC19 (Centrocyte Aff bin 19)";
    names[MEM0] = "MEM0 (GC Memory Aff bin 0)";
    names[MEM1] = "MEM1 (GC Memory Aff bin 0)";
    names[MEM2] = "MEM2 (GC Memory Aff bin 0)";
    names[MEM3] = "MEM3 (GC Memory Aff bin 0)";
    names[MEM4] = "MEM4 (GC Memory Aff bin 0)";
    names[MEM5] = "MEM5 (GC Memory Aff bin 0)";
    names[MEM6] = "MEM6 (GC Memory Aff bin 0)";
    names[MEM7] = "MEM7 (GC Memory Aff bin 0)";
    names[MEM8] = "MEM8 (GC Memory Aff bin 0)";
    names[MEM9] = "MEM9 (GC Memory Aff bin 0)";
    names[MEM10] = "MEM10 (GC Memory Aff bin 0)";
    names[MEM11] = "MEM11 (GC Memory Aff bin 0)";
    names[MEM12] = "MEM12 (GC Memory Aff bin 0)";
    names[MEM13] = "MEM13 (GC Memory Aff bin 0)";
    names[MEM14] = "MEM14 (GC Memory Aff bin 0)";
    names[MEM15] = "MEM15 (GC Memory Aff bin 0)";
    names[MEM16] = "MEM16 (GC Memory Aff bin 0)";
    names[MEM17] = "MEM17 (GC Memory Aff bin 0)";
    names[MEM18] = "MEM18 (GC Memory Aff bin 0)";
    names[MEM19] = "MEM19 (GC Memory Aff bin 0)";
    names[PC0] = "PC0 (Plasma Cell Aff bin 0)";
    names[PC1] = "PC1 (Plasma Cell Aff bin 0)";
    names[PC2] = "PC2 (Plasma Cell Aff bin 0)";
    names[PC3] = "PC3 (Plasma Cell Aff bin 0)";
    names[PC4] = "PC4 (Plasma Cell Aff bin 0)";
    names[PC5] = "PC5 (Plasma Cell Aff bin 0)";
    names[PC6] = "PC6 (Plasma Cell Aff bin 0)";
    names[PC7] = "PC7 (Plasma Cell Aff bin 0)";
    names[PC8] = "PC8 (Plasma Cell Aff bin 0)";
    names[PC9] = "PC9 (Plasma Cell Aff bin 0)";
    names[PC10] = "PC10 (Plasma Cell Aff bin 0)";
    names[PC11] = "PC11 (Plasma Cell Aff bin 0)";
    names[PC12] = "PC12 (Plasma Cell Aff bin 0)";
    names[PC13] = "PC13 (Plasma Cell Aff bin 0)";
    names[PC14] = "PC14 (Plasma Cell Aff bin 0)";
    names[PC15] = "PC15 (Plasma Cell Aff bin 0)";
    names[PC16] = "PC16 (Plasma Cell Aff bin 0)";
    names[PC17] = "PC17 (Plasma Cell Aff bin 0)";
    names[PC18] = "PC18 (Plasma Cell Aff bin 0)";
    names[PC19] = "PC19 (Plasma Cell Aff bin 0)";
    names[CBtot] = "CBtot";
    names[CCtot] = "CCtot";
    names[MEMtot] = "GCmbc (GC-derived Memory B Cells)";
    names[GCsize] = "GCsize (CB + CC)";
    names[PCtot] = "PCtot (EF+GC PCs)";
    names[AffPC] = "AffPC (average affinity PCs)";
    names[AffMBC] = "AffMBC (average affinity MBC)";
    names[availInter] = "Available TB interactions per day";
    names[avgCaptureProba] = "average probability of antigen capture per cell";
    names[absoluteCapturePerDay] = "absolute antigen capture";
    names[AvgTfhHelp] = "Average Tfh Help";



    extNames[Ag] = "Ag";
    extNames[SAg] = "SAg";
    extNames[Inf] = "Inf";
    extNames[TN] = "TN";
    extNames[TE] = "TE";
    extNames[Tfh] = "Tfh";
    extNames[BN] = "BN";
    extNames[BE] = "BE";
    extNames[EFpc] = "EFpc";
    extNames[GCpc] = "GCpc";
    extNames[Ab] = "Ab";
    extNames[CB0] = "CB0";
    extNames[CB1] = "CB1";
    extNames[CB2] = "CB2";
    extNames[CB3] = "CB3";
    extNames[CB4] = "CB4";
    extNames[CB5] = "CB5";
    extNames[CB6] = "CB6";
    extNames[CB7] = "CB7";
    extNames[CB8] = "CB8";
    extNames[CB9] = "CB9";
    extNames[CB10] = "CB10";
    extNames[CB11] = "CB11";
    extNames[CB12] = "CB12";
    extNames[CB13] = "CB13";
    extNames[CB14] = "CB14";
    extNames[CB15] = "CB15";
    extNames[CB16] = "CB16";
    extNames[CB17] = "CB17";
    extNames[CB18] = "CB18";
    extNames[CB19] = "CB19";
    extNames[CC0] = "CC0";
    extNames[CC1] = "CC1";
    extNames[CC2] = "CC2";
    extNames[CC3] = "CC3";
    extNames[CC4] = "CC4";
    extNames[CC5] = "CC5";
    extNames[CC6] = "CC6";
    extNames[CC7] = "CC7";
    extNames[CC8] = "CC8";
    extNames[CC9] = "CC9";
    extNames[CC10] = "CC10";
    extNames[CC11] = "CC11";
    extNames[CC12] = "CC12";
    extNames[CC13] = "CC13";
    extNames[CC14] = "CC14";
    extNames[CC15] = "CC15";
    extNames[CC16] = "CC16";
    extNames[CC17] = "CC17";
    extNames[CC18] = "CC18";
    extNames[CC19] = "CC19";
    extNames[MEM0] = "MEM0";
    extNames[MEM1] = "MEM1";
    extNames[MEM2] = "MEM2";
    extNames[MEM3] = "MEM3";
    extNames[MEM4] = "MEM4";
    extNames[MEM5] = "MEM5";
    extNames[MEM6] = "MEM6";
    extNames[MEM7] = "MEM7";
    extNames[MEM8] = "MEM8";
    extNames[MEM9] = "MEM9";
    extNames[MEM10] = "MEM10";
    extNames[MEM11] = "MEM11";
    extNames[MEM12] = "MEM12";
    extNames[MEM13] = "MEM13";
    extNames[MEM14] = "MEM14";
    extNames[MEM15] = "MEM15";
    extNames[MEM16] = "MEM16";
    extNames[MEM17] = "MEM17";
    extNames[MEM18] = "MEM18";
    extNames[MEM19] = "MEM19";
    extNames[PC0] = "PC0";
    extNames[PC1] = "PC1";
    extNames[PC2] = "PC2";
    extNames[PC3] = "PC3";
    extNames[PC4] = "PC4";
    extNames[PC5] = "PC5";
    extNames[PC6] = "PC6";
    extNames[PC7] = "PC7";
    extNames[PC8] = "PC8";
    extNames[PC9] = "PC9";
    extNames[PC10] = "PC10";
    extNames[PC11] = "PC11";
    extNames[PC12] = "PC12";
    extNames[PC13] = "PC13";
    extNames[PC14] = "PC14";
    extNames[PC15] = "PC15";
    extNames[PC16] = "PC16";
    extNames[PC17] = "PC17";
    extNames[PC18] = "PC18";
    extNames[PC19] = "PC19";
    extNames[CBtot] = "CBtot";
    extNames[CCtot] = "CCtot";
    extNames[MEMtot] = "MEMtot";
    extNames[GCsize] = "GCsize";
    extNames[PCtot] = "PCtot";
    extNames[AffPC] = "AffPC";
    extNames[AffMBC] = "AffMBC";




    // associates the indices of the variables inside the model with the 'official' names/index of variables to be accessed by outside.
    // so, when the model have a different number of variables, they are always accessed with the same 'official' name (ex : 'N::IL2'), from outside, even if they actually have a different indice/order inside the model ('IL2')

    paramNames[Dag] = "Dag (Degradation Ag)";
    paramNames[Ssag] = "Ssag (Generation Stored Ag)";
    paramNames[Dsag] = "Dsag (Degradation Stored Ag)";
    paramNames[Pab] = "Pab (Production Ab)";
    paramNames[Dab] = "Dab (Degradation Ab)";
    paramNames[Sbn] = "Sbn (Source Naïve B)";
    paramNames[Dbn] = "Dbn (Death Naïve B)";
    paramNames[actB] = "actB (Actiavtion Naïve B)";
    paramNames[actBMem] = "actBMem (Reactivation of Memory)";
    paramNames[Stn] = "Stn (Source Naïve T)";
    paramNames[Dtn] = "Dtn (Death Naïve T)";
    paramNames[actT] = "actT (Activation Naïve T)";
    paramNames[Di] = "Di (Downregulation of Infection)";
    paramNames[actI] = "actI (Activation of Infection)";
    paramNames[flowBinter] = "flowBinter (flow B at T-B border)";
    paramNames[flowTinter] = "flowTinter (flow T at T-B border)";
    paramNames[fTBtoT] = "fTBtoT (flow TB border to Tfh)";
    paramNames[fTBtoB] = "fTBtoB (flow TB border to GC or EF)";
    paramNames[fracEF] = "fracEF (fraction of TB border to EF)";
    paramNames[Dtfh] = "Dtfh (Death Tfh)";
    paramNames[Ptfh] = "Ptfh (Proliferation Tfh)";
    paramNames[Dpc] = "Dpc (Death EFPC and GCPC)";
    paramNames[Pcb] = "Pcb (expansion CB)";
    paramNames[Tcb] = "Tcb (outflow CB to CC or death)";
    paramNames[Dcc] = "Dcc (Death CC)";
    paramNames[Tcc] = "Tcc (outflow CC to exit death or back CB)";
    paramNames[Fdel] = "Fdel (frac deleterious mutations)";
    paramNames[Fdec] = "Fdec (frac decreasing aff mutations)";
    paramNames[Fimp] = "Fimp (frac improving aff mutations)";
    paramNames[Fjump] = "Fjump (frac key mutations)";
    paramNames[earlyExit] = "earlyExit (rate exit CB0)";
    paramNames[CmaxAgCapture] = "CmaxAgCapture (factor proba to Ag capture numbers)";
    paramNames[KAgCapture] = "KAgCapture";
    paramNames[C1capture] = "C1capture";
    paramNames[C2capture] = "C2capture";
    paramNames[KtfhSupport] = "KtfhSupport";
    paramNames[alphaTfhSel] = "alphaTfhSel";
    paramNames[K1TfhSel] = "K1TfhSel";
    paramNames[K2TfhSel] = "K2TfhSel";
    paramNames[recycleSpeed] = "recycleSpeed";
    paramNames[memoryRate] = "memoryRate (rate exit memory)";
    paramNames[plasmaRate] = "plasmaRate (rate exit plasma cell)";
    paramNames[Dmem] = "Dmem (death memory cells)";
    paramNames[N_levelsMut] = "N_levelsMut (nb affinity bins max 19)";


    paramLowBounds[Dag] = 0.1;		paramUpBounds[Dag] = 0.5;
    paramLowBounds[Ssag] = 0;		paramUpBounds[Ssag] = 0;
    paramLowBounds[Dsag] = 0;		paramUpBounds[Dsag] = 0;
    paramLowBounds[Pab] = 0;		paramUpBounds[Pab] = 0;
    paramLowBounds[Dab] = 0;		paramUpBounds[Dab] = 0;
    paramLowBounds[Sbn] = 0;		paramUpBounds[Sbn] = 0;
    paramLowBounds[Dbn] = 0;		paramUpBounds[Dbn] = 0;
    paramLowBounds[actB] = 0;		paramUpBounds[actB] = 0;
    paramLowBounds[actBMem] = 0;		paramUpBounds[actBMem] = 0;
    paramLowBounds[Stn] = 0;		paramUpBounds[Stn] = 0;
    paramLowBounds[Dtn] = 0;		paramUpBounds[Dtn] = 0;
    paramLowBounds[actT] = 0;		paramUpBounds[actT] = 0;
    paramLowBounds[Di] = 0;		paramUpBounds[Di] = 0;
    paramLowBounds[actI] = 0;		paramUpBounds[actI] = 0;
    paramLowBounds[flowBinter] = 0;		paramUpBounds[flowBinter] = 0;
    paramLowBounds[flowTinter] = 0;		paramUpBounds[flowTinter] = 0;
    paramLowBounds[fTBtoT] = 0;		paramUpBounds[fTBtoT] = 0;
    paramLowBounds[fTBtoB] = 0;		paramUpBounds[fTBtoB] = 0;
    paramLowBounds[fracEF] = 0;		paramUpBounds[fracEF] = 0;
    paramLowBounds[Dtfh] = 0;		paramUpBounds[Dtfh] = 0;
    paramLowBounds[Ptfh] = 0;		paramUpBounds[Ptfh] = 0;
    paramLowBounds[Dpc] = 0;		paramUpBounds[Dpc] = 0;
    paramLowBounds[Pcb] = 0;		paramUpBounds[Pcb] = 0;
    paramLowBounds[Tcb] = 0;		paramUpBounds[Tcb] = 0;
    paramLowBounds[Dcc] = 0;		paramUpBounds[Dcc] = 0;
    paramLowBounds[Tcc] = 0;		paramUpBounds[Tcc] = 0;
    paramLowBounds[Fdel] = 0;		paramUpBounds[Fdel] = 0;
    paramLowBounds[Fdec] = 0;		paramUpBounds[Fdec] = 0;
    paramLowBounds[Fimp] = 0;		paramUpBounds[Fimp] = 0;
    paramLowBounds[Fjump] = 0;		paramUpBounds[Fjump] = 0;
    paramLowBounds[earlyExit] = 0;		paramUpBounds[earlyExit] = 0;
    paramLowBounds[CmaxAgCapture] = 0;		paramUpBounds[CmaxAgCapture] = 0;
    paramLowBounds[KAgCapture] = 0;		paramUpBounds[KAgCapture] = 0;
    paramLowBounds[C1capture] = 0;		paramUpBounds[C1capture] = 0;
    paramLowBounds[C2capture] = 0;		paramUpBounds[C2capture] = 0;
    paramLowBounds[KtfhSupport] = 0;		paramUpBounds[KtfhSupport] = 0;
    paramLowBounds[alphaTfhSel] = 0;		paramUpBounds[alphaTfhSel] = 0;
    paramLowBounds[K1TfhSel] = 0;		paramUpBounds[K1TfhSel] = 0;
    paramLowBounds[K2TfhSel] = 0;		paramUpBounds[K2TfhSel] = 0;
    paramLowBounds[recycleSpeed] = 0;		paramUpBounds[recycleSpeed] = 0;
    paramLowBounds[memoryRate] = 0;		paramUpBounds[memoryRate] = 0;
    paramLowBounds[plasmaRate] = 0;		paramUpBounds[plasmaRate] = 0;
    paramLowBounds[Dmem] = 0;		paramUpBounds[Dmem] = 0;
    paramLowBounds[N_levelsMut] = 0;		paramUpBounds[N_levelsMut] = 0;

}

void modelOwnV2::setBaseParameters(){
    // would like the model to have scaling for more GCs but less antigen and vice-versa
    params.clear();                 // to make sure they are all put to zero
	params.resize(NBPARAM, 0.0);
    params[Dag] = 0.4;
    params[Ssag] = 0.5;  // only scales the amount of Sag versus Dag (only during availability time) We did it up to 1000 units
    params[Dsag] = 0.03; // stays at least 50 days or so
    params[Pab] = 0.1;   // each cells produces something in whatever unit
    params[Dab] = 0.02;  // slow decay in log scale but fast decay still
    params[Sbn] = 1000;  // 1000 cells per day
    params[Dbn] = 0.001;
    params[actB] = 1e-5;
    params[actBMem] = 1e-5;
    params[Stn] = 1000;
    params[Dtn] = 0.001;
    params[actT] = 1e-5;
    params[Di] = 0.08;
    params[actI] = 0.0006;
    params[flowBinter] = 3;
    params[flowTinter] = 3;
    params[fTBtoT] = 0.00025; // the speed is given by TB interactions, here is just the amount
    params[fTBtoB] = 0.002; // same
    params[fracEF] = 0.2;
    params[Dtfh] = 0.05; // slow decay without support
    params[Ptfh] = 0.04; // This one makes a crazy feedback loop
    params[Dpc] = 0.15; // might need different ones for EF PC and GC PC
    params[Pcb] = 0.01;
    params[Tcb] = 0.8;
    params[Dcc] = 0.5;
    params[Tcc] = 0.8;
    params[Fdel] = 0.3;
    params[Fdec] = 0.2;
    params[Fimp] = 0.2;
    params[Fjump] = 0.01;
    params[earlyExit] = 0.02;
    params[CmaxAgCapture] = 4e-6; // in this range there is a lot of antigen eating
    params[KAgCapture] = 500;
    params[C1capture] = 1;
    params[C2capture] = 20;
    params[KtfhSupport] = 0.5;
    params[alphaTfhSel] = 1.1;
    params[K1TfhSel] = 100;
    params[K2TfhSel] = 1;
    params[recycleSpeed] = 0.8;
    params[memoryRate] = 0.05;
    params[plasmaRate] = 0.05;
    params[Dmem] = 0.001;
    params[N_levelsMut] = 10;
	setBaseParametersDone();
}

void modelOwnV2::initialise(long long _background){ // don't touch to parameters !
	background = _background;
	val.clear();
	val.resize(NBVAR, 0.0);
	init.clear();
	init.resize(NBVAR, 0.0);       

    init[Ag] = 0;
    init[SAg] = 0;
    init[Inf] = 0;
    init[TN] = 1000000;
    init[TE] = 0;
    init[Tfh] = 0;
    init[BN] = 1000000;
    init[BE] = 0;
    init[EFpc] = 0;
    init[GCpc] = 0;
    init[AvgTfhHelp] = 0;
    init[Ab] = 0;
    init[CB0] = 0;
    init[CB1] = 0;
    init[CB2] = 0;
    init[CB3] = 0;
    init[CB4] = 0;
    init[CB5] = 0;
    init[CB6] = 0;
    init[CB7] = 0;
    init[CB8] = 0;
    init[CB9] = 0;
    init[CB10] = 0;
    init[CB11] = 0;
    init[CB12] = 0;
    init[CB13] = 0;
    init[CB14] = 0;
    init[CB15] = 0;
    init[CB16] = 0;
    init[CB17] = 0;
    init[CB18] = 0;
    init[CB19] = 0;
    init[CC0] = 0;
    init[CC1] = 0;
    init[CC2] = 0;
    init[CC3] = 0;
    init[CC4] = 0;
    init[CC5] = 0;
    init[CC6] = 0;
    init[CC7] = 0;
    init[CC8] = 0;
    init[CC9] = 0;
    init[CC10] = 0;
    init[CC11] = 0;
    init[CC12] = 0;
    init[CC13] = 0;
    init[CC14] = 0;
    init[CC15] = 0;
    init[CC16] = 0;
    init[CC17] = 0;
    init[CC18] = 0;
    init[CC19] = 0;
    init[MEM0] = 0;
    init[MEM1] = 0;
    init[MEM2] = 0;
    init[MEM3] = 0;
    init[MEM4] = 0;
    init[MEM5] = 0;
    init[MEM6] = 0;
    init[MEM7] = 0;
    init[MEM8] = 0;
    init[MEM9] = 0;
    init[MEM10] = 0;
    init[MEM11] = 0;
    init[MEM12] = 0;
    init[MEM13] = 0;
    init[MEM14] = 0;
    init[MEM15] = 0;
    init[MEM16] = 0;
    init[MEM17] = 0;
    init[MEM18] = 0;
    init[MEM19] = 0;
    init[PC0] = 0;
    init[PC1] = 0;
    init[PC2] = 0;
    init[PC3] = 0;
    init[PC4] = 0;
    init[PC5] = 0;
    init[PC6] = 0;
    init[PC7] = 0;
    init[PC8] = 0;
    init[PC9] = 0;
    init[PC10] = 0;
    init[PC11] = 0;
    init[PC12] = 0;
    init[PC13] = 0;
    init[PC14] = 0;
    init[PC15] = 0;
    init[PC16] = 0;
    init[PC17] = 0;
    init[PC18] = 0;
    init[PC19] = 0;
    // other variables are derived


    for(size_t i = 0; i < NBVAR; ++i){
        val[i] = init[i];}
	t = 0;
	initialiseDone();
}









// Hill function, not yet used
double Hill(double x, double K){
    if(x <= 0) return 0;
    if(x+K < 1e-12) return 0;
    return(x / (x+K));
}

