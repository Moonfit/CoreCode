include("../Moonfit/moonfit.pri")

# This is the version without librarires
#include("../Moonfit/moonfitNoLib.pri")
#QT -= opengl
#CONFIG -= QT
#DEFINES += "WITHOUT_QT"

#name of the executable file generated
TARGET = MiniGC_3.121


HEADERS += \
    Erwin2017/ErwinA.h \
    OwnV1/OwnV1.h \
    OwnV2/OwnV2.h \
    OwnV3/OwnV3.h \
    OwnV4/OwnV4.h \
    expGC.h
	
SOURCES += \
    Erwin2017/ErwinA.cpp \
    OwnV1/OwnV1.cpp \
    OwnV2/OwnV2.cpp \
    OwnV3/OwnV3.cpp \
    OwnV4/OwnV4.cpp \
    expGC.cpp \
    miniMain.cpp


