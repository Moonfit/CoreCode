#include "../Moonfit/moonfit.h"

#include "expGC.h"
#include "Erwin2017/ErwinA.h"
#include "OwnV1/OwnV1.h"
#include "OwnV2/OwnV2.h"
#include "OwnV3/OwnV3.h"
#include "OwnV4/OwnV4.h"

static string folder = "C:/Work/Softwares/NewArchaeropteryx/Sources/ODEGC/";
static string folderBaseResults = "C:/Work/Softwares/NewArchaeropteryx/Sources/ODEGC/Results/";

#define nRep 10

int main(int argc, char *argv[]){
    if(argc < 2) {cerr << "MiniMain takes only 1 argument (which combination)" << endl; return 0;}
    int IDcomb = -1;
    if(argc == 2){IDcomb = atoi(argv[1]);}
    cout << "Will process combination " << IDcomb << endl;

    createFolder(folderBaseResults);

    TableCourse* Turner2021Nat_avg  = new TableCourse(folder + string("DATA/Turner2021Nature_AVG_LNSwell.txt"));
    TableCourse* Turner2021Nat_std  = new TableCourse(folder + string("DATA/Turner2021Nature_STD.txt"));

    TableCourse* DoriaRose2021NEJM_avg  = new TableCourse(folder + string("DATA/Doria-Rose2021NEJM_AVG.txt"));
    TableCourse* DoriaRose2021NEJM_std  = new TableCourse(folder + string("DATA/Doria-Rose2021NEJM_STD.txt"));

    TableCourse* DoriaRose2021NEJM_avg_resc  = new TableCourse(folder + string("DATA/Doria-Rose2021NEJM_AVG_rescaled.txt"));
    TableCourse* DoriaRose2021NEJM_std_resc  = new TableCourse(folder + string("DATA/Doria-Rose2021NEJM_STD_rescaled.txt"));

    TableCourse* Pape2021CellRep_avg  = new TableCourse(folder + string("DATA/Pape2021CellReports_AVG.txt"));

    TableCourse* Goel2021Science_Tfh = new TableCourse(folder + string("DATA/Goel2021Science_Tfh.txt"));

    // Note : never create overrider as a non pointer, to be used by the graphical interface because they will be erased when function closes and gives control to the interface --> use a pointer and new ...

    //    case SQUARE_COST: res << "RSS"; break;
    //    case SQUARE_COST_STD: res << "RSS + StdDev"; break;
    //    case LOG_COST: res << "Log"; break;
    //    case PROPORTION_COST: res << "Ratios"; break;

    //    case NO_NORM: res << " (No Norm)"; break;
    //    case NORM_AVERAGE: res << " Norm/Avg vars"; break;
    //    case NORM_NB_PTS: res << " Norm/Nb Points"; break;
    //    case NORM_AVG_AND_NB_PTS: res << " Norm/Avg and Nb Pts "; break;
    //    case NORM_MAX: res << " Norm/Max vars"; break;
    //    case NORM_MAX_AND_NB_PTS: res << " Norm/Max and Nb Pts"; break;
    setTypeCost(SQUARE_COST);
    setTypeNorm(NORM_AVERAGE);

    string configFile = string("ConfigForGlobal.txt");
    Model* currentModel = new modelOwnV4(); //modelErwinA();
    //expGC* currentExperiment = new expGC(currentModel);
    expDrugs* currentExperiment = new expDrugs(currentModel);

    currentExperiment->giveData(Turner2021Nat_avg, NODRUG, Turner2021Nat_std);
//currentExperiment->giveData(DoriaRose2021NEJM_avg, NODRUG, DoriaRose2021NEJM_std);
    currentExperiment->giveData(DoriaRose2021NEJM_avg_resc, NODRUG, DoriaRose2021NEJM_std_resc);
    currentExperiment->giveData(Pape2021CellRep_avg, NODRUG);
    currentExperiment->giveData(Goel2021Science_Tfh, NODRUG);

    if(false){
    TableCourse* ControlNonDmard_Mean = new TableCourse(folder + string("SCQM/AggMoonfitA_non_dmard_medication_covid_19_vaccine_modernaMean.txt"));
    TableCourse* ControlNonDmard_SD = new TableCourse(folder + string("SCQM/AggMoonfitA_non_dmard_medication_covid_19_vaccine_modernaSD.txt"));

    TableCourse* Abatacept_Mean = new TableCourse(folder + string("SCQM/AggMoonfitA_Abatacept_covid_19_vaccine_modernaMean.txt"));
    TableCourse* Abatacept_SD = new TableCourse(folder + string("SCQM/AggMoonfitA_Abatacept_covid_19_vaccine_modernaSD.txt"));

    TableCourse* Rituximab_Mean = new TableCourse(folder + string("SCQM/AggMoonfitA_Rituximab_covid_19_vaccine_modernaMean.txt"));
    TableCourse* Rituximab_SD = new TableCourse(folder + string("SCQM/AggMoonfitA_Rituximab_covid_19_vaccine_modernaSD.txt"));

    TableCourse* TNFCombi_Mean = new TableCourse(folder + string("SCQM/AggMoonfitA_TNF_combi_covid_19_vaccine_modernaMean.txt"));
    TableCourse* TNFCombi_SD = new TableCourse(folder + string("SCQM/AggMoonfitA_TNF_combi_covid_19_vaccine_modernaSD.txt"));

    TableCourse* TNF_Mean = new TableCourse(folder + string("SCQM/AggMoonfitA_TNF_covid_19_vaccine_modernaMean.txt"));
    TableCourse* TNF_SD = new TableCourse(folder + string("SCQM/AggMoonfitA_TNF_covid_19_vaccine_modernaSD.txt"));


    bool test_pred = false;
    if(test_pred){
        ControlNonDmard_Mean = new TableCourse(folder + string("SCQM/AggMoonfitB1_non_dmard_medication_covid_19_vaccine_modernaMean.txt"));
        ControlNonDmard_SD = new TableCourse(folder + string("SCQM/AggMoonfitB1_non_dmard_medication_covid_19_vaccine_modernaSDx.txt"));

        Abatacept_Mean = new TableCourse(folder + string("SCQM/AggMoonfitB1_Abatacept_covid_19_vaccine_modernaMean.txt"));
        Abatacept_SD = new TableCourse(folder + string("SCQM/AggMoonfitB1_Abatacept_covid_19_vaccine_modernaSDx.txt"));

        Rituximab_Mean = new TableCourse(folder + string("SCQM/AggMoonfitB1_Rituximab_covid_19_vaccine_modernaMean.txt"));
        Rituximab_SD = new TableCourse(folder + string("SCQM/AggMoonfitB1_Rituximab_covid_19_vaccine_modernaSDx.txt"));

        //TNFCombi_Mean = new TableCourse(folder + string("SCQM/AggMoonfitB1_TNF_combi_covid_19_vaccine_modernaMean.txt"));
        //TNFCombi_SD = new TableCourse(folder + string("SCQM/AggMoonfitB1_TNF_combi_covid_19_vaccine_modernaSDx.txt"));

        TNF_Mean = new TableCourse(folder + string("SCQM/AggMoonfitB1_TNF_covid_19_vaccine_modernaMean.txt"));
        TNF_SD = new TableCourse(folder + string("SCQM/AggMoonfitB1_TNF_covid_19_vaccine_modernaSDx.txt"));
    }


    currentExperiment->giveData(ControlNonDmard_Mean, NODRUG, ControlNonDmard_SD);
    currentExperiment->giveData(Abatacept_Mean, JAK, Abatacept_SD);
    currentExperiment->giveData(Rituximab_Mean, RITUXIMAB, Rituximab_SD);
    currentExperiment->giveData(TNFCombi_Mean, TNF_Combi, TNFCombi_SD);
    currentExperiment->giveData(TNF_Mean, ANTITNF, TNF_SD);
    }
    currentExperiment->loadEvaluators();

    //enum  {NODRUG, RITUXIMAB, JAK, ANTITNF, N_DRUGEXP};
    //struct expDrugs

    overrider* OverHealthy = new overrider(Turner2021Nat_avg);
    currentExperiment->setOverrider(NODRUG, OverHealthy);
    cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";

    if(IDcomb >= 0){

        manageSims* msi = new manageSims(currentExperiment);
        msi->loadConfig(folder + configFile);
        vector<string> listGeneratedFilesSets;

        for(int i = 0; i < msi->nbCombs; ++i){
            if(IDcomb < 0 || IDcomb == i){

                stringstream headerOptimizer;                                                   // each further script might use different optimizer options, will be stored in the following stringstream
                if(IDcomb == 0) headerOptimizer << optFileHeader(Genetic1M);
                else headerOptimizer << optFileHeader(Genetic50k);

                for(int j = 0; j < nRep; ++j){
                    stringstream codeSimu;      codeSimu << "CombNr" << i << "-" << codeTime();               // generates a text code for this particular optimization, in case parallel optimizations are running
                    stringstream folderComb;    folderComb << folderBaseResults << codeSimu.str() << "/";        // creates a folder for this particular optimization, to create figures etc ...
                    createFolder(folderComb.str());

                    cout << "   -> Optimizing combination (" << i << ") with ID: " << codeSimu.str() << "\n";
                    msi->resetParamSetFromConfig(folder + configFile);
                    currentExperiment->m->setPrintMode(false, 5000./3600.);
                    currentExperiment->m->dt = 10./3600.;

                    string optOptions = msi->motherCreateOptimizerFile(i, headerOptimizer.str());       // for each combination, will need to re-create an optimizer file
                    ofstream f1((folderComb.str() + string("Optimizer.txt")).c_str(), ios::out); if(f1) {f1 << optOptions << "\n"; f1.close();}

                    msi->motherOverrideUsingComb(i);                                                    // chose the variables to simulate and to replace by data according to this combination
                    msi->motherOptimize(folderComb.str() + string("Optimizer.txt"), 1000);              // DOES THE OPTIMIZATION !!!, and records the 1000 best sets

                    msi->saveHistory(folderComb.str() + string("History.txt"));                         // SAVES all the best parameter sets. by default, 10 000, can be modified by             msi->history.resize(max_nb_sets_to_record);
                    listGeneratedFilesSets.push_back(folderComb.str() + string("History.txt"));         // list[i] = historyFile for comb i

                    msi->useParamSetFromHistory(0);                                                     // takes the first set of parameters (the best), also possible to use msi->useParamSetFromHistory(0, i); for overriding only parameters from this combination,
                    msi->simulate();
                    ofstream f2((folderComb.str() + string("FitnessBestSetOf") + codeSimu.str() + string(".txt")).c_str(), ios::out); if(f2) {f2 << currentExperiment->costReport() << "\n"; f2.close();}
                    ofstream f3((folderComb.str() + string("CostEvolutionDuringOptimization.txt") + codeSimu.str()).c_str(), ios::out); if(f3) {f3 << msi->costRecords.print() << "\n"; f3.close();}

                }
            }
        }
    } else {
        #ifndef WITHOUT_QT
        QApplication b(argc, argv);
        simuWin* p = new simuWin(currentExperiment);
        cout << "Launching Graphical Interface ..." << endl;
        //p->loadConfig(folder + configFile);
        p->show();
        b.exec();
        #else
        cout << "Script finished (without qt, because WITHOUT_QT was defined)\n";
        #endif
    }

    cout << "END";
    return 0;
}

